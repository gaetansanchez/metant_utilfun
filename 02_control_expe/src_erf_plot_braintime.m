function [] = src_erf_plot_braintime(config,inputfile,toifile,dosavefile,Tscale,precision_grid,outputpath)


%% adapt time to plot to the sig. cluster

% Load
%------

[~,fname,~] = fileparts(inputfile); % fr saving name later

stat_data = load(inputfile);

%stat = stat_data.stat_bl;
stat = stat_data.stat;
% Do plot
%---------
toi = toifile;
stat.clus = stat.stat.*stat.mask;
param2plot = 'clus';
param2mask = 'mask';
%Tscale = [0 10];
stat.mask = double(stat.mask);

parcellation = [];
if precision_grid == 1.5
  load mni_grid_1_5_cm_889pnts
elseif precision_grid == 2
  load mni_grid_2cm_372pnts
elseif ischar(precision_grid)% if you provide a file name for a parcellation grid
  load(precision_grid); % load parcellation
  template_grid = parcellation.parcel_grid; % parcel grid with centroid position
  load atlas_parcel333.mat
  
else
  error(['No precision mni_grid for precision = ' num2str(precision_grid)]);
end

load('standard_mri_segmented');
cfg = [];
%cfg.latency   = [stat.time stat.time];
cfg.latency   = [toi(1) toi(end)];
cfg.parameter = {param2plot param2mask};
cfg.sourcegrid = template_grid;
cfg.mri = mri_seg.bss;
inter_source = obob_svs_virtualsens2source(cfg, stat);

% % interpolate the mask
% load standard_mri_better
% cfg = [];
% cfg.parameter = {param2plot param2mask};
% inter_source = ft_sourceinterpolate(cfg, source, mri);
% inter_source.coordsys = 'mni';
%


% plot  carret style
cfg = [];
cfg.funparameter = param2plot;
cfg.funcolorlim = Tscale;
%cfg.funcolorlim = 'maxabs';
cfg.maskparameter = param2mask;
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';

if dosavefile == 1
  
  inter_source = rmfield(inter_source,'coordsys'); % because if not create problem with caret plot....
  outputfile = fullfile(outputpath,fname);
  
  % make figures for right hemisphere
  hemibrain = {'left' 'right'};
  viewbrain = {'left' 'right' 'dorsal'};
  
  for ip =1:numel(hemibrain)
    
    for jp = 1:numel(viewbrain)
      h = hemibrain{ip};
      cfg.surffile = ['surf_caret' h '.mat'];
      ft_sourceplot(cfg, inter_source);
      
      v = viewbrain{jp};
      name = [outputfile '_' h];
      resolution = 100;
      plot_caret_style(name, resolution, [], v);
    end
    
  end
  
else
  
  ft_sourceplot(cfg, inter_source);
 
end







