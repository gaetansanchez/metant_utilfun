% RUN on cluster src ERF analysis (call in a loop = Do_srcERF.m)

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Parameters
%----------------
sub = get_subject_mat(config); % get all subject code and fifpath
% remove subject with less than 30 trials in one condition
Suj = Do_sel_subjects(config,'metaNT_v2');


precision_grid = 'parcellations_3mm.mat'; % file where to find template grid
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'}
toilim      = [-0.4 0.6]; % in sec (period to localize)
CFilter     = 'alltrials';% Beamformer common filter computation =  'alltrials' or 'sensmod'
paradigm    = 'all';% 'all' or 'hit_vs_miss' or 'supra_vs_sham' or 'miss_vs_sham' 
do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'


%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2parsrc/' date]);
pathpreproc = fullfile(config.path,['preproc' do_trans_suffix]); % Path clean data
Pathsave = fullfile(config.path2work,['src' do_trans_suffix],'parsrcERF_lp30Hz',['1.5cm_grid_CFilter' CFilter '_' paradigm]);

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '10G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
for i = 1:numel(Suj)
  id = Suj(i);
  hdmfile = fullfile(config.path,'hdm',sub(id).code,[sub(id).code,'_hdm' do_trans_suffix '.mat']);
  condor_struct = obob_condor_addjob(condor_struct,'Do_srcERF',id,hdmfile,sensmod,toilim,pathpreproc,Pathsave,precision_grid,CFilter,paradigm);
 
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);

