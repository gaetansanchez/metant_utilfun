%% SCRIPT do source localization


%% Path
% My path
%---------
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%% Set paths
%------------
sub = get_subject_mat(config); % get all subject code and fifpath

%% Perform cleaning 
%-------------------

%%% Automatic
%%%-------------
% condor_00_do_artdet.m

%%% Manual
%%%---------
% script_readepoch_manual_clean.m

%%% Count rejected trials 
%%%-----------------------
% condor_01bisbis_Do_trialsNb_count_after_reject.m


%% Coregistration

do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'

for isub = 1:numel(sub)
  
  idblock = 1; % just first block
  %pathfif = fullfile(config.path,'fif');
  pathfif = sub(isub).pathfif;
  hdmDir  = fullfile(config.path,'hdm');
  
  all_fileName = fullfile(pathfif,sprintf('%s_s%02d_*%s.fif',sub(isub).code,sub(isub).nb,do_trans_suffix));
  filetmp = dir(all_fileName);
  nblocks = numel(filetmp); % number of blocks
  fileName = fullfile(filetmp(idblock).folder,filetmp(idblock).name);
  [p,f,e] = fileparts(fileName);
  
  
  % Coregistration
  %-----------------
  pathhdm = fullfile(hdmDir,sub(isub).code);
  hdmfile = fullfile(pathhdm,[sub(isub).code,'_hdm' do_trans_suffix '.mat']);
  
  if ~exist(hdmfile,'file') 
    
    [hdm] = do_coreg_headmodel(sub(isub).code,fileName,hdmfile); % if hdm does not exist --> do coreg
    
    disp('************');
    disp(['Sub = ' sub(isub).code ' --> DONE']);
    disp('************');
    disp('Press any keys to continue...');
    pause;
    
  else
  
  disp('************');
  disp(['Sub = ' sub(isub).code ' --> DONE']);
  pause(1);
  
  end
end


%% Do source localization

condor_03_do_epo2src;

%% Do parcellation source localization

condor_03bis_do_epo2parsrc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bonus Plot Publi all source time-course
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT_v2',cfg);

do_trans_suffix = []; % [] or '_trans_sss'

Pathsrc = fullfile(config.path2work,['src' do_trans_suffix]);
%Pathsrc = fullfile(config.path2work,['test_src' do_trans_suffix]);

% Load & Prepare individual source localisation files
%-------------------------------------------------------
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFiltersensmod_all');
PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFiltersensmod_hit_vs_miss');
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFilteralltrials_all');
%PathERFfiles = fullfile(Pathsrc,'parsrcERF_lp30Hz','1.5cm_grid_CFiltersensmod_all');

% Suj
%-----
% remove subject with less than 30 trials in one condition
Suj = Do_sel_subjects(config,'metaNT_v2');

% Load loop
%----------
erf = []; % initalize variable 'erf' before so that it is not confused with the builtin function 'erf'
erfbl = [];
srerf = {};
srerf_bl = {};
for i = 1:numel(Suj)
  
  iSub = Suj(i);
  [~, sub, ~] = fileparts(fullfile(PathERFfiles, sprintf('s%02d_src_erf.mat',iSub)));
  disp(['--> Load : ' sub]);
  load(fullfile(PathERFfiles,sub));
  % select conditions
  srerf{i}    = erf;
  srerf_bl{i} = erfbl;
  
end
  

T = srerf{1}.aud_hit.time;
fsample = 1/(T(end)-T(end-1));
data_ga = srerf{1}.aud_hit;% initialize structure
data_ga.fsample = fsample;
data_ga_bl = srerf_bl{1}.aud_hit;% initialize structure
data_ga_bl.fsample = fsample;

sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss'};
%condlabel = {'sham' 'supra'};
%condlabel = {'hit' 'miss' 'sham' 'supra'};

for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:numel(Suj)
      datint = srerf{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
      
      datint_bl = srerf_bl{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga_bl.([sensmod{i} condlabel{j}])(n,:,:) = datint_bl.avg;
      
    end
  end
end
disp('********************');
disp('DONE !!!');

%% Plot + statistics
%-------------------

testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};

% testlabel = {'audhit','vishit','tachit','audsupra','vissupra','tacsupra';...
%    'audmiss','vismiss','tacmiss','audsham','vissham','tacsham'};
% 

%testlabel = {'audsupra','vissupra','tacsupra';...
%    'audsham','vissham','tacsham'};

 
timeWin = [-200 550];
sel = 'SRC';%'GRC'; %'MAG';
%Pathoutput = fullfile(config.path,'doc/src');
Pathoutput = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures','SRC_erf');
%Pathoutput = [];

timemax = {}; % max timing of peaks for first condition
for i = 1:size(testlabel,2)
  test1 = testlabel{1,i};
  test2 = testlabel{2,i};
  [timewins,timemax{i}] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput,timeWin)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 4/ run Do_srcERF_stat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT_v2',cfg);

do_trans_suffix = []; % [] or '_trans_sss'
Pathsrc = fullfile(config.path2work,['src' do_trans_suffix]);

Pathsrc_ERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFiltersensmod_hit_vs_miss');
Pathoutdata = fullfile(Pathsrc_ERFfiles, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs');

paradigm = 'metaNT_v2'; % load specific path later

condarray = {'aud_hit' 'aud_miss';...
             'vis_hit' 'vis_miss';...
             'tac_hit' 'tac_miss'};
            
% condarray = {'aud_supra' 'aud_sham';...
%              'vis_supra' 'vis_sham';...
%              'tac_supra' 'tac_sham'};
    

toiarray = { [.4 .48];...
             [.24 .37];...
             [.13 .15]};        
           
avgtoi = 'yes';
correct  = 'cluster'; % 'no' or 'cluster'
method   = 'montecarlo'; % 'analytic' or 'montecarlo'
stattest = 'depsamplesT';%'depsamplesT';%'cimec_statfun_normchange'; % % first study: ttest...
precision_grid = 1.5;

condor_srcERF_stat(paradigm, condarray, toiarray, avgtoi, correct, method, stattest, Pathoutdata,Pathsrc_ERFfiles,precision_grid); % call ==> Do_srcERF_stat.m


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT_v2',cfg);

Pathsrc = fullfile(config.path2work,'src');
Pathsrc_ERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFiltersensmod_hit_vs_miss');
Pathdata = fullfile(Pathsrc_ERFfiles, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs');

if strcmp(computer,'MACI64')
  outputpath = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures','SRC_erf');
else
  outputpath = fullfile(config.path,'doc','src');
end

precision_grid = 1.5; % file with parcellation template grid  

condarray = {'aud_hit' 'aud_miss';...
  'vis_hit' 'vis_miss';...
  'tac_hit' 'tac_miss'};   

% condarray = {'aud_supra' 'aud_sham';...
%              'vis_supra' 'vis_sham';...
%              'tac_supra' 'tac_sham'};
% toiarrayfile = { [.4 .48], [];...
%              [.26 .29],[.36 .38];...
%              [.13 .15],[.21 .23]};  

toiarrayfile = { [.4 .48];...
             [.24 .37];...
             [.13 .15]};        
                  
iCond = [1:3];
toi2select = [1];
dosavefile = 1;
Tscale = [0 5];


for i = 1:numel(iCond)
  for j = 1:numel(toi2select)
    cond1 = condarray{iCond(i),1};
    cond2 = condarray{iCond(i),2};
    toifile = toiarrayfile{iCond(i),toi2select(j)};
    
    if isempty(toifile)
    else
      inputfile = fullfile(Pathdata, [cond1 '-vs-' cond2 '_montecarlo_cluster_' num2str(toifile(1)*1000) '-' num2str(toifile(2)*1000) 's.mat']);
      src_erf_plot_braintime(config,inputfile,toifile,dosavefile,Tscale,precision_grid,outputpath);
    end
    
  end
end


%% Explore source atlas
%-----------------------

clear all;
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT_v2',cfg);

Pathsrc = fullfile(config.path2work,'src');
Pathsrc_ERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','1.5cm_grid_CFiltersensmod_hit_vs_miss');
Pathoutdata = fullfile(Pathsrc_ERFfiles, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs');


condarray = {'aud_hit' 'aud_miss';...
  'vis_hit' 'vis_miss';...
  'tac_hit' 'tac_miss'};
           
%%% win = 30ms // 10ms overlap
%------------------------------
toiarray = { [.4 .48];...
             [.24 .37];...
             [.13 .15]};        

iCond = 2;
toi2select = 1;
cond1 = condarray{iCond,1};
cond2 = condarray{iCond,2};
Tscale = [0 5];

% Load
%------
toifile = toiarray{iCond,toi2select};
filename = [cond1 '-vs-' cond2 '_montecarlo_cluster_' num2str(toifile(1)*1000) '-' num2str(toifile(2)*1000) 's.mat'];
statdata = fullfile(Pathoutdata,filename);
stat_data = load(statdata);

load mni_grid_1_5_cm_889pnts
stat = stat_data.stat;

% Do plot
%---------
toi = stat.time(find(sum(stat.mask==1)));
toi = stat.time;
stat.clus = stat.stat.*stat.mask;

param2plot = 'clus';
param2mask = 'mask';

stat.mask = double(stat.mask);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.latency   = [toi(1) toi(end)];
cfg.parameter = {param2plot param2mask};
cfg.sourcegrid = template_grid;
source = obob_svs_virtualsens2source(cfg, stat);

cfg = [];
cfg.parameter = {param2plot param2mask};
inter_source = ft_sourceinterpolate(cfg, source, mri);
inter_source.coordsys = 'mni';

cfg = [];
cfg.funparameter = param2plot;
cfg.funcolorlim = Tscale;
%cfg.funcolorlim = 'maxabs';
cfg.maskparameter = param2mask;
cfg.atlas = 'ROI_MNI_V4.nii';%'TTatlas+tlrc.BRIK';
cfg.method = 'ortho';% 'surface' 'ortho' 'slice'
cfg.projmethod = 'nearest'; % needed for method ='nearest'; 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.location = 'max';


ft_sourceplot(cfg, inter_source);

%%%%%%%%%%%%%
% Extract ROI
%%%%%%%%%%%%%%

testdat = source;
testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)
sigVox  = find(testdat.mask==1); % only significant voxels
intval  = testdat.clus(sigVox); % mean values for these voxels
% sort voxels indices based on their values
%--------------------------------------------
[sigVal,indv] = sort(intval, 'descend');
sigVox = sigVox(indv);

% load atlas
%------------
A = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
savefilename = fullfile(config.path,'doc','atlas_labels',['labelsAALatlasMNI_' filename]);

[Tab] = Do_extractROIname_atlas(A,sigVox,sigVal,testdat,savefilename)





