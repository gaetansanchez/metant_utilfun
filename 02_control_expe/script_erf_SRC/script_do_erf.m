
%% SCRIPT Plot ERF Publication
%-----------------------------

clear all;
config = addmypaths_gs('metaNT_v2');
pathbase = config.path;

%% Perform cleaning 
%-------------------

%%% Automatic
%%%-------------
% condor_00_do_artdet.m

%%% Manual
%%%---------
% script_readepoch_manual_clean.m

%%% Count rejected trials 
%%%-----------------------
% condor_01bisbis_Do_trialsNb_count_after_reject.m

%% 01 --> Compute individual ERF
%---------------------------------
condor_02_do_epo2ERF

%% 02 --> Grand average
%-----------------------
condor_02bis_do_erf2ga

%% Load ERF
%-----------
sub = get_subject_mat(config); % get all subject code and fifpath
% remove subject with less than 30 trials in one condition
Suj = Do_sel_subjects(config,'metaNT_v2');

% Load all subjects ERFs
%------------------------
% pathavg = fullfile(config.path2work,'erf');
% load(fullfile(pathavg,'allERF_N16_allblocks_-0.5-1s_lp30_erf_percond.mat'));
% load(fullfile(pathavg,'ga_erf','ga_ERF_N16_allblocks_-0.5-1s_lp30_erf_percond.mat'));

pathavg = fullfile(config.path2work,'erf');
load(fullfile(pathavg,'allERF_N14_allblocks_-0.5-0.7s_lp30_erf_percond.mat'))

load(fullfile(pathavg,'ga_erf','ga_ERF_N14_allblocks_-0.5-0.7s_lp30_erf_percond.mat'))


%% Organize data to plot (Combine GRAD)
%---------------------------

%data_ga = ft_combineplanar([],erf{1}.aud.hit);% initialize structure
data_ga = erf{1}.aud.hit;
sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss' 'supra' 'sham'};
for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    
    if numel(erf) == 14
      for n = 1:numel(Suj)
        datint = erf{n}.(sensmod{i}).(condlabel{j});
        data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
      end
    else
      for n = 1:numel(Suj)
        %datint = ft_combineplanar([],erf{n}.(sensmod{i}).(condlabel{j}));
        datint = erf{Suj(n)}.(sensmod{i}).(condlabel{j});
        data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
      end
    end
  end
end

%% Plot PUBLI
%------------------

testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};

testlabel = {'audsupra','vissupra','tacsupra';...
   'audsham','vissham','tacsham'};

% testlabel = {'audmiss','vismiss','tacmiss';...
%     'audsham','vissham','tacsham'};


timeWin = [-200 550]; % in ms
data_ga.fsample = 512;
sel = 'GRC';%'GRC'; %'MAG';
Pathoutput = fullfile(config.path,'doc/erf');
%Pathoutput = [];
for i = 1:size(testlabel,2)
test1 = testlabel{1,i};
test2 = testlabel{2,i};
[timewins,timemax] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput,timeWin)
end


%% Quick plot to check all the subjects
%-------------------------------------

% For individual subjects
%-------------------------
Nsub = {[1:5] [6:10] [11:15] [16:21]};
Nsub = {[1:5]};
for i = 1:numel(Nsub)
  Nsub_part = Nsub{i};
  channel = 'MAG';% 'GRC', 'MAG'
  Do_plot_ERF(erf,Nsub_part,channel,0)
end

%% For Grand average
%------------------
ga{1} = ga_erf;
Nsub = 1;
channel = 'GRC';% 'GRC', 'MAG'
Do_plot_ERF(ga,Nsub,channel,0)

%% Explore single subjects data
%-------------------------------
fileName = fullfile('/mnt/obob/staff/gsanchez/metaNT_v2/preproc','05_allblocks.mat');

[data_clean_corr] = do_reject4cleaning(fileName);

% Apply signal space projection (SSP): reject external noise
%-----------------------------------------------------------
disp('  ');
disp('*****************');
disp('Apply SSP...');
disp('  ');

do_trans_suffix = []; % [] or '_trans_sss'
fifName = ['/mnt/sinuhe/data_raw/meta_nt/subject_subject/171027/19861231ANSR_s13_b01_aud' do_trans_suffix '.fif']; % any fif_file to get SSP components

cfg = [];
cfg.inputfile = fifName;
data_ssp = obob_apply_ssp(cfg, data_clean_corr);

% Here the idea is to just look and identify really bad sensors
%---------------------------------------------------------------
disp('  ');
disp('*****************');
disp('1st Visu...');
disp('  ');


cfg = [];
cfg.channel        = {'MEG'}; % all MEG channels together
cfg.megscale       = 1e2;
cfg.gradscale      = 0.04;  % factor to compensate different scale between GRAD and MAG
cfg.layout         = 'neuromag306mag.lay';
cfg.viewmode       = 'butterfly';
cfg.showlabel      = 'yes';
% lowpass filter (for visualization)
cfg.preproc.lpfreq   = 45;
cfg.preproc.lpfilter = 'yes';
ft_databrowser(cfg, data_ssp);


%% Plot multiplot Grand Average
%-----------------------------
toplot = 'vis';
dat_1 = ga_erf.(toplot).hit;
dat_2 = ga_erf.(toplot).miss;

cfg = [];
cfg.latency = [0 0.6];
dat_1 = ft_selectdata(cfg,dat_1);
dat_2 = ft_selectdata(cfg,dat_2);

% contrast
dat_diff = dat_1;
dat_diff.avg = dat_1.avg - dat_2.avg;

cfg = [];
cfg.layout = 'neuromag306cmb.lay';
%cfg.layout = 'neuromag306mag.lay';
%ft_multiplotER(cfg, dat_diff);
%ft_movieplotER(cfg, dat_diff);
ft_multiplotER(cfg, dat_1,dat_2);

%% Organize data to plot (Combine GRAD)
%---------------------------


%data_ga = ft_combineplanar([],erf{1}.aud.hit);% initialize structure
data_ga = erf{1}.aud.hit
sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss' 'supra' 'sham'};
for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:numel(erf)
      %datint = ft_combineplanar([],erf{n}.(sensmod{i}).(condlabel{j}));
      datint = erf{n}.(sensmod{i}).(condlabel{j});
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
    end
  end
end

%% Plot PUBLI
%------------------
% test1 = 'vishit';%'ud';%'ud';%'dd';
% test2 = 'vismiss';%'ud';%'ud';%'dd';
% 
% sel = 'GRC';%'GRC'; %'MAG';
% Pathoutput = fullfile(config.path,'doc/erf');
% Pathoutput = [];
% 
% [timewins] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput)
% %[timewins] = Do_Publi_plot_ERF(data_ga,test1,test2,sel)



testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};

testlabel = {'audsupra','vissupra','tacsupra';...
   'audsham','vissham','tacsham'};

sel = 'MAG';%'GRC'; %'MAG';
%Pathoutput = fullfile(config.path,'doc/src');
Pathoutput = [];
for i = 1:size(testlabel,2)
test1 = testlabel{1,i};
test2 = testlabel{2,i};
[timewins] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput)
end


%% Load erf4MVPA
%---------------

cosmofileName = fullfile(config.path2work,'mvpa','erf4MVPA','MVPA_erfhpfilt_0.1_supra_vs_sham_toilim_-200to600_tstep10ms_10.mat');
load(cosmofileName);

dat = cosmo_map2meeg(ds);


trlab = sort(unique(dat.trialinfo(:,1))); % sort + unique extraction of conditions labels


M = [];
for i = 1:numel(trlab)
    tr2avg = find(dat.trialinfo(:,1)==trlab(i));
    
    M(i,:,:) = squeeze(mean(dat.trial(tr2avg,:,:),1));
    
end

Mabs = squeeze(mean(abs(M),2));

[conds,conds_erf] = make_condsTRIG4src('metaNT');

figure;
n = 0;
for id = 1:3
  subplot(3,1,id)
  n = n+1;
  plot(dat.time,Mabs(n,:),'--k','LineWidth',2)
  hold on;
  n = n+1;
  plot(dat.time,Mabs(n,:),'r','LineWidth',2)
  title(conds{id,2});
  
end


%% Fieldtrip plot
id2p = find(ismember(cell2mat(conds_erf(:,1)),trlab));

daterf = {};
for i = 1:numel(trlab)
  cfg = [];
  cfg.trials = find(dat.trialinfo(:,1)==trlab(i));
  daterf.(conds_erf{id2p(i),2}) = ft_timelockanalysis(cfg,dat);
  
end

lab2p = {'aud_sham' 'aud_supra'};

cfg = [];
cfg.layout = 'neuromag306cmb.lay';
ft_multiplotER(cfg,daterf.(lab2p{1}),daterf.(lab2p{2}));



