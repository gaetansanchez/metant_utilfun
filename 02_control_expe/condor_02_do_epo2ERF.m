
% RUN epo2ERF
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2ERF/' date]);

pathpreproc = fullfile(config.path,'preproc');
outdir = fullfile(config.path2work,'erf');

if ~exist(outdir,'dir') % create output dir if dont exist
  mkdir(outdir);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);


%% Parameters ERF
%--------------------
filt_lim = 30;
toi      = [-0.5 0.7];
blwin    = [-0.2 0];% [-0.2 -0.1]
singletrials = 0; % 1 or 0

%% Jobs additions
%-----------------
sub = get_subject_mat(config); % get all subject code and fifpath
Nsub   = numel(sub);

for i = 1:Nsub
  id = sub(i).nb;
  
  input = fullfile(pathpreproc,sprintf('%02d_allblocks.mat',id));
  condor_struct = obob_condor_addjob(condor_struct, 'hnc_epo2erf',...
                        input, ...
                        outdir,...
                        singletrials,...
                        toi,...
                        filt_lim,...
                        blwin);
end

%% Submit
%---------
obob_condor_submit(condor_struct);

