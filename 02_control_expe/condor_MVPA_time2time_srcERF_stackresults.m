% RUN stack results fo time2time analysis from condor_MVPA_time2time_srcERF_parcomp
% (call in a loop = Do_mvpa_results_stack.m)
% With more paralelle computing !!!!

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT_v2',cfg);

Pathlogs = fullfile(config.path,['jobs/mvpa_t2t_srcERF_stackresults/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
[Suj] = Do_sel_subjects(config,'metaNT_v2');

%% Files name
%-------------
sensmod = {'all'}; % {'aud' 'vis' 'tac'} or {'all'} -> just decode hit vs miss across all sensmod
%sensmod = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'} -> just decode hit vs miss across all sensmod
paradigm = 'hit_vs_miss_respcontrol';% 'hit_vs_miss' or 'supravs_sham' or []
lcmvtype = 'BeamCFiltsensmod_'; % 'OBOB_BeamCFiltsensmod_' or 'BeamCFiltsensmod_' or [] (previous analysis)

if contains(sensmod{1},'all')
  type_decoding = 'dd_vs_ud_allsens_';
  Restype1 = '_allsens';
else
  type_decoding = [];
  Restype1 = '_persens';
end

if contains(sensmod{1},'resp')
    Restype2 = '_respvsnoresp';
else
    Restype2 = '_hitvsmiss';
end

Restypeall = [Restype1 Restype2];

HPfilt = [type_decoding 'hpfilt_0.1_' paradigm]; %  hpfilt_1 or hpfilt_0.1
Type1 = 'zscoreNorm_trad0';
Type2 = ['_toilim_-100to600_tstep10ms_' lcmvtype 'srad30_Bayes'];

Pathinput = fullfile(config.path2work,...
  ['mvpa/t2t_temp/cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);

Pathoutput = fullfile(config.path2work,...
  ['mvpa/t2t_srcERF/cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);

Nametype = 'MVPA_src';

%% Parameters + add jobs
%-----------------------
Nbrfiles = 7;
Delfile  = 1; % delete files after stack results : 1 = yes / 0 = no

for i = 1:numel(Suj)
  id = Suj(i);
  condor_struct = obob_condor_addjob(condor_struct, 'Do_mvpa_results_stack',id,Nbrfiles,Pathinput,Pathoutput,Nametype,Delfile);
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);







