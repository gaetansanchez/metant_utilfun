function hnc_epo2erf(input, outdir, singletrials, toi, filt_lim, blwin)

%% path
%--------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Output fileName
%------------------
if singletrials
    post_name = 'alltrials';
else
    post_name = 'percond';
end

[pathpreproc, fname] = fileparts(input);

saveName = fullfile(outdir,...
  [fname '_' num2str(toi(1)) '-' num2str(toi(end)) ...
  's_lp' num2str(filt_lim) '_erf_' post_name '.mat']);


%% load data
%-------------
[data_raw] = do_reject4cleaning(input); 

%% Apply SSP
%------------
% Apply signal space projection (SSP): reject external noise
%-----------------------------------------------------------
disp('  ');
disp('*****************');
disp('Apply SSP...');
disp('  ');

do_trans_suffix = []; % [] or '_trans_sss'
fifName = ['/mnt/sinuhe/data_raw/meta_nt/subject_subject/171027/19861231ANSR_s13_b01_aud' do_trans_suffix '.fif']; % any fif_file to get SSP components

cfg = [];
cfg.inputfile = fifName;
data_raw = obob_apply_ssp(cfg, data_raw);

%% Triggers and condition labels
%--------------------------------
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'}
paradigm    = 'hit_vs_miss';

[tr1,tr2,tr3,condlabel] = do_set_condtriggers(sensmod,paradigm);

[~,tr2_all,tr3_all,condlabel_all] = do_set_condtriggers(sensmod,'all');

% if sensmod = all --> Recode trialinfo (remove 100 200 or 300)
%--------------------------------------------------------------
if strcmp(sensmod{1},'all')
  Val2rm = floor( data_raw.trialinfo(:,1)./100)*100;
  data_raw.trialinfo(:,1) = data_raw.trialinfo(:,1) - Val2rm;
  tr1       = 0;
end

%% Equalize trial number per condition
%--------------------------------------
[data_raw_v2,trig,trial_id,trig_sens] = Do_equalcond(pathpreproc,fname,sensmod,tr1,condlabel,tr2,data_raw,paradigm);

clear data_raw;

%% lowpass filter
%-------------------
cfg = [];
if ~isempty(filt_lim)
  cfg.lpfilter ='yes';
  cfg.lpfreq = [filt_lim];
  cfg.lpfilttype = 'firws';
  cfg.lpfiltdf = 6;
  cfg.lpfiltwintype = 'kaiser';
end
data = ft_preprocessing(cfg, data_raw_v2);

clear data_raw

%% cut off (filter articfact)
%------------------------------
cfg = [];
cfg.trials = 'all';
cfg.toilim = toi;
data   = ft_redefinetrial(cfg, data);

%% fix channels
%----------------
% this is still an ugly fix
data.hdr.label = data.grad.label;

% fix channels
%-------------
cfg = [];
cfg.load_default = 1;
cfg.method = 'template';% 'triangulation' 'template' 'spline'
data = obob_fixchannels(cfg, data);

origLabel = data.origLabel;

if singletrials
  
    %% Average but keep single trials
    %----------------------------
    cfg=[];
    %cfg.preproc.demean         = 'yes';
    cfg.preproc.baselinewindow = [blwin];
    cfg.keeptrials = 'yes';
    data_avg = ft_timelockanalysis(cfg,data);    
    
    % make data smaller
    %-------------------
    data_avg = rmfield(data_avg, 'cfg');   
    
else
    %% Average for each condition
    %-------------------------------
    for j = 1:numel(sensmod)
      for k = 1:numel(condlabel_all)
        
        % triggers specific
        %------------------
        trig     = tr1(j)+tr2_all(k);
        trial_id = find(data.trialinfo(:,1) == trig);
        
        % check number of remaining trials
        %----------------------------------
        if numel(trial_id) < 10
          erf = []; % not enough trial to compute average
        else
          % avg + baseline
          %-------------
          cfg=[];
          cfg.keeptrials = 'no';
          cfg.trials     = trial_id;
          %cfg.preproc.demean         = 'yes';
          %cfg.preproc.baselinewindow = [blwin];
          cfg.vartrllength = 2; % accept variable trial length, missing data will be NaN
          erf = ft_timelockanalysis(cfg,data);
          
          % baseline-normalization
          %------------------------
          cfg = [];
          cfg.baseline     = blwin;
          cfg.channel      = 'all';
          cfg.parameter    = 'avg';
          erf = ft_timelockbaseline(cfg,erf);
          
          % combine
          %--------
          cfg=[];
          erf = ft_combineplanar(cfg,erf);
          
          % make data smaller
          %-------------------
          erf = rmfield(erf, 'cfg');
          
          % Keep track original channels
          %------------------------------
          erf.origLabel = origLabel;
          
        end
        % Setup subject/sensorymod/comdition specific struct
        %---------------------------------------------------
        data_avg.(sensmod{j}).(condlabel_all{k}) = erf;
        
        
        
      end
    end
    
end
%% saving subject wise avg
%--------------------------
disp(['Saving: ' saveName]);
save(saveName, '-v7.3', '-struct', 'data_avg');



