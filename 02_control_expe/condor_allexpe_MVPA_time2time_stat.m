function [] = condor_allexpe_MVPA_time2time_stat(Pathlogs,Pathsaved)

% RUN on cluster MVPA t2t no searchlight (nosl) analysis (call in a loop = Do_mvpa_srcERF.m)

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT_v2',cfg);

%% Parameters
%-----------------
h0_mean = 1/2; % 1/2 for crossvalidation or 0 for correlation
niter   = 10000; % 10000 for publication 
nproc   = 4; % number of parallel processes requested

sensmod = {'all'}; % {'all'} or {'aud' 'vis' 'tac'}

%% Condor call
%---------------
cfg = [];
cfg.mem       = '50G';
cfg.jobsdir   = Pathlogs;
cfg.cpus      = nproc;
cfg.java      = true;
condor_struct = obob_condor_create(cfg);

%% Setup directory name and filename
%-------------------------------------
% File type/Name
%----------------
Nametype = ['metaNT'];

% Get files
%-----------
Files = dir(fullfile(Pathsaved,[Nametype '*']));
if isempty(Files)
  error('No such files... Check paths and files name')
end

% Path to saved stat
%--------------------
Pathsave = fullfile(Pathsaved,'stat');
if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Jobs additions
%-----------------
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    cond_train = i;
    cond_test  = j;
    outputfile = fullfile(Pathsave,['STAT_niter' num2str(niter) '_train_' sensmod{cond_train} '_vs_test_' sensmod{cond_test} '_' Nametype '_N' num2str(numel(Files)) '.mat']);
    
    condor_struct = obob_condor_addjob(condor_struct, 'Do_mvpa_stat_t2t_allexpe',Pathsaved,Files,cond_train,cond_test,h0_mean,niter,outputfile,nproc);
    
  end
end
 
%% Submit
%---------
obob_condor_submit(condor_struct);