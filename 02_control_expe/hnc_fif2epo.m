function [] = hnc_fif2epo(pathfif,pathpreproc,sub,filthp)


%% Defaults settings
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);


subBlocks = {[1:9];...% sub01
    [1:9];...% sub02
    [1:9];...% sub03
    [1:9];...% sub04
    [1:9];...% sub05
    [1:9];...% sub06
    [1:9];...% sub07
    [1:9];...% sub08
    [1:9];...% sub09
    [1:9];...% sub10
    [1:9];...% sub11
    [1 3:10];... % sub12
    [1:9];...    % sub13
    [1:9];...    % sub14
    [1:6 8:10];...% sub15
    [1:9];... % sub16
    [2:10];...% sub17
    [1:9];... % sub18
    [1:9];... % sub19
    [1:9];... % sub20
    [1:9];... % sub21
    };


%% start loop
for iSub = sub 
    
    data_set = {};
    for iBlock = 1:numel(subBlocks{iSub})
        
        % define triggers and correct for diode
        pathsub = sprintf('s%02d',iSub);
        iB      = subBlocks{iSub}(iBlock);
        fiffile = [sprintf('sub%02d_',iSub),sprintf('block%02d_',iB),'*.fif'];
        
        fileName = strtrim(ls(fullfile(pathfif,pathsub,fiffile)));
        
        cfg = [];
        cfg.dataset = fileName;
        cfg.eventformat  = 'neuromag_fif';
        cfg.trialfun     = 'ft_trialfun_metaNT';
        cfg.trialdef.prestim        = 2;
        cfg.trialdef.poststim       = 2;
        cfgnew = ft_definetrial(cfg);
        
        
        %% change target trigger according to trigger response
        % + add sensory modality code = +100 AUD / +200 VIS / +300 TAC
        sensmod = fileName(end-6:end-4);
        switch sensmod
            case 'AUD'
                addtrigger = 100;
            case 'VIS'
                addtrigger = 200;
            case 'TAC'
                addtrigger = 300;
        end
        cfgnew.trl(:,4) = cfgnew.trl(:,4) + addtrigger;
        
        %% Change trial indexing for block
        if iBlock == 1
        last_blocktr = 0;
        else
        end
        cfgnew.trl(:,5) = cfgnew.trl(:,5) + last_blocktr;
        last_blocktr = cfgnew.trl(end,5); % keep track of last trial index for next block
        
        %% filter continuous data
        % do definetrial for continuous data...
        cfg = [];
        cfg.dataset = fileName;
        cfg.trialdef.triallength = Inf; % this is the trick: infinite trial length...
        cfg.trialdef.ntrials = 1;       % ...for one trial!
        cfg = ft_definetrial(cfg);
       
       
        cfg.hpfilter = 'yes';
        cfg.hpfreq = filthp;
        cfg.hpfilttype = 'firws';
        cfg.hpfiltdf = filthp;
        cfg.hpfiltwintype = 'kaiser';
        alldata = ft_preprocessing(cfg);
        
        
        %% now epoch
        data_raw = ft_redefinetrial(cfgnew, alldata);
        
        %% downsample to 512hz (save space)
        cfg = [];
        cfg.resamplefs = 512;
        cfg.detrend = 'no';
        
        data_raw = ft_resampledata(cfg, data_raw);
        
        data_set{iBlock} = data_raw;
        
        %% saving
        saveName = [sprintf('%02d_',iSub),sprintf('b%02d_',iBlock),sensmod,'.mat'];
    
        save(fullfile(pathpreproc, saveName),'-v7.3','-struct', 'data_raw');
        
        
    end
    
    % Append data
    %-------------
    data = ft_appenddata([],data_set{:}); % append blocks to one file
    data.grad = data_set{1}.grad;

    saveName = fullfile(pathpreproc,[sprintf('%02d_',iSub),'allblocks','.mat']);

    %% save files
    disp(' ');
    disp(['Saving: ' saveName])
    disp(' ');
   
    save(saveName,'-v7.3','-struct', 'data');
    
    % Deleted previous file
    %-----------------------
    system(['rm ' fullfile(pathpreproc,[sprintf('%02d_b',iSub),'*'])]);
    
end

