function [timewins,timemax] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput,timeWin)
%
% INPUTS
%-------
% data_ga         : should be fieldtrip structure with extra field for all subjects
%                   ERF with dimensions [Subj x Sensors x Time]
% test1 and test2 : Name fo the field from 'data_ga' to plot
% sel             : sensors selection %'GRC'; %'MAG'; %'SRC'
% timeWin         : time [beg end] to plot in ms

% test1 = 'audhit';%'ud';%'ud';%'dd';
% test2 = 'audmiss';%'ud';%'ud';%'dd';
% sel = 'GRC';%'SRC'; %'MAG';

if nargin < 5
  Pathoutput = [];
end


%% Parameters & Time definition
%-------------------------------
%timeWin = [-200 800]; % in ms
pnts    = time2points(timeWin, data_ga.fsample, min(data_ga.time)*1000);
toipnts = [pnts(1):pnts(2)];

do_plotstatuncorrected = 0;
topo_timeresolved = 1; % do time resolved topos (1) or not (0) by plotting only significant toi

slwin   = 30; % length of sliding window (for stat)
overlap = 10; %  overlap of slideing window

timeWinstat = [0 550];
pnts    = time2points(timeWinstat, data_ga.fsample, min(data_ga.time)*1000);
toistat = [pnts(1):pnts(2)];

%% Absolute value
%-----------------
data_ga.(test1) = abs(data_ga.(test1));
data_ga.(test2) = abs(data_ga.(test2));
% data_ga.(test1) = sqrt(data_ga.(test1).^2);
% data_ga.(test2) = sqrt(data_ga.(test2).^2);

%% Baseline correction (absolute)
%----------------------------------
prestim = find(data_ga.time(toipnts) < 0);
% data_ga.(test1) = data_ga.(test1) - mean(data_ga.(test1)(:,:,prestim),3);
% data_ga.(test2) = data_ga.(test2) - mean(data_ga.(test2)(:,:,prestim),3);

%% axis defintion
%------------------

xaxis = timeWin./1000;

switch sel
  
  case 'GRC'
    factor = 1e12;
    ylabel = 'pt/m^2';
    % for catch vs. sham
    yaxis = [-1 3.5];
    zlim = [-4 4]./(factor); 
    
%     % for det vs. undet.
%     yaxis = [-0.5 1.5];
%     zlim = [-1 1]./(factor);
    
    ytick = 0.5;
    rms_rm = 1;
    sens_sel = meg_select_sensors(sel);
    sens_idx = ismember(data_ga.label, sens_sel);
    rois = sens_idx;
    
  case 'MAG'
    factor = 1e15;
    ylabel = 'ft';
    yaxis = [0 70];
    ytick = 10;
    rms_rm = 1;
    sens_sel = meg_select_sensors(sel);
    sens_idx = ismember(data_ga.label, sens_sel);
    rois = sens_idx;
    
  case 'SRC'
    factor = 1;
    factor = 1e12;
    ylabel = 'Amplitude (a.u.)';
    if contains(test1,'supra')
      yaxis = [-0.5 8];ytick = 0.5;
    else
      yaxis = [-0.5 1];ytick = 0.5;
    end
    rms_rm = 1;
    rois = 1:numel(data_ga.label);
  otherwise
    error('''sel'' parameter must be: ''GRC'', ''MAG'' or ''SRC''...')
    
end



%% do stats first to get sig intervals
%--------------------------------------

statdata = [];
stats = [];
for iSub = 1:size(data_ga.(test1),1)
  statdata(:,:,1, iSub) = data_ga.(test1)(iSub,rois,toistat)*factor; %fT
  statdata(:,:,2, iSub) = data_ga.(test2)(iSub,rois,toistat)*factor;
end
cfg =[];
cfg.data      = statdata;
cfg.alpha     = 0.05;
cfg.type      = 'abs';
cfg.sfreq     = data_ga.fsample;
cfg.contrasts = [1 2];
cfg.slwin     = slwin;
cfg.overlap   = overlap;
cfg.time      = data_ga.time(toistat)*1000;
cfg.dylim     = [0 yaxis(2)];
cfg.dcolor    = {'r' 'b'};
cfg.con_names = {'Hit' 'Miss'};
%cfg.cor_mode = 'fdr';
stats = meeg_plot_ttest_tcourse(cfg);

% Track start and end of significant period
%------------------------------------------
onset = find(diff(abs(stats.tvals) > stats.btcrit) == 1)' + 1;
offset = find(diff(abs(stats.tvals) > stats.btcrit) == -1)';

% Id_single_sig = find(onset == offset);
% for id = 1:numel(Id_single_sig)
%   offset(Id_single_sig(id)) = onset(Id_single_sig(id))+1; % just add one more sig_win for visu small significatn windows
% end

if ~isempty(offset) && offset(1) == 1 % in case only one sample significant at start then all significant until the end
  offset = numel(stats.tvals);
end

if isempty(offset)
  sig_time = [stats.time(onset) stats.time(end)];
elseif numel(onset) > numel(offset)
  sig_time = [stats.time(onset)' [stats.time(offset)';stats.time(end)]];
elseif numel(onset) < numel(offset)
  sig_time = [[stats.time(1);stats.time(onset)'] stats.time(offset)'];
else
  sig_time = stats.time([onset offset]);
end

stats.sig_time = sig_time;
stats.sensors = sel;

% No corrected
%--------------
onset2 = find(diff(abs(stats.tvals) > stats.utcrit) == 1)' + 1;
offset2 = find(diff(abs(stats.tvals) > stats.utcrit) == -1)';

if ~isempty(offset2) && offset2(1) == 1 % in case only one sample significant at start then all significant until the end
  offset2 = numel(stats.tvals);
end

if isempty(offset2)
  sig_time2 = [stats.time(onset2) stats.time(end)];
elseif numel(onset2) > numel(offset2)
  sig_time2 = [stats.time(onset2)' [stats.time(offset2)';stats.time(end)]];
elseif numel(onset2) < numel(offset2)
  sig_time2 = [[stats.time(1);stats.time(onset2)'] stats.time(offset2)'];
else
  sig_time2 = stats.time([onset2 offset2]);
end

stats.sig_time_unc = sig_time2;


%% Plot time course with errorbar
%----------------------------------
cfg = [];

Mdata(1,:) = squeeze(mean(mean(data_ga.(test1)(:,rois,toipnts),2),1).*factor); %fT (check top)
Mdata(2,:) = squeeze(mean(mean(data_ga.(test2)(:,rois,toipnts),2),1).*factor);

% Baseline correction for visu only
%-----------------------------------
Mdata = Mdata - mean(Mdata(:,prestim),2);

Sdata(1,:) = squeeze(std(mean(data_ga.(test1)(:,rois,toipnts),2),1).*factor)./sqrt(size(data_ga.(test1),1));
Sdata(2,:) = squeeze(std(mean(data_ga.(test2)(:,rois,toipnts),2),1).*factor)./sqrt(size(data_ga.(test2),1));

% Sdata(1,:) = squeeze(std(mean(data_ga.(test1)(:,rois,toipnts),2),1).*factor);%./sqrt(size(data_ga.(test1),1));
% Sdata(2,:) = squeeze(std(mean(data_ga.(test2)(:,rois,toipnts),2),1).*factor);%./sqrt(size(data_ga.(test2),1));


% Find max peaks
%----------------
[PKS,LOCS]= findpeaks(Mdata(1,:),'MinPeakWidth',10,'MinPeakHeight',1)
timemax = data_ga.time(toipnts(LOCS));

% Plot
%------
figure;set(gcf,'color','w');
shadedErrorBar(data_ga.time(toipnts),Mdata(1,:),Sdata(1,:),'-r',0.5);hold on;
shadedErrorBar(data_ga.time(toipnts),Mdata(2,:),Sdata(2,:),'-b',0.5);

set(gca, 'Visible', 'off');
cfgaxes = [];
cfgaxes.lims       = [xaxis(1) xaxis(2) yaxis(1) yaxis(2)*rms_rm];
cfgaxes.ylabel     = ylabel;
cfgaxes.line_width = 2;
cfgaxes.origin     = [0 yaxis(1)];
%cfgaxes.xlabel = 'Time (s)';
cfgaxes.tick_steps = [.1 ytick]; %round to an even tenth
cfgaxes.text_dis   = [0.02*ytick 0.01];
plot_axes(cfgaxes);
axis(cfgaxes.lims);

% stat corrected
%-----------------
sig_time = stats.sig_time;
if mean(sig_time(:)) > 1
  timewins = sig_time ./1000;
end
for i = 1:size(timewins,1)
  halfytick = -ytick/2;
  plot(timewins(i,:), repmat(halfytick+halfytick/2,1,2), 'k', 'LineWidth', 3)
end

if do_plotstatuncorrected
  % stat uncorrected
  %------------------
  sig_time_unc = stats.sig_time_unc;
  if mean(sig_time_unc(:)) > 1
    timewins_unc = sig_time_unc ./1000;
  end
  for i = 1:size(timewins_unc,1)
    halfytick = -ytick/2;
    plot(timewins_unc(i,:), repmat(halfytick+halfytick/2.5,1,2),'Color',[.6 .6 .6], 'LineWidth', 2)
  end
end

% save figure
%------------
figure(gcf);
if ~isempty(Pathoutput)
  pause(1);
  save_name = fullfile(Pathoutput, [ 'erf_' test1 '-' test2 '_sens' sel '.svg']);
  save_name2 = fullfile(Pathoutput, [ 'erf_' test1 '-' test2 '_sens' sel '.png']);
  save_name3 = fullfile(Pathoutput, [ 'erf_' test1 '-' test2 '_sens' sel '.eps']);
  
  save_figure(save_name,100);
  save_figure(save_name2,100);
  save_figure(save_name3,200);
end
%% topos
%-------

cfg =[];
data_ga.avg = squeeze(mean(data_ga.(test1)-data_ga.(test2),1));
data_ga.dimord = 'chan_time';

% reduce even more as for rms (cause this is the difference, i.e. tiny)
yreduce = rms_rm + 2;

switch topo_timeresolved
  
  case 0
    
    if ~strcmp(sel, 'SRC') && numel(timewins) > 1
      
      idTop = find(timewins(:,1)- timewins(:,2)); % more than 1ms topo
      
      for i = 1:length(idTop)
        if strcmp(sel, 'MAG')
          cfg.layout = 'neuromag306mag.lay';
        elseif strcmp(sel, 'GRC')
          cfg.layout = 'neuromag306cmb.lay';
        end
        cfg.parameter = 'avg';
        cfg.fontsize = 16;
        cfg.comment = 'no';
        cfg.xlim = timewins(idTop(i),:);
        if strcmp(sel, 'GRC')
          %cfg.zlim = [yaxis(1)./yreduce yaxis(2)./yreduce ]./(factor);
          cfg.zlim = zlim;
        else
          cfg.zlim = [-yaxis(2)./yreduce yaxis(2)./yreduce ]./(factor);
        end
        figure;set(gcf,'color','w'); ft_topoplotER(cfg, data_ga);
        title(num2str(cfg.xlim.*1000),'FontSize', 20, 'FontWeight','b');
        colorbar;
        % colormap jet
        ft_hastoolbox('brewermap',1);
        colormap(flipud(brewermap(64,'RdBu')));
        
        
        if ~isempty(Pathoutput)
          fname = ['erf_' test1 '-' test2 '_sens' sel '_topo_time_' num2str(round(timewins(idTop(i),1)*1000)) '-' num2str(round(timewins(idTop(i),2)*1000))];
          save_name = fullfile(Pathoutput, [fname '.svg']);
          save_name2 = fullfile(Pathoutput, [fname '.png']);
          save_name3 = fullfile(Pathoutput, [fname '.eps']);
          
          save_figure(save_name,100);
          save_figure(save_name2,100);
          save_figure(save_name3,200);
        end
        
      end
    end
    
    
  case 1
    
    if strcmp(sel, 'MAG')
      cfg.layout = 'neuromag306mag.lay';
    elseif strcmp(sel, 'GRC')
      cfg.layout = 'neuromag306cmb.lay';
    end
    cfg.parameter = 'avg';
    cfg.comment = 'no';
    cfg.xlim = [0.05:0.1:0.55];
    cfg.comment = 'xlim';
    cfg.commentpos = 'title';
    if strcmp(sel, 'GRC')
      %cfg.zlim = [yaxis(1)./yreduce yaxis(2)./yreduce ]./(factor);
      cfg.zlim = zlim;
    else
      cfg.zlim = [-yaxis(2)./yreduce yaxis(2)./yreduce ]./(factor);
    end
    figure;set(gcf,'color','w'); ft_topoplotER(cfg, data_ga);
    
    ft_hastoolbox('brewermap',1);
    colormap(flipud(brewermap(64,'RdBu')));
   
    
    if ~isempty(Pathoutput)
      fname = ['erf_' test1 '-' test2 '_sens' sel '_topo_time_resolved'];
      save_name = fullfile(Pathoutput, [fname '.svg']);
      save_name2 = fullfile(Pathoutput, [fname '.png']);
      save_name3 = fullfile(Pathoutput, [fname '.eps']);
      
      save_figure(save_name,100);
      save_figure(save_name2,100);
      save_figure(save_name3,200);
    end
    
    
end