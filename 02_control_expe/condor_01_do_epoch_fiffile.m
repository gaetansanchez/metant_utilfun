%% SCRIPT call condor for epoch data

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

Pathlogs = fullfile(config.path,['jobs/fif2epo/' date]);

%% Set parameters
%-----------------
sub = get_subject_mat(config); % get all subject code and fifpath
Nsub   = numel(sub);

do_trans_suffix = []; % [] or '_trans_sss'
filthp = [];
epoch_time = [1 1]; % pre and post time (in sec) [0.5 0.7]

%outDir   = fullfile(config.path,sprintf('preproc%s',do_trans_suffix));
outDir   = fullfile(config.path,'data2share');
if ~exist(outDir,'dir') % create output dir if necessary
  mkdir(outDir);
end

%% Condor call
%---------------
cfg = [];
cfg.mem       = '50G';
cfg.jobsdir   = Pathlogs;
cfg.java      = true;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
for isub = 1:Nsub
  
  % Read and detect artefact
  %---------------------------
  pathpreproc = outDir;
  
  saveName = fullfile(pathpreproc,sprintf('%02d_allblocks.mat',isub));
  if ~exist(saveName,'file')
    condor_struct = obob_condor_addjob(condor_struct,'do_epoch_fiffile',isub,epoch_time,filthp,pathpreproc,do_trans_suffix);
  end
end

%% Submit
%---------
obob_condor_submit(condor_struct);
