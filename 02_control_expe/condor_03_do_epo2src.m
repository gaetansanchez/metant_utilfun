% RUN on cluster src ERF analysis (call in a loop = Do_srcERF.m)

%% Path settings
%----------------
project = 'metaNT_v2';
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs(project,cfg);

%% Parameters
%----------------
sub = get_subject_mat(config); % get all subject code and fifpath
% remove subject with less than 30 trials in one condition
Suj = Do_sel_subjects(config,'metaNT_v2');
Nsub = numel(Suj);

precision_grid = 1.5; % in cm (template grid) or 'parcellations_3mm.mat'
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'}
toilim      = [-0.5 0.5]; % in sec used for cov_win
CFilter     = 'sensmod';% Beamformer common filter computation =  'alltrials' or 'sensmod'
paradigm    = 'hit_vs_miss';% 'all' or 'hit_vs_miss' or 'supra_vs_sham' or 'miss_vs_sham' 
do_trans_suffix = []; % [] or '_trans_sss'


%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2src/' date]);
pathpreproc = fullfile(config.path,['preproc' do_trans_suffix]); % Path clean data
%Pathsave = fullfile(config.path2work,['src' do_trans_suffix],'srcERF_lp30Hz',['1.5cm_grid_CFilter' CFilter '_' paradigm]);
Pathsave = fullfile(config.path2work,['test_src' do_trans_suffix],'srcERF_lp30Hz',['1.5cm_grid_CFilter' CFilter '_' paradigm]);

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '10G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
for i = 1:Nsub
  id = Suj(i);
  hdmfile = fullfile(config.path,'hdm',sub(id).code,[sub(id).code,'_hdm' do_trans_suffix '.mat']);
  condor_struct = obob_condor_addjob(condor_struct,'Do_srcERF',project,id,hdmfile,sensmod,toilim,pathpreproc,Pathsave,precision_grid,CFilter,paradigm);                                                 
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);

