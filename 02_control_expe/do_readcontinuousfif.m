function [data_out] = do_readcontinuousfif(fileName,filthp,dossp)

if nargin < 3
    dossp = 1;
end

% Read MEG & EEG
%----------------
cfg = [];
cfg.dataset              = fileName;
cfg.trialdef.ntrials     = 1;
cfg.trialdef.triallength = Inf;
cfg = ft_definetrial(cfg);

cfg.channel = {'MEG' 'E*'}; % read MEG channels + all EOG001 EOG002 ECG003

% HPfilt preprocess contiuous data
%---------------------------------
if filthp > 0
  cfg.hpfilter      = 'yes';
  cfg.hpfreq        = filthp;
  cfg.hpfilttype    = 'firws';
  cfg.hpfiltdf      = filthp;
  cfg.hpfiltwintype = 'kaiser';
end

data = ft_preprocessing(cfg);

% Apply signal space projection (SSP): reject external noise
%-----------------------------------------------------------
if dossp
    
    disp('  ');
    disp('*****************');
    disp('Apply SSP...');
    disp('  ');

    cfg = [];
    cfg.inputfile = fileName;
    data_out = obob_apply_ssp(cfg, data);
    
else
    % No SSP
    %--------
    data_out = data;
end


