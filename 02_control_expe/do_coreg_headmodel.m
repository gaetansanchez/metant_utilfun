function [hdm] = do_coreg_headmodel(iSub,fiffile,hdmfile)
% Do HDM
%---------
% input ==> i.e. iSub = '19861231ANSR';

% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT_v2',cfg);
pathexp = config.path;


%pathhdm = fullfile(pathexp,'hdm',sprintf(iSub));
[pathhdm,~,~] = fileparts(hdmfile);

if exist(fullfile(pathhdm,'mri'), 'dir')
  try
    mrifile = strtrim(ls(fullfile(pathhdm,'mri','HR*.nii')));
  catch nonifti
    % if no nifti, try other formats
    try
      mrifile = strtrim(ls(fullfile(pathhdm,'mri','*.img')));
    catch noimage
      files = dir(fullfile(pathhdm,'mri'));
      % this should print an error, if not a file
      % take the 3rd name, as first is '.' and second is '..'
      mrifile = strtrim(ls(fullfile(pathhdm,'mri',files(3).name)));
    end
  end
else
  mrifile = []; %if no anatomical mri is available for this subject
  disp('*************');
  disp(['No aMRI available for ' iSub ' : create fake one !']);
  disp('*************');
  pause(1);
end


%% Coregister
%-------------
cfg = [];
cfg.mrifile   = mrifile;
cfg.headshape = fiffile;
cfg.viewmode  = 'surface';

if strcmp(iSub,'19861231ANSR') || strcmp(iSub,'19861231ANSR_trans')
  cfg.reslice = 'no';
  cfg.viewmode  = [];
end

[mri_aligned,shape,vol,mri_segmented]=obob_coregister(cfg);

mri_aligned.gray  = mri_segmented.gray;
mri_aligned.white = mri_segmented.white;
mri_aligned.csf   = mri_segmented.csf;

%% warp grid
% load default grid
%load /mnt/storage/tier1/natwei/Shared/julia/templates/mni_grid_1_5_cm_889pnts
load mni_grid_1_5_cm_889pnts

% warp grid
cfg = [];
cfg.coordsys = 'neuromag';
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = template_grid;
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.mri            = mri_aligned;
warped_grid = ft_prepare_sourcemodel(cfg);
% 
% %% check
% figure;
% vol = ft_convert_units(vol, 'cm');
% ft_plot_vol(vol, 'edgecolor', 'none'); alpha 0.4;
% ft_plot_mesh(warped_grid.pos(warped_grid.inside,:));

%% preparing headmodel structure
if ~exist('hdm', 'var')
  hdm = struct;
end

hdm.headshape        = shape;
hdm.mri              = mri_aligned;
hdm.vol              = vol;
hdm.grid_templateMNI = template_grid;
hdm.grid_warped      = warped_grid;
hdm.outputpath       = pathhdm;

hdm = orderfields(hdm);

%% saving headmodel if intended
disp(' ')
disp(['Saving headmodel in path ' pathhdm])
disp(' ')
save(hdmfile, '-struct', 'hdm');




