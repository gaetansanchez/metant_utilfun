
% RUN epo2ERF
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/erf2ga/' date]);

pathERF = fullfile(config.path2work,'erf');
pathgaERF = fullfile(pathERF,'ga_erf');

if ~exist(pathgaERF,'dir') % create output dir if dont exist
  mkdir(pathgaERF);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '10G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Parameters 
%-------------
erffilename = 'allblocks_-0.5-0.7s_lp30_erf_percond';


%% Subjects
%-----------
sub = get_subject_mat(config); % get all subject code and fifpath
% remove subject with less than 30 trials in one condition
Suj = Do_sel_subjects(config,'metaNT_v2');

%% Jobs additions
%-----------------
condor_struct = obob_condor_addjob(condor_struct, 'hnc_erf2ga',...
                        pathERF,...
                        pathgaERF,...
                        Suj,...
                        erffilename);

%% Submit
%---------
obob_condor_submit(condor_struct);

