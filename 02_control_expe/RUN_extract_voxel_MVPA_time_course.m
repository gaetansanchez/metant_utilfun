function [] = RUN_extract_voxel_MVPA_time_course(atlas2read,sigVox,noteintval,testdat,M_tc,val,stat_mask)

%% Process Tab for atlas Parcel
%-------------------------------

[Tab_net] = Do_extractROIname_atlas(atlas2read,sigVox,noteintval,testdat,[],1);

LabFam = {'Au', 'Vi', 'Sm' ,'De' ,'Ci', 'Do','Ve','Fr'};
ValFam = [11 21 31 42 43 44 44 45];
Net_Lab = {'Sens' 'DMN' 'Cing' 'AtVD' 'FrPa'};
%ValFam = [11 22 33 44 45 46 46 47];
%Net_Lab = {'Au' 'Vi' 'Sm'  'DMN' 'Cing' 'AtVD' 'FrPa'};
%LabFam = {'Au', 'Vi', 'Sm'};
%ValFam = [1 2 3];
%Net_Lab = {'Au' 'Vi' 'Sm'};



NbClTab = size(Tab_net,2);

for i = 1:size(Tab_net,1)
  
  id_Fam = find(ismember(LabFam,Tab_net{i,1}(1:2)));
  
  if id_Fam
    Tab_net{i,NbClTab+1} = ValFam(id_Fam);
  else
    Tab_net{i,NbClTab+1} = 0;
  end
  
end
Tab_net

%% Tim course of different networks
%-----------------------------------
% Get Voxels indices for different network
%------------------------------------------

Net_Vox = {};
n = 1;
NbvarNet = numel(Net_Lab);
AbsentLab = [];
for i = 1:NbvarNet
  
  IDNet = cell2mat(Tab_net(find(mod(cell2mat(Tab_net(:,end)),10)==i),5));
  if isempty(IDNet)
    AbsentLab = [AbsentLab i];
  else
    Net_Vox{n} = IDNet;
    n = n+1;
  end
end
Net_Lab(AbsentLab) = [];


% Loop average
%-------------
OnlySig  = 0; % Only significant time points : 1=yes or 0=all time points
Nsuj     = size(M_tc,1);
Nsensmod = size(M_tc,2);
Ntime    = size(M_tc,6);
MaccNet       = zeros(Nsuj,Nsensmod,Nsensmod,numel(Net_Vox),Ntime);
MaccNet_BETW  = zeros(Nsuj,Nsensmod*2,numel(Net_Vox),Ntime);
MaccNet_WITH  = zeros(Nsuj,Nsensmod,numel(Net_Vox),Ntime);
for s = 1:Nsuj % suj
  b = 1;
  w = 1;
  for i = 1:Nsensmod % modality
    for j = 1:Nsensmod % modality
      for k = 1:numel(Net_Vox) % network
        tmpdat = squeeze(M_tc(s,i,j,Net_Vox{k},:,:));
        
        sig_traintime = logical(sum(squeeze(stat_mask(i,j,:,:)),2));% significant time point over training axes
        tmpdat_sig = tmpdat;
        tmpdat_sig(:,repmat(~sig_traintime,1,size(tmpdat,3))) = 0.5;% set non significant time point to 0.5 significant time points
        
        if OnlySig
          tmpdat = tmpdat_sig;
        end
        
        MaccNet(s,i,j,k,:) = mean(mean(tmpdat,3),1); % average over : test time + Network sources
        if i ~= j % only between sensmod decoding
          MaccNet_BETW(s,b,k,:) = mean(mean(tmpdat,3),1);
        else
          MaccNet_WITH(s,w,k,:) = mean(mean(tmpdat,3),1);
        end
      end
      if i ~= j % only between sensmod decoding
        b = b+1;
      else
        w = w+1;
      end
    end
  end
end


MaccNet_BETW_all = squeeze(mean(mean(MaccNet_BETW,2),1)); % average all between decoding + all subjects
SaccNet_BETW_all = squeeze(std(mean(MaccNet_BETW,2),1)./sqrt(size(MaccNet_BETW,1))); % SEM
MaccNet_WITH_all = squeeze(mean(mean(MaccNet_WITH,2),1)); % average all within decoding + all subjects
SaccNet_WITH_all = squeeze(std(mean(MaccNet_WITH,2),1)./sqrt(size(MaccNet_WITH,1))); % SEM

%%------
%% Plot
%------
figure; set(gcf,'color','w');
color4Net ={'-k' '-r' '-g' '-c' '-b' '-m' '-y'};
LimYAx = [0.45 0.55];
subplot(2,2,1);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_WITH_all(k,:),SaccNet_WITH_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,3);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_WITH_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
title('Within Modality','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');

subplot(2,2,2);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_BETW_all(k,:),SaccNet_BETW_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,4);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_BETW_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
title('Between Modalities','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
