
% RUN on cluster fif2epo

%% clear
%--------
clear all global
close all


%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/DotrialsNbCount/' date]);

%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
Suj = 1:21;
do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'
pathpreproc_file   = fullfile(config.path,['preproc' do_trans_suffix]);
pathpreproc_artdef = fullfile(config.path,['preproc' do_trans_suffix],'track_artefact_rejected');

for i = 1:numel(Suj)
  id = Suj(i);
  condor_struct = obob_condor_addjob(condor_struct, 'Do_trialsNb_count_after_reject',id,pathpreproc_file,pathpreproc_artdef);
end

%% Submit
%---------
obob_condor_submit(condor_struct);
