function [] = RUN_extract_voxel_MVPAS_time_course()

%% Script extract voxels activity and plot it
%-----------------------------------------------

if ~exist('ds_ft','var')
    Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
    load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end

%%%% options:
%---------------
% 1 = only 10% acc from mask intersect significant
% 2 = make mask from 10% acc between sensmod decoding
% 0 = plot all acc from mask intersect significant
acc10 = 0;
dosavefile = 0;
dosurface  = 1;
toi = [0 0.5]; % in sec (time period to average to plot)
%val2plot = [0.45 0.55];
val2plot = [0 1]; % normalize accuracies


if numel(size(ds_ft)) == 3
    data = ds_ft{1,1,1};
else
    data = ds_ft{1,1};
end

%%% Masking
%-------------
selroi = CVroi_all; % only spatial features between modalities (intersect)
mask = zeros(1,889);
mask(selroi) = 1;

%%% Data organization
%----------------------
data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

if numel(sensmod) == 3
  Msig_acc2plot = Msig_acc_Between;
  Maxsig_acc2plot = Maxsig_acc_Between;
  Mlatsig_vox2plot = Mlatsig_BETW_IUroi_all*1000; % Mean Latency for these voxels (in ms)
elseif numel(sensmod) == 1
  Msig_acc2plot = squeeze(Msig_acc);
  Maxsig_acc2plot = squeeze(Maxsig_acc);
  Mlatsig_vox2plot = Mlatsig_Nodes(selroi)*1000;% Mean Latency for these voxels (in ms)
end


if acc10 == 1
    % Find 10% of significant accuracy
    %----------------------------------
    opt = []; opt.lim = 10; opt.flag = 0;
    x = Msig_acc2plot;
    [bound] = find_distrib_percent(x,opt);
    mask  = zeros(1,889);
    mask(Msig_acc2plot>=bound(2)) = 1;% mask > 10% acc
    
    selroi = 1:889; % No selection of intersection mask
    %val2plot = [bound(2) max(x)];
    %val2plot = [0.45 max(x)];
    
elseif acc10 == 2
    % Mask of intersect of union 10% accuracy
    %----------------------------------
    mask  = zeros(1,889);
    mask  = mask10_IU';
    
    selroi = 1:889; % No selection of intersection mask
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
    
    
else
    %val2plot = [0 max(Msig_acc_Between(selroi))];
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
end

if val2plot(1)==0 && val2plot(2)==1
  % Normalize accuracy between 0 and 1 with y = (x - min(x))/(max(x)-min(x))
  %%%%%%%%%%%%%%  
  [x] = Do_normalize(x,1);
end

val2plotstore = val2plot;

% Fill-in with accuracy values found
for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
        data.pow(selroi,i,j) = x(selroi)-val2plot(1);
        data.mask(:,i,j) = mask;
        data.maskInt(:,i,j) = mask;
        data.maskInt(selroi,i,j) = AIDroi_intensity(selroi);
    end
end

%%
testdat = data;
sigVox  = find(squeeze(sum(sum(data.mask,2),3))); % only significant voxels

MeanAccval  = Msig_acc2plot(sigVox); % mean Acc values for these voxels
MaxAccval   = Maxsig_acc2plot(sigVox); % max Acc values for these voxels
LatVox      = Mlatsig_vox2plot; % Mean Latency for these voxels (in ms)

% sort voxels indices based on their values
%--------------------------------------------
[sort_MeanAccval,indv] = sort(MeanAccval, 'descend');

% sort according to mean accuracy
%---------------------------------
LatVox = LatVox(indv);
MaxAccval = MaxAccval(indv);
sigVox = sigVox(indv);

noteintval = [LatVox sort_MeanAccval MaxAccval]; % mean latency / mean Acc / Max Acc

testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)

%% Process Tab for atlas Parcel
%-------------------------------
B = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
[Tab_net] = Do_extractROIname_atlas(B,sigVox,noteintval,testdat,[],1);

%LabFam = {'Au', 'Vi', 'Sm' ,'De' ,'Ci', 'Do','Ve','Fr'};
%ValFam = [11 21 31 42 43 44 44 45];
%ValFam = [11 22 33 44 45 46 46 47];
LabFam = {'Au', 'Vi', 'Sm'};
ValFam = [1 2 3];

NbClTab = size(Tab_net,2);

for i = 1:size(Tab_net,1)
  
  id_Fam = find(ismember(LabFam,Tab_net{i,1}(1:2)));
  
  if id_Fam
    Tab_net{i,NbClTab+1} = ValFam(id_Fam);
  else
    Tab_net{i,NbClTab+1} = 0;
  end
  
end
Tab_net

%% Tim course of different networks
%-----------------------------------
%startpoint = 17; stoppoint = 33;
% Get Voxels indices for different network
%------------------------------------------
%Net_Lab = {'Sens' 'Att'};
%Net_Lab = {'Sens' 'DMN' 'Cing' 'AtVD' 'FrPa'};
%Net_Lab = {'Au' 'Vi' 'Sm'  'DMN' 'Cing' 'AtVD' 'FrPa'};
Net_Lab = {'Au' 'Vi' 'Sm'};
Net_Vox = {};
n = 1;
NbvarNet = numel(Net_Lab);
AbsentLab = [];
for i = 1:NbvarNet
  
  IDNet = cell2mat(Tab_net(find(mod(cell2mat(Tab_net(:,end)),10)==i),6));
  if isempty(IDNet)
    AbsentLab = [AbsentLab i];
  else
    Net_Vox{n} = IDNet;
    n = n+1;
  end
end
Net_Lab(AbsentLab) = [];

if ~exist('M_tc','var')
  % Load single subject Acc values
  %---------------------------------
  Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['SubjectsAcc_' AnalysisName '.mat']));
  
  % Cut accroding to start and stop point
  M_tc = M_t(:,:,:,:,startpoint:stoppoint,startpoint:stoppoint);
  val =val(startpoint:stoppoint);
end

% Loop average
%-------------
OnlySig  = 1; % Only significant time points : 1=yes or 0=all time points
Nsuj     = size(M_tc,1);
Nsensmod = size(M_tc,2);
Ntime    = size(M_tc,6);
MaccNet       = zeros(Nsuj,Nsensmod,Nsensmod,numel(Net_Vox),Ntime);
MaccNet_BETW  = zeros(Nsuj,6,numel(Net_Vox),Ntime);
MaccNet_WITH  = zeros(Nsuj,3,numel(Net_Vox),Ntime);
for s = 1:Nsuj % suj
  b = 1;
  w = 1;
  for i = 1:Nsensmod % modality
    for j = 1:Nsensmod % modality
      for k = 1:numel(Net_Vox) % network
        tmpdat = squeeze(M_tc(s,i,j,Net_Vox{k},:,:));
        
        sig_traintime = logical(sum(squeeze(stat_mask(i,j,:,:)),2));% significant time point over training axes
        tmpdat_sig = tmpdat;
        tmpdat_sig(:,repmat(~sig_traintime,1,size(tmpdat,3))) = 0.5;% set non significant time point to 0.5 significant time points
        
        if OnlySig
          tmpdat = tmpdat_sig;
        end
        
        MaccNet(s,i,j,k,:) = mean(mean(tmpdat,3),1); % average over : test time + Network sources
        if i ~= j % only between sensmod decoding
          MaccNet_BETW(s,b,k,:) = mean(mean(tmpdat,3),1);
        else
          MaccNet_WITH(s,w,k,:) = mean(mean(tmpdat,3),1);
        end
      end
      if i ~= j % only between sensmod decoding
        b = b+1;
      else
        w = w+1;
      end
    end
  end
end


MaccNet_BETW_all = squeeze(mean(mean(MaccNet_BETW,2),1)); % average all between decoding + all subjects
SaccNet_BETW_all = squeeze(std(mean(MaccNet_BETW,2),1)./sqrt(size(MaccNet_BETW,1))); % SEM
MaccNet_WITH_all = squeeze(mean(mean(MaccNet_WITH,2),1)); % average all within decoding + all subjects
SaccNet_WITH_all = squeeze(std(mean(MaccNet_WITH,2),1)./sqrt(size(MaccNet_WITH,1))); % SEM

%------
% Plot
%------
figure; set(gcf,'color','w');
color4Net ={'-k' '-r' '-g' '-c' '-b' '-m' '-y'};
LimYAx = [0.495 0.515];
subplot(2,2,1);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_WITH_all(k,:),SaccNet_WITH_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,3);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_WITH_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
title('Within Modality','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');

subplot(2,2,2);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_BETW_all(k,:),SaccNet_BETW_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,4);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_BETW_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
title('Between Modalities','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
