
%% SCRIPT analyse MVPA t2t
%----------------------------

%% Path definition
%-------------------

clear all;

% Path definition
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Load all files into fieldtrip format
%---------------------------------------
Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad0/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_toilim_-100to600_tstep10ms_OBOB_BeamCFiltsensmod_srad30_Bayes';

Pathsaved = fullfile(config.path2work,Pathdata); % saved data
Path2saved = fullfile(Pathsaved,'MeanDSFT');

if ~exist(Path2saved,'dir') % create output dir if dont exist
  mkdir(Path2saved);
else
end

AnalysisName = 'MVPA_src';


Files = dir(fullfile(Pathsaved,[AnalysisName '*']));
nbF = numel(Files);
if nbF ~= 14
  error(['Number of files is: ' num2str(nbF)]);
end

for i = 1:nbF
  disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
  load(fullfile(Pathsaved,Files(i).name));
  
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      M_tot(i,j,k) = M{j,k};
      
      cdt_ds = M_tot(i,j,k);
      
      % move {train,test}_time from being sample dimensions to feature
      % dimensions, so they can be mapped to a fieldtrip struct & use in cosmo montecarlo cluster stat):
      %----------------------------------------------------------------
      cdt_tf_ds = cosmo_dim_transpose(cdt_ds,{'train_time','test_time'},2);
      
      % store variable for statistic later
      %----------------------------------
      %ds_stat{i,j,k} = cdt_tf_ds;
      
      % % trick fieldtrip into thinking this is a time-freq-chan dataset, by
      % % renaming train_time and test_time to freq and time, respectively
      %--------------------------------------------------------------------
      cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'train_time','freq');
      cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'test_time','time');
      
      % convert to fieldtrip format
      %-----------------------------
      ds_ft{i,j,k} = cosmo_map2meeg(cdt_tf_ds);
      
      % Extract accuracy
      %------------------
      M_t(i,j,k,:,:,:) = ds_ft{i,j,k}.avg.pow;
      
    end
  end
end

% Mean accuracy
%-------------
id_src = find(size(M_t)==889);
Mg = squeeze(mean(squeeze(mean(M_t,id_src)),1)); % all sources + all subjects
Ms = squeeze(mean(M_t,id_src)); % avg all sources
Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
val   = round(ds_ft{1,1,1}.time*1000);
% Save
%--------
disp('*** Saving Files ***');

% % Save STAT
% %-----------
% FileNameSaved = fullfile(Path2saved,['DSstatdimtranspose_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
% save(FileNameSaved,'-v7.3','ds_stat');
% disp('...');
% Save FT-like struct
%---------------------
FileNameSaved = fullfile(Path2saved,['FTstruct_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','ds_ft');
disp('......');

clear ds_ft
% 
% Save single subjects accuracy
%--------------------------------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','M_t','val');
disp('............');

clear M_t

% Save Mean accuracy
%-------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','Msacc','Mg','val');

disp('*** DONE ! ***');

%% Statistics with Cosmo MVPA
%------------------------------
% refer to script : SCRIPT_run_mvpa_with_condor_parcomp.m
% Run  ==> Do_mvpa_stat_t2t
% With ==> condor_MVPA_time2time_srcERF_stat.m

% Plot with ==> SCRIPT_mvpa_t2t_analyse_plotonly

%
%% Plot just accuracy
%---------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' AnalysisName '_N14.mat']);
load(FileNameSaved);

try
  data = [];
  data(1,1,:,:) = Mg;
catch
  data = Mg;
end


if ~exist('val','var')
  load(fullfile(Path2saved,['FTstruct_' AnalysisName '.mat']));
  val   = round(ds_ft{1,1,1}.time*1000);
end

cfg =[];
cfg.xaxis = val;
cfg.smooth = 2;
cfg.LimAx = [0.5 0.53];
%cfg.LimAx = [0 0.2];
plot_3by3_MVPA(data,cfg);

%% Anayse STAT output
%---------------------
AnalysisName = 'tstep30ms_srad4_MVPA_src_t2tERF_N=15';
Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
lab = {'train_time' 'test_time'};
% val   = round(ds_ft{1,1,1}.time*1000);
% ntest = numel(val);
% toi = [250 500];
% 
% % find closest values in val for toi
% [c, id(1)] = min(abs(val-toi(1)));
% [c, id(2)] = min(abs(val-toi(2)));
% twin_id = [id(1):id(2)];

Pathstat = fullfile(Pathsaved,'stat');
sensmod   = {'aud' 'vis' 'tac'};

% Load and threshold
%--------------------
stat_tot = [];

for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    cond_train = i;
    cond_test  = j;
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map) 
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    data_mat = squeeze(data);
    
%     % with z-values
%     %-----------------
%     sig  = data_mat>1.6449; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
    
    % with p-values
    %-----------------
    pdata = normcdf(data_mat);
    sig  = pdata>0.99;% p-values / (alpha 0.01) value > H0mean (H0mean = 0.5)

    mask = squeeze(sum(sig,1))>=1; % map train / test (average sensors)
    mask_p{i,j} = squeeze(max(pdata));
    
    %figure;imagesc(mask_p{i,j});colorbar;caxis([0.99 1]);
    
    sig_roi = squeeze(sum(sig(:,mask),2));
    sig_toi{i,j} = sig_roi>1; % just ROI with more than 2 significant time point
    
%     figure;plot(sig_roi);
%     plot(sig_toi);
    
    % Store var
    %-----------
    stat_mask(i,j,:,:) = mask;
  end
end

% Plot & analysis
%-----

% Analysis similarity
%----------------------
AIDroi = {};
for i = 1:3
  for j = 1:3
    AIDroi{i,j} = find(sig_toi{i,j});
  end
end

CV_audtrain = intersect(AIDroi{1,2},AIDroi{1,3});
CV_vistrain = intersect(AIDroi{2,1},AIDroi{2,3});
CV_tactrain = intersect(AIDroi{3,1},AIDroi{3,2});

CV_visaud = intersect(CV_audtrain,CV_vistrain);


CVroi_all = intersect(intersect(CV_audtrain,CV_vistrain),CV_tactrain);

% Plot 3by3
%-----------
transp = 0.2; % transparency level
stat_mask_t = stat_mask+transp;
stat_mask_t(stat_mask_t>1) = 1;



Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

data = Mg;

Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));

val   = round(ds_ft{1,1,1}.time*1000);

cfg =[];
cfg.xaxis = val;
cfg.smooth = 2;
cfg.mask  = stat_mask_t;
cfg.mask_int = 'spline';
plot_3by3_MVPA(data,cfg);

%% Observation & fieldtrip Grandaverage
%-----------------------------------------
Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));

% take data
%-----
%
data = ds_ft{1,1,1};

% plot voxel brain
%------------------
selroi = CVroi_all;

% load template_mni
% figure, ft_plot_vol(template_vol, 'edgecolor', [0.5 0.5 0.5]);
% alpha 0
% ft_plot_mesh(data.pos(selroi,:),'facecolor','red','vertexcolor',[1 0 0]);

data.pow = data.avg.pow(data.inside,:,:);

data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));
for i = 1:33
  for j = 1:33
    data.pow(selroi,i,j) = 5;
  end
end

data.dimord = 'chan_freq_time';

% data.stat = data.pow;
% cfg = [];
% cfg.param = 'stat';
% cfg.type  = 'orig';
% cfg.tmp   = 'fs';
% %cfg.range = [0 100];
% cfg.lat = 'both';
% cfg.opacity = 'auto';
% %cfg.mask = [];
% [handles] = MegSurf(cfg,data);


% Do some plotting
%-----------------
load standard_mri
load mni_grid_1_5_cm_889pnts.mat

% now virtual sensor to src
cfg = [];
cfg.mri = mri;
cfg.sourcegrid = template_grid;
cfg.parameter = 'pow';
cfg.toilim = [0 0.5];
cfg.foilim = [0 0.5];
source = obob_svs_virtualsens2source(cfg,data);

cfg = [];
cfg.funparameter = 'pow';
cfg.maskparameter = 'pow';
cfg.funcolorlim = 'zeromax';
cfg.method = 'surface';
cfg.method = 'slice';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
ft_sourceplot(cfg, source)
    
cfg = [];
cfg.funparameter = 'pow';
cfg.maskparameter = 'pow';
cfg.atlas = 'ROI_MNI_V4.nii';%'TTatlas+tlrc.BRIK';
cfg.interactive = 'no';
cfg.crosshair = 'yes';%'yes';
cfg.axis = 'off';
cfg.colorbar = 'yes';
cfg.location = 'max'; % I can put also MNI coordinates
cfg.funcolormap = 'hot';
cfg.funcolorlim = [0 5];
ft_sourceplot(cfg, source);
    

% % now plot it
% % 
% cfg=[];%%
% %cfg.method='slice';%%
% cfg.funparameter='pow';
% %cfg.funcolorlim=[0.5 0.53];
% cfg.atlas =  '/Users/gaetan/git/obob_ownft/external/fieldtrip/template/atlas/afni/TTatlas+tlrc.BRIK';
% cfg.interactive='yes';
% cfg.funcolorlim = 'zeromax';
% ft_sourceplot(cfg, source);

% Plot pow
%-----------
data.pow = data.avg.pow(data.inside,:,:);

pow_group = squeeze(mean(M_t,1));
pow = squeeze(pow_group(2,2,:,:,:));
data.pow = pow;


%%%
%




% 






