
% Files with serachlight decoding
%--------------------------------
AnalysisName = 'MVPA_src_N14';

toi = [0 0.5]; % in sec
%toi = [0.35 0.5]; % in sec

toi_mask = stat_mask_nosl; % store previous stat to get significant brain

%%%%%% Files with serachlight decoding
%--------------------------------
Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
Pathstat  = fullfile(Pathoutput_sl,'stat'); % saved stat

lab = {'train_time' 'test_time'};
%sensmod   = {'aud' 'vis' 'tac'};
%sensmod   = {'all'};


flag = 0;
% Load and threshold
%--------------------
stat_mask = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_roi   = {};
sig_toi   = {};
sig_roi_diag   = {};
sig_roi_diag_t = {};
mask_int       = 0;
mask_brain_int = 0;
Allbound = {};
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    timepnts = round(values{2},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(:,startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
        % sig  = data_mat>1.65; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>1.9600; % z-values / (alpha 0.01) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>2.54;% z-values / (alpha 0.05 + Bonferronir correction on 9 decoding)
        %sig  = abs(pdata)>2.58;% z-values / (alpha 0.005) value > H0mean (H0mean = 0.5)
        sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
        
%         % Find 10% of significant Z-values
%         %----------------------------------
%         opt = []; opt.lim = 10; opt.flag = flag;
%         x = pdata(abs(pdata)>3.1);% z-values / (alpha 0.001)
%         [bound] = find_distrib_percent(x,opt);
%         
%         Allbound{i,j} = bound;
%         
%         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        %th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        th = 1 - 0.005; % alpha 0.005 new threshold approved by the comunity
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points from previous time2time no
    % searchlight decoding
    %-----------------------
   
    mask = logical(squeeze(toi_mask(i,j,:,:))); % map train / test
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j} = squeeze(max(pdata));
    mask_bin{i,j} = mask;
    
    if flag
        figure;
        subplot(1,3,1);
        imagesc(mask_p{i,j});colorbar;caxis([0.9 1]);
        subplot(1,3,2);
        imagesc(mask);colorbar;
        subplot(1,3,3);
        imagesc(squeeze(sum(sig,1)));colorbar;
    end
    
    % Extract spatial mask according to significant time points
    %-----------------------------------------------------------
    Number2sig_time = 1;
    sig_roi{i,j} = squeeze(sum(sig(:,mask),2));
    sig_toi{i,j} = sig_roi{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    if i ~= j
      mask_brain_int = sig_toi{i,j}+mask_brain_int;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_roi_diag{i,j} = squeeze(sum(sig(:,logical(eye(size(data_mat,2),size(data_mat,3)))),2));
    sig_toi_diag{i,j} = sig_roi_diag{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    % Extract diagonal spatial feature (movie)
    %------------------------------------------
    for t = 1:size(sig,3)
      sig_roi_diag_t{i,j,t} = logical(squeeze(sig(:,t,t))); % take spatial mask for each diagonal time
    end
  
    % Store temporal mask
    %----------------------
    stat_mask(i,j,:,:) = mask;
    
  end
end

if flag
  figure;
  surf(mask_int);colorbar;
end

% Analysis intersect
%----------------------
[CVroi_all,IUroi_all,AIDroi_intensity,AIDroi] = Do_t2t_metaNTdec_statmask_sl_intersect(sig_toi,sig_toi_diag,sig_roi);

% Average Brain Accuracy for only significant time point
%---------------------------------------------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

if numel(sensmod) == 1 % if just one modality 
  Msacc2process(1,1,:,:,:) = Msacc;
else
  Msacc2process = Msacc;
end

Msacc_small = Msacc2process(:,:,:,startpoint:stoppoint,startpoint:stoppoint);% remove prestim data

Msig_acc = [];
Maxsig_acc = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    % average Acc for significant Time point
    %----------------------------------------
    Msig_acc(i,j,:) = mean(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),2);
    
    % Max Acc for significant Time point
    %----------------------------------------
    Max4vox = max(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),[],2);
    if isempty(Max4vox)
      Max4vox = nan(1,size(Msacc_small,3));
    end
    Maxsig_acc(i,j,:) = Max4vox;
    
    % Normalize between 0 and 1
    %--------------------------
    % [Msig_acc(i,j,:)] = Do_normalize(Msig_acc(i,j,:),3);
  end
end

if numel(sensmod) == 3
  Msig_acc_Between = mean([squeeze(Msig_acc(1,2,:)) squeeze(Msig_acc(1,3,:)) ...
    squeeze(Msig_acc(2,1,:)) squeeze(Msig_acc(2,3,:)) ...
    squeeze(Msig_acc(3,1,:)) squeeze(Msig_acc(3,2,:))],2);
  
  Maxsig_acc_Between = max([squeeze(Maxsig_acc(1,2,:)) squeeze(Maxsig_acc(1,3,:)) ...
    squeeze(Maxsig_acc(2,1,:)) squeeze(Maxsig_acc(2,3,:)) ...
    squeeze(Maxsig_acc(3,1,:)) squeeze(Maxsig_acc(3,2,:))],[],2);
end

% Mask of intersect 10% accuracy
%----------------------------------
opt = []; opt.lim = 10; opt.flag = 0;
mask10  = zeros(numel(sensmod),numel(sensmod),889);

for strain = 1:numel(sensmod)
  for stest = 1:numel(sensmod)
      x = squeeze(Msig_acc(strain,stest,:));
      [bound] = find_distrib_percent(x,opt);
      mask10(strain,stest,x>=bound(2)) = 1;% mask > 10% acc
  end
end

if numel(sensmod) == 3
  mask10_Uaudvistrain = logical(sum([squeeze(mask10(1,2,:)) squeeze(mask10(2,1,:))],2)>=1);
  mask10_Uaudtactrain = logical(sum([squeeze(mask10(1,3,:)) squeeze(mask10(3,1,:))],2)>=1);
  mask10_Utacvistrain = logical(sum([squeeze(mask10(2,3,:)) squeeze(mask10(3,2,:))],2)>=1);
  
  mask10_IU = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)==3);
  mask10_U  = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)>=1);
end

% Find average latency for each brain nodes of Intersect(Union)
%----------------------------------------------------------------
val = timepnts(startpoint:stoppoint);

Nnodes   = 889;
Nsensmod = numel(sensmod);

Mlatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);
Slatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);

for k = 1:Nnodes
  for i = 1:Nsensmod
    for j = 1:Nsensmod
      latsig = [];
      latsig = val(find(sum(squeeze(dat_sig{i,j}(k,:,:)),2))); % find only significant latency
      Mlatsig_Nodes(k,i,j) = mean(latsig);
      Slatsig_Nodes(k,i,j) = std(latsig)./sqrt(length(latsig));
    end
  end
end

if numel(sensmod) == 3
  Dat4Between = [Mlatsig_Nodes(IUroi_all,logical(~eye(3,3)))];
  Mlatsig_BETW_IUroi_all = nanmean(Dat4Between,2);
  Slatsig_BETW_IUroi_all = nanstd(Dat4Between,[],2)./sqrt(sum(~isnan(Dat4Between),2));
  
  Mlatsig_BETW_CVroi_all = nanmean([Mlatsig_Nodes(CVroi_all,logical(~eye(3,3)))],2);
  
  Mlatsig_AUD_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,1,1)],2);
  Mlatsig_VIS_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,2,2)],2);
  Mlatsig_TAC_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,3,3)],2);
  
end

% Max Zvalue compilation
%------------------------
Mzscore = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    Mzscore(i,j,:,:) = mask_p{i,j};
  end
end

disp('*************');
disp('DONE !!!');