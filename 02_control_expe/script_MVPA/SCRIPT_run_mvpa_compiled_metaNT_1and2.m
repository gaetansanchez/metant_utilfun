%% Script to run MVPA with high decoding accuracy
%------------------------------------------------
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Time2Time decoding at source level (SRC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Run these function with the same parameter for :
% metaNT and metaNT_v2

%% 1 ==> Prepare SRC (sort trials + epoched + downsample + source localization...)
%---------------------
condor_srcERF4MVPA

%% 2 ==> Do real MVPA computation with parallelisation (9 jobs per subjects)
%----------------------------------------------------------
condor_MVPA_time2time_nosl_parcomp

%% or brain searchlight (around 100 jobs per subjects)
%--------------------------------------------------------
condor_MVPA_time2time_srcERF_parcomp

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Organize files from both experiments (filename/path) ==> No brain searchlight
%------------------------------
AnalysisName = ['MVPA_t2tSRC'];
do_group_average = 0;

% no brain searchlight
%----------------------
PathfilemetaNT = ['/mnt/obob/staff/gsanchez/DATA_processing/metaNT/mvpa/t2t_nosl_SRC'...
  '/cross_acc_zscoreNorm_trad1'...
  '/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_toilim_-200to600_tstep10ms_chan_SRC_Bayes'];

PathfilemetaNT_v2 = ['/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_nosl_SRC'...
  '/cross_allsens_hitvsmiss_acc_zscoreNorm_trad1'...
  '/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_respcontrol_toilim_-200to600_tstep10ms_OBOB_BeamCFiltsensmod_chan_SRC_Bayes'];

Pathoutput= '/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_nosl_SRC/metaNTv1v2';

Path2saved = fullfile(Pathoutput,'MeanDSFT');
Pathstat   = fullfile(Pathoutput,'stat'); % saved stat
Pathfig    = fullfile(Pathoutput,'figures'); % saved figs


if ~exist(Pathoutput,'dir') % create output dir if dont exist
  mkdir(Pathoutput);
else
end

%%%%%%%%%%%%%%%%%%%%%
% Transfert files
%------------------



FilesmetaNT = dir(fullfile(PathfilemetaNT,sprintf('%s_*',AnalysisName)));
nbF = numel(FilesmetaNT);
for i = 1:nbF
  SOURCE_file = fullfile(FilesmetaNT(i).folder,FilesmetaNT(i).name);
  DESTINATION_file = fullfile(Pathoutput,['metaNT_' FilesmetaNT(i).name]);
  disp(['---> Process metaNT ' sprintf('File %02d/%02d ...',i,nbF)]);
  if exist(DESTINATION_file,'file')
     disp('NO COPY because File already exists in this folder!')
  else
    copyfile(SOURCE_file,DESTINATION_file)
    do_group_average = 1;
  end
end

FilesmetaNTv2 = dir(fullfile(PathfilemetaNT_v2,sprintf('%s_*',AnalysisName)));
nbF = numel(FilesmetaNTv2);
for i = 1:nbF
  SOURCE_file = fullfile(FilesmetaNTv2(i).folder,FilesmetaNTv2(i).name);
  DESTINATION_file = fullfile(Pathoutput,['metaNTv2_' FilesmetaNTv2(i).name]);
  disp(['---> Process metaNTv2 ' sprintf('File %02d/%02d ...',i,nbF)]);
  if exist(DESTINATION_file,'file')
     disp('NO COPY because File already exists in this folder!')
  else
    copyfile(SOURCE_file,DESTINATION_file)
    do_group_average = 1;
  end
end

if do_group_average
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %--------------------------------
  % Compute average Acc over group
  %--------------------------------
  
  % inspired from : SCRIPT_mvpa_t2t_analyse.m (average Acc over subjects)
  
  if ~exist(Path2saved,'dir') % create output dir if dont exist
    mkdir(Path2saved);
  else
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Files = dir(fullfile(Pathoutput,sprintf('%s*','metaNT')));
  nbF = numel(Files);
  
  M_t = [];
  for i = 1:nbF
    disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
    load(fullfile(Pathoutput,Files(i).name));
    
    for j = 1:size(M,1)
      for k = 1:size(M,2)
        
        M_tot(i,j,k) = M{j,k};
        
        cdt_ds = M_tot(i,j,k);
        
        % convert to fieldtrip format
        %-----------------------------
        
        [data, labels, values] = cosmo_unflatten(cdt_ds,1);
        
        % Extract accuracy
        %------------------
        M_t(i,j,k,:,:) = data;
        
      end
    end
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % Mean accuracy
  %---------------
  Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
  val   = round(values{1}*1000);
  
  % Save
  %--------
  disp('*** Saving Files ***');
  
  % Save single subjects accuracy
  %--------------------------------
  FileNameSaved = fullfile(Path2saved,['SubjectsAcc_' sprintf('N%02d',nbF) '.mat']);
  save(FileNameSaved,'-v7.3','M_t','val');
  disp('............');
  
  % Save Mean accuracy
  %-------------------
  FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' sprintf('N%02d',nbF) '.mat']);
  save(FileNameSaved,'-v7.3','Msacc','val');
  
end

disp('*** DONE ! ***');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Organize files from both experiments (filename/path) ==> Brain searchlight
%------------------------------
AnalysisName_sl = 'MVPA_src';
toilim = [-200 600];
trad = 1;
do_group_average = 0;

% brain searchlight
%----------------------
PathfilemetaNT = ['/mnt/obob/staff/gsanchez/DATA_processing/metaNT/mvpa/t2t_srcERF'...
  '/cross_acc_zscoreNorm_trad' num2str(trad)...
  '/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_toilim_' num2str(toilim(1)) 'to' num2str(toilim(end)) '_tstep10ms_OBOB_BeamCFiltsensmod_srad30_Bayes'];

PathfilemetaNT_v2 = ['/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_srcERF'...
  '/cross_allsens_hitvsmiss_acc_zscoreNorm_trad' num2str(trad) ...
  '/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_respcontrol_toilim_' num2str(toilim(1)) 'to' num2str(toilim(end)) '_tstep10ms_OBOB_BeamCFiltsensmod_srad30_Bayes'];


Pathoutput_sl= ['/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_srcERF/metaNTv1v2_toilim_' num2str(toilim(1)) 'to' num2str(toilim(end)) '_trad' num2str(trad)];

Path2saved_sl = fullfile(Pathoutput_sl,'MeanDSFT');
Pathstat_sl   = fullfile(Pathoutput_sl,'stat'); % saved stat
Pathfig_sl    = fullfile(Pathoutput_sl,'figures'); % saved figs


if ~exist(Pathoutput_sl,'dir') % create output dir if dont exist
  mkdir(Pathoutput_sl);
else
end

%%%%%%%%%%%%%%%%%%%%%
% Transfert files
%------------------


FilesmetaNT = dir(fullfile(PathfilemetaNT,sprintf('%s_*',AnalysisName_sl)));
nbF = numel(FilesmetaNT);
for i = 1:nbF
  SOURCE_file = fullfile(FilesmetaNT(i).folder,FilesmetaNT(i).name);
  DESTINATION_file = fullfile(Pathoutput_sl,['metaNT_' FilesmetaNT(i).name]);
  disp(['---> Process metaNT ' sprintf('File %02d/%02d ...',i,nbF)]);
  if exist(DESTINATION_file,'file')
    disp('NO COPY because File already exists in this folder!')
  else
    copyfile(SOURCE_file,DESTINATION_file)
    do_group_average = 1;
  end
end

FilesmetaNTv2 = dir(fullfile(PathfilemetaNT_v2,sprintf('%s_*',AnalysisName_sl)));
nbF = numel(FilesmetaNTv2);
for i = 1:nbF
  SOURCE_file = fullfile(FilesmetaNTv2(i).folder,FilesmetaNTv2(i).name);
  DESTINATION_file = fullfile(Pathoutput_sl,['metaNTv2_' FilesmetaNTv2(i).name]);
  disp(['---> Process metaNTv2 ' sprintf('File %02d/%02d ...',i,nbF)]);
  if exist(DESTINATION_file,'file')
    disp('NO COPY because File already exists in this folder!')
  else
    copyfile(SOURCE_file,DESTINATION_file)
    do_group_average = 1;
  end
end

if do_group_average
  % Inspired from ==> Run first part of SCRIPT_mvpa_t2t_analyse.m to import into fieldtrip friendly format
  %-----------------------------------------------------
  % SCRIPT_mvpa_t2t_analyse.m
  
  if ~exist(Path2saved_sl,'dir') % create output dir if dont exist
    mkdir(Path2saved_sl);
  else
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Files = dir(fullfile(Pathoutput_sl,sprintf('%s*','metaNT')));
  nbF = numel(Files);
  
  for i = 1:nbF
    disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
    load(fullfile(Pathoutput_sl,Files(i).name));
    
    for j = 1:size(M,1)
      for k = 1:size(M,2)
        
        M_tot(i,j,k) = M{j,k};
        
        cdt_ds = M_tot(i,j,k);
        
        % move {train,test}_time from being sample dimensions to feature
        % dimensions, so they can be mapped to a fieldtrip struct & use in cosmo montecarlo cluster stat):
        %----------------------------------------------------------------
        cdt_tf_ds = cosmo_dim_transpose(cdt_ds,{'train_time','test_time'},2);
        
        % store variable for statistic later
        %----------------------------------
        %ds_stat{i,j,k} = cdt_tf_ds;
        
        % % trick fieldtrip into thinking this is a time-freq-chan dataset, by
        % % renaming train_time and test_time to freq and time, respectively
        %--------------------------------------------------------------------
        cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'train_time','freq');
        cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'test_time','time');
        
        % convert to fieldtrip format
        %-----------------------------
        ds_ft{i,j,k} = cosmo_map2meeg(cdt_tf_ds);
        
        % Extract accuracy
        %------------------
        M_t(i,j,k,:,:,:) = ds_ft{i,j,k}.avg.pow;
        
      end
    end
  end
  
  % Mean accuracy
  %-------------
  id_src = find(size(M_t)==889);
  Mg = squeeze(mean(squeeze(mean(M_t,id_src)),1)); % all sources + all subjects
  Ms = squeeze(mean(M_t,id_src)); % avg all sources
  Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
  val   = round(ds_ft{1,1,1}.time*1000);
  % Save
  %--------
  disp('*** Saving Files ***');

  % Save FT-like struct
  %---------------------
  FileNameSaved = fullfile(Path2saved_sl,['FTstruct_metaNT_' sprintf('N%02d',nbF) '.mat']);
  save(FileNameSaved,'-v7.3','ds_ft');
  disp('......');
  
  clear ds_ft
  
  % Save single subjects accuracy
  %--------------------------------
  FileNameSaved = fullfile(Path2saved_sl,['SubjectsAcc_metaNT_' sprintf('N%02d',nbF) '.mat']);
  save(FileNameSaved,'-v7.3','M_t','val');
  disp('............');
  
  clear M_t
  
  % Save Mean accuracy
  %-------------------
  FileNameSaved = fullfile(Path2saved_sl,['MeanGroupAcc_metaNT_' sprintf('N%02d',nbF) '.mat']);
  save(FileNameSaved,'-v7.3','Msacc','Mg','val');

end
  
disp('*** DONE ! ***');

%% 4 ==> Statistics ==> no searchlight
%--------------------
Pathlogs = fullfile(config.path,['jobs/allexpe_mvpa_t2t_nosl_stat/' date]);
condor_allexpe_MVPA_time2time_stat(Pathlogs,Pathoutput)

%% 4 ==> Statistics ==> brain searchlight
%--------------------
Pathlogs = fullfile(config.path,['jobs/allexpe_mvpa_t2t_stat/' date]);
condor_allexpe_MVPA_time2time_stat(Pathlogs,Pathoutput_sl)


%% Statistics compilation
%---------------------------
sensmod = {'all'};
AnalysisName = 'metaNT_N30';

toi = [0 0.5]; % in sec
%toi = [0 0.25]; % in sec
%toi = [0.25 0.35]; % in sec
%toi = [0.35 0.5]; % in sec
%%%%%

flag = 0; % 1 = doplot stat maps

% Load and threshold
%--------------------
stat_mask_nosl = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_diag   = {};
mask_int   = 0;
data_mat = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    
    timepnts = round(values{1},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
       %sig  = abs(pdata)>2.54;% z-values / (alpha 0.05 + Bonferronir correction on 9 decoding)
       % sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
       sig  = abs(pdata)>2.58;% z-values / (alpha 0.005) value > H0mean (H0mean = 0.5)
        
        %         % Find 10% of significant Z-values
        %         %----------------------------------
        %         opt = []; opt.lim = 10; opt.flag = flag;
        %         x = pdata(abs(pdata)>1.6449);
        %         [bound] = find_distrib_percent(x,opt);
        %         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        %th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        th = 1 - 0.005; % alpha 0.005 new threshold approved by the comunity
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points
    %----------------------------------
    mask = sig; % map train / test (average sensors)
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j}   = pdata;
    mask_bin{i,j} = mask;
    
    if flag
      figure;
      subplot(1,3,1);
      imagesc(mask_p{i,j});colorbar;caxis([0.6 1]);
      subplot(1,3,2);
      imagesc(mask);colorbar;
      subplot(1,3,3);
      imagesc(sig);colorbar;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_diag{i,j} = sig(logical(eye(size(data_mat,1),size(data_mat,2))));
    
    % Store temporal mask
    %----------------------
    stat_mask_nosl(i,j,:,:) = mask;
  end
end

if flag
  figure;
  surf(mask_int);colorbar;
end

disp('*************');
disp('DONE !!!');
%------------------------------
%% Plot nice figure with stat
%------------------------------

dosave = 1;

% LOad data and cut accordingly
%------------------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_N30.mat']);
load(FileNameSaved);


data = [];
if numel(size(Msacc))==2
  data(1,1,:,:) = Msacc(startpoint:stoppoint,startpoint:stoppoint);
  val2plot  = val(startpoint:stoppoint)+1;
else
  data = Msacc(:,:,startpoint:stoppoint,startpoint:stoppoint);
  val2plot  = val(startpoint:stoppoint);
end

% transparency level
%--------------------
transp = 0.25;
%transp = 1;
stat_mask_t = stat_mask_nosl+transp;
stat_mask_t(stat_mask_t>1) = 1;

%----------------
% Do the plotting
%----------------
smooth = 2;
LimAx  = [0.4 0.6];

cfg =[];
cfg.xaxis  = val2plot;
cfg.xytick = find(ismember(val2plot',[0:100:500]));
cfg.smooth = smooth;
cfg.mask   = stat_mask_t;
cfg.LimAx  = LimAx;
cfg.mask_int = 'linear';
[h] = plot_3by3_MVPA(data,cfg);
colormap('jet');
pause(1);
%set(h, 'Position', [0, 0, 1400, 1000]);


if strcmp(computer,'MACI64')
  fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT','figures',['MVPA_t2t_3vs3_MaskStat_' AnalysisName]);
else
  fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_MaskStat_' AnalysisName]);
end

if dosave
  
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
end


if size(data,1) == 3 % if there are 3 sensory modalities
  %%%%%%%%%%%%%%
  % Do plot only within sensory modality
  %%%%%%%%%%%%%%
  for i = 1:size(data,1)
    
    data2p(1,1,:,:) = data(i,i,:,:);
    cfg =[];
    cfg.xaxis  = val2plot;
    cfg.xytick = find(ismember(val2plot',[0:100:500]));
    cfg.smooth = smooth;
    cfg.mask   = stat_mask_t(i,i,:,:);
    cfg.LimAx  = LimAx;
    cfg.mask_int = 'linear';
    [h] = plot_3by3_MVPA(data2p,cfg);
    colormap('jet');
    title(sensmod{i});
    pause(1);
    %set(h, 'Position', [0, 0, 1200, 1600]);
    
    
    dosave = 1;
    if strcmp(computer,'MACI64')
      fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT','figures',['MVPA_t2t_3vs3_MaskStat_' AnalysisName '_' sensmod{i}]);
    else
      fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_MaskStat_' AnalysisName '_' sensmod{i}]);
    end
    
    if dosave
      
      % save png for record
      %----------------------
      name2 = [fname01 '.png'];
      img = getframe(h);
      imwrite(img.cdata,name2);
    end
    
  end
end


%% Do plot generalization
%--------------------------
close all;

dosave = 1;
factms    = 10; % in ms
yborders  = [0 500]; % in ms
xborders  = [0 500];% in ms
%acclimits = [0.5 0.55];
acclimits = [0 1]; % normalized accuracies in the plot
Do_t2t_metaNTdec_statmask_plot_generalization(data,val2plot,stat_mask_nosl,acclimits,xborders,yborders,factms)

h = figure(3);
set(h, 'Position', [0, 0, 800, 800]);


if strcmp(computer,'MACI64')
  fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT','figures',['MVPA_t2t_generalization_' AnalysisName]);
else
  fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_generalization_' AnalysisName]);
end

if dosave
  
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
  
end


if size(data,1) == 3 % if there are 3 sensory modalities
  close all;
  %%%%%%%%%%%%%%%%%%%
  % Do plot only within sensory modality
  %%%%%%%%%%%%%%%%%%%
  for i = 1:size(data,1)
    data2p(1,1,:,:) = data(i,i,:,:);
    stat_mask_nosl2p(1,1,:,:) = stat_mask_nosl(i,i,:,:);
    Do_t2t_metaNTdec_statmask_plot_generalization(data2p,val2plot,stat_mask_nosl2p,acclimits,xborders,yborders,factms)
    
    h = figure(3);
    set(h, 'Position', [0, 0, 800, 800]);
    
    dosave = 1;
    if strcmp(computer,'MACI64')
      fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT','figures',['MVPA_t2t_generalization_' AnalysisName '_' sensmod{i}]);
    else
      fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_generalization_' AnalysisName '_' sensmod{i}]);
    end
    
    if dosave
      
      % save png for record
      %----------------------
      name2 = [fname01 '.png'];
      img = getframe(h);
      imwrite(img.cdata,name2);
      close all;
    end
    
  end
  
end



%% Find searchlight brain regions inside significant time point of no searchlight decoding
%------------------------------------------------------------------------------------------

% Files with serachlight decoding
%--------------------------------
AnalysisName = 'metaNT_N30';

%toi = [0 0.5]; % in sec
%toi = [0.2 0.3]; % in sec
%toi = [0.3 0.5]; % in sec

toi_mask = stat_mask_nosl; % store previous stat to get significant brain

%%%%%% Files with serachlight decoding
%--------------------------------
Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
Pathstat  = fullfile(Pathoutput_sl,'stat'); % saved stat

lab = {'train_time' 'test_time'};
%sensmod   = {'aud' 'vis' 'tac'};
sensmod   = {'all'};


flag = 0;
% Load and threshold
%--------------------
stat_mask = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_roi   = {};
sig_toi   = {};
sig_roi_diag   = {};
sig_roi_diag_t = {};
mask_int       = 0;
mask_brain_int = 0;
Allbound = {};
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    timepnts = round(values{2},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(:,startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
        % sig  = data_mat>1.65; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>1.9600; % z-values / (alpha 0.01) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>2.54;% z-values / (alpha 0.05 + Bonferronir correction on 9 decoding)
        sig  = abs(pdata)>2.58;% z-values / (alpha 0.005) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>3.7;% z-values / (alpha 0.0001) value > H0mean (H0mean = 0.5)
        
%         % Find 10% of significant Z-values
%         %----------------------------------
%         opt = []; opt.lim = 1; opt.flag = flag;
%         %x = pdata(abs(pdata)>3.1);% z-values / (alpha 0.001)
%         x = abs(pdata(:));% all data
%         [bound] = find_distrib_percent(x,opt);
%         Allbound{i,j} = bound;
%         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        %th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        th = 1 - 0.005; % alpha 0.005 new threshold approved by the comunity
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points from previous time2time no
    % searchlight decoding
    %-----------------------
   
    mask = logical(squeeze(toi_mask(i,j,:,:))); % map train / test
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j} = squeeze(max(pdata));
    mask_bin{i,j} = mask;
    
    if flag
        figure;
        subplot(1,3,1);
        imagesc(mask_p{i,j});colorbar;caxis([0.9 1]);
        subplot(1,3,2);
        imagesc(mask);colorbar;
        subplot(1,3,3);
        imagesc(squeeze(sum(sig,1)));colorbar;
    end
    
    % Extract spatial mask according to significant time points
    %-----------------------------------------------------------
    Number2sig_time = 1;
    sig_roi{i,j} = squeeze(sum(sig(:,mask),2));
    sig_toi{i,j} = sig_roi{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    if i ~= j
      mask_brain_int = sig_toi{i,j}+mask_brain_int;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_roi_diag{i,j} = squeeze(sum(sig(:,logical(eye(size(data_mat,2),size(data_mat,3)))),2));
    sig_toi_diag{i,j} = sig_roi_diag{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    % Extract diagonal spatial feature (movie)
    %------------------------------------------
    for t = 1:size(sig,3)
      sig_roi_diag_t{i,j,t} = logical(squeeze(sig(:,t,t))); % take spatial mask for each diagonal time
    end
  
    % Store temporal mask
    %----------------------
    stat_mask(i,j,:,:) = mask;
    
  end
end

if flag
  figure;
  surf(mask_int);colorbar;
end

% Analysis intersect
%----------------------
[CVroi_all,IUroi_all,AIDroi_intensity,AIDroi] = Do_t2t_metaNTdec_statmask_sl_intersect(sig_toi,sig_toi_diag,sig_roi);

disp(['Number of significant ROI = ' num2str(numel(CVroi_all)) '/' num2str(numel(sig_toi{1}))])

% Average Brain Accuracy for only significant time point
%---------------------------------------------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

if numel(sensmod) == 1 % if just one modality 
  Msacc2process(1,1,:,:,:) = Msacc;
else
  Msacc2process = Msacc;
end

Msacc_small = Msacc2process(:,:,:,startpoint:stoppoint,startpoint:stoppoint);% remove prestim data

Msig_acc = [];
Maxsig_acc = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    % average Acc for significant Time point
    %----------------------------------------
    Msig_acc(i,j,:) = mean(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),2);
    
    
    % Max Acc for significant Time point
    %----------------------------------------
    Max4vox = max(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),[],2);
    if isempty(Max4vox) % if not significant time point ==> set to NaN
      Max4vox = nan(1,size(Msacc_small,3));
    end
    Maxsig_acc(i,j,:) = Max4vox;
    
    % Normalize between 0 and 1
    %--------------------------
    % [Msig_acc(i,j,:)] = Do_normalize(Msig_acc(i,j,:),3);
  end
end

if numel(sensmod) == 3
  Msig_acc_Between = mean([squeeze(Msig_acc(1,2,:)) squeeze(Msig_acc(1,3,:)) ...
    squeeze(Msig_acc(2,1,:)) squeeze(Msig_acc(2,3,:)) ...
    squeeze(Msig_acc(3,1,:)) squeeze(Msig_acc(3,2,:))],2);
  
  Maxsig_acc_Between = max([squeeze(Maxsig_acc(1,2,:)) squeeze(Maxsig_acc(1,3,:)) ...
    squeeze(Maxsig_acc(2,1,:)) squeeze(Maxsig_acc(2,3,:)) ...
    squeeze(Maxsig_acc(3,1,:)) squeeze(Maxsig_acc(3,2,:))],[],2);
end

% Mask of intersect 10% accuracy
%----------------------------------
opt = []; opt.lim = 10; opt.flag = 0;
mask10  = zeros(numel(sensmod),numel(sensmod),889);

for strain = 1:numel(sensmod)
  for stest = 1:numel(sensmod)
      x = squeeze(Msig_acc(strain,stest,:));
      [bound] = find_distrib_percent(x,opt);
      mask10(strain,stest,x>=bound(2)) = 1;% mask > 10% acc
  end
end

if numel(sensmod) == 3
  mask10_Uaudvistrain = logical(sum([squeeze(mask10(1,2,:)) squeeze(mask10(2,1,:))],2)>=1);
  mask10_Uaudtactrain = logical(sum([squeeze(mask10(1,3,:)) squeeze(mask10(3,1,:))],2)>=1);
  mask10_Utacvistrain = logical(sum([squeeze(mask10(2,3,:)) squeeze(mask10(3,2,:))],2)>=1);
  
  mask10_IU = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)==3);
  mask10_U  = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)>=1);
end

% Find average latency for each brain nodes of Intersect(Union)
%----------------------------------------------------------------
val = timepnts(startpoint:stoppoint);

Nnodes   = max(size(data)); % max size dimension is usually the number of sources
Nsensmod = numel(sensmod);

Mlatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);
Slatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);

for k = 1:Nnodes
  for i = 1:Nsensmod
    for j = 1:Nsensmod
      latsig = [];
      latsig = val(find(sum(squeeze(dat_sig{i,j}(k,:,:)),2))); % find only significant latency
      Mlatsig_Nodes(k,i,j) = mean(latsig);
      Slatsig_Nodes(k,i,j) = std(latsig)./sqrt(length(latsig));
    end
  end
end

if numel(sensmod) == 3
  Dat4Between = [Mlatsig_Nodes(IUroi_all,logical(~eye(3,3)))];
  Mlatsig_BETW_IUroi_all = nanmean(Dat4Between,2);
  Slatsig_BETW_IUroi_all = nanstd(Dat4Between,[],2)./sqrt(sum(~isnan(Dat4Between),2));
  
  Mlatsig_BETW_CVroi_all = nanmean([Mlatsig_Nodes(CVroi_all,logical(~eye(3,3)))],2);
  
  Mlatsig_AUD_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,1,1)],2);
  Mlatsig_VIS_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,2,2)],2);
  Mlatsig_TAC_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,3,3)],2);
  
end

% Max Zvalue compilation
%------------------------
Mzscore = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    Mzscore(i,j,:,:) = mask_p{i,j};
  end
end

disp('*************');
disp('DONE !!!');



%% plot voxel CARET brain : Only intersect Between Modalities
%------------------------------------------------------
close all;

if ~exist('ds_ft','var')
    Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
    load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end


% acc10 options:
%---------------
% 1 = only 10% acc from mask intersect significant
% 2 = make mask from 10% acc between sensmod decoding
% 0 = plot all acc from mask intersect significant
acc10 = 1;
dosavefile = 1;
dosurface  = 0;
toi = [0 0.5]; % in sec (time period to average to plot)
val2plot = [0.5 0.55];
%val2plot = [0 1]; % normalize accuracies


if acc10 == 1
  prefixfilename = '10percent_';
else
  prefixfilename = [];
end

if strcmp(computer,'MACI64')
  fname = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',[prefixfilename 'Brain_Acc_MetaNTv1v2_' AnalysisName]);
else
  %fname = fullfile(config.path,'doc/mvpa',[prefixfilename 'Brain_Acc_MetaNTv1v2_' AnalysisName]);
  fname = fullfile(config.path,'doc/mvpa',[prefixfilename 'Brain_Acc_overlapNetworks_MetaNTv1v2_' AnalysisName]);
end

if numel(size(ds_ft)) == 3
    data = ds_ft{1,1,1};
else
    data = ds_ft{1,1};
end

%%% Masking
%-------------
selroi = CVroi_all; % only spatial features between modalities (intersect)
%selroi = IUroi_all; % only spatial features between modalities (intersect)
mask = zeros(1,889);
mask(selroi) = 1;


%%% Data organization
%----------------------
data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

if numel(sensmod) == 3
  Msig_acc2plot = Msig_acc_Between;
  Maxsig_acc2plot = Maxsig_acc_Between;
  Mlatsig_vox2plot = Mlatsig_BETW_IUroi_all*1000; % Mean Latency for these voxels (in ms)
elseif numel(sensmod) == 1
  Msig_acc2plot = squeeze(Msig_acc);
  Maxsig_acc2plot = squeeze(Maxsig_acc);
  Mlatsig_vox2plot = Mlatsig_Nodes(selroi)*1000;% Mean Latency for these voxels (in ms)
end


if acc10 == 1
    % Find 10% of significant accuracy
    %----------------------------------
    opt = []; opt.lim = 10; opt.flag = 0;
    x = Msig_acc2plot;
    [bound] = find_distrib_percent(x,opt);
    mask  = zeros(1,889);
    mask(Msig_acc2plot>=bound(2)) = 1;% mask > 10% acc
    
    selroi = 1:889; % No selection of intersection mask
    val2plot = [bound(2) max(x)];
    %val2plot = [0.513 max(x)];
    
elseif acc10 == 2
    % Mask of intersect of union 10% accuracy
    %----------------------------------
    mask  = zeros(1,889);
    mask  = mask10_IU';
    
    selroi = 1:889; % No selection of intersection mask
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
    
    
else
    %val2plot = [0 max(Msig_acc_Between(selroi))];
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
end

if val2plot(1)==0 && val2plot(2)==1
  % Normalize accuracy between 0 and 1 with y = (x - min(x))/(max(x)-min(x))
  %%%%%%%%%%%%%%  
  [x] = Do_normalize(x,1);
end

val2plotstore = val2plot;

% Fill-in with accuracy values found
for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
        data.pow(selroi,i,j) = x(selroi)-val2plot(1);
        data.mask(:,i,j) = mask;
        data.maskInt(:,i,j) = mask;
        data.maskInt(selroi,i,j) = AIDroi_intensity(selroi);
    end
end

val2plot = val2plot-val2plot(1);

mask2plot = {'mask'};
%mask2plot = {'maskInt'};
param2plot = {'pow'};

% now virtual sensor to src
%---------------------------
colormaplabel = 'autumn';
Tab = [];
[Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile,toi);
Tab(:,5) = num2cell(cell2mat(Tab(:,5))+val2plotstore(1))

val2plotstore

