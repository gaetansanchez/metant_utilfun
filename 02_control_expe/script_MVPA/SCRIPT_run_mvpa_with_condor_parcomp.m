%% SUMMARY Script to run all condor parallel computing 
% in order to get metaNT MVPA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% 1 ==> Do source and transform to cosmoMVPA format 
%-----------------------------------------------------

condor_srcERF4MVPA

%% 2 ==> Do real MVPA computation with BIG parallelisation (about 100 jobs per subjects)
%----------------------------------------------------------

condor_MVPA_time2time_srcERF_parcomp

%% 3 ==> Get results and stack data per subjects
%------------------------------------------------
Pathdata = config.path;

[Suj] = Do_sel_subjects(config,'metaNT_v2');

sensmod = {'all'}; % {'aud' 'vis' 'tac'} or {'all'} -> just decode hit vs miss across all sensmod
%sensmod = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'} -> just decode hit vs miss across all sensmod
paradigm = 'hit_vs_miss_respcontrol';% 'hit_vs_miss' or 'supravs_sham' or []
lcmvtype = 'OBOB_BeamCFiltsensmod_'; % 'parcelOBOB_BeamCFiltsensmod_' or'OBOB_BeamCFiltsensmod_' or 'BeamCFiltsensmod_' or [] (previous analysis)

if contains(sensmod{1},'all')
  type_decoding = 'dd_vs_ud_allsens_';
  Restype1 = '_allsens';
else
  type_decoding = [];
  Restype1 = '_persens';
end

if contains(sensmod{1},'resp')
    Restype2 = '_respvsnoresp';
else
    Restype2 = '_hitvsmiss';
end

Restypeall = [Restype1 Restype2];

HPfilt = [type_decoding 'hpfilt_0.1_' paradigm]; %  hpfilt_1 or hpfilt_0.1
Type1 = 'zscoreNorm_trad0';
%Type2 = ['_toilim_-200to600_tstep10ms_' lcmvtype 'srad0_Bayes'];
Type2 = ['_toilim_-100to600_tstep10ms_' lcmvtype 'srad30_Bayes'];

Pathinput = fullfile(config.path2work,...
  ['mvpa/t2t_temp/cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);

Pathoutput = fullfile(config.path2work,...
  ['mvpa/t2t_srcERF/cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);

Nametype = 'MVPA_src';

Nbrfiles = 7;
Delfile  = 1; % delete files after stack results : 1 = yes / 0 = no

[MissF] = Do_mvpa_results_stack(Suj,Nbrfiles,Pathinput,Pathoutput,Nametype,Delfile,sensmod);

%% or 

condor_MVPA_time2time_srcERF_stackresults

%% Organize results in feildtrip format
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Pathsaved = Pathoutput; % saved data
Path2saved = fullfile(Pathsaved,'MeanDSFT');

if ~exist(Path2saved,'dir') % create output dir if dont exist
  mkdir(Path2saved);
else
end

AnalysisName = 'MVPA_src';


Files = dir(fullfile(Pathsaved,[AnalysisName '*']));
nbF = numel(Files);
if nbF ~= 14
  error(['Number of files is: ' num2str(nbF)]);
end

for i = 1:nbF
  disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
  load(fullfile(Pathsaved,Files(i).name));
  
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      M_tot(i,j,k) = M{j,k};
      
      cdt_ds = M_tot(i,j,k);
      
      % move {train,test}_time from being sample dimensions to feature
      % dimensions, so they can be mapped to a fieldtrip struct & use in cosmo montecarlo cluster stat):
      %----------------------------------------------------------------
      cdt_tf_ds = cosmo_dim_transpose(cdt_ds,{'train_time','test_time'},2);
      
      % store variable for statistic later
      %----------------------------------
      %ds_stat{i,j,k} = cdt_tf_ds;
      
      % % trick fieldtrip into thinking this is a time-freq-chan dataset, by
      % % renaming train_time and test_time to freq and time, respectively
      %--------------------------------------------------------------------
      cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'train_time','freq');
      cdt_tf_ds = cosmo_dim_rename(cdt_tf_ds,'test_time','time');
      
      % convert to fieldtrip format
      %-----------------------------
      ds_ft{i,j,k} = cosmo_map2meeg(cdt_tf_ds);
      
      % Extract accuracy
      %------------------
      M_t(i,j,k,:,:,:) = ds_ft{i,j,k}.avg.pow;
      
    end
  end
end

% Mean accuracy
%-------------
id_src = find(size(M_t)==max(size(M_t)));% find source dimension inside this matrix
Mg = squeeze(mean(squeeze(mean(M_t,id_src)),1)); % all sources + all subjects
Ms = squeeze(mean(M_t,id_src)); % avg all sources
Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
val   = round(ds_ft{1,1,1}.time*1000);
% Save
%--------
disp('*** Saving Files ***');

% % Save STAT
% %-----------
% FileNameSaved = fullfile(Path2saved,['DSstatdimtranspose_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
% save(FileNameSaved,'-v7.3','ds_stat');
% disp('...');
% Save FT-like struct
%---------------------
FileNameSaved = fullfile(Path2saved,['FTstruct_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','ds_ft');
disp('......');

clear ds_ft
% 
% Save single subjects accuracy
%--------------------------------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','M_t','val');
disp('............');

clear M_t

% Save Mean accuracy
%-------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' AnalysisName '_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','Msacc','Mg','val');

disp('*** DONE ! ***');
%% 4 ==> Statistics
%--------------------

condor_MVPA_time2time_srcERF_stat
