%% Script plot overlap saliency and attention network

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT_v2',cfg);


%% Load atlas parcel
%---------------------
%load(fullfile(config.pathgroup,'gsanchez','templates','atlas','atlas_parcel333.mat'));

load standard_mri
load parcellations_3mm.mat
template_grid = parcellation.template_grid;
parcel_grid = parcellation.parcel_grid;

%load mni_grid_1_5_cm_889pnts.mat

% Change units
%-------------
parcel_grid   = ft_convert_units(parcel_grid,'m'); 
mri           = ft_convert_units(mri, 'm');
template_grid = ft_convert_units(template_grid, 'm');


%% choose region
%----------------

idx_inside2all = find(parcel_grid.inside);%important to apply find the indeces of all fields not only inside...
label = parcel_grid.label;
%labelnames = {'Vis';'Aud';'Smhand';'Sal';'At'};
%val_toplot = [.1,.2,.3,.4,.5];
labelnames = {'Sal';'At'};
val_toplot = [.9,.1];

for i = 1:numel(labelnames)
  chansel = ft_channelselection(cell({['*' labelnames{i} '*']}), label);
  [C,id{i}]=intersect(label, chansel);
  id_all{i} = idx_inside2all(id{i});
end

newgrid = parcel_grid;
newgrid.toplot(parcel_grid.inside==1) = 0;
newgrid.toplot = newgrid.toplot';

% all networks
masklabelnames = {};


for i = 1:numel(id_all)
  newgrid.toplot(id_all{i}) = val_toplot(i);
  
  newgrid.(labelnames{i})(parcel_grid.inside==1) = 0;
  newgrid.(labelnames{i}) = newgrid.(labelnames{i})';
  newgrid.(labelnames{i})(id_all{i}) = val_toplot(i);
  
  masklabelnames{i,1} = ['mask' labelnames{i}];
  newgrid.(masklabelnames{i}) = zeros(size(newgrid.(labelnames{i})));
  newgrid.(masklabelnames{i})(newgrid.(labelnames{i})~=0) = 1;
end

newgrid.mask = zeros(size(newgrid.toplot));
newgrid.mask(newgrid.toplot~=0) = 1;

cfg = [];
cfg.parameter = [labelnames;masklabelnames;{'toplot';'mask'}];
interp = ft_sourceinterpolate(cfg, newgrid, mri);

%% Plot caret brain all together
%--------------------------------------
source = interp;
cfg = [];
cfg.funparameter = 'toplot';
cfg.maskparameter = 'mask';
%cfg.funcolorlim = val2plot;
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.funcolorlim = [0 1];
cfg.projthresh = 0.1;

% make figures for right hemisphere
hemibrain = {'right'};
viewbrain = {'right'};
for i = 1:numel(hemibrain)
  for j = 1:numel(viewbrain)
    h = hemibrain{i};
    cfg.surffile = ['surf_caret' h '.mat'];
    ft_sourceplot(cfg, source);
    %colormap('colorcube'); 
    colorsmix = [0 0 1;0 1 0; 1 1 0; 1 0.6 0 ; 1 0 0];
    colormap(colorsmix); 
    v = viewbrain{j};
    plot_caret_style([], 100, [], v);
    
  end
end

%% Plot with atlas
%-------------------

source = interp;
cfg = [];
cfg.funparameter = 'toplot';
cfg.maskparameter = 'mask';
cfg.method = 'ortho';
cfg.atlas = 'ROI_MNI_V4.nii';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.funcolorlim = [0 1];
cfg.projthresh = 0.1;

ft_sourceplot(cfg, source);
colorsmix = [0 0 1;0 1 0; 1 1 0; 1 0.6 0 ; 1 0 0];
colormap(colorsmix); 

%% Plot caret brain one by one
%-----------------------------
source = interp;

param2plot = labelnames(1:3);
mask2plot = masklabelnames(1:3);
colorvizu = {[1 0 0] [1 1 0] [0 0 1]};


cfg = [];
cfg.funparameter = param2plot;
cfg.maskparameter = 'mask';
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.funcolorlim = [0 1];

hemibrain = {'right'};
viewbrain = {'right'};
for i =1:numel(hemibrain)
  for j = 1:numel(viewbrain)
    h = hemibrain{i};
    cfg.surffile = ['surf_caret' h '.mat'];
    for k = 1:numel(param2plot)
      cfg.funparameter  = param2plot{k};
      cfg.maskparameter = mask2plot{k};
      ft_sourceplot(cfg, source);
      map = colormap;
      valbar = size(map,1);
      map(1:valbar,:) = repmat(colorvizu{k},valbar,1);
      colormap(map);
      
    end
    v = viewbrain{j};
    plot_caret_style([], 100, [], v);
  end
end








%% test colormap
%----------------
surf(peaks);
colormap(prism);

%% Load parcel data
%----------------------
pathdata = '/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_srcERF/cross_allsens_hitvsmiss_acc_zscoreNorm_trad0/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_respcontrol_toilim_-100to600_tstep10ms_parcelOBOB_BeamCFiltsensmod_srad0_Bayes';
load(fullfile(pathdata,'MeanDSFT','FTstruct_MVPA_src_N14.mat'));


data = ds_ft{1};

% Masking
%-------------
selroi = 1:333; % No selection of intersection mask
valpow = 2;
%id1 = 1:150;
id1 = isal;
valmask = 1;

mask = zeros(1,333);
mask(id1) = valmask;

%%% Data organization
%----------------------
data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

% Fill-in with accuracy values found
for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
        data.pow(selroi,i,j) = valpow;
        data.mask(:,i,j) = mask;
    end
end

mask2plot = {'mask'};
param2plot = {'pow'};

data.dimord = 'chan_freq_time';


%% now virtual sensor to src
%---------------------------
cfg = [];
cfg.mri = mri;
cfg.sourcegrid = template_grid;
cfg.parameter = [param2plot mask2plot];
cfg.latency = [data.time(1) data.time(end)];
cfg.frequency = [data.freq(1) data.freq(end)];
source = obob_svs_virtualsens2source(cfg,data);

%% plot source
%---------------

cfg = [];
cfg.method = 'ortho';
cfg.funparameter = param2plot;
cfg.maskparameter = mask2plot; 
cfg.coordsys = 'mni';
cfg.inputcoord = 'mni';
cfg.interactive = 'yes';
cfg.atlas = atlas;
cfg.atlas.coordsys = 'mni';
% cfg.marker        = template_grid.pos(ia_all,:,1);
ft_sourceplot(cfg, source);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ù
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test
%---------
pathdata = '/mnt/obob/staff/gsanchez/DATA_processing/metaNT_v2/mvpa/t2t_srcERF/cross_allsens_hitvsmiss_acc_zscoreNorm_trad0/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_respcontrol_toilim_-100to600_tstep10ms_parcelOBOB_BeamCFiltsensmod_srad0_Bayes';
load(fullfile(pathdata,'MeanDSFT','FTstruct_MVPA_src_N14.mat'));


data = ds_ft{1};

%%% Data organization
%----------------------
data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

%%% fake data
%-------------
data.pow(selroi,i,j) = x(selroi)-val2plot(1);
data.mask(:,i,j) = mask;


mask2plot = {'mask'};
param2plot = {'pow'};


[Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile,toi)
