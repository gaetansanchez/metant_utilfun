%% Script to run MVPA with high decoding accuracy
%------------------------------------------------
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Time2Time decoding at source level (SRC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1 ==> Prepare SRC (sort trials + epoched + downsample + source localization...)
%---------------------
condor_srcERF4MVPA

%% 2 ==> Do real MVPA computation with parallelisation (9 jobs per subjects)
%----------------------------------------------------------
condor_MVPA_time2time_nosl_parcomp

%% 3 ==> Do only Brain searchlight on specific time period

condor_MVPA_Brain_searchlight_srcERF_parcomp


%% Define type of filename/path
%------------------------------
chan_types    = {'SRC'};
sensmod = {'all'}; % {'aud' 'vis' 'tac'} or {'aud_resp' 'vis_resp' 'tac_resp'} or {'all_resp'} or {'all'} -> just decode hit vs miss across all sensmod
%sensmod = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'} -> just decode hit vs miss across all sensmod
paradigm = 'hit_vs_miss_respcontrol';% 'hit_vs_miss_respcontrol' or 'hit_vs_miss' or 'supravs_sham' or []
lcmvtype = 'OBOB_BeamCFiltsensmod_'; % 'parcelOBOB_BeamCFiltsensmod_' or 'OBOB_BeamCFiltsensmod_' or 'BeamCFiltsensmod_' or [] (previous analysis)
source_radius = 3;

if contains(sensmod{1},'all')
  type_decoding = 'dd_vs_ud_allsens_';
  Restype1 = '_allsens';
else
  type_decoding = [];
  Restype1 = '_persens';
end

if contains(sensmod{1},'resp')
    Restype2 = '_respvsnoresp';
else
    Restype2 = '_hitvsmiss';
end

Restypeall = [Restype1 Restype2];


HPfilt = [type_decoding 'hpfilt_0.1_' paradigm]; %  hpfilt_1 or hpfilt_0.1
Type1    = 'zscoreNorm_trad1';
Type1_sl = 'zscoreNorm_trad1';
Type2 = ['_toilim_-200to600_tstep10ms_' lcmvtype 'chan_' [chan_types{:}] '_Bayes'];
Type2_sl = ['_toilim_-200to600_tstep10ms_' lcmvtype 'srad' num2str(source_radius*10) '_Bayes'];

Pathinput = fullfile(config.path2work,'mvpa','t2t_nosl_temp',...
  ['cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);
Pathoutput = fullfile(config.path2work,'mvpa','t2t_nosl_SRC',...
  ['cross' Restypeall '_acc_' Type1 '/' HPfilt Type2]);


Pathoutput_sl = fullfile(config.path2work,'mvpa','t2t_srcERF',...
  ['cross' Restypeall '_acc_' Type1_sl '/' HPfilt Type2_sl]);

% AnalysisName = [Type1 '_' Type2  '_MVPA_t2tERF_cross'];
AnalysisName = ['MVPA_t2tSRC'];

Path2saved = fullfile(Pathoutput,'MeanDSFT');
Pathstat   = fullfile(Pathoutput,'stat'); % saved stat
Pathfig    = fullfile(Pathoutput,'figures'); % saved figs

Path2saved_sl = fullfile(Pathoutput_sl,'MeanDSFT');
Pathstat_sl   = fullfile(Pathoutput_sl,'stat'); % saved stat



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 3 ==> Get results and stack data per subject
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sub = get_subject_mat(config); % get all subject code and fifpath
% remove subject with less than 30 trials in one condition
%Suj = Do_sel_subjects(config,'metaNT_v2_testwo17');
Suj = Do_sel_subjects(config,'metaNT_v2');

Nbrfiles = 1;
Delfile = 1; % delete files after stack results : 1 = yes / 0 = no

[MissF] = Do_mvpa_results_stack(Suj,Nbrfiles,Pathinput,Pathoutput,AnalysisName,Delfile,sensmod)

%--------------------------------
% Compute average Acc over group
%--------------------------------

% inspired from : SCRIPT_mvpa_t2t_analyse.m (average Acc over subjects)

if ~exist(Path2saved,'dir') % create output dir if dont exist
  mkdir(Path2saved);
else
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbF = numel(Suj);
M_t = [];
for i = 1:nbF
  idsuj = Suj(i);
  Files = dir(fullfile(Pathoutput,sprintf('%s_%02d*',AnalysisName,idsuj)));
  disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
  load(fullfile(Pathoutput,Files(1).name));
  
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      M_tot(i,j,k) = M{j,k};
      
      cdt_ds = M_tot(i,j,k);
     
      % convert to fieldtrip format
      %-----------------------------
      
      [data, labels, values] = cosmo_unflatten(cdt_ds,1);
      
      % Extract accuracy
      %------------------
      M_t(i,j,k,:,:) = data;
      
    end
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Mean accuracy
%---------------
Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
val   = round(values{1}*1000);

% Save
%--------
disp('*** Saving Files ***');

% Save single subjects accuracy
%--------------------------------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','M_t','val');
disp('............');

% Save Mean accuracy
%-------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','Msacc','val');

disp('*** DONE ! ***');



%% 4 ==> Statistics
%--------------------

condor_MVPA_time2time_nosl_stat


%% Plot nice figure !
%---------------------

dosave = 1;

% Plot 3by3
%-----------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_N14.mat']);
%FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_N13.mat']);
load(FileNameSaved);


if numel(size(Msacc))==2
  timepnts = round(val,2)+1;
else
  timepnts = round(val,2);
end
toi = [0 500];
startpoint = find(timepnts == toi(1));
stoppoint  = find(timepnts == toi(2));

%----------------
% Do the plotting
%----------------
smooth = 2;
LimAx  = [0.4 0.6];


val  = val(startpoint:stoppoint);
val2plot = val+1;

data = [];
if numel(size(Msacc))==2
  data(1,1,:,:) = Msacc(startpoint:stoppoint,startpoint:stoppoint);
else
  data = Msacc(:,:,startpoint:stoppoint,startpoint:stoppoint);
end

cfg =[];
cfg.xaxis  = val2plot;
cfg.xytick = find(ismember(val2plot',[0:100:500]));
cfg.smooth = smooth;
cfg.LimAx  = LimAx;
cfg.mask_int = 'linear';
[h] = plot_3by3_MVPA(data,cfg);
colormap('jet');
pause(1);
%set(h, 'Position', [0, 0, 1200, 1600]);



if strcmp(computer,'MACI64')
  fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_3vs3_NoStat_' AnalysisName]);
else
  fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_NoStat_' AnalysisName]);
end

if dosave
  
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
end


if size(data,1) == 3 % if there are 3 sensory modalities
  %%%%%%%%%%%%%%
  % Do plot only within sensory modality
  %%%%%%%%%%%%%%
  for i = 1:size(data,1)
    
    data2p(1,1,:,:) = data(i,i,:,:);
    cfg =[];
    cfg.xaxis  = val2plot;
    cfg.xytick = find(ismember(val2plot',[0:100:500]));
    cfg.smooth = smooth;
    cfg.LimAx  = LimAx;
    cfg.mask_int = 'linear';
    [h] = plot_3by3_MVPA(data2p,cfg);
    colormap('jet');
    title(sensmod{i});
    pause(1);
    %set(h, 'Position', [0, 0, 1200, 1600]);
    
    if strcmp(computer,'MACI64')
      fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_3vs3_NoStat_' AnalysisName '_' sensmod{i}]);
    else
      fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_NoStat_' AnalysisName '_' sensmod{i}]);
    end
    
    if dosave
      
      % save png for record
      %----------------------
      name2 = [fname01 '.png'];
      img = getframe(h);
      imwrite(img.cdata,name2);
    end
    
  end
end


%% Plot individual subject
%---------------------------
Suj = Do_sel_subjects(config,'metaNT_v2');

% Plot 3by3
%-----------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_N14.mat']);
load(FileNameSaved);


for i = 1:size(M_t,1)
  
    M_t2plot = squeeze(M_t(i,:,:,:,:));
    
    data = [];
    if numel(size(M_t2plot))==2
      data(1,1,:,:) = M_t2plot;
    else
      data = M_t2plot;
    end
    
    %----------------
    % Do the plotting
    %----------------
    smooth = 0;
    LimAx  = [0.4 0.6];
    
    cfg =[];
    cfg.xaxis  = val;
    cfg.xytick = find(ismember(val',[0:100:600]));
    cfg.smooth = smooth;
    %cfg.mask   = stat_mask_t;
    cfg.LimAx  = LimAx;
    cfg.mask_int = 'linear';
    [h] = plot_3by3_MVPA(data,cfg);
   % colormap('jet');
   % set(h, 'Position', [0, 0, 1200, 1600]);
    title(['Sub = ' num2str(Suj(i))],'FontSize',13,'FontWeight','bold');
    
end

%% Statistics compilation
%---------------------------

AnalysisName = 'MVPA_t2tSRC_N14';

toi = [0 0.5]; % in sec
%toi = [0.35 0.5]; % in sec
%%%%%

flag = 0; % 1 = doplot stat maps

% Load and threshold
%--------------------
stat_mask_nosl = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_diag   = {};
mask_int   = 0;

for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    
    timepnts = round(values{1},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
        %sig  = abs(pdata)>1.65; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>1.9600; % z-values / (alpha 0.01) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>2.54;% z-values / (alpha 0.05 + Bonferronir correction on 9 decoding)
        sig  = abs(pdata)>2.58;% z-values / (alpha 0.005) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
        
%         % Find 10% of significant Z-values
%         %----------------------------------
%         opt = []; opt.lim = 10; opt.flag = flag;
%         x = pdata(abs(pdata)>1.6449);
%         [bound] = find_distrib_percent(x,opt);
%         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        %th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        th = 1 - 0.005; % alpha 0.005 new threshold approved by the comunity
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points
    %----------------------------------
    mask = sig; % map train / test (average sensors)
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j}   = pdata;
    mask_bin{i,j} = mask;
    
    if flag
    figure;
    subplot(1,3,1);
    imagesc(mask_p{i,j});colorbar;caxis([0.6 1]);
    subplot(1,3,2);
    imagesc(mask);colorbar;
    subplot(1,3,3);
    imagesc(sig);colorbar;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_diag{i,j} = sig(logical(eye(size(data_mat,1),size(data_mat,2))));
 
    % Store temporal mask
    %----------------------
    stat_mask_nosl(i,j,:,:) = mask;
  end
end

if flag
  if mask_int ~= 0
    figure;
    surf(mask_int);colorbar;
  end
end

disp('*************');
disp('DONE !!!');
%------------------------------
%% Plot nice figure with stat
%------------------------------


% LOad data and cut accordingly
%------------------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_N14.mat']);
load(FileNameSaved);

val  = val(startpoint:stoppoint);
val2plot = val+1;

data = [];
if numel(size(Msacc))==2
  data(1,1,:,:) = Msacc(startpoint:stoppoint,startpoint:stoppoint);
else
  data = Msacc(:,:,startpoint:stoppoint,startpoint:stoppoint);
end

% transparency level
%--------------------
transp = 0.25;
%transp = 1;
stat_mask_t = stat_mask_nosl+transp;
stat_mask_t(stat_mask_t>1) = 1;

%----------------
% Do the plotting
%----------------
smooth = 2;
LimAx  = [0.4 0.6];

cfg =[];
cfg.xaxis  = val2plot;
cfg.xytick = find(ismember(val2plot',[0:100:500]));
cfg.smooth = smooth;
cfg.mask   = stat_mask_t;
cfg.LimAx  = LimAx;
cfg.mask_int = 'linear';
[h] = plot_3by3_MVPA(data,cfg);
colormap('jet');
pause(1);
%set(h, 'Position', [0, 0, 1200, 1600]);


dosave = 1;
if strcmp(computer,'MACI64')
  fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_3vs3_MaskStat_' AnalysisName]);
else
  fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_MaskStat_' AnalysisName]);
end

if dosave
  
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
end


if size(data,1) == 3 % if there are 3 sensory modalities
  %%%%%%%%%%%%%%
  % Do plot only within sensory modality
  %%%%%%%%%%%%%%
  for i = 1:size(data,1)
    
    data2p(1,1,:,:) = data(i,i,:,:);
    cfg =[];
    cfg.xaxis  = val2plot;
    cfg.xytick = find(ismember(val2plot',[0:100:500]));
    cfg.smooth = smooth;
    cfg.mask   = stat_mask_t(i,i,:,:);
    cfg.LimAx  = LimAx;
    cfg.mask_int = 'linear';
    [h] = plot_3by3_MVPA(data2p,cfg);
    colormap('jet');
    title(sensmod{i});
    pause(1);
    %set(h, 'Position', [0, 0, 1200, 1600]);
    
    
    dosave = 1;
    if strcmp(computer,'MACI64')
      fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_3vs3_MaskStat_' AnalysisName '_' sensmod{i}]);
    else
      fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_MaskStat_' AnalysisName '_' sensmod{i}]);
    end
    
    if dosave
      
      % save png for record
      %----------------------
      name2 = [fname01 '.png'];
      img = getframe(h);
      imwrite(img.cdata,name2);
    end
    
  end
end


%% Do plot generalization
%--------------------------
close all;

factms    = 10; % in ms
yborders  = [0 400]; % in ms
xborders  = [0 500];% in ms
%acclimits = [0.5 0.57];
acclimits = [0 1]; % normalized accuracies in the plot
Do_t2t_metaNTdec_statmask_plot_generalization(data,val2plot,stat_mask_nosl,acclimits,xborders,yborders,factms)

h = figure(3);
set(h, 'Position', [0, 0, 800, 800]);

dosave = 1;
if strcmp(computer,'MACI64')
  fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_generalization_' AnalysisName]);
else
  fname01 = fullfile(Pathoutput,'figure',['MVPA_t2t_generalization_' AnalysisName]);
end

if dosave
  
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
  
end


if size(data,1) == 3 % if there are 3 sensory modalities
  close all;
  %%%%%%%%%%%%%%%%%%%
  % Do plot only within sensory modality
  %%%%%%%%%%%%%%%%%%%
  for i = 1:size(data,1)
    data2p(1,1,:,:) = data(i,i,:,:);
    stat_mask_nosl2p(1,1,:,:) = stat_mask_nosl(i,i,:,:);
    Do_t2t_metaNTdec_statmask_plot_generalization(data2p,val2plot,stat_mask_nosl2p,acclimits,xborders,yborders,factms)
    
    h = figure(3);
    set(h, 'Position', [0, 0, 800, 800]);
    
    dosave = 1;
    if strcmp(computer,'MACI64')
      fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',['MVPA_t2t_generalization_' AnalysisName '_' sensmod{i}]);
    else
      fname01 = fullfile(Pathoutput,'figure',['MVPA_t2t_generalization_' AnalysisName '_' sensmod{i}]);
    end
    
    if dosave
      
      % save png for record
      %----------------------
      name2 = [fname01 '.png'];
      img = getframe(h);
      imwrite(img.cdata,name2);
      close all;
    end
    
  end
  
end

%% Find searchlight brain regions inside significant time point of no searchlight decoding
%------------------------------------------------------------------------------------------

% Files with serachlight decoding
%--------------------------------
AnalysisName = 'MVPA_src_N14';

toi = [0 0.5]; % in sec
%toi = [0.35 0.5]; % in sec

toi_mask = stat_mask_nosl; % store previous stat to get significant brain

%%%%%% Files with serachlight decoding
%--------------------------------
Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
Pathstat  = fullfile(Pathoutput_sl,'stat'); % saved stat

lab = {'train_time' 'test_time'};
%sensmod   = {'aud' 'vis' 'tac'};
%sensmod   = {'all'};


flag = 0;
% Load and threshold
%--------------------
stat_mask = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_roi   = {};
sig_toi   = {};
sig_roi_diag   = {};
sig_roi_diag_t = {};
mask_int       = 0;
mask_brain_int = 0;
Allbound = {};
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    timepnts = round(values{2},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(:,startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
        % sig  = data_mat>1.65; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>1.9600; % z-values / (alpha 0.01) value > H0mean (H0mean = 0.5)
        %sig  = abs(pdata)>2.54;% z-values / (alpha 0.05 + Bonferronir correction on 9 decoding)
        %sig  = abs(pdata)>2.58;% z-values / (alpha 0.005) value > H0mean (H0mean = 0.5)
        sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
        
%         % Find 10% of significant Z-values
%         %----------------------------------
%         opt = []; opt.lim = 10; opt.flag = flag;
%         x = pdata(abs(pdata)>3.1);% z-values / (alpha 0.001)
%         [bound] = find_distrib_percent(x,opt);
%         
%         Allbound{i,j} = bound;
%         
%         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        %th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        th = 1 - 0.005; % alpha 0.005 new threshold approved by the comunity
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points from previous time2time no
    % searchlight decoding
    %-----------------------
   
    mask = logical(squeeze(toi_mask(i,j,:,:))); % map train / test
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j} = squeeze(max(pdata));
    mask_bin{i,j} = mask;
    
    if flag
        figure;
        subplot(1,3,1);
        imagesc(mask_p{i,j});colorbar;caxis([0.9 1]);
        subplot(1,3,2);
        imagesc(mask);colorbar;
        subplot(1,3,3);
        imagesc(squeeze(sum(sig,1)));colorbar;
    end
    
    % Extract spatial mask according to significant time points
    %-----------------------------------------------------------
    Number2sig_time = 1;
    sig_roi{i,j} = squeeze(sum(sig(:,mask),2));
    sig_toi{i,j} = sig_roi{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    if i ~= j
      mask_brain_int = sig_toi{i,j}+mask_brain_int;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_roi_diag{i,j} = squeeze(sum(sig(:,logical(eye(size(data_mat,2),size(data_mat,3)))),2));
    sig_toi_diag{i,j} = sig_roi_diag{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    % Extract diagonal spatial feature (movie)
    %------------------------------------------
    for t = 1:size(sig,3)
      sig_roi_diag_t{i,j,t} = logical(squeeze(sig(:,t,t))); % take spatial mask for each diagonal time
    end
  
    % Store temporal mask
    %----------------------
    stat_mask(i,j,:,:) = mask;
    
  end
end

if flag
  figure;
  surf(mask_int);colorbar;
end

% Analysis intersect
%----------------------
[CVroi_all,IUroi_all,AIDroi_intensity,AIDroi] = Do_t2t_metaNTdec_statmask_sl_intersect(sig_toi,sig_toi_diag,sig_roi);

% Average Brain Accuracy for only significant time point
%---------------------------------------------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

if numel(sensmod) == 1 % if just one modality 
  Msacc2process(1,1,:,:,:) = Msacc;
else
  Msacc2process = Msacc;
end

Msacc_small = Msacc2process(:,:,:,startpoint:stoppoint,startpoint:stoppoint);% remove prestim data

Msig_acc = [];
Maxsig_acc = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    % average Acc for significant Time point
    %----------------------------------------
    Msig_acc(i,j,:) = mean(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),2);
    
    
    % Max Acc for significant Time point
    %----------------------------------------
    Max4vox = max(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),[],2);
    if isempty(Max4vox) % if not significant time point ==> set to NaN
      Max4vox = nan(1,size(Msacc_small,3));
    end
    Maxsig_acc(i,j,:) = Max4vox;
    
    % Normalize between 0 and 1
    %--------------------------
    % [Msig_acc(i,j,:)] = Do_normalize(Msig_acc(i,j,:),3);
  end
end

if numel(sensmod) == 3
  Msig_acc_Between = mean([squeeze(Msig_acc(1,2,:)) squeeze(Msig_acc(1,3,:)) ...
    squeeze(Msig_acc(2,1,:)) squeeze(Msig_acc(2,3,:)) ...
    squeeze(Msig_acc(3,1,:)) squeeze(Msig_acc(3,2,:))],2);
  
  Maxsig_acc_Between = max([squeeze(Maxsig_acc(1,2,:)) squeeze(Maxsig_acc(1,3,:)) ...
    squeeze(Maxsig_acc(2,1,:)) squeeze(Maxsig_acc(2,3,:)) ...
    squeeze(Maxsig_acc(3,1,:)) squeeze(Maxsig_acc(3,2,:))],[],2);
end

% Mask of intersect 10% accuracy
%----------------------------------
opt = []; opt.lim = 10; opt.flag = 0;
mask10  = zeros(numel(sensmod),numel(sensmod),889);

for strain = 1:numel(sensmod)
  for stest = 1:numel(sensmod)
      x = squeeze(Msig_acc(strain,stest,:));
      [bound] = find_distrib_percent(x,opt);
      mask10(strain,stest,x>=bound(2)) = 1;% mask > 10% acc
  end
end

if numel(sensmod) == 3
  mask10_Uaudvistrain = logical(sum([squeeze(mask10(1,2,:)) squeeze(mask10(2,1,:))],2)>=1);
  mask10_Uaudtactrain = logical(sum([squeeze(mask10(1,3,:)) squeeze(mask10(3,1,:))],2)>=1);
  mask10_Utacvistrain = logical(sum([squeeze(mask10(2,3,:)) squeeze(mask10(3,2,:))],2)>=1);
  
  mask10_IU = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)==3);
  mask10_U  = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)>=1);
end

% Find average latency for each brain nodes of Intersect(Union)
%----------------------------------------------------------------
val = timepnts(startpoint:stoppoint);

Nnodes   = max(size(data)); % max size dimension is usually the number of sources
Nsensmod = numel(sensmod);

Mlatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);
Slatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);

for k = 1:Nnodes
  for i = 1:Nsensmod
    for j = 1:Nsensmod
      latsig = [];
      latsig = val(find(sum(squeeze(dat_sig{i,j}(k,:,:)),2))); % find only significant latency
      Mlatsig_Nodes(k,i,j) = mean(latsig);
      Slatsig_Nodes(k,i,j) = std(latsig)./sqrt(length(latsig));
    end
  end
end

if numel(sensmod) == 3
  Dat4Between = [Mlatsig_Nodes(IUroi_all,logical(~eye(3,3)))];
  Mlatsig_BETW_IUroi_all = nanmean(Dat4Between,2);
  Slatsig_BETW_IUroi_all = nanstd(Dat4Between,[],2)./sqrt(sum(~isnan(Dat4Between),2));
  
  Mlatsig_BETW_CVroi_all = nanmean([Mlatsig_Nodes(CVroi_all,logical(~eye(3,3)))],2);
  
  Mlatsig_AUD_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,1,1)],2);
  Mlatsig_VIS_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,2,2)],2);
  Mlatsig_TAC_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,3,3)],2);
  
end

% Max Zvalue compilation
%------------------------
Mzscore = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    Mzscore(i,j,:,:) = mask_p{i,j};
  end
end

disp('*************');
disp('DONE !!!');



%% plot voxel CARET brain : Only intersect Between Modalities
%------------------------------------------------------
close all;

if ~exist('ds_ft','var')
    Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
    load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end


% acc10 options:
%---------------
% 1 = only 10% acc from mask intersect significant
% 2 = make mask from 10% acc between sensmod decoding
% 0 = plot all acc from mask intersect significant
acc10 = 1;
dosavefile = 1;
dosurface  = 0;
toi = [0 0.5]; % in sec (time period to average to plot)
%val2plot = [0.45 0.55];
val2plot = [0 1]; % normalize accuracies


if acc10 == 1
  prefixfilename = '10percent_';
else
  prefixfilename = [];
end

if strcmp(computer,'MACI64')
  fname = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures',[prefixfilename 'Brain_Acc_SensMod_' AnalysisName]);
else
  fname = fullfile(config.path,'doc/mvpa',[prefixfilename 'Brain_Acc_SensMod_' AnalysisName]);
end

if numel(size(ds_ft)) == 3
    data = ds_ft{1,1,1};
else
    data = ds_ft{1,1};
end

%%% Masking
%-------------
selroi = CVroi_all; % only spatial features between modalities (intersect)
%selroi = IUroi_all; % only spatial features between modalities (intersect)
%selroi = CVroi_all_union; % union

mask = zeros(1,889);
mask(selroi) = 1;

%%% Data organization
%----------------------
data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

if numel(sensmod) == 3
  Msig_acc2plot = Msig_acc_Between;
  Maxsig_acc2plot = Maxsig_acc_Between;
  Mlatsig_vox2plot = Mlatsig_BETW_IUroi_all*1000; % Mean Latency for these voxels (in ms)
elseif numel(sensmod) == 1
  Msig_acc2plot = squeeze(Msig_acc);
  Maxsig_acc2plot = squeeze(Maxsig_acc);
  Mlatsig_vox2plot = Mlatsig_Nodes(selroi)*1000;% Mean Latency for these voxels (in ms)
end


if acc10 == 1
    % Find 10% of significant accuracy
    %----------------------------------
    opt = []; opt.lim = 20; opt.flag = 0;
    x = Msig_acc2plot;
    [bound] = find_distrib_percent(x,opt);
    mask  = zeros(1,889);
    mask(Msig_acc2plot>=bound(2)) = 1;% mask > 10% acc
    
    selroi = 1:889; % No selection of intersection mask
    %val2plot = [bound(2) max(x)];
    %val2plot = [0.45 max(x)];
    
elseif acc10 == 2
    % Mask of intersect of union 10% accuracy
    %----------------------------------
    mask  = zeros(1,889);
    mask  = mask10_IU';
    
    selroi = 1:889; % No selection of intersection mask
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
    
    
else
    %val2plot = [0 max(Msig_acc_Between(selroi))];
    x = Msig_acc2plot;
    %val2plot = [0.5 max(x)];
end

if val2plot(1)==0 && val2plot(2)==1
  % Normalize accuracy between 0 and 1 with y = (x - min(x))/(max(x)-min(x))
  %%%%%%%%%%%%%%  
  [x] = Do_normalize(x,1);
end

val2plotstore = val2plot;

% Fill-in with accuracy values found
for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
        %data.pow(selroi,i,j) = Msig_acc_Between(selroi)-val2plot(1);
        data.pow(selroi,i,j) = x(selroi)-val2plot(1);
        %data.pow(selroi,i,j) = Maxsig_acc_Between(selroi)-val2plot(1);
        data.mask(:,i,j) = mask;
        data.maskInt(:,i,j) = mask;
        data.maskInt(selroi,i,j) = AIDroi_intensity(selroi);
    end
end

val2plot = val2plot-val2plot(1);

mask2plot = {'mask'};
%mask2plot = {'maskInt'};
param2plot = {'pow'};

% now virtual sensor to src
%---------------------------
colormaplabel = 'jet';
Tab = [];
[Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile,toi);
Tab(:,5) = num2cell(cell2mat(Tab(:,5))+val2plotstore(1))



%% Check ROI
%------------
testdat = data;
sigVox  = find(squeeze(sum(sum(data.mask,2),3))); % only significant voxels

MeanAccval  = Msig_acc2plot(sigVox); % mean Acc values for these voxels
MaxAccval   = Maxsig_acc2plot(sigVox); % max Acc values for these voxels
LatVox      = Mlatsig_vox2plot; % Mean Latency for these voxels (in ms)

% sort voxels indices based on their values
%--------------------------------------------
[sort_MeanAccval,indv] = sort(MeanAccval, 'descend');

% sort according to mean accuracy
%---------------------------------
LatVox = LatVox(indv);
MaxAccval = MaxAccval(indv);
sigVox = sigVox(indv);

noteintval = [LatVox sort_MeanAccval MaxAccval]; % mean latency / mean Acc / Max Acc

Pathatlas = fullfile(config.pathgroup,'gsanchez/util_functions/parcellation');
A = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
%A = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
savefilename = 'Allsensmod_labelsMNI2Parcelatlas_queryrange01to03_MVPA_latency.mat';

testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)
[Tab] = Do_extractROIname_atlas(A,sigVox,noteintval,testdat,[],1);

% Extract other regions name from VoxID identified by previous atlas
%-------------------------------------------------------------------
%sigVox = cell2mat(Tab(:,6));
%B = ft_read_atlas('ROI_MNI_V4.nii');
B = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
[Tab2] = Do_extractROIname_atlas(B,sigVox,noteintval,testdat,[],3);

% Link both atlas based on Voxels indices
%------------------------------------------
NbClTab = size(Tab,2);
Tab_all = {};
for i = 1:size(Tab,1)
  id_betw_atlas = find(cell2mat(Tab2(:,5))==Tab{i,5});
  if ~isempty(id_betw_atlas)
    Tab_all(i,:) = [Tab(i,1) Tab2(id_betw_atlas,1) Tab(i,2:end)];
  else
    Tab_all(i,:) = [Tab(i,1) {[]} Tab(i,2:end)];
  end
end

Tab_all
save(savefilename, 'Tab_all');

%% Plot voxel time course according to label
%---------------------------------------------

if ~exist('M_tc','var')
  % Load single subject Acc values
  %---------------------------------
  Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['SubjectsAcc_' AnalysisName '.mat']));
  % Cut accroding to start and stop point
  M_tc = M_t(:,:,:,:,startpoint:stoppoint,startpoint:stoppoint);
  val =val(startpoint:stoppoint);
end
atlas2read = load(fullfile(Pathatlas,'atlas_grid_333.mat'));

RUN_extract_voxel_MVPA_time_course(atlas2read,sigVox,noteintval,testdat,M_tc,val,stat_mask)



%% plot voxel brain: each decoding
%----------------------------------

if ~exist('ds_ft','var')
  Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end

TabALL = {};
val2plotstore = {};
for strain = 1:3
  for stest = 1:3
    data = ds_ft{1,1,1};
    
    selroi = AIDroi{strain,stest};
    
    mask = zeros(1,889);
    mask(selroi) = 1;
    
    data.pow = data.avg.pow(data.inside,:,:);
    data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));
    
    binary_plot = 2; % 0 = number of time found as significant / 2 = accuracy / 1 = significant or not
    dosurface   = 0; % 0 = caret brain / 1 = surface brain
    dosavefile  = 1;
    NormAcc = 1;
    
    if strcmp(computer,'MACI64')
      fname = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT','figures',['1Dec_Acc_Brain_train_' sensmod{strain} '_vs_test_' sensmod{stest} '_' AnalysisName]);
    else
      fname = fullfile(Pathfig,['1Dec_Acc_Brain_train_' sensmod{strain} '_vs_test_' sensmod{stest} '_' AnalysisName]);
    end

    if binary_plot == 1
      val2plot = [0 5];
      % Fill-in with fix value when source was found
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          data.pow(selroi,i,j) = 5;
          data.mask(:,i,j) = mask;
        end
      end
      
    elseif binary_plot == 0
      val2plot = [0 max(sig_roi{strain,stest}(selroi))];
      %val2plot = [0 20];
      
      % Fill-in with number of time source was found
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          data.pow(selroi,i,j) = sig_roi{strain,stest}(selroi);
          data.mask(:,i,j) = mask;
        end
      end
      
    else
      
      acc10 = 0;
      if acc10 == 1
        % Find 10% of significant accuracy
        %----------------------------------
        opt = []; opt.lim = 10; opt.flag = 0;
        %x = squeeze(Msig_acc(strain,stest,selroi));
        x = squeeze(Msig_acc(strain,stest,:));
        [bound] = find_distrib_percent(x,opt);
        mask  = zeros(1,889);
        mask(squeeze(Msig_acc(strain,stest,:))>=bound(2)) = 1;% mask > 10% acc
        val2plot = [bound(2) max(x)];
        
        val2plotstore{strain,stest} = val2plot;
      else
        x = squeeze(Msig_acc(strain,stest,:));
        val2plot = [0.5 max(x)];
      end
      
      if NormAcc
        % Normalize accuracy between 0 and 1 with y = (x - min(x))/(max(x)-min(x))
        %%%%%%%%%%%%%%
        [x] = Do_normalize(x,1);
        val2plot = [0 1];
      end
      
      % Fill-in with accuracy
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          %data.pow(selroi,i,j) = Msig_acc(strain,stest,selroi)-val2plot(1);
          data.pow(selroi,i,j) = x(selroi)-val2plot(1);
          %data.pow(selroi,i,j) = Msig_acc(strain,stest,selroi);
          data.mask(:,i,j) = mask;
        end
      end
      
      val2plot = val2plot-val2plot(1);
      
    end
    
    param2plot = {'pow'};
    mask2plot  = {'mask'};
    colormaplabel = 'jet';
    %colormaplabel = 'autumn';
    [TabALL{strain,stest}] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile);
    
  end
end

%% Process Tab for atlas Parcel
%-------------------------------
B = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
[Tab_net] = Do_extractROIname_atlas(B,sigVox,noteintval,testdat,[],1);

%LabFam = {'Au', 'Vi', 'Sm' ,'De' ,'Ci', 'Do','Ve','Fr'};
%ValFam = [11 21 31 42 43 44 44 45];
%ValFam = [11 22 33 44 45 46 46 47];
LabFam = {'Au', 'Vi', 'Sm'};
ValFam = [1 2 3];

NbClTab = size(Tab_net,2);

for i = 1:size(Tab_net,1)
  
  id_Fam = find(ismember(LabFam,Tab_net{i,1}(1:2)));
  
  if id_Fam
    Tab_net{i,NbClTab+1} = ValFam(id_Fam);
  else
    Tab_net{i,NbClTab+1} = 0;
  end
  
end
Tab_net

%% Tim course of different networks
%-----------------------------------
%startpoint = 17; stoppoint = 33;
% Get Voxels indices for different network
%------------------------------------------
%Net_Lab = {'Sens' 'Att'};
%Net_Lab = {'Sens' 'DMN' 'Cing' 'AtVD' 'FrPa'};
%Net_Lab = {'Au' 'Vi' 'Sm'  'DMN' 'Cing' 'AtVD' 'FrPa'};
Net_Lab = {'Au' 'Vi' 'Sm'};
Net_Vox = {};
n = 1;
NbvarNet = numel(Net_Lab);
AbsentLab = [];
for i = 1:NbvarNet
  
  IDNet = cell2mat(Tab_net(find(mod(cell2mat(Tab_net(:,end)),10)==i),6));
  if isempty(IDNet)
    AbsentLab = [AbsentLab i];
  else
    Net_Vox{n} = IDNet;
    n = n+1;
  end
end
Net_Lab(AbsentLab) = [];

if ~exist('M_tc','var')
  % Load single subject Acc values
  %---------------------------------
  Pathsaved = fullfile(Pathoutput_sl,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['SubjectsAcc_' AnalysisName '.mat']));
  
  % Cut accroding to start and stop point
  M_tc = M_t(:,:,:,:,startpoint:stoppoint,startpoint:stoppoint);
  val =val(startpoint:stoppoint);
end

% Loop average
%-------------
OnlySig  = 1; % Only significant time points : 1=yes or 0=all time points
Nsuj     = size(M_tc,1);
Nsensmod = size(M_tc,2);
Ntime    = size(M_tc,6);
MaccNet       = zeros(Nsuj,Nsensmod,Nsensmod,numel(Net_Vox),Ntime);
MaccNet_BETW  = zeros(Nsuj,6,numel(Net_Vox),Ntime);
MaccNet_WITH  = zeros(Nsuj,3,numel(Net_Vox),Ntime);
for s = 1:Nsuj % suj
  b = 1;
  w = 1;
  for i = 1:Nsensmod % modality
    for j = 1:Nsensmod % modality
      for k = 1:numel(Net_Vox) % network
        tmpdat = squeeze(M_tc(s,i,j,Net_Vox{k},:,:));
        
        sig_traintime = logical(sum(squeeze(stat_mask(i,j,:,:)),2));% significant time point over training axes
        tmpdat_sig = tmpdat;
        tmpdat_sig(:,repmat(~sig_traintime,1,size(tmpdat,3))) = 0.5;% set non significant time point to 0.5 significant time points
        
        if OnlySig
          tmpdat = tmpdat_sig;
        end
        
        MaccNet(s,i,j,k,:) = mean(mean(tmpdat,3),1); % average over : test time + Network sources
        if i ~= j % only between sensmod decoding
          MaccNet_BETW(s,b,k,:) = mean(mean(tmpdat,3),1);
        else
          MaccNet_WITH(s,w,k,:) = mean(mean(tmpdat,3),1);
        end
      end
      if i ~= j % only between sensmod decoding
        b = b+1;
      else
        w = w+1;
      end
    end
  end
end


MaccNet_BETW_all = squeeze(mean(mean(MaccNet_BETW,2),1)); % average all between decoding + all subjects
SaccNet_BETW_all = squeeze(std(mean(MaccNet_BETW,2),1)./sqrt(size(MaccNet_BETW,1))); % SEM
MaccNet_WITH_all = squeeze(mean(mean(MaccNet_WITH,2),1)); % average all within decoding + all subjects
SaccNet_WITH_all = squeeze(std(mean(MaccNet_WITH,2),1)./sqrt(size(MaccNet_WITH,1))); % SEM

%------
% Plot
%------
figure; set(gcf,'color','w');
color4Net ={'-k' '-r' '-g' '-c' '-b' '-m' '-y'};
LimYAx = [0.495 0.515];
subplot(2,2,1);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_WITH_all(k,:),SaccNet_WITH_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,3);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_WITH_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
title('Within Modality','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');

subplot(2,2,2);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_BETW_all(k,:),SaccNet_BETW_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,4);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_BETW_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
title('Between Modalities','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');

