function [] = Do_trialsNb_count_after_reject_respcontrol(nsub,pathpreproc_artdef)

%% Path settings
%------------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Loop = load file and count number of clean trials
%----------------------------------------------

for id = 1:numel(nsub)
  
  iSub = nsub(id);
  
  %% load data 
  %-----------------------
  datafile    = fullfile(config.path2work,'mvpa','src4MVPA',sprintf('MVPA_srchpfilt_0.1_hit_vs_miss_respcontrol_toilim_-100to600_tstep10ms_BeamCFiltsensmod_%02d.mat',iSub));
  saveName_art = fullfile(pathpreproc_artdef,sprintf('Numtrial_percond_afterReject_respcontrol_s%02d.mat',iSub));
  
  load(datafile);
 
  %% Conditions check
  %------------------
  CondtrNumber = [];
  n = 1;
  for i = 1:3
    % 'AUD' = 100 / 'VIS' = 200 / 'TAC' = 300
    id_sensmod = find(ds.sa.modality==i);
    
    lab_cond = [1 2 -1 -2];% dd_r=1 ud_r=2 dd_nr=-1 ud_nr=-2
    for j = 1:numel(lab_cond)
      CondtrNumber(1,n) = numel(find(ds.sa.targets(id_sensmod,:) == lab_cond(j))); 
      n = n+1;
    end
  end
  %%
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)]);
  disp('Post rejection Number of trials per conditions = ');
  CondtrNumber
  
  disp(' ');
  disp(['Saving: ' saveName_art])
  disp(' ');
  
  save(saveName_art,'-v7.3','CondtrNumber');
end





