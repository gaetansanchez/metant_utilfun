function [data_clean_corr] = do_reject4cleaning(fileName)

[p,f,e] = fileparts(fileName);
infoclean_filename = fullfile(p,['infoclean_',f,e]); % output from reject_visual_blocks.m

% load data + info clean (from reject_visual_blocks.m)
%-----------------------
disp('****************');
disp(['Loading: ' fileName])
data = load(fileName); % load epoched data

load(infoclean_filename); % load 2nd cleaning (structure = okmanualrejection)

disp(' ');
disp('****************');
disp(['cleaning with: ' infoclean_filename])

% get and remove bad trials and bad channels
%--------------------------------------------
% use second column of trialinfo because it is id_trial from raw data reading
[~,ID_goodtrials,~] = intersect(data.trialinfo(:,2),okmanualrejection.trialinfo(:,2)); % get id of remaining good trials inside data
ID_goodchan = okmanualrejection.chanlabel;

cfg = [];
cfg.trials  = ID_goodtrials; % keep good trials id
cfg.channel = ID_goodchan; % keep Good MEG channels
data_clean  = ft_selectdata(cfg,data);

%% Correct offset because sound and visual delay
%------------------------------------------------
disp(' ');
disp('****************');
disp(['correct stim timing...']);

type_of_stim = (data_clean.trialinfo(:,1)-mod(data_clean.trialinfo(:,1),100))/100; % 1 = aud / 2 = vis / 3 = tac
delay_ms   = [29 10 0]; % delays for stim : aud / vis / tac (in ms)
delay_samp = round(data_clean.fsample*delay_ms/1000); % delays in samples

cfg = [];
cfg.offset = delay_samp(type_of_stim);
[data_clean_corr] = ft_redefinetrial(cfg, data_clean);


