%% SCRIPT check number of trial rejected during epoching + trials rejection

cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%% Set paths
%------------
sub = get_subject_mat(config); % get all subject code and fifpath

% %% Copy fif file 1st block
% 
% outDir = fullfile(config.path,'fif');
% if ~exist(outDir,'dir') % create output dir if necessary
%   mkdir(outDir);
% end
% 
% for i = 1:numel(sub)
%   
%   idsubnum  = sub(i).nb;
%   idsubcode = sub(i).code;
%   
%   all_fileName = fullfile(sub(i).pathfif,...
%     sprintf('%s_s%02d_b01_*.fif',idsubcode,idsubnum));
%   
%   filetmp = dir(all_fileName);
%   nfiles  = numel(filetmp); % number of files under this name
%   if nfiles > 1
%     disp(['Filename = ' all_fileName]);
%     error('More than one file under this name... Please check')
%   end
%   
%   fifname_source = fullfile(filetmp.folder,filetmp.name);
%   fifname_destination = fullfile(outDir,filetmp.name);
%   if ~exist(fifname_destination,'file')
%     copyfile(fifname_source,fifname_destination);
%     fprintf('Sub = %02d/%02d \n',i,numel(sub));
%   end
%   
%   
% end

%%
do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'

path2check = fullfile(config.path,['preproc' do_trans_suffix],'track_artefact_rejected');
allfile = dir(fullfile(path2check,'*_allblocks_artdet_tracking.mat'));

Nfile = numel(allfile);
M = struct();
M.ntrials_postart = nan(Nfile,10);
M.muscle = nan(Nfile,10);
M.blink  = nan(Nfile,10);

for i = 1:Nfile
  load(fullfile(path2check,allfile(i).name));
  
  nblock = size(M_artdet.ntrials_postart,2);
  M.ntrials_postart(i,1:nblock) = M_artdet.ntrials_postart;
  M.muscle(i,1:nblock) = M_artdet.muscle;
  M.blink(i,1:nblock)  = M_artdet.blink;
  
end




