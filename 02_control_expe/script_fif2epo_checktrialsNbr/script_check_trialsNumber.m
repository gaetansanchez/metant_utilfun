
clear all;

config = addmypaths_gs('metaNT_v2');

do_trans_suffix = []; % [] or '_trans_sss'
pathpreproc_artdef = fullfile(config.path,['preproc' do_trans_suffix],'track_artefact_rejected');

%% Compute number of trials remaining after artefact rejection
%--------------------------------------------------------------

% Run ==> condor_01bisbis_Do_trialsNb_count_after_reject

%% Save big matrix with all subjects trials number
%--------------------------------------------------
nsub = 1:21;

Numtr =[];
for i = 1:numel(nsub)
  
  iSub = nsub(i);
  
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)])
  
  filepoc = dir(fullfile(pathpreproc_artdef,sprintf('Numtrial_percond_afterReject_s%02d.mat',iSub)));
  
  fileName = fullfile(pathpreproc_artdef,filepoc.name);
  
  % Load and reshape trial length
  %-------------------------------
  disp(' ');
  disp(['Loading: ' fileName])
  
  load(fileName);
  
  Numtr(i,:) = [iSub CondtrNumber(1,:) CondtrNumber(2,:) CondtrNumber(3,:) CondtrNumber(4,:)];% hit / miss / supra / sham
  
end

save(fullfile(config.path,['preproc' do_trans_suffix],'Numtrial_percondsuj_afterReject_allsubjects.mat'),'Numtr');

%% Plot remaining trials
%-----------------------

load(fullfile(config.path,['preproc' do_trans_suffix],'Numtrial_percondsuj_afterReject_allsubjects.mat'))

Numtr
dat2plot = Numtr(:,2:7);
A = (dat2plot<30);

badsub_meg = find(sum(A,2));

figure; set(gcf,'color','w');
imagesc(dat2plot)

%% Save big matrix with all subjects trials number - respcontrol
%--------------------------------------------------
nsub = Do_sel_subjects(config,'metaNT_v2');

Numtr =[];
for i = 1:numel(nsub)
  
  iSub = nsub(i);
  
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)])
  
  filepoc = dir(fullfile(pathpreproc_artdef,sprintf('Numtrial_percond_afterReject_respcontrol_s%02d.mat',iSub)));
  
  fileName = fullfile(pathpreproc_artdef,filepoc.name);
  
  % Load and reshape trial length
  %-------------------------------
  disp(' ');
  disp(['Loading: ' fileName])
  
  load(fileName);
  
  Numtr(i,:) = [iSub CondtrNumber]; 

end

save(fullfile(config.path,['preproc' do_trans_suffix],'Numtrial_percondsuj_afterReject_respcontrol_allsubjects.mat'),'Numtr');

%% Organize table for publication 
%------------------------------------
clear all;
config = addmypaths_gs('metaNT_v2');

do_trans_suffix = []; % [] or '_trans_sss'
path_trialcountfile = fullfile(config.path,['preproc' do_trans_suffix]);

Numtr = [];
load(fullfile(path_trialcountfile,'Numtrial_percondsuj_afterReject_allsubjects.mat'));
Mall = Numtr;
load(fullfile(path_trialcountfile,'Numtrial_percondsuj_afterReject_respcontrol_allsubjects.mat'));
Mresp = Numtr;

% remove bad subject in Mall and remove first column
nsub = Do_sel_subjects(config,'metaNT_v2');
Mall = Mall(nsub,:);
if size(Mall,2) == 13
Mall(:,1) = [];
end
if size(Mresp,2) == 13
Mresp(:,1) = [];
end

% organize table
Nsub = size(Mall,1);
idsens = 0:2;
Mfinal = [];
for i = 1:Nsub
  
  for j = 1:length(idsens)
    addidsens = idsens(j);
    Minval_hitmiss     = min(Mall(i,[1+1*addidsens 4+1*addidsens]));
    Minval_catchsh     = min(Mall(i,[7+1*addidsens 10+1*addidsens]));
    Minval_hitmissresp = Mresp(i,1+4*addidsens);
    
    Mfinal(i,[1:3]+3*addidsens) = [Minval_hitmiss Minval_catchsh Minval_hitmissresp];
  end

end



