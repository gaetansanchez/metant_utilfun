%% script read and manual clean epoched file

% My path
%---------
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%% Set paths
%------------
sub = get_subject_mat(config); % get all subject code and fifpath


%%
for iSub = 1:numel(sub)
  
  % Get fif-name
  %---------------
  idsubcode = sub(iSub).code;
  idsubnum = sub(iSub).nb;
  idblock = 1;
  do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'
  pathpreproc = fullfile(config.path,['preproc' do_trans_suffix]);
  all_fileName = fullfile(sub(iSub).pathfif,...
    sprintf('%s_s%02d_b%02d_*%s.fif',idsubcode,idsubnum,idblock,do_trans_suffix));
  filetmp = dir(all_fileName);
  fifname = fullfile(filetmp(1).folder,filetmp(1).name);
  
  % run interactive artefact summary rejection
  %--------------------------------------------
  badchan = sub(iSub).badchan;
  reject_visual_allblocks(pathpreproc,iSub,badchan,fifname)
  
end



