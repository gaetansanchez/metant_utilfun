function [data] = do_epoch_fiffile(isub,epoch_time,filthp,pathpreproc,do_trans_suffix)
%
% idsub - subject number
% allblock - vector of all block number to process and append
% epoch_time - vector of time before and after target onset for epoc (in sec)
% filthp - highpass filter in Hz
% pathpreproc - where the data are saved
% do_trans_suffix - [] or '_trans_sss' (i.e. without or with maxfilter fiffile)

%% My path
%---------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%% Set paths
%------------
sub = get_subject_mat(config); % get all subject code and fifpath

idsubnum  = sub(isub).nb;
idsubcode = sub(isub).code;
allblock  = sub(isub).blocks;

%%
M_artdet = [];
data_set = {};
for ib = 1:numel(allblock)
  
  idblock = allblock(ib);
  
  %% Get files
  %------------
  all_fileName = fullfile(sub(isub).pathfif,...
    sprintf('%s_s%02d_b%02d_*%s.fif',idsubcode,idsubnum,idblock,do_trans_suffix));
  
  filetmp = dir(all_fileName);
  nfiles  = numel(filetmp); % number of files under this name
  
  if nfiles > 1
    if strcmp(idsubcode,'19821223KRHR')
      % first fif-file of block02 was stopped in middle of acquisition
      fifname = fullfile(filetmp(2).folder,filetmp(2).name);
    else
      disp(['Filename = ' all_fileName]);
      fifname = fullfile(filetmp(1).folder,filetmp(1).name);
      disp('More than one file under this name... take the first one (Please check)');
    end
    
  else
    fifname = fullfile(filetmp(1).folder,filetmp(1).name);
  end
  
  
  
  %% Get some behavioral file
  %----------------------------
  fileName_beh_tmp = fullfile(config.path,'data_beh',...
    sprintf('metaNT_sub%02d',idsubnum),...
    sprintf('metaNT_sub%02d_block%02d_*',idsubnum,idblock));
  filetmp = dir(fileName_beh_tmp);
  if numel(filetmp) == 1
    fileName_beh = fullfile(filetmp.folder,filetmp.name);
  else
    disp(['FileName = ' fileName_beh_tmp]);
    error('there is more than one file under this name...');
  end
  
  data_beh = load(fileName_beh);
  
  %% Do epoching
  %---------------
  cfg = [];
  cfg.dataset = fifname;
  cfg.eventformat  = 'neuromag_fif';
  cfg.trialfun     = 'ft_trialfun_metaNT_v2';
  cfg.trialdef.prestim        = epoch_time(1);
  cfg.trialdef.poststim       = epoch_time(end);
  cfgnew = ft_definetrial(cfg);
  
  %% Check number of trials in fif and behavioral data
  %----------------------------
  ntrials = size(cfgnew.trl,1);
  if ntrials ~= numel(data_beh.trials)
    disp(['fif_file = ' fileName]);
    disp(['beh_file = ' fileName_beh]);
    error('Trials number in beh_file and fif_file are not similar...');
  end
  
  %% Get back old trigger code
  %----------------------------
  % Old metaNT:
  %%%%%%%%%%%%%
  % condlabel = {'hit' 'miss' 'sham' 'supra'};
  % tr2       = [34 32 12 54];
  for i = 1:ntrials
    
    % response type (detection or not) accroding to circle color
    %-----------------------------------------------------------
    if data_beh.trials{i}.detection == 1
      cfgnew.trl(i,4) = cfgnew.trl(i,4)+4; % detection
    else
      cfgnew.trl(i,4) = cfgnew.trl(i,4)+2;
    end
    
    % type of response screen color (yellow (1) or blue (2))
    %------------------------------------------------------
    if strcmp(data_beh.trials{i}.response_color,'yellow')
      cfgnew.trl(i,10) = 1;
    else
      cfgnew.trl(i,10) = 2;
    end
    
  end
  
  % change target trigger according to trigger response
  % + add sensory modality code = +100 AUD / +200 VIS / +300 TAC
  %---------------------------------------------------------------
  if strcmp(do_trans_suffix,'_trans_sss')
    sensmod = fifname(end-16:end-14);
  else
    sensmod = fifname(end-6:end-4);
  end
  switch sensmod
    case 'aud'
      addtrigger = 100;
    case 'vis'
      addtrigger = 200;
    case 'tac'
      addtrigger = 300;
  end
  cfgnew.trl(:,4) = cfgnew.trl(:,4) + addtrigger;
  
  %% Change trial indexing for block
  if ib == 1
    last_blocktr = 0;
  else
  end
  cfgnew.trl(:,5) = cfgnew.trl(:,5) + last_blocktr;
  last_blocktr = cfgnew.trl(end,5); % keep track of last trial index for next block
  
  %% filter continuous data
  % do definetrial for continuous data...
  cfg = [];
  cfg.dataset = fifname;
  cfg.trialdef.triallength = Inf; % this is the trick: infinite trial length...
  cfg.trialdef.ntrials = 1;       % ...for one trial!
  cfg = ft_definetrial(cfg);
  
  if ~isempty(filthp)
    %   cfg.hpfreq = filthp;
    %   cfg.hpinstabilityfix =  'split';
    cfg.hpfilter = 'yes';
    cfg.hpfreq = filthp;
    cfg.hpfilttype = 'firws';
    cfg.hpfiltdf = filthp;
    cfg.hpfiltwintype = 'kaiser';
  end
  cfg.channel  = {'MEG'}; %all together
  alldata = ft_preprocessing(cfg);
  
  %% now epoch
  data_raw = ft_redefinetrial(cfgnew, alldata);
  
  %% Remove bad trials based on artefact detection
  [p,f,e] = fileparts(fifname);
  fileName_artifact = fullfile(config.path,'artefact_detection',sprintf('%s_artdet.mat',f));
  load(fileName_artifact);
  
  % remove some artefact detected
  cfg_art.artfctdef = rmfield(cfg_art.artfctdef,'jump_GRAD');
  cfg_art.artfctdef = rmfield(cfg_art.artfctdef,'jump_MAG');
  cfg_art.artfctdef = rmfield(cfg_art.artfctdef,'blinkEOG');
  
  % Do rejection
  cfg_art.artfctdef.reject = 'complete';
  data_raw = ft_rejectartifact(cfg_art,data_raw);
  
  % Keep track of number of trials
  M_artdet.ntrials_postart(ib) = numel(data_raw.trial);
  M_artdet.artfctdef{ib} = cfg_art.artfctdef;
  A = fields(cfg_art.artfctdef);
  for j = 1:numel(A)-1 % do not take last field because its reject option 
    M_artdet.(A{j})(ib) = size(cfg_art.artfctdef.(A{j}).artifact,1);
  end
  
  %% downsample to 512hz (save space)
  cfg = [];
  cfg.resamplefs = 100; % 512
  cfg.detrend = 'no';
  data_raw = ft_resampledata(cfg, data_raw);
  
  data_set{ib} = data_raw;
  
  %% saving
  saveName = sprintf('%02d_b%02d_%s.mat',idsubnum,idblock,sensmod);
  
  save(fullfile(pathpreproc, saveName),'-v7.3','-struct', 'data_raw');
end

%% Append data
%-------------
data = ft_appenddata([],data_set{:}); % append blocks to one file
data.grad = data_set{1}.grad;


%% Save files
saveName = fullfile(pathpreproc,sprintf('clean_%02d_allblocks.mat',idsubnum));

outdir_art = fullfile(pathpreproc,'track_artefact_rejected');
if ~exist(outdir_art,'dir') % create output dir if necessary
  mkdir(outdir_art);
end
saveName_arttrack = fullfile(outdir_art,sprintf('%02d_allblocks_artdet_tracking.mat',idsubnum));


disp(' ');
disp(['Saving: ' saveName])
disp(' ');

save(saveName,'-v7.3','-struct', 'data');
save(saveName_arttrack,'M_artdet');

% Deleted previous file
%-----------------------
system(['rm ' fullfile(pathpreproc,[sprintf('%02d_b',idsubnum),'*'])]);



