function [Suj] = Do_sel_subjects(config,expe)



switch expe
  
  case 'metaNT_v2'
    
    Suj = 1:21;
    BadSuj = [];
    do_trans_suffix = []; % [] or '_trans_sss'
    
    % Keep only subject with OK behavioral data (see SCRIPT_behavior)
    %--------------------------------------------
    load(fullfile(config.path,'BadsubjectsBehav'));
    Suj(BadSuj) = []; % remove bad subjects based on behavior
    
    % Keep only subject data with a sufficient number of trials (see check_trialsNumber.m)
    %--------------------------------------------------------------
    load(fullfile(config.path,['preproc' do_trans_suffix],'Numtrial_percondsuj_afterReject_allsubjects.mat'));
    Numtr(BadSuj,:) = [];
    A = Numtr(:,2:7)<30;% find numtrials above 30 for each conditions
    
    Suj(find(sum(A,2))) = [];
    
  case 'metaNT_v2_testwo17'
    
    Suj = 1:21;
    BadSuj = [];
    do_trans_suffix = []; % [] or '_trans_sss'
    
    % Keep only subject with OK behavioral data (see SCRIPT_behavior)
    %--------------------------------------------
    load(fullfile(config.path,'BadsubjectsBehav'));
    BadSuj = [BadSuj;17];
    Suj(BadSuj) = []; % remove bad subjects based on behavior
    
    % Keep only subject data with a sufficient number of trials (see check_trialsNumber.m)
    %--------------------------------------------------------------
    load(fullfile(config.path,['preproc' do_trans_suffix],'Numtrial_percondsuj_afterReject_allsubjects.mat'));
    Numtr(BadSuj,:) = [];
    A = Numtr(:,2:7)<30;% find numtrials above 30 for each conditions
    
    Suj(find(sum(A,2))) = [];
    
    
  otherwise
    error('expe is unknown ...')
    
end