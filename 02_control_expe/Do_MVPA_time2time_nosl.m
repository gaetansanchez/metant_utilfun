function [ds_result] = Do_MVPA_time2time_nosl(datafile,train_modality,test_modality,opt,chan_types,savefilename,sensmod)
% Do time generalization without searchlight over sensors or sources
% Based on previously time lock ERF analysis data saved in datafile 
% (output from: Do_erf4cosmoMVPA.m)
%---------
% INPUTS
%----------
% datafile        = epoched data in CosmoMVPA format
% train_modality  = number of sensory modality used for training
% test_modality   = number of sensory modality used for testing
% opt             = opt for decoding (see cosmo_searchlight) 
%     for instance:
%            opt = struct();
%            opt.classifier = @cosmo_classify_svm;
%            opt.normalization = 'zscore';
%            opt.dimension = 'time';
%            opt.measure = @cosmo_crossvalidation_measure;
% chan_types      = channels type to use:
%                   if source level   --> {'SRC'}
%                   if channels level --> {'meg_axial'} or {'meg_planar_combined'} or {'meg_axial','meg_planar_combined'}
% savefilename    = output path and filename
% sensmod         = {'aud' 'vis' 'tac'} or {'all'} 
%                   {'all'} -> just decode hit vs miss across all sensmod (with equal number of targets)
%
%----------------------
% Gaetan - 21/01/2017

%% init obob
%------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Load cosmoMVPA data structure (output from: Do_srcERF4cosmoMVPA.m)
%--------------------------------
[~, fname] = fileparts(datafile);
load(datafile);

%% Identify data type (SRC or channels)
%--------------------------------------
if strcmp(chan_types{1},'SRC')
  doSRC = 1; % apply t2t sources level
else
  doSRC = 0; % apply t2t channels level
end

if strcmp(sensmod{1},'all')
  doallsensmod = 1;
else
  doallsensmod = 0;
end

%% Compute t2t MVPA
%------------------------------------------------------

if doallsensmod
  % select only trials to make equal number of targets for each sensmod (see Do_srcERF4cosmoMVPA)
  %----------------------------------------------------------------------------------------------
  msk  = cosmo_match(ds.sa.equal_hit_miss_trials,1);
  ds_c = cosmo_slice(ds,msk);
else
  % select data in train and test modality
  msk  = cosmo_match(ds.sa.modality,[train_modality test_modality]);
  ds_c = cosmo_slice(ds,msk);
end

if doSRC
  
  ds_c = cosmo_dim_prune(ds_c,'matrix_labels',{'pos'}); % update dim attributes
  
else
  
  ds_c = cosmo_dim_prune(ds_c); % update dim attributes
  
  % separate or not magnetometers and gradiometers cmb
  %---------------------------------------------------
  % chan_types={'meg_axial','meg_planar_combined'};
  use_chan_type = chan_types;
  
  % get layout for mag or planar labels, and select only those channels
  ds_chan_types = cosmo_meeg_chantype(ds_c);
  ds_chan_idxs  = find(cosmo_match(ds_chan_types,use_chan_type));
  feature_msk   = cosmo_match(ds_c.fa.chan,ds_chan_idxs);
  
  ds_c = cosmo_slice(ds_c, feature_msk, 2);
  ds_c = cosmo_dim_prune(ds_c);
end

% Adapt chunks values 
%----------------------
if train_modality==test_modality
  
  % within-modality cross-validation
  %---------------------------------
  
  % Redefine chunks
  %-----------------
  ds_c.sa.chunks = (1:size(ds_c.samples,1))';
  nchunks=2;
  ds_c.sa.chunks=cosmo_chunkize(ds_c,nchunks);
  
  ds_sel = ds_c;
  
else
  % cross-modality decoding
  %------------------------
  
  % select data in train and test modality
  %-----------------------------------------
  msk_train = cosmo_match(ds_c.sa.modality,train_modality);
  msk_test  = cosmo_match(ds_c.sa.modality,test_modality);
  
  ds_sel_train = cosmo_slice(ds_c,msk_train);
  ds_sel_test  = cosmo_slice(ds_c,msk_test);
  
  % Redefine chunks (chunk = 1 -> train // chunk = 2 -> test)
  %-----------------------------------------------------------
  ds_sel_train.sa.chunks = ones(size(ds_sel_train.samples,1),1);
  ds_sel_test.sa.chunks = 2*ones(size(ds_sel_test.samples,1),1);
  
  % Append data
  %---------------
  ds_sel = cosmo_stack({ds_sel_train, ds_sel_test});
  
end


% Remove features with NaN, Inf or non informative (similar data for all targets)
%-------------------------------------------------------------------------------
% ds_sel = cosmo_remove_useless_data(ds_sel);


% make 'time' a sample dimension
% (this necessary for cosmo_dim_generalization_measure)
%---------------------------------------------------------
ds_time = cosmo_dim_transpose(ds_sel,'time',1);


% Measure
%----------
ds_result = cosmo_dim_generalization_measure(ds_time,opt);

%% save
%-------
save(savefilename,'-v7.3','ds_result');
