%% SCRIPT call condor for clean data

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

Pathlogs = fullfile(config.path,['jobs/artdet/' date]);

%% Set parameters
%-----------------
sub = get_subject_mat(config); % get all subject code and fifpath
Nsub   = numel(sub);

do_trans_suffix = '_trans_sss'; % [] or '_trans_sss'
do_interactive = 'no';
do_visu = 0;

outDir   = fullfile(config.path,'artefact_detection');
if ~exist(outDir,'dir') % create output dir if necessary
  mkdir(outDir);
end

%% Condor call
%---------------
cfg = [];
cfg.mem       = '50G';
cfg.jobsdir   = Pathlogs;
cfg.java      = true;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
for isub = 1:Nsub
  
  % Read and detect artefact
  %---------------------------
  all_fileName = fullfile(sub(isub).pathfif,sprintf('%s_s%02d_*%s.fif',sub(isub).code,sub(isub).nb,do_trans_suffix));
  filetmp = dir(all_fileName);
  Nblocks = numel(filetmp); % number of blocks
  
  for iblock = 1:Nblocks
    fileName = fullfile(filetmp(iblock).folder,filetmp(iblock).name);
    [p,f,e] = fileparts(fileName);
    saveName = fullfile(outDir,sprintf('%s_artdet.mat',f));
    if ~exist(saveName,'file')
      condor_struct = obob_condor_addjob(condor_struct,'do_artefact_detection',fileName,saveName,do_interactive,do_visu);
    else
      disp(['File already exist = ' saveName]);
    end
  end
  
end

%% Submit
%---------
obob_condor_submit(condor_struct);