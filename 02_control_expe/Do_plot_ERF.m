function [] = Do_plot_ERF(erf,Nsub,channel,allcond)

% accept erf structure with all individual average (i.e. allsub20_avg_erf.mat)

sensmod ={'tac' 'aud' 'vis'};
if nargin < 4
  allcond = 0;
end

if allcond
  trig = {'hit' 'miss' 'supra'};
  color_trig = {'b' 'r' '--g'};
else
  trig = {'hit' 'miss'};
  color_trig = {'b' 'r'};
end
%Nsub = 1:10;
%channel = 'GRC'; % 'GRC', 'MAG'

cuttime = [-.2 .9];

switch channel
  case 'GRC'
    valax = [-.5 1];
  case 'MAG'
    valax = [-10 60];
end

idplot = 1; % starting plot index

figure;
set(gcf, 'Color', [1 1 1]);

for i = 1:numel(sensmod)
  for j = 1:numel(Nsub)
    
    
    for k = 1:numel(trig)
      data{k} = erf{Nsub(j)}.(sensmod{i}).(trig{k});
      
      
      if ~isempty(data{k})
        
     
      % Cut timing
      %------------
      cfg = [];
      cfg.latency = cuttime;
      data{k} = ft_selectdata(cfg,data{k});
      
      % Combine Grad
      %--------------
      try
      data{k} = ft_combineplanar([],data{k});
      catch
      end
      
      % Select sensors
      %-----------------
      chan_names = meg_select_sensors(channel); % ATTENTION: MAG -->  1.0e-13; GRC -->  1.0e-12
      [~,indchan]=intersect(data{k}.label,chan_names);
      
      if strcmp(channel,'MAG')
        data{k}.avg = sqrt(mean(data{k}.avg(indchan,:).^2,1)).*1e15; % femto-Tesla
      else
        data{k}.avg = sqrt(mean(data{k}.avg(indchan,:).^2,1)).*1e12; % pico-Tesla
      end
      
      % Baseline correction
      %---------------------
      prestim = find(data{k}.time < 0);
      data{k}.avg = data{k}.avg - mean(data{k}.avg(prestim));
      
      else
        data{k}.avg = 0;
        data{k}.time = 0;
      end
      
      % Subplot
      %----------
      subplot(numel(sensmod),numel(Nsub),idplot);
      plot(data{k}.time,data{k}.avg,color_trig{k},'linewidth',2);
      hold on;
      
    end
    
    set(gca, 'FontSize', 10,'FontWeight','bold');
    title(['Sub=' num2str(Nsub(j)) ' / ' sensmod{i}],'fontsize', 10,'FontWeight','bold');
    %ylim(valax);
    %xlim(cuttime);
    
    if i == numel(sensmod) && j == numel(Nsub)
      legend(trig);
    else
    end
    
    idplot = idplot +1;
  end
  
end





