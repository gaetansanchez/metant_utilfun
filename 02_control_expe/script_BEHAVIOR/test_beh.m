
allsubdata = {FAdet_ok NTdet2p SUdet_ok};

figure;set(gcf,'color','white');
Xax = 1:numel(sensmod);

M = [mean(allsubdata{1},1,'omitnan')' mean(allsubdata{2},1,'omitnan')' mean(allsubdata{3},1,'omitnan')'];
S = [std(allsubdata{1},1)' std(allsubdata{2},1)' std(allsubdata{3},1)'];

%M = [mean(FAdet_ok,1)' mean(NTdet2p,1)' mean(SUdet_ok,1)'];
%S = [std(FAdet_ok,1)' std(NTdet2p,1)' std(SUdet_ok,1)'];

bar_handle = bar(Xax,M,'grouped');

colorFace = {[1,1,1] [.7,.7,.7] [.35,.35,.35]};
lineW = 3;

set(bar_handle(1),'FaceColor',colorFace{1},'LineWidth',lineW);
set(bar_handle(2),'FaceColor',colorFace{2},'LineWidth',lineW);
set(bar_handle(3),'FaceColor',colorFace{3},'LineWidth',lineW);

legend({'Sham';'NT';'Catch'},'Location','EastOutside','FontSize',30,'FontWeight','bold');
hold on
numgroups = size(M, 1);
numbars = size(M, 2);
groupwidth = min(0.8, numbars/(numbars+1.5));
for i = 1:numbars
  
  % Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
  %-------------------
  x = (1:numgroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*numbars);  % Aligning error bar with individual bar
  
  % plot single subjects data
  %--------------------------
  addr = repmat(x,1,size(allsubdata{1},1)); %the x axis location
  addr = addr+(rand(size(addr))-0.5)*0.1; %add a little random "jitter" to aid visibility
  
  plot(addr,reshape(allsubdata{i}',1,numgroups*size(allsubdata{1},1)),'o','LineWidth',2,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','w',...
    'MarkerSize',10);
  
  % plot errorbar
  %--------------
  errorbar(x, M(:,i), S(:,i), 'k', 'linestyle', 'none','LineWidth',lineW);
end

set(gca,'Ylim',[0 1],'XTickLabel',sensmod,'FontSize',30,'FontWeight','bold');
xlabel('sensory modality','FontSize',25,'FontWeight','bold');
ylabel('Detection Rate','FontSize',25,'FontWeight','bold');
title('Behavioral data','FontSize',25,'FontWeight','bold');
