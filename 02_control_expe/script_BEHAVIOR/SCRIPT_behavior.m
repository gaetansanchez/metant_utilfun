% script analysis behavior

clear all;

% My path
%---------
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT_v2',cfg);

%%% Set paths
%------------
sub = get_subject_mat(config); % get all subject code and fifpath

pathbehavior = fullfile(config.path,'data_beh');

blocktype = 'blocks'; % 'blocks' or 'blocks_OK'
savefile = fullfile(pathbehavior,['Data_beh_' blocktype '_Nsub' num2str(numel(sub)) '.mat']);

%% Find block where stim intensity is identical (because staircase adaptation)
%---------------------------------------------------------------------------

stim_int = {};
sensmod_code  = [1 2 3];
sensmod_label = {'aud' 'vis' 'tac'};
for i = 1:numel(sub)
  
  Nsub = sub(i).nb;
  disp(['Processing ',sprintf('sub%02d_',Nsub)]);
  
  pathsuj = fullfile(pathbehavior,sprintf('metaNT_sub%02d',Nsub));
  Nblock = sub(i).blocks; % take all blocks
  
  for j = 1:numel(Nblock)
    
    % Define file
    %-------------
    Fname = ['metaNT_',sprintf('sub%02d_',Nsub),sprintf('block%02d_',Nblock(j)),'*'];
    filetmp = dir(fullfile(pathsuj,Fname));
    
    if numel(filetmp) > 1 || isempty(filetmp)
      error(['Check if thereis more than one file : ' Fname]);
    end
    
    filename = fullfile(pathsuj,filetmp.name);
    sensmod = filename(end-6:end-4);
    
    % Load file
    %-----------
    clear trials exp_cfg;
    load(filename);
    stim_int{i}(1,j) = sensmod_code(strcmp(sensmod_label,sensmod));
    stim_int{i}(2,j) = exp_cfg.thresh;
    
  end
end

% get correct block (with similar stim intensity)
%-------------------------------------------------
all_ok_block = {};
avg_stim_persuj_persensmod = [];
for i = 1:numel(sub)
  
  ok_block = [];
  for j = 1:3 % check all sensmod individually
    
    id_blocksens = find(stim_int{i}(1,:) == j); % get block number for one sensmod
    stim_int2check = stim_int{i}(2,id_blocksens);
    
    avg_stim_persuj_persensmod(i,j) = mean(stim_int2check);
    if isempty(find(diff(stim_int2check)==0)) % if there is no similar value
      id_block2keep = length(id_blocksens); % take last block indice
    else
      [c,ia,ic] = unique(stim_int2check); % get unique value
      mfv = mode(ic); % find most frequent value
      id_block2keep = find(ic == mfv); % find indices of block with most frequent value
    end
    
    ok_block = [ok_block id_blocksens(id_block2keep)]; % compile ok blocks
  end
  
  all_ok_block{i} = sort(ok_block);
  
end


% save file
%------------
savefile = fullfile(pathbehavior,['Stim_intensity_perBlock_Nsub' num2str(numel(sub)) '.mat']);
%save(savefile,'stim_int','all_ok_block');

%% Extract data of interest
%--------------------------

Tab = {};
n = 1;
for i = 1:numel(sub)
  
  Nsub = sub(i).nb;
  disp(' ');
  disp(['Processing ',sprintf('sub%02d_',Nsub)]);
  disp('****************************');
  
  % Define subject path
  %-------------------
  pathsuj = fullfile(pathbehavior,sprintf('metaNT_sub%02d',Nsub));
  
  Nblock = sub(i).(blocktype); % take relevant blocks
  
  for j = 1:numel(Nblock)
    
    % Define file
    %-------------
    Fname = ['metaNT_',sprintf('sub%02d_',Nsub),sprintf('block%02d_',Nblock(j)),'*'];
    filetmp = dir(fullfile(pathsuj,Fname));
    
    if numel(filetmp) > 1 || isempty(filetmp)
      error('Check if the file...');
    end
    
    filename = fullfile(pathsuj,filetmp.name);
    sensmod = filename(end-6:end-4);
    
    
    % Load file
    %-----------
    clear trials exp_cfg;
    load(filename);
    
    % Extract data of interest
    %-------------------------
    for k = 1:numel(trials)
      
      Tab{n,1} = Nsub; % sub number
      Tab{n,2} = j; % block number
      Tab{n,3} = sensmod; % sensory modality
      Tab{n,4} = trials{k}.stim_trigger; % target trigger (sham=1, NT=3,supra=5)
      Tab{n,5} = trials{k}.detection; % response (3=noresp, 1=det, 0=undet)
      Tab{n,6} = trials{k}.timestamps.RT; % reaction time
      Tab{n,7} = cell2mat(trials{k}.response); % response or not
      Tab{n,8} = trials{k}.response_color; % color of response screen
      
      if exp_cfg.sub.blue_press_if_detected % what is th "GO" color
        Tab{n,9} = 'blue';
      else
        Tab{n,9} = 'yellow';
      end
      
      n = n+1;
      
    end
  end
end

% save file
%------------
savefile = fullfile(pathbehavior,['Data_beh_' blocktype '_Nsub' num2str(numel(sub)) '.mat']);
save(savefile,'Tab');

%% Analysis
%------------

load(savefile);

% global values
%---------------
idall = find(cell2mat(Tab(:,1))); % all trials
idNT  = find(cell2mat(Tab(:,4)) == 3);
idsham= find(cell2mat(Tab(:,4)) == 1);
idsupr= find(cell2mat(Tab(:,4)) == 5);
iddet = find(cell2mat(Tab(:,5)) == 1);
idudet= find(cell2mat(Tab(:,5)) == 0);
iddetreport  = union(iddet,idudet);
idNTresp= intersect(idNT,iddetreport);% only when there is an answer recorded
idNThit = intersect(idNTresp,iddet);
idNTmiss= intersect(idNTresp,idudet);
idFA    = intersect(idsham,iddet);
idsuhit = intersect(idsupr,iddet);
    
idtrialcolor_y = find(strcmp(Tab(:,8),'yellow'));
idtrialcolor_b = find(strcmp(Tab(:,8),'blue'));
idcontextgocolor_y    = find(strcmp(Tab(:,9),'yellow'));
idcontextgocolor_b    = find(strcmp(Tab(:,9),'blue'));

idgotrial_context_y   = intersect(idtrialcolor_y,idcontextgocolor_y);
idnogotrial_context_y = intersect(idtrialcolor_b,idcontextgocolor_y);
idgotrial_context_b   = intersect(idtrialcolor_b,idcontextgocolor_b);
idnogotrial_context_b = intersect(idtrialcolor_y,idcontextgocolor_b);
idgotrial   = union(idgotrial_context_y,idgotrial_context_b);
idnogotrial = union(idnogotrial_context_y,idnogotrial_context_b);


idtrials_type_context = {idall idgotrial idnogotrial idtrialcolor_y idtrialcolor_b};
sensmod = { 'aud' 'vis' 'tac'};

NTdet = [];
NTdet_Wresp = [];
RThit = [];
RTmiss= [];
RTall = [];
for i = 1:numel(sub)
  
  idsuj = find(cell2mat(Tab(:,1)) == sub(i).nb);
  
  for j = 1:numel(sensmod)
    
    idsens = find(strcmp(Tab(:,3),sensmod{j}));
    
    for k = 1:numel(idtrials_type_context)
      
      idsuj_sens = intersect(intersect(idsuj,idsens),idtrials_type_context{k});
      
      % id subject data
      %-------------------
      idsuj_sensNThit = intersect(idsuj_sens,idNThit);
      idsuj_sensNTmiss= intersect(idsuj_sens,idNTmiss);
      idsuj_sensFA    = intersect(idsuj_sens,idFA);
      idsuj_senssuhit = intersect(idsuj_sens,idsuhit);
      
      Nbtr_NTresp = length(intersect(idsuj_sens,idNTresp));
      Nbtr_NT     = length(intersect(idsuj_sens,idNT));
      Nbtr_sham   = length(intersect(idsuj_sens,idsham));
      Nbtr_supra  = length(intersect(idsuj_sens,idsupr));
      
      % compute metrics for all trials
      %-------------------------------
      NTdet(i,j,k) = length(idsuj_sensNThit)/Nbtr_NTresp; % detection rate (only when resp)
      SUdet(i,j,k) = length(idsuj_senssuhit)/Nbtr_supra; % SUpra detection rate (all trials : no resp = undet)
      FAdet(i,j,k) = length(idsuj_sensFA)/Nbtr_sham; % FA rate
      
      if FAdet(i,j,k) == 0
        FAdet(i,j,k) = 1/(2*Nbtr_sham); % avoid infinite values for Dprim calculus
      elseif SUdet(i,j,k) == 1
        SUdet(i,j,k) = 1-(1/(2*Nbtr_supra)); % avoid infinite values for Dprim calculus
      end
      
      Dprim(i,j,k) = norminv(NTdet(i,j,k)) - norminv(FAdet(i,j,k));
      DprimSvsS(i,j,k) = norminv(SUdet(i,j,k)) - norminv(FAdet(i,j,k));
      RThit(i,j,k) = mean(cell2mat(Tab(idsuj_sensNThit,6))); % mean RT for Hit
      RTmiss(i,j,k)= mean(cell2mat(Tab(idsuj_sensNTmiss,6))); % mean RT for miss
      RTall(i,j,k) = mean(cell2mat(Tab(idsuj_sens,6))); % RT for all trials
    end
  end
end


save(savefile,'Tab','SUdet','FAdet','Dprim','DprimSvsS','NTdet','NTdet_Wresp','RThit','RTmiss','sensmod');


%% get data from all trials
%-------------------------
NTdet_all = squeeze(NTdet(:,:,1));
FAdet_all = squeeze(FAdet(:,:,1));
SUdet_all = squeeze(SUdet(:,:,1)); 
Dprim_all = squeeze(Dprim(:,:,1)); 
DprimSvsS_all = squeeze(DprimSvsS(:,:,1)); 
RTall_all = squeeze(RTall(:,:,1)); 

%% Find bad subjects
%--------------------
% Based on behavior
%------------------------

limFA = (squeeze(FAdet_all) >= 0.3); % FA limit exceeded
limSU = (squeeze(SUdet_all) <= 0.7); % FA limit exceeded
limDet = (squeeze(NTdet_all) >= 0.85 | squeeze(NTdet_all) <= 0.15); % NT detection too high

limBad = limFA + limDet + limSU

idBadS = [];
idBadS = find(sum(limBad,2)); %

idBadS = [idBadS;18] % subject 18 was bad acquisition

% Save BadSuj for MEG analysis restriction
%--------------------------------------------
if ~exist(fullfile(config.path,'BadsubjectsBehav.mat'))
  BadSuj = idBadS;
  save(fullfile(config.path,'BadsubjectsBehav.mat'),'BadSuj');
end

%% PLOT based on context and clean data
%--------------------------------------
iddatatype = 1; % 1 = all / 2 = go trials / 3 = nogo trials / 4 = trial yellow / 5 = trials blue
doplot_singlesub = 0; % 1 = yes / 0 = no
fname01 = fullfile('/Users/gaetan/Documents/POSTDOC/work/metaNT_v2','figures','Beh_barplot');
%fname01 = []; % dont save fig

load(fullfile(config.path,'BadsubjectsBehav.mat'));
idBadS = BadSuj;

label_datatype = {'all trials' 'GO trials' 'NOGO trials' 'Yellow trials' 'Blue trials'};
label2p = label_datatype{iddatatype};

NTdet2p = squeeze(NTdet(:,:,iddatatype));
NTdet2p(idBadS,:) = [];

RThit2p = squeeze(RThit(:,:,iddatatype));
RThit2p(idBadS,:) = [];

RTmiss2p= squeeze(RTmiss(:,:,iddatatype));
RTmiss2p(idBadS,:) = [];

SUdet_ok = squeeze(SUdet(:,:,iddatatype));
SUdet_ok(idBadS,:) = [];

FAdet_ok = squeeze(FAdet(:,:,iddatatype));
FAdet_ok(idBadS,:) = [];

Dprim_ok = squeeze(Dprim(:,:,iddatatype));
Dprim_ok(idBadS,:) = [];

Nsub_ok = 1:size(NTdet,1);
Nsub_ok(idBadS) = []

% Plot Detection rate
%-----------------------
allsubdata = {FAdet_ok NTdet2p SUdet_ok};

figure;set(gcf,'color','white');
Xax = 1:numel(sensmod);

M = [mean(allsubdata{1},1,'omitnan')' mean(allsubdata{2},1,'omitnan')' mean(allsubdata{3},1,'omitnan')'];
S = [std(allsubdata{1},1,'omitnan')' std(allsubdata{2},1,'omitnan')' std(allsubdata{3},1,'omitnan')'];

%M = [mean(FAdet_ok,1)' mean(NTdet2p,1)' mean(SUdet_ok,1)'];
%S = [std(FAdet_ok,1)' std(NTdet2p,1)' std(SUdet_ok,1)'];

bar_handle = bar(Xax,M,'grouped');

colorFace = {[1,1,1] [.7,.7,.7] [.2,.2,.2]};
lineW = 1;

set(bar_handle(1),'FaceColor',colorFace{1},'LineWidth',lineW);
set(bar_handle(2),'FaceColor',colorFace{2},'LineWidth',lineW);
set(bar_handle(3),'FaceColor',colorFace{3},'LineWidth',lineW);

legend({'Sham';'NT';'Catch'},'Location','EastOutside','FontSize',30,'FontWeight','bold');
hold on
numgroups = size(M, 1);
numbars = size(M, 2);
groupwidth = min(0.8, numbars/(numbars+1.5));
for i = 1:numbars
  
  % Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
  %-------------------
  x = (1:numgroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*numbars);  % Aligning error bar with individual bar
  
  if doplot_singlesub
    % plot single subjects data
    %--------------------------
    addr = repmat(x,1,size(allsubdata{1},1)); %the x axis location
    addr = addr+(rand(size(addr))-0.5)*0.1; %add a little random "jitter" to aid visibility
    
    plot(addr,reshape(allsubdata{i}',1,numgroups*size(allsubdata{1},1)),'o','LineWidth',2,...
      'MarkerEdgeColor','k',...
      'MarkerFaceColor','w',...
      'MarkerSize',10);
  end
  
  % plot errorbar
  %--------------
  errorbar(x, M(:,i), S(:,i), 'k', 'linestyle', 'none','LineWidth',lineW);
end

set(gca,'Ylim',[0 1],'XTick',[],'FontSize',30,'FontWeight','bold');

if isempty(fname01)
  set(gca,'Ylim',[0 1],'XTickLabel',sensmod,'FontSize',30,'FontWeight','bold');
  xlabel('sensory modality','FontSize',25,'FontWeight','bold');
  ylabel('Detection Rate','FontSize',25,'FontWeight','bold');
  title(['Behavioral data ' label2p ' N=' num2str(size(allsubdata{1},1))],'FontSize',25,'FontWeight','bold');
end

set(gcf, 'Position', [0, 0, 1000, 700]);

saveas(gcf,fname01,'fig');
saveas(gcf,fname01,'png');

%% Reaction Time
%-----------------

M = [mean(RThit2p,1,'omitnan')*1000;mean(RTmiss2p,1,'omitnan')*1000];
S = [std(RThit2p,1,'omitnan')*1000;std(RTmiss2p,1,'omitnan')*1000];
errorbar_groups(M,S);
set(gcf,'color','white');
set(gca,'Ylim',[100 1000],'XTickLabel',sensmod,'FontSize',30,'FontWeight','bold');
legend({'Hit';'Miss'},'Location','EastOutside','FontSize',30,'FontWeight','bold')
xlabel('sensory modality','FontSize',25,'FontWeight','bold');
ylabel('Reaction time (ms)','FontSize',25,'FontWeight','bold');
title(['Behavioral data ' label2p],'FontSize',25,'FontWeight','bold');


%% Test stats
sensmod
Mfa = mean(FAdet_ok)
Sfa = std(FAdet_ok)

Msu = mean(SUdet_ok)
Ssu = std(SUdet_ok)

Mdp = mean(Dprim_ok)
Sdp = std(Dprim_ok)

Mnt = mean(NTdet2p)
Snt = std(NTdet2p)
sensmod

disp('*****************');
disp('test NT vs. Catch');
[H,P,CI,STATS] = ttest(NTdet2p,SUdet_ok)

disp('*****************');
disp('test NT vs. Sham');
[H,P,CI,STATS] = ttest(NTdet2p,FAdet_ok)


disp('*****************');
disp('test NT (avg all sensmod) vs. 50%');
MNTdet2p = mean(NTdet2p,2);
testNT = MNTdet2p-0.5;
[H,P,CI,STATS] = ttest(testNT)

disp('*****************');
disp('test NT vs. 50%');
testNT = NTdet2p-0.5;
[H,P,CI,STATS] = ttest(testNT)

%
% NTdetallsens = mean(NTdet2p,2)
% testNTallsens = NTdetallsens-0.5;
% [H,P,CI,STATS] = ttest(testNTallsens)

disp('*****************');
disp('test NT aud vs. NT vis');
[H,P,CI,STATS] = ttest(NTdet2p(:,1),NTdet2p(:,2))
disp('*****************');
disp('test NT vis vs. NT tac');
[H,P,CI,STATS] = ttest(NTdet2p(:,2),NTdet2p(:,3))
disp('*****************');
disp('test NT aud vs. NT tac');
[H,P,CI,STATS] = ttest(NTdet2p(:,1),NTdet2p(:,3))


disp('*****************');
disp('test ANOVA');
sub  = repmat([1:14]',3,1);
Smod = kron([1 2 3],ones(1,14))';
perf = [NTdet2p(:,1);NTdet2p(:,2);NTdet2p(:,3)];

[p,tbl,stats] = anovan(perf,{sub,Smod})

%% Check Bad subject
%--------------------

Nsub = 1:21;

% Based on behavior
%------------------------

%limFA = (DprimSvsS <= 1); % Dprim limit exceeded
limFA = (FAdet >= 0.2); % FA limit exceeded
limDet = (NTdet >= 0.8); % NT detection too high
limBad = limFA + limDet;

idBadS = find(sum(limBad,2));
BadS = Nsub(idBadS)

% Based on trial rejection
%---------------------------
BadSuj = [];
load(fullfile(config.path,'BadsubjectsBehav')); % NTdet > 80% + FAdet < 30%

load(fullfile(config.path,'Numtrial_percondsuj_afterReject_allsubjects.mat'));
Numtr(BadSuj,:) = [];
A = Numtr(:,2:7)<40;% find numtrials above 40 for each conditions (hit and miss only)
Badsuj_meg = sort([Numtr(find(sum(A,2)),1); BadSuj']);


Bmap = zeros(25,3);

Bmap(BadSuj,1) = 1; % based on behavior old
Bmap(Badsuj_meg,2)   = 1; % based on MEG + old behavior
Bmap(BadS,3)   = 1; % based on behavior new


figure;
imagesc(Bmap);


