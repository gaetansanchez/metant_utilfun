function [stat,stat_bl] = Do_srcERF_stat(PathsrcERF,cond1,cond2,toi, avgtoi, correct, method, stattest, outdata,precision_grid)

%% paths
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

% Path saved data
%------------------
[Pathsave,~, ~] = fileparts(outdata); 
if ~exist(Pathsave,'dir')
    mkdir(Pathsave)
else
end


%% SET data
subfiles = dir(fullfile(PathsrcERF,'s*_src_erf.mat')); 
nSub = length(subfiles); 

for iSub = 1:nSub 
  
    % Load data
    %-----------
    [~, sub, ~] = fileparts(subfiles(iSub).name); 
    disp(['--> Load : ' sub]);
    load(fullfile(PathsrcERF,sub))

    cond_bl_1{iSub} = erfbl.(cond1); 
    cond_bl_2{iSub} = erfbl.(cond2); 
    
    cond_1{iSub} = erf.(cond1); 
    cond_2{iSub} = erf.(cond2); 
    
    % absolute value
    %----------------
    cond_1{iSub}.avg = abs(cond_1{iSub}.avg);
    cond_2{iSub}.avg = abs(cond_2{iSub}.avg);
    cond_bl_1{iSub}.avg = abs(cond_bl_1{iSub}.avg);
    cond_bl_2{iSub}.avg = abs(cond_bl_2{iSub}.avg);
 
end

%% Load grid 
%------------
parcellation = [];
if precision_grid == 1.5
  load mni_grid_1_5_cm_889pnts
elseif precision_grid == 2
  load mni_grid_2cm_372pnts
elseif ischar(precision_grid)% if you provide a file name for a parcellation grid
  load(precision_grid); % load parcellation
  template_grid = parcellation.parcel_grid; % parcel grid with centroid position
else
  error(['No precision mni_grid for precision = ' num2str(precision_grid)]);
end


%% Stat
%-------

cfg                     = []; 
cfg.latency             = toi; 
cfg.avgovertime         = avgtoi;
cfg.method              = method; 
cfg.statistic           = stattest; 

cfg.correctm            = correct; 
cfg.tail                = 0; % 0 =  two tailed / 1 = cond1 > cond2
cfg.alpha               = 0.05; % p value (corrected because 2 tails)

cfg.correcttail         = 'prob';% correct in case of 2 tails
cfg.clusteralpha        = 0.05;


cfg.numrandomization    = 10000; % (minimum 500)
cfg.design              = [1:nSub 1:nSub; ones(1,nSub), ones(1,nSub)*2]; % first row defines the subjects, second row defines the conditions
cfg.uvar                = 1; % specifications of subjects in design
cfg.ivar                = 2; % specifications in data (grandaverages) in design (second row)

cfg.minnbchan = 0;

% build an elec-file to make neighbours
cfgneigh.elec.label = cond_1{1}.label;
cfgneigh.elec.chanpos = template_grid.pos(template_grid.inside,:);
cfgneigh.elec.unit = 'cm';
cfgneigh.neighbourdist = 1.8;
cfgneigh.method = 'distance';
cfgneigh.feedback = 'no';
cfg.neighbours = ft_prepare_neighbours(cfgneigh);

stat_bl = ft_timelockstatistics(cfg,cond_bl_1{1:nSub}, cond_bl_2{1:nSub}); 
stat = ft_timelockstatistics(cfg,cond_1{1:nSub}, cond_2{1:nSub}); 

switch avgtoi
  case 'no'
    % check significant time
    %-------------------------
    stat_bl.mask2 = mean(stat_bl.mask,1);
    stat_bl.mask2(stat_bl.mask2>0)=1;
    stat_bl.sigtime = stat_bl.time .* stat_bl.mask2;
    
    stat.mask2 = mean(stat.mask,1);
    stat.mask2(stat.mask2>0)=1;
    stat.sigtime = stat.time .* stat.mask2;
  case 'yes'
end

%% save
%--------
[Pathsave,~, ~] = fileparts(outdata); 
if ~exist(Pathsave,'dir')
    mkdir(Pathsave)
else
end

save(outdata, 'stat','stat_bl')

end

