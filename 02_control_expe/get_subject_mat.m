function [sub] = get_subject_mat(config)
% 
% OUTPUT:
% sub - structure with path and subject specific name of fif file


all_pathfif = fullfile(config.pathfif,'meta_nt','subject_subject');

% subject code / sub number / folder name
%-----------------------------------------
subinfo = {
  '19950801DRAS',1,'171024';... % fake - OK 
  '19930423EEHB',2,'171024';... % MRI - weird coregistration - bad headshape registration...
  '19930819CRBO',3,'171024';... % MRI - OK
  '19940218HIRE',4,'171024';... % MRI - OK
  '19791024EETU',5,'171025';... % MRI - OK
  '19910711EERI',6,'171025';... % fake - OK
  '19961118BRSH',7,'171026';... % MRI - OK
  '19971215TEHR',8,'171026';... % fake - OK
  '19890720ADLH',9,'171026';... % MRI - OK
  '19821223KRHR',10,'171027';... % MRI - OK
  '19970123IGRD',11,'171027';... % fake - Okish... (top hds is not fitting)
  '19970320MRDR',12,'171027';... % fake - Okish... (top hds is not fitting)
  '19861231ANSR',13,'171027';... % MRI - OK
  '19960418GBSH',14,'171101';... % MRI - headshape does not fit with front head???
  '19970801RGHM',15,'171101';... % fake - OKish... (back does not fit)
  '19840206MRLO',16,'171101';... % MRI - OK
  '19910823SSLD',17,'171101';... % MRI - OK
  '19690423DRWA',18,'171103';... % fake - Okish (back does not fit)
  '19970218CRPO',19,'171103';... % fake - OK
  '19810918SLBR',20,'171103';... % MRI - Okish (front and back does not fit)
  '19830114RFTM',21,'171103'     % MRI - Okish (front and back does not fit)
  };

subBlocks = {
    [1:9];...% sub01
    [1:9];...% sub02
    [1:9];...% sub03
    [1:7 9 10];...% sub04
    [1:9];...% sub05
    [1:10];...% sub06
    [1:10];...% sub07
    [1:9];...% sub08
    [1 3 4 6 7 9 10];...% sub09
    [1:9];...% sub10
    [1:9];...% sub11
    [1:9];... % sub12
    [1:10];...    % sub13
    [1:9];...    % sub14
    [1:9];...% sub15
    [1:9];... % sub16
    [1:9];...% sub17
    [1:9];... % sub18
    [1:10];... % sub19
    [1:10];... % sub20
    [1:10];... % sub21
    };
  
% based on stim intensity control (see SCRIPT_behavior)
load(fullfile(config.path,'data_beh','Stim_intensity_perBlock_Nsub21.mat'));
for id = 1:numel(all_ok_block)
  subBlocks_OK{id,1} = all_ok_block{id};
end

% subBlocks_OK = {
%     [2 4 5 6 7 8 9];...% sub01
%     [2 5 7 8 9];...% sub02
%     [1:9];...% sub03
%     [1:7 9 10];...% sub04
%     [1:9];...% sub05
%     [1:10];...% sub06
%     [1:10];...% sub07
%     [1:9];...% sub08
%     [1 3 4 6 7 9 10];...% sub09
%     [1:9];...% sub10
%     [1:9];...% sub11
%     [1:9];... % sub12
%     [1:10];...    % sub13
%     [1:9];...    % sub14
%     [1:9];...% sub15
%     [1:9];... % sub16
%     [1:9];...% sub17
%     [1:9];... % sub18
%     [3 5 6 7 8 9 10];... % sub19
%     [1:10];... % sub20
%     [1:10];... % sub21
%     };
  
badsensors = {
    [];...% sub01
    [];...% sub02
    [];...% sub03
    [];...% sub04
    {'MEG1323' 'MEG0811' 'MEG0812'};...% sub05
    {'MEG1323' 'MEG0811' 'MEG0812'};...% sub06
    [];...% sub07
    [];...% sub08
    [];...% sub09
    {'MEG0811' 'MEG0812'};...% sub10
    {'MEG0811' 'MEG0812'};...% sub11
    {'MEG0811' 'MEG0812'};...% sub12
    {'MEG0811' 'MEG0812'};...% sub13
    [];...% sub14
    [];...% sub15
    [];...% sub16
    [];...% sub17
    [];...% sub18
    {'MEG2623'};...% sub19
    [];...% sub20
    [];...% sub21
    };


sub = [];
for i = 1:size(subinfo,1)
  
  sub(i).code     = subinfo{i,1};
  sub(i).nb       = subinfo{i,2};
  sub(i).pathfif  = fullfile(all_pathfif,subinfo{i,3});
  sub(i).blocks   = subBlocks{i};
  sub(i).blocks_OK = subBlocks_OK{i};
  sub(i).badchan  = badsensors{i};
  
end



