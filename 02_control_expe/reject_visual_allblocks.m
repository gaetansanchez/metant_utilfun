function [] = reject_visual_allblocks(pathpreproc,iSub,badchan,fifname)
% reject visual all blocks
%%% INPUTS
%---------
% pathpreproc - path where are file to process
% iSub        - subject number
% badchan     - cell-array of bads channels labels identified during acquisition or []
% fifname     - full path and filename for fiffile (used in SSP)

%% Path settings
%------------------
% pathpreproc = '/Volumes/obob/staff/gsanchez/metaNT/preproc';


%% Loop
%-------


disp(' ');
disp('**********************');
disp(['Sub = ',sprintf('%02d',iSub)]);

filepoc = dir(fullfile(pathpreproc,[sprintf('%02d_',iSub),'allblocks.mat']));
fileName = fullfile(pathpreproc,filepoc.name);

saveName = fullfile(pathpreproc,['infoclean_',filepoc.name]);

if exist(saveName,'file') ~= 0
  disp(' ');
  disp(['Clean files already exists: ' saveName])
else
  
  
  disp(' ');
  disp(['Loading: ' fileName])
  
  data = load(fileName);
  
  %% Reject badchan (identified during acquisition)
  %------------------
  if ~isempty(badchan)
    disp(' ');
    disp('Reject badchan ...')
    disp(' ');
    
    id_badchan = cellfun(@(x) find(strcmp(data.label,x)),badchan);
    ok_label = data.label;
    ok_label(id_badchan) = [];
    
    cfg = [];
    cfg.channel = ok_label; % keep Good MEG channels
    data = ft_selectdata(cfg,data);
  end
  
  
  %% Apply SSP before summary cleaning
  %-------------------------------------
  disp('  ');
  disp('*****************');
  disp('Apply SSP...');
  disp('  ');
  
  cfg = [];
  cfg.inputfile = fifname;
  data_ssp = obob_apply_ssp(cfg, data);
  
  
  
  %% reject visual on each blocks
  %-------------------------------
  disp(' ');
  disp('Oooh yeah ! Running visual rejection baby !')
  disp(' ');
  
  cfg        = [];
  cfg.latency = [-0.5 0.7];
  cfg.method = 'summary';
  cfg.layout = 'neuromag306mag.lay';
  cfg.channel = 'MEG';
  cfg.gradscale   = 0.04;
  % filtering only for artifact rejection
  cfg.preproc.lpfilter    = 'yes';
  cfg.preproc.lpfilttype  = 'firws';
  cfg.preproc.lpfiltwintype = 'kaiser';
  cfg.preproc.lpfreq      = 40;
  cfg.preproc.lpfiltdf    = 6;
  cfg.preproc.rectify     = 'yes';
  data_ssp_post = obob_rejectvisual(cfg,data_ssp);
  
  %% Store correct/clean data infos
  %----------------------------------
  okmanualrejection.param_rejectvisual.cfg = cfg;
  okmanualrejection.chanlabel = data_ssp_post.label;
  okmanualrejection.trialinfo = data_ssp_post.trialinfo;
  
  
  %% save and delete block files
  disp(' ');
  disp(['Saving: ' saveName])
  disp(' ');
  
  save(saveName,'-v7.3','okmanualrejection');
  
  close all;
end



