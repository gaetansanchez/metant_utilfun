%% Script test parcellation with metaNT data
%---------------------------------------------

clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

% Path definition
Pathsrc = fullfile(config.path2work,'src');
Pathdata = config.path;


%% Create parcel grid & atlas
%----------------------------
pathparcel   = fullfile(config.pathgroup,'gsanchez/util_functions/parcellation');
atlasfilenii = fullfile(pathparcel,'Parcels','Parcels_MNI_333.nii'); % nifti file
atlasfiletxt = fullfile(pathparcel,'Parcels','Parcels_MNI_333_allinf.txt'); % txtfile label

Pathoutput   = pathparcel;

flag = 1; % plot grid
[atlas,parcel_grid] = Do_read_Parcelatlas(atlasfilenii,atlasfiletxt,Pathoutput,flag);

%% Load 
%-------

parcel_grid = load(fullfile(Pathoutput,'parcel_grid_333')); % load parcel_grid and parcel_array output of read_par_atlas

parcel_array = parcel_grid.parcel_array; % mask for 3mm grid based on parcellation

%% Test
%--------
iSub = 1;
precision_grid = fullfile(Pathoutput,'parcel_grid_333');


Pathclean = fullfile(config.path,'preproc'); % Path clean data
Pathsave = fullfile(config.path2work,'src','srcERF_lp30Hz','parcel');

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

Do_srcERF(iSub,Pathsave,Pathclean,precision_grid)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stats & Plot
%--------------
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

%Dirfiles = 'parcel'; % 'parcel' or 'hpfilt_1'
Dirfiles = 'hpfilt_1'; % 'parcel' or 'hpfilt_1'
Dirfiles_dir2 = '1cm_grid'; % '1cm_grid' or []

if ~isempty(Dirfiles_dir2)
    PathSRCfiles = fullfile(config.path2work,'src','srcERF_lp30Hz',Dirfiles,Dirfiles_dir2);
else
    PathSRCfiles = fullfile(config.path2work,'src','srcERF_lp30Hz',Dirfiles);
end

% Load 
%-------
subfiles = dir(fullfile(PathSRCfiles, 's*_src_erf.mat'));
nSub = length(subfiles);
erf = []; % initalize variable 'erf' before so that it is not confused with the builtin function 'erf'
erfbl = [];
for iSub=1:nSub
  [~, sub, ~] = fileparts(subfiles(iSub).name);
  disp(['--> Load : ' sub]);
  load(fullfile(PathSRCfiles,sub));
  
  audhit{iSub}=erfbl.aud_hit;
  audmis{iSub}=erfbl.aud_miss;
  tachit{iSub}=erfbl.tac_hit;
  tacmis{iSub}=erfbl.tac_miss;
  vishit{iSub}=erfbl.vis_hit;
  vismis{iSub}=erfbl.vis_miss;
  
end

% Neighbours structure
%-----------------------
grad = []; 

if strcmp(Dirfiles,'parcel')
  Pathgrid = fullfile(config.pathgroup,'gsanchez','util_functions','parcellation');
  parcel_grid = load(fullfile(Pathgrid,'parcel_grid_333'));
  parcel_grid.inside = logical(ones(numel(parcel_grid.parcel_array),1));
  
  grad.chanpos = parcel_grid.pos;
  grad.label   = parcel_grid.label;
  
else
  
    if strcmp(Dirfiles_dir2,'1cm_grid')
        load mni_grid_1_cm_2982pnts.mat
    else
        load mni_grid_1_5_cm_889pnts.mat
    end
  parcel_grid = template_grid;
  %parcel_grid.inside = logical(ones(numel(parcel_grid.label),1));
  
  grad.chanpos = template_grid.pos(template_grid.inside,:);
  grad.label   = erfbl.vis_hit.label;
  
end


cfg = [];
cfg.method = 'triangulation'; 
cfg.grad   = grad;
neigh      = ft_prepare_neighbours(cfg);

%% Statistics
%------------

cfg                     = []; 
cfg.latency             = [.05 .45]; 
cfg.avgovertime         = 'yes';
cfg.method              = 'montecarlo'; 
cfg.statistic           = 'depsamplesT'; 

cfg.correctm            = 'cluster'; 
cfg.tail                = 0; % 0 =  two tailed / 1 = cond1 > cond2
cfg.alpha               = 0.025; % p value (corrected because 2 tails)

cfg.correcttail         = 'prob';
cfg.clusteralpha        = 0.05;
%cfg.clusterthreshold    = 'nonparametric_common';

cfg.numrandomization    = 1000; % (minimum 500)
cfg.design              = [1:nSub 1:nSub; ones(1,nSub), ones(1,nSub)*2]; % first row defines the subjects, second row defines the conditions
cfg.uvar                = 1; % specifications of subjects in design
cfg.ivar                = 2; % specifications in data (grandaverages) in design (second row)

cfg.minnbchan           = 0;
cfg.neighbours          = neigh;

dat2test = {audhit audmis;vishit vismis;tachit tacmis};
stat_all = {};
for i = 1:3
  stat_all{i} = ft_timelockstatistics(cfg, dat2test{i,1}{:}, dat2test{i,2}{:});
end

%% Create source structure
%-------------------------
dosavefile = 1;
% load MRI
%------------
mri = ft_read_mri('/Volumes/obob/obob_ownft/external/fieldtrip/template/anatomy/single_subj_T1.nii');
mri = ft_convert_units(mri, 'm');
% mri.coordsys='spm';
% cfg=[];
% mri_seg=ft_volumesegment(cfg,mri);
load('/Volumes/obob/staff/ahauswald/mri_seg'); % load segmented mri

% Load atlas 
%-------------
if strcmp(Dirfiles,'parcel')
  
  atlas = load(fullfile(Pathgrid,'atlas_grid_333.mat'));
  atlas = ft_convert_units(atlas, 'm');
  
else
  
  atlas = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
  atlas = ft_convert_units(atlas, 'm');
  
end

%
for i = 1:3
  
  stat_avg = stat_all{i};
  
  cfg = [];
  cfg.latency    = [stat_avg.time(1) stat_avg.time(end)];
  cfg.parameter  = 'stat';
  cfg.sourcegrid = parcel_grid;
  
  src  = obob_svs_virtualsens2source(cfg, stat_avg);
  
  if strcmp(Dirfiles,'parcel')
    src  = rmfield(src, {'xgrid', 'ygrid', 'zgrid', 'dim'});% this is needed, becuase the grid is not equally distributed
  end
  
  % interpolate
  %-------------
  cfg = [];
  cfg.parameter = 'stat';
  inter_src = ft_sourceinterpolate(cfg, src, mri);
  
  % Restrict to inside brin points
  %---------------------------------
  inter_src.stat2    = inter_src.stat.*(mri_seg.white | mri_seg.gray);
  inter_src.coordsys = 'spm';
  
  % plot
  %------
  cfg=[];
  cfg.method     = 'surface';
  %cfg.method     = 'ortho';
  cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
  cfg.colorbar = 'no';
  cfg.camlight = 'no';
  cfg.funparameter  = 'stat2';
  cfg.maskparameter = cfg.funparameter;
  cfg.funcolorlim   = [0 5];
  %cfg.location      = 'max';
  cfg.interactive   = 'yes';
  cfg.crosshair     = 'no';
  cfg.atlas         = atlas;
  
  ft_sourceplot(cfg, inter_src);
  colormap('jet');
  colorbar;
  
  % Save
  %------
  if dosavefile == 1
    condname = {'aud' 'vis' 'tac'};
    fname    = fullfile(config.path,'doc','tmp',[Dirfiles Dirfiles_dir2 '_SRC_' condname{i}]);
    
    view(-90,0) % left
    name = [fname '_SURF_LEFTview.png'];
    save_figure(name, 100);
    
    view(0,90) % top
    name = [fname '_SURF_TOPview.png'];
    save_figure(name, 100);
    
    view(90,0) % right
    name = [fname '_SURF_RIGHTview.png'];
    save_figure(name, 100);
  end
end


%% New way for parcellation
%-----------------------------











