%% Script source localization test
%-----------------------------------

clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

Pathsrc = fullfile(config.path2work,'src');

%% Load data

iSub = 1; % only one subject

Pathclean = fullfile(config.path,'preproc','hpfilt_1'); % Path clean data
Pathhdm   = fullfile(config.path,'mri',sprintf('s%02d',iSub));
Pathfif   = fullfile(config.path,'fif',sprintf('s%02d',iSub));

% fiffile = [sprintf('sub%02d_',iSub),sprintf('block%02d_',1),'*.fif'];
% fiffilename   = strtrim(ls(fullfile(Pathfif,fiffile)));

datafile = fullfile(Pathclean,sprintf('clean_%02d_allblocks.mat',iSub));
hdmfile  = fullfile(Pathhdm,sprintf('sub%02d_hdm.mat',iSub));

filename_tr = fullfile(config.path,'preproc',sprintf('clean_%02d_allblocks_equalcond_trialindex.mat',iSub));

data = load(datafile);

%% equalize condition
%---------------------
% Load previous sorted conditions
%--------------------------------
load(filename_tr);

% select trials
%--------------
cfg         = [];
cfg.trials  = find(ismember(data.trialinfo(:,2),sel_tr));
data        = ft_selectdata(cfg, data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PREPARE SOURCE LCMV-DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

toilim   = [-1 1];
cov_win  = 'all';
freq     = 'low';
paradigm = 'metaNT_HitMiss';

unival   = 'cm'; % unit value for source localization 'm' or 'cm'

hdm = load(hdmfile);% load hdm


data.grad3 = obob_adjust_tra(data.grad); % NO NEED TO DO THIS ANYMORE

data.grad2 = ft_convert_units(data.grad, unival);
hdm.vol   = ft_convert_units(hdm.vol, unival);
hdm.mri   = ft_convert_units(hdm.mri, unival);

load parcellations_3mm.mat; % load parcellation

% warp grid
%-----------
cfg = [];
cfg.coordsys       = 'neuromag';
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = ft_convert_units(parcellation.template_grid, unival);
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.grid.unit      = unival;
cfg.mri            = hdm.mri;
hdm.grid_warped2   = ft_prepare_sourcemodel(cfg);

% calculate leadfield, normalize to get rid of depth bias
%----------------------------------------------------------
cfg=[];
cfg.vol       = hdm.vol;
cfg.channel   = data.label;
cfg.grid      = ft_convert_units(hdm.grid_warped2, unival);
cfg.grad      = data.grad;
cfg.normalize = 'yes';
lf = ft_prepare_leadfield(cfg, data);

%% select smaller data set
%-------------------------
cfg = [];
cfg.trials = 'all';
cfg.toilim = toilim;
data   = ft_redefinetrial(cfg, data);

% make triggers conditions for this expe
%----------------------------------------
[conds,~] = make_condsTRIG4src(paradigm);

%% now project condition wise if necessary
%-----------------------------------------
for iC = 1:size(conds,1)
  
  % select conditions (and time if you want)
  %-----------------------------------------
  cfg = [];
  cfg.trials = find(ismember(data.trialinfo(:,1), conds{iC, 1}));
  temp = ft_selectdata(cfg, data);
  
  % beam time series to sources
  %-----------------------------
  cfg=[];
  switch freq
    case 'low'
      cfg.lpfilter='yes';
      cfg.lpfreq=30;
    case 'high'
      cfg.lpfilter='yes';
      cfg.lpfreq=100;
      cfg.hpfilter='yes';
      cfg.hpfreq=30;
    case 'no'
  end
  cfg.channel  = data.label;
  %cfg.regfac='5%';
  cfg.hdm      = hdm.vol;
  cfg.grid     = lf;
  cfg.cov_win  = cov_win;
  cfg.parcel   = parcellation; 
  
  tmpsrc_cond{iC} = obob_svs_beamtrials_lcmv(cfg, temp);
  
  clear temp
end


datasrc = ft_appenddata([],tmpsrc_cond{:});
clear tmpsrc_cond















