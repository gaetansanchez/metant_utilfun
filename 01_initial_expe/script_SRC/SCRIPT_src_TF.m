
% SCRIPT Do spectral analysis on source data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Path
%-----

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
config = addmypaths_gs('metaNT',cfg);

% Path definition
Pathsrc = fullfile(config.path2work,'src');
Pathdata = config.path;

Pathsave = fullfile(Pathsrc,'gaERF');

condarray = {'aud_hit' 'aud_miss';...
  'vis_hit' 'vis_miss';...
  'tac_hit' 'tac_miss'};

for iCond = 1:size(condarray,1);
  
  cond1 = condarray{iCond,1};
  cond2 = condarray{iCond,2};
  
  % LOAD srcERF per subjects
  %--------------------------
  PathERFfiles = fullfile(Pathsrc, 'srcERF');
  subfiles = dir(fullfile(PathERFfiles, 's*_src_erf.mat'));
  nSub = length(subfiles);
  erf = []; % initalize variable 'erf' before so that it is not confused with the builtin function 'erf'
  erfbl = [];
  for iSub = 1:nSub
    [~, sub, ~] = fileparts(subfiles(iSub).name);
    disp(['--> Load : ' sub]);
    load(fullfile(PathERFfiles,sub));
    % select conditions
    cond_1{iSub}    = erf.(cond1);
    cond_bl_1{iSub} = erfbl.(cond1);
    cond_2{iSub}    = erf.(cond2);
    cond_bl_2{iSub} = erfbl.(cond2);
  end
  
  %% ga
  cfg = [];
  cfg.keepindividual = 'no';
  hit_ga_bl = ft_timelockgrandaverage(cfg,cond_bl_1{:});
  hit_ga    = ft_timelockgrandaverage(cfg,cond_1{:});
  
  cfg = [];
  cfg.keepindividual = 'no';
  miss_ga_bl = ft_timelockgrandaverage(cfg, cond_bl_2{:});
  miss_ga    = ft_timelockgrandaverage(cfg, cond_2{:});
  
  %% Save
  outdata = ['ga_srcERF_' cond1 '-vs-' cond2 '.mat'];
  
  if ~exist(Pathsave,'dir')
    mkdir(Pathsave)
  else
  end
  
  save(fullfile(Pathsave,outdata),'hit_ga_bl','hit_ga','miss_ga_bl','miss_ga');
  
end