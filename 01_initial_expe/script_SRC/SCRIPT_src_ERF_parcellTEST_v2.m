%% Script src test parcellation mm or m
%----------------------------------------

clear all; close all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

% Path definition
Pathsrc = fullfile(config.path2work,'src');
Pathdata = config.path;


%% Create or load parcellation structure
%-----------------------------------------

% cfg = [];
% cfg.resolution = 0.003;% 3mm
% [parcellation, template_grid] = obob_svs_create_parcellation(cfg)


load parcellations_3mm.mat

%% Test
%--------

iSub = 1;
precision_grid = 'parcellations_3mm.mat';

Pathclean = fullfile(config.path,'preproc','hpfilt_1'); % Path clean data
Pathsave = fullfile(config.path2work,'src','srcERF_lp30Hz','parcel_units_cm_noadjust_v2');

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

Do_srcERF(iSub,Pathsave,Pathclean,precision_grid)

%% Visu results
%--------------

% load data
Pathsave1 = fullfile(config.path2work,'src','srcERF_lp30Hz','parcel_units_m_noadjust_v2');
Pathsave2 = fullfile(config.path2work,'src','srcERF_lp30Hz','parcel_units_cm_noadjust_v2');

fileName1 = fullfile(Pathsave1,sprintf('s%02d_src_erf.mat',iSub));
fileName2 = fullfile(Pathsave2,sprintf('s%02d_src_erf.mat',iSub));

load parcellations_3mm.mat

data1 = load(fileName1);
data2 = load(fileName2);

%%
cond = {'aud_hit' 'aud_miss';...
    'vis_hit' 'vis_miss';...
    'tac_hit' 'tac_miss'};
smod = 1;

dat2p1_c1 = data1.erfbl.(cond{smod,1});
dat2p1_c2 = data1.erfbl.(cond{smod,2});

dat2p2_c1 = data2.erfbl.(cond{smod,1});
dat2p2_c2 = data2.erfbl.(cond{smod,2});

%%
cfg = [];
cfg.layout = parcellation.layout;
cfg.xlim = [-.2 .5];

figure; ft_multiplotER(cfg, dat2p1_c1,dat2p2_c1);
%%
cond = {'aud_hit' 'aud_miss';...
    'vis_hit' 'vis_miss';...
    'tac_hit' 'tac_miss'};

for i = 3%1:size(cond,1)
    dat2p1_c1 = data1.erfbl.(cond{i,1});
    dat2p2_c1 = data2.erfbl.(cond{i,1});
    
    cfg = [];
    cfg.layout = parcellation.layout;
    cfg.xlim = [-.2 .5];
    
    figure; ft_multiplotER(cfg, dat2p1_c1,dat2p2_c1);
    
end

%% Brain plot

cond = {'aud_hit' 'aud_miss';...
    'vis_hit' 'vis_miss';...
    'tac_hit' 'tac_miss'};

dat2p1_c1 = data1.erfbl.(cond{3,1});
dat2p2_c1 = data2.erfbl.(cond{3,1});


load('standard_mri_segmented');

cfg = [];
cfg.sourcegrid = parcellation.parcel_grid;
cfg.parameter = 'avg';
cfg.latency = [0 0.2];
cfg.mri = mri_seg.bss;

svs_dat2p1_c1 = obob_svs_virtualsens2source(cfg, dat2p1_c1);
svs_dat2p2_c1 = obob_svs_virtualsens2source(cfg, dat2p2_c1);

load atlas_parcel333.mat

cfg = [];
cfg.funparameter = 'avg';
cfg.maskparameter = 'brain_mask';
cfg.atlas = atlas;
cfg.funcolorlim = [-1.5 1.5];

ft_sourceplot(cfg, svs_dat2p1_c1);
ft_sourceplot(cfg, svs_dat2p2_c1);


