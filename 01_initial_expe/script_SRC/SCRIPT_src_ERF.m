
% script to call all functions necessary for source-level analyses

clear all;
% Path
%-----

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

% Path definition
Pathsrc = fullfile(config.path2work,'src');
Pathdata = config.path;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 1/ run do_coreg_headmodel for each subject
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Nsub = 1:25;

for i = 1:numel(Nsub)
  iSub = Nsub(i);
  [hdm] = do_coreg_headmodel(iSub);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 2/ run Do_srcERF (with condor_srcERF) with or without parcellation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

condor_04_epo2src; % call ==> Do_srcERF.m 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bonus Plot Publi all source time-course
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

Pathsrc = fullfile(config.path2work,'src');

% Load & Prepare individual source localisation files
%-------------------------------------------------------
PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','hpfilt_0.1','parcel_grid_CFiltersensmod_hit_vs_miss');
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','hpfilt_0.1','1.5cm_grid_all_conds_CFiltersensmod_metaNT');
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','hpfilt_0.1','1.5cm_grid_all_conds_CFiltersensmod_metaNT_HitMiss');
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','hpfilt_0.1','1.5cm_grid_all_conds_CFiltersensmod_metaNT_SupraSham');
%PathERFfiles = fullfile(Pathsrc,'srcERF_lp30Hz','hpfilt_1','1.5cm_grid_all_conds_CFiltersensmod_metaNT');

% Suj
%-----
[Suj] = Do_sel_subjects(config,'metaNT');

%Suj = 22

% Load loop
%----------
erf = []; % initalize variable 'erf' before so that it is not confused with the builtin function 'erf'
erfbl = [];
srerf = {};
srerf_bl = {};
for i = 1:numel(Suj)
  
  iSub = Suj(i);
  [~, sub, ~] = fileparts(fullfile(PathERFfiles, sprintf('s%02d_src_erf.mat',iSub)));
  disp(['--> Load : ' sub]);
  load(fullfile(PathERFfiles,sub));
  % select conditions
  srerf{i}    = erf;
  srerf_bl{i} = erfbl;
  
end
  

T = srerf{1}.aud_hit.time;
fsample = 1/(T(end)-T(end-1));
data_ga = srerf{1}.aud_hit;% initialize structure
data_ga.fsample = fsample;
data_ga_bl = srerf_bl{1}.aud_hit;% initialize structure
data_ga_bl.fsample = fsample;

sensmod   = {'aud' 'vis' 'tac'};
%condlabel = {'hit' 'miss'};
%condlabel = {'sham' 'supra'};
condlabel = {'hit' 'miss' 'sham' 'supra'};

for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:numel(Suj)
      datint = srerf{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
      
      datint_bl = srerf_bl{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga_bl.([sensmod{i} condlabel{j}])(n,:,:) = datint_bl.avg;
      
    end
  end
end
disp('********************');
disp('DONE !!!');

%% Plot + statistics
%-------------------

testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};

% testlabel = {'audhit','vishit','tachit','audsupra','vissupra','tacsupra';...
%    'audmiss','vismiss','tacmiss','audsham','vissham','tacsham'};


% testlabel = {'audmiss','vismiss','tacmiss';...
%    'audsham','vissham','tacsham'};

 
timeWin = [-200 550];
sel = 'SRC';%'GRC'; %'MAG';
Pathoutput = fullfile(config.path,'doc/src');
Pathoutput = [];

timemax = {}; % max timing of peaks for first condition
for i = 1:size(testlabel,2)
  test1 = testlabel{1,i};
  test2 = testlabel{2,i};
  [timewins,timemax{i}] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput,timeWin)
end

%% Just plot source
%------------------

testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};

testlabel = {'audsham','vissham','tacsham';...
  'audmiss','vismiss','tacmiss'};
testlabel = {'audsupra','vissupra','tacsupra';...
   'audsham','vissham','tacsham'};

dat = data_ga;

toi   = [-0.5 0.6];
%toibl = [-0.2 0];
toibl = [];

for i = 1:size(testlabel,2)
    
    test1 = testlabel{1,i};
    test2 = testlabel{2,i};
    
    
    start_toi = find(round(dat.time,2)==toi(1),1);
    stop_toi  = find(round(dat.time,2)==toi(2),1);

    
    % average SMR signal over subjects & sources 
    %-------------------------------------------
    ga1 = squeeze(sqrt(mean(mean(dat.(test1).^ 2,1),2)))';
    ga2 = squeeze(sqrt(mean(mean(dat.(test2).^ 2,1),2)))';
    
    if ~isempty(toibl)
      start_toibl = find(round(dat.time,2)==toibl(1),1);
      stop_toibl  = find(round(dat.time,2)==toibl(2),1);
      Mbl1 = mean(ga1(start_toibl:stop_toibl));
      Mbl2 = mean(ga2(start_toibl:stop_toibl));
      ga1 = (ga1 - Mbl1)./ Mbl1;
      ga2 = (ga2 - Mbl2)./ Mbl2;
    end
    
    ga1 = ga1(start_toi:stop_toi);
    ga2 = ga2(start_toi:stop_toi);
    
    yvaluemax = max([ga1 ga2]);
    yvaluemin = min([ga1 ga2]);
    
    cfg = [];
    cfg.data  = ga1;
    cfg.data2 = ga2;
    cfg.col = {'b'};
    cfg.col2 = {'r'};
    cfg.limits	= [dat.time(start_toi) dat.time(stop_toi) yvaluemin yvaluemax];
    
    cfg.caption = [test1 ' vs ' test2];
    
    meeg_plot_butterfly_col(cfg);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 3/ run grand average srcERF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SCRIPT_gaSRC;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 4/ run Do_srcERF_stat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);
Pathsrc = fullfile(config.path2work,'src');
Pathdata = config.path;

paradigm = 'metaNT'; % load specific path later

% condarray = {'aud_hit' 'aud_miss';...
%              'vis_hit' 'vis_miss';...
%              'tac_hit' 'tac_miss'};

%%% win = 20ms // 0ms overlap
%-----------------------------
% toiarray = { [.186 .205],[.244 .420],[.459 0.479];...
%              [.225 .264],[.303 .479],[];...
%              [.088 .147],[.205 .420],[]};       

%%% win = 30ms // 10ms overlap
%------------------------------
% toiarray = { [.190 .210],[.250 .425],[.464 0.5];...
%              [.230 .250],[.308 .5],[];...
%              [.093 .151],[.190 .425],[]};       
           

% toiarray = { [0 .1] [.1 .2] [.2 .3] [.3 .4] [.4 .5];...
%              [0 .1] [.1 .2] [.2 .3] [.3 .4] [.4 .5];...
%              [0 .1] [.1 .2] [.2 .3] [.3 .4] [.4 .5]};
         
           
condarray = {'aud_supra' 'aud_sham';...
             'vis_supra' 'vis_sham';...
             'tac_supra' 'tac_sham'};
% toiarray = { [.090 .150];...
%              [.090 .150];...
%              [.090 .150]};        
toiarray = { [.05 .5];...
             [.132 .5];...
             [.05 .5]};        
           
%            
         
avgtoi = 'yes';
correct  = 'cluster'; % 'no' or 'cluster'
method   = 'montecarlo'; % 'analytic' or 'montecarlo'
stattest = 'depsamplesT';%'depsamplesT';%'cimec_statfun_normchange'; % % first study: ttest...
%Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs');
Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs_V2_30ms10olp');
Pathsrc     = fullfile(config.path2work,'src','srcERF_lp30Hz','hpfilt_0.1','1.5cm_grid_all_conds_CFiltersensmod_metaNT');

condor_srcERF_stat(paradigm, condarray, toiarray, avgtoi, correct, method, stattest, Pathoutdata,Pathsrc); % call ==> Do_srcERF_stat.m


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 4/ run Do_srcERF_stat (Without CONDOR)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

Pathsrc = fullfile(config.path2work,'src');
PathERFfiles = fullfile(Pathsrc,'parsrcERF_lp30Hz','hpfilt_0.1','parcel_grid_all_conds_CFiltersensmod_metaNT');

% condarray = {'aud_hit' 'aud_miss';...
%              'vis_hit' 'vis_miss';...
%              'tac_hit' 'tac_miss'};
%            
% toiarray = { [.210 .241] [.273 .288];...
%               [] [.242 .476];...
%               [.100 .179] [.351 .382]};
            
condarray = {'aud_supra' 'aud_sham';...
             'vis_supra' 'vis_sham';...
             'tac_supra' 'tac_sham'};
toiarray = { [.090 .150];...
             [.090 .150];...
             [.090 .150]};  
           
precision_grid = 'parcellations_3mm.mat'; % file with parcellation template grid  

avgtoi = 'yes';
correct = 'cluster'; % 'no' or 'cluster'
method = 'montecarlo'; % 'analytic' or 'montecarlo'
stattest = 'depsamplesT';%'depsamplesT';%'cimec_statfun_normchange'; % % first study: ttest...
%Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'analytic_allbrain_1_5cm_specificTOI_twotails_with_abs');
Pathoutdata = fullfile(Pathsrc, 'parcel_stat_ERF', 'analytic_allbrain_specificTOI_twotails_with_abs');

S = {};
Sbl= {};
for iCond = 1:size(condarray,1);
  
  cond1 = condarray{iCond,1};
  cond2 = condarray{iCond,2};
  
  for j = 1:numel(toiarray(iCond,:))
    toi = toiarray{iCond,j};
    if isempty(toi)
      S{iCond,j} = [];
      Sbl{iCond,j} = [];
    else
      outdata = fullfile(Pathoutdata, [cond1 '-vs-' cond2 '_' method '_' correct '_' num2str(toi(1)*1000) '-' num2str(toi(2)*1000) 's.mat']);
      [S{iCond,j},Sbl{iCond,j}] = Do_srcERF_stat(PathERFfiles,cond1,cond2,toi, avgtoi, correct, method, stattest, outdata,precision_grid);
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

Pathsrc = fullfile(config.path2work,'src');

Pathdata = fullfile(Pathsrc, 'parcel_stat_ERF', 'analytic_allbrain_specificTOI_twotails_with_abs');
%Pathdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs_V2_30ms10olp');

precision_grid = 'parcellations_3mm.mat'; % file with parcellation template grid  

% condarray = {'aud_hit' 'aud_miss';...
%   'vis_hit' 'vis_miss';...
%   'tac_hit' 'tac_miss'};

%%% win = 20ms // 0ms overlap
%-----------------------------
% toiarrayfile = { [.186 .205],[.244 .420],[.459 0.479];...
%              [.225 .264],[.303 .479],[];...
%              [.088 .147],[.205 .420],[]};       

%%% win = 30ms // 10ms overlap
%------------------------------
% toiarrayfile = { [.190 .210],[.250 .425],[.464 0.5];...
%              [.230 .250],[.308 .5],[];...
%              [.093 .151],[.190 .425],[]};       

condarray = {'aud_supra' 'aud_sham';...
             'vis_supra' 'vis_sham';...
             'tac_supra' 'tac_sham'};
toiarrayfile = { [.090 .150];...
             [.090 .150];...
             [.090 .150]};  
% toiarrayfile = { [.05 .5];...
%              [.132 .5];...
%              [.05 .5]};
            
                  
iCond = [1:3];
toi2select = [1:3];
dosavefile = 1;
Tscale = [0 5];

TabALL = {};
for i = 1:numel(iCond)
  for j = 1:numel(toi2select)
    cond1 = condarray{iCond(i),1};
    cond2 = condarray{iCond(i),2};
    toifile = toiarrayfile{iCond(i),toi2select(j)};
   
    if isempty(toifile)
    else
    inputfile = fullfile(Pathdata, [cond1 '-vs-' cond2 '_montecarlo_cluster_' num2str(toifile(1)*1000) '-' num2str(toifile(2)*1000) 's.mat']);
    [TabALL{i,j}] = src_erf_plot_braintime(config,inputfile,toifile,dosavefile,Tscale,precision_grid);
    
    
%     [~,fname,~] = fileparts(inputfile);
%     outputfile = fullfile(config.path,'doc','src',fname);
%     plot_src4caretbrain(outputfile,inputfile,toifile,'mni_grid_1_5_cm_889pnts',Tscale)
    
    end
    
  end
end


%% Explore source atlas
%-----------------------

clear all;
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);
Pathsrc = fullfile(config.path2work,'src');

condarray = {'aud_hit' 'aud_miss';...
  'vis_hit' 'vis_miss';...
  'tac_hit' 'tac_miss'};
           
%%% win = 30ms // 10ms overlap
%------------------------------
toiarray = { [.190 .210],[.250 .425],[.464 0.5];...
             [.230 .250],[.308 .5],[];...
             [.093 .151],[.190 .425],[]};     

iCond = 3;
toi2select = 2;
cond1 = condarray{iCond,1};
cond2 = condarray{iCond,2};
Tscale = [0 5];
% Load
%------
toifile = toiarray{iCond,toi2select};
%Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI');
%Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails');
Pathoutdata = fullfile(Pathsrc, 'stat_ERF', 'allbrain_1_5cm_specificTOI_twotails_with_abs_V2_30ms10olp');


filename = [cond1 '-vs-' cond2 '_montecarlo_cluster_' num2str(toifile(1)*1000) '-' num2str(toifile(2)*1000) 's.mat'];
statdata = fullfile(Pathoutdata,filename);
stat_data = load(statdata);

load mni_grid_1_5_cm_889pnts
stat = stat_data.stat;

% Do plot
%---------
toi = stat.time(find(sum(stat.mask==1)));
toi = stat.time;
stat.clus = stat.stat.*stat.mask;

param2plot = 'clus';
param2mask = 'mask';

stat.mask = double(stat.mask);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.latency   = [toi(1) toi(end)];
cfg.parameter = {param2plot param2mask};
cfg.sourcegrid = template_grid;
source = obob_svs_virtualsens2source(cfg, stat);

cfg = [];
cfg.parameter = {param2plot param2mask};
inter_source = ft_sourceinterpolate(cfg, source, mri);
inter_source.coordsys = 'mni';

cfg = [];
cfg.funparameter = param2plot;
cfg.funcolorlim = Tscale;
%cfg.funcolorlim = 'maxabs';
cfg.maskparameter = param2mask;
cfg.atlas = 'ROI_MNI_V4.nii';%'TTatlas+tlrc.BRIK';
cfg.method = 'ortho';% 'surface' 'ortho' 'slice'
cfg.projmethod = 'nearest'; % needed for method ='nearest'; 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.location = 'max';


ft_sourceplot(cfg, inter_source);

%%%%%%%%%%%%%
% Extract ROI
%%%%%%%%%%%%%%

testdat = source;
testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)
sigVox  = find(testdat.mask==1); % only significant voxels
intval  = testdat.clus(sigVox); % mean values for these voxels
% sort voxels indices based on their values
%--------------------------------------------
[sigVal,indv] = sort(intval, 'descend');
sigVox = sigVox(indv);

% load atlas
%------------
A = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
savefilename = fullfile(config.path,'doc','atlas_labels',['labelsAALatlasMNI_' filename]);

[Tab] = Do_extractROIname_atlas(A,sigVox,sigVal,testdat,savefilename)

  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plotting Merge STAT all sens mod
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load mni_grid_1_5_cm_889pnts

dosavefile = 0;

stataud = Sbl{1}.mask;
statvis = Sbl{2}.mask;
stattac = Sbl{3}.mask;

maskallsens = intersect(intersect(find(stataud),find(statvis)),find(stattac));

fname = ['Merge_allsens'];

stat = Sbl{1};
stat.maskallsens = logical(zeros(size(stat.mask)));
stat.maskallsens(maskallsens) = 1;

stat.clusallsens = 3.*stat.maskallsens; % make T stat at 3

param2plot = 'clusallsens';

cfg = [];
cfg.latency   = [stat.time stat.time];
cfg.parameter = param2plot;
cfg.sourcegrid = template_grid;
source = obob_svs_virtualsens2source(cfg, stat);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.parameter = param2plot;
inter_source = ft_sourceinterpolate(cfg, source, mri);
inter_source.coordsys = 'mni';

% plot  carret style
cfg = [];
cfg.funparameter = param2plot;
cfg.funcolorlim = [0 4];
cfg.maskparameter = param2plot;
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';

% make figures for right hemisphere
cfg.surffile = 'surf_caretleft.mat';
ft_sourceplot(cfg, inter_source)

if dosavefile == 1
  name = [fname '_left_left'];
  plot_caret_style(name, 100, [], 'left');
end

cfg.surffile = 'surf_caretright.mat';
ft_sourceplot(cfg, inter_source)

if dosavefile == 1
  name = [fname '_right_right'];
  plot_caret_style(name, 100, [], 'right');
end

%% function: srcERF_plot_timecourse
%----------------------------------
Path2work = config.path2work;
Path_stat_srcERF = fullfile(Path2work,'src/stat_ERF/allbrain_SVSblandSVS');

condarray = {'aud_hit' 'aud_miss';...
  'vis_hit' 'vis_miss';...
  'tac_hit' 'tac_miss'};

condarray = {'vis_hit' 'vis_miss'};

for iCond = 1:size(condarray,1);
    
    cond1 = condarray{iCond,1};
    cond2 = condarray{iCond,2};

    % LOAD stat srcERF per conditions
    %-----------------------------------
    % CLUSTER EFFECT:
    % aud_hit-vs-aud_miss_montecarlo_cluster_0-500s.mat
    statfile = [cond1 '-vs-' cond2 '_montecarlo_cluster_0-500s.mat'];
    load(fullfile(Path_stat_srcERF,statfile));
    roi = str2double(stat.label(find(sum(stat.mask,2))));
    %roi = str2double(stat.label(mean(stat.mask,2)>mean(unique(mean(stat.mask,2)))));
    roistr = 'ClusterEffect';
    outdata = ['srcERF_gf_' cond1 'vs' cond2 '_' roistr];
    
    % LOAD Data src_ERF
    %-------------------
    gafile = ['ga_srcERF_' cond1 '-vs-' cond2 '.mat'];
    load(fullfile(Path2work,'src','gaERF',gafile)); % saved : hit_ga / hit_ga_bl / miss_ga / miss_ga_bl
    
    ga1 = hit_ga;
    ga2 = miss_ga;
    
    Do_plot_srcERF_timecourse(Path2work, ga1, ga2, roi, outdata)

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% function: srcERF_plot_brain
%-------------------------------
% function to plot source reconstruction of srcERF (STATISTICS!)
%erc = '/mnt/storage/erc-win2con/gaetan/metaNT';
%erc = '/Users/gaetan/Documents/POSTDOC/work/metaNT';
erc = config.path2work;
folder = fullfile(erc,'src/stat_ERF/allbrain_SVSblandSVS');

condfiles = dir(fullfile(folder, '*hit-vs-*mat'));
nCond = length(condfiles);

for iCond = 1:nCond;

    indata = fullfile(folder, condfiles(iCond).name);
    T = 4;
    dpi = 100;
    
    %timingplot = {[0 100] [100 200] [300 450]};
    timingplot = {[250 350]};
    for i =1:numel(timingplot)
      toi = timingplot{i};
      Do_plot_srcERF_brain(indata, toi, T, dpi);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXTRA BONUS TEST PLOT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% single-subject
load(fullfile(outdir, 'sub01_erf.mat'))
%
hit = erfbl.ntH_noT;
miss = erfbl.ntM_noT;

% select data
cfg = [];
cfg.latency = [-0.3 0.5];
hit = ft_selectdata(cfg,hit); 
miss = ft_selectdata(cfg,miss); 

%
%     hit.avg = sqrt(   squeeze(mean(mean(hit.individual,1),2)) .^2  )
%     miss.avg = sqrt(   squeeze(mean(mean(miss.individual,1),2)) .^2  )

hit.avg = sqrt(   squeeze(mean(hit.avg(roi,:),1)) .^2  )
miss.avg = sqrt(   squeeze(mean(miss.avg(roi,:),1)) .^2  )

cfg = [];
cfg.data = hit.avg;
cfg.data2 = miss.avg;
cfg.col = {'b'};
cfg.col2 = {'r'};
cfg.limits	= [hit.time(1) hit.time(end) 0 max(hit.avg(:))]; % unit is arbitrary
%cfg.limits	= [hit.time(1) hit.time(end) 0 0.002]; % unit is arbitrary
meeg_plot_butterfly_col(cfg);

title('sub04 NThit vs. NTmiss, right hemisphere')

%%