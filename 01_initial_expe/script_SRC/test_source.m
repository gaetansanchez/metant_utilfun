

% Test source locaization
%--------------------------
clear all;

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);

Pathsrc = fullfile(config.path2work,'src');

%% Load files
%------------
PathERFfiles = fullfile(Pathsrc, 'srcERF_lp30Hz','hpfilt_1');
subfiles = dir(fullfile(PathERFfiles, 's*_src_erf.mat'));

nSub = length(subfiles);
erf = []; % initalize variable 'erf' before so that it is not confused with the builtin function 'erf'
erfbl = [];
srerf = {};
srerf_bl = {};
for iSub = 1:nSub
  [~, sub, ~] = fileparts(subfiles(iSub).name);
  disp(['--> Load : ' sub]);
  load(fullfile(PathERFfiles,sub));
  % select conditions
  srerf{iSub}    = erf;
  srerf_bl{iSub} = erfbl;
  
end
  
%
T = srerf{1}.aud_hit.time;
fsample = 1/(T(end)-T(end-1));
data_ga = srerf{1}.aud_hit;% initialize structure
data_ga.fsample = fsample;
data_ga_bl = srerf_bl{1}.aud_hit;% initialize structure
data_ga_bl.fsample = fsample;
data_ga_bl_abs = data_ga_bl;% initialize structure

sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss'};
for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:nSub
      datint = srerf{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
      
      datint_bl = srerf_bl{n}.([sensmod{i} '_' condlabel{j}]);
      data_ga_bl.([sensmod{i} condlabel{j}])(n,:,:) = datint_bl.avg;
      
    end
  end
end

%% Plot
%------
test1 = 'audhit';%
test2 = 'audmiss';%

A = srerf_bl{1}.aud_hit;

figure;
plot(A.time,mean(A.avg,1),'k')



















