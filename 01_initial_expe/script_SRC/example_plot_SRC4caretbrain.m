


load mni_grid_1_5_cm_889pnts

dosavefile = 0; % 1 = if you want to save it as an image (use function plot_caret_style.m at the end)

stataud = Sbl{1}.mask;
statvis = Sbl{2}.mask;
stattac = Sbl{3}.mask;

maskallsens = intersect(intersect(find(stataud),find(statvis)),find(stattac));

fname = ['Merge_allsens'];

stat = Sbl{1};
stat.maskallsens = logical(zeros(size(stat.mask)));
stat.maskallsens(maskallsens) = 1;

stat.clusallsens = 3.*stat.maskallsens; % make T stat at 3

param2plot = 'clusallsens';

cfg = [];
cfg.latency   = [stat.time stat.time];
cfg.parameter = param2plot;
cfg.sourcegrid = template_grid;
source = obob_svs_virtualsens2source(cfg, stat);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.parameter = param2plot;
inter_source = ft_sourceinterpolate(cfg, source, mri);
inter_source.coordsys = 'mni';

% plot  carret style
cfg = [];
cfg.funparameter = param2plot;
cfg.funcolorlim = [0 4];
cfg.maskparameter = param2plot;
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';

% make figures for right hemisphere
cfg.surffile = 'surf_caretleft.mat';
ft_sourceplot(cfg, inter_source)

if dosavefile == 1
  name = [fname '_left_left'];
  plot_caret_style(name, 100, [], 'left');
end

cfg.surffile = 'surf_caretright.mat';
ft_sourceplot(cfg, inter_source)

if dosavefile == 1
  name = [fname '_right_right'];
  plot_caret_style(name, 100, [], 'right');
end