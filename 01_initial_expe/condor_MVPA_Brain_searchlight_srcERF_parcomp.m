
% RUN on cluster MVPA searchlight analysis (call in a loop = Do_MVPA_Brain_searchlight.m)
% With more paralelle computing !!!!

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT',cfg);

Pathlogs = fullfile(config.path,['jobs/mvpa_brainsl_srcERF_parcomp/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------

%% Subjects
%-----------
[Suj] = Do_sel_subjects(config,'metaNT');

%% Parameter for data load 
%-------------------------------
%sensmod     = {'aud' 'vis' 'tac'};
sensmod     = {'all'};
toilim      = [-0.2 0.6]; % in sec
win         = 0.01; % down sampling (1/win = sampling frequency)
CFilter     = 'sensmod';% Beamformer common filter computation =  'alltrials' or 'sensmod'
paradigm    = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham' or []
preprocfilt = 'hpfilt_0.1'; %  hpfilt_1 or hpfilt_0.1

%% Parameters for MVPA time2time decoding
%------------------------------------------
toi           = [0.25 0.5]; % time of interest to use for brain space decoding
time_radius   = NaN; % here we are using all time point to decoding brain sources
source_radius = 3;
Classifier    = 'Bayes'; % 'Bayes' or 'LDA'
Restype       = 'cross'; % 'cross' or 'corr'

% SetUp options for cosmoMVPA: cosmo_dim_generalization_measure
%--------------------------------------------------------------
opt=struct();

switch Classifier
  case 'Bayes'
    opt.classifier = @cosmo_classify_naive_bayes;
  case 'LDA'
    opt.classifier = @cosmo_classify_lda;
  case 'SVM'
    opt.classifier = @cosmo_classify_libsvm;
    opt.svm        = 'libsvm';
end

switch Restype
  case 'cross'
    opt.measure = @cosmo_crossvalidation_measure;
  case 'corr'
    opt.measure = @cosmo_correlation_measure;
end

opt.normalization = 'zscore';

%% Setup directory name and filename
%-------------------------------------
% File type/Name
%----------------
if isfield(opt,'normalization')
  Normtype   = [opt.normalization 'Norm_trad' num2str(time_radius)];
else
  Normtype   = ['NoNorm_trad' num2str(time_radius)];
end

% Set Name & Run Jobs Loop
%---------------------------
radtype   = ['srad' num2str(source_radius*10) '_' Classifier];
tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms'];

if strcmp(sensmod{1},'all') == 1
  Pathtype = fullfile([Restype '_acc_' Normtype],['dd_vs_ud_allsens_' tsteptype '_' radtype]);
  Restype = [Restype '_allsens'];
else
  Pathtype = fullfile([Restype '_acc_' Normtype],[tsteptype '_' radtype]);
  Restype = [Restype '_persens'];
end

%% Datafilename to load (output from: Do_srcERF4cosmoMVPA.m)
%-----------------------------------------------------
Nametype = ['MVPA_src' tsteptype '_BeamCFilt' CFilter];
Pathdatasrc = fullfile(config.path2work,'mvpa/src4MVPA'); % saved data

%% Set up output directory
%----------------------------
Pathsave = fullfile(config.path2work,'mvpa/t2t_temp',Pathtype); % data to save
if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Do parallel computing
%-------------------------
n_modalities = numel(sensmod);
n_sources    = 889;

% %n_parcomp    = 127;
% n_parcomp    = 7;
% sourceids_par = kron(0:n_parcomp,n_sources/n_parcomp);
nbrids = 1:n_sources;

for i = 1:numel(Suj)
    id = Suj(i);
    datafile = fullfile(Pathdatasrc,sprintf([Nametype '_%02d.mat'],id));
    
    if ~exist(datafile,'file') % check if file exists...
        error(['File does not exist: ' datafile]);
    end
    
    for train_modality=1:n_modalities
        for test_modality=1:n_modalities
            
%             for k = Nb_k
%                 nbrids = sourceids_par(k)+1:sourceids_par(k+1);
                
                NameID   = sprintf('toi%03d_%03dms_train%02d_test%02d_centerids_%03d_to_%03d',...
                 toi(1)*1000,toi(2)*1000,...
                  train_modality,test_modality,...
                  nbrids(1),nbrids(end));
                
                savefilename = fullfile(Pathsave,sprintf(['sub%02d_' NameID '.mat'],id));
                
                if exist(savefilename,'file')
                else
                    condor_struct = obob_condor_addjob(condor_struct, 'Do_MVPA_Brain_searchlight',...
                      datafile,toi,train_modality,test_modality,opt,source_radius,nbrids,savefilename);
                end
                
%             end

        end
    end
    
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);

