% RUN on cluster MVPA t2t no searchlight (nosl) analysis (call in a loop = Do_mvpa_srcERF.m)

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT',cfg);

Pathlogs = fullfile(config.path,['jobs/mvpa_t2t_nosl_stat/' date]);
Pathdata = config.path;

%% Parameters
%-----------------
h0_mean = 1/2; % 1/2 for crossvalidation or 0 for correlation
niter   = 10000; % 10000 for publication 
nproc   = 4; % number of parallel processes requested

% File parameters
%-----------------
sensmod       = {'all'}; % {'all'} or {'aud' 'vis' 'tac'}
toilim        = [-.2 .6]; % in sec or empty (if old analysis to verify folder's name)
win           = 0.01;
preprocfilt   = 'hpfilt_0.1'; %  hpfilt_0.1 or hpfilt_0.1
paradigm      = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham'

% File type/Name
%----------------
Restype    = 'cross'; % 'cross' or 'corr'
Normtype   = 'zscoreNorm_trad1'; % NozscoreNorm or zscoreNorm + Time radius
Classifier = 'Bayes'; % 'Bayes' or 'LDA' or 'SVM'
chan_types = {'SRC'};%{'SRC'} {'meg_axial'} or {'meg_planar_combined'} or {'meg_axial';'planar_combined'}
%% Condor call
%---------------
cfg = [];
cfg.mem       = '50G';
cfg.jobsdir   = Pathlogs;
cfg.cpus      = nproc;
cfg.java      = true;
condor_struct = obob_condor_create(cfg);

%% Setup directory name and filename
%-------------------------------------
% File type/Name
%----------------
radtype   = ['chan_' [chan_types{:}] '_' Classifier];
tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms'];

if strcmp(sensmod{1},'all') == 1
  Pathtype = fullfile([Restype '_acc_' Normtype],['dd_vs_ud_allsens_' tsteptype '_' radtype]);

else
  Pathtype = fullfile([Restype '_acc_' Normtype],[tsteptype '_' radtype]);

end

if strcmp(chan_types{1},'SRC')
  Pathsaved = fullfile(config.path2work,'mvpa','t2t_nosl_SRC',Pathtype); % data to save
else
  Pathsaved = fullfile(config.path2work,'mvpa','t2t_nosl_ERF',Pathtype); % data to save
end

Nametype = ['MVPA_t2tSRC'];

% Get files
%-----------
Files = dir(fullfile(Pathsaved,[Nametype '_*']));
if isempty(Files)
  error('No such files... Check paths and files name')
end

% Path to saved stat
%--------------------
Pathsave = fullfile(Pathsaved,'stat')
if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Jobs additions
%-----------------
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    cond_train = i;
    cond_test  = j;
    outputfile = fullfile(Pathsave,['STAT_niter' num2str(niter) '_train_' sensmod{cond_train} '_vs_test_' sensmod{cond_test} '_' Nametype '_N' num2str(numel(Files)) '.mat']);
    
    condor_struct = obob_condor_addjob(condor_struct, 'Do_mvpa_stat_t2t',Pathsaved,Files,cond_train,cond_test,h0_mean,niter,outputfile,nproc);
    
  end
end
 
%% Submit
%---------
obob_condor_submit(condor_struct);