function [] = Do_trialsNb_count_after_reject(nsub,pathpreproc_file,pathpreproc_artdef)

%% Path settings
%------------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Loop = load file and count number of clean trials
%----------------------------------------------

for id = 1:numel(nsub)
  
  iSub = nsub(id);
  
  fileclean    = fullfile(pathpreproc_file,sprintf('clean_%02d_allblocks.mat',iSub));
  saveName_art = fullfile(pathpreproc_artdef,sprintf('Numtrial_percond_afterReject_s%02d.mat',iSub));
  
  % Load
  %--------
  data_clean = load(fileclean);
  
  % Conditions check
  %------------------
  CondtrNumber = [];
  for i = 1:3
    % 'AUD' = 100 / 'VIS' = 200 / 'TAC' = 300
    CondtrNumber(1,i) = numel(find(data_clean.trialinfo(:,1) == (34+100*i))); % hit
    CondtrNumber(2,i) = numel(find(data_clean.trialinfo(:,1) == (32+100*i))); % miss
    CondtrNumber(3,i) = numel(find(data_clean.trialinfo(:,1) == (54+100*i))); % supra
    CondtrNumber(4,i) = numel(find(data_clean.trialinfo(:,1) == (12+100*i))); % sham
  end
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)]);
  disp('Post rejection Number of trials per conditions = ');
  CondtrNumber
  
  disp(' ');
  disp(['Saving: ' saveName_art])
  disp(' ');
  
  save(saveName_art,'-v7.3','CondtrNumber');
end





