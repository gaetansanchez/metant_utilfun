function [hdm] = do_coreg_headmodel(iSub)
% Do HDM
%---------
% input ==> i.e. iSub = 1;

% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
config = addmypaths_gs('metaNT',cfg);
pathexp = config.path;

pathhdm = fullfile(pathexp,'mri',sprintf('s%02d',iSub));
pathfif = fullfile(pathexp,'fif',sprintf('s%02d',iSub));

if exist(fullfile(pathhdm,'mri'), 'dir')
  try
    mrifile = strtrim(ls(fullfile(pathhdm,'mri','*.nii')));
  catch nonifti
    % if no nifti, try other formats
    try
      mrifile = strtrim(ls(fullfile(pathhdm,'mri','*.img')));
    catch noimage
      files = dir(fullfile(pathhdm,'mri'));
      % this should print an error, if not a file
      % take the 3rd name, as first is '.' and second is '..'
      mrifile = strtrim(ls(fullfile(pathhdm,'mri',files(4).name)));
    end
  end
else
  mrifile = []; %if no anatomical mri is available for this subject
  disp('*************');
  disp(['No aMRI available for s' num2str(iSub) ' : create fake one !']);
  disp('*************');
  pause(1);
end


fiffile = strtrim(ls(fullfile(pathfif,[sprintf('sub%02d_',iSub),'block01_*.fif'])));


%% Coregister
%-------------
cfg = [];
cfg.mrifile   = mrifile;
cfg.headshape = fiffile;
cfg.viewmode  = 'surface';

if iSub == 14
cfg.reslice = 'no';
cfg.viewmode  = [];
else
end

[mri_aligned,shape,vol,mri_segmented]=obob_coregister(cfg);

mri_aligned.gray = mri_segmented.gray;
mri_aligned.white = mri_segmented.white;
mri_aligned.csf = mri_segmented.csf;

%% warp grid
% load default grid
%load /mnt/storage/tier1/natwei/Shared/julia/templates/mni_grid_1_5_cm_889pnts
load mni_grid_1_5_cm_889pnts

% warp grid
cfg = [];
cfg.coordsys='neuromag';
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = template_grid;
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.mri            = mri_aligned;
warped_grid   = ft_prepare_sourcemodel(cfg);
% 
% %% check
% figure;
% vol = ft_convert_units(vol, 'cm');
% ft_plot_vol(vol, 'edgecolor', 'none'); alpha 0.4;
% ft_plot_mesh(warped_grid.pos(warped_grid.inside,:));

%% preparing headmodel structure
if ~exist('hdm', 'var')
  hdm = struct;
end

hdm.headshape        = shape;
hdm.mri              = mri_aligned;
hdm.vol              = vol;
hdm.grid_templateMNI = template_grid;
hdm.grid_warped      = warped_grid;
hdm.outputpath       = pathhdm;

hdm = orderfields(hdm);

%% saving headmodel if intended
disp(' ')
disp(['Saving headmodel in path ' hdm.outputpath])
disp(' ')
save(fullfile(hdm.outputpath,[sprintf('sub%02d_',iSub),'hdm.mat']), '-struct', 'hdm');




