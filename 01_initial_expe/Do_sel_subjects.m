function [Suj] = Do_sel_subjects(config,expe)



switch expe
    
    case 'metaNT'
        
        Suj = 1:25;
        BadSuj = [];
        load(fullfile(config.path,'BadsubjectsBehav'));
        Suj(BadSuj) = []; % remove bad subjects
        
        load(fullfile(config.path,'Numtrial_percondsuj_afterReject_allsubjects.mat'));
        Numtr(BadSuj,:) = [];
        A = Numtr(:,2:7)<30;% find numtrials above 40 for each conditions
        
        Suj(find(sum(A,2))) = [];
        
    case 'metaNT_v2'
    
      Suj = 1:21;
      BadSuj = [];
      
      load(fullfile(config.path,'preproc','Numtrial_percondsuj_afterReject_allsubjects.mat'));
      Numtr(BadSuj,:) = [];
      A = Numtr(:,2:7)<30;% find numtrials above 40 for each conditions
        
      Suj(find(sum(A,2))) = [];
      
      
    otherwise
        error('expe is unknown ...')
        
end