function [] = reject_visual_allblocks(Nsub)
% reject visual all blocks

%% Path settings
%------------------
pathpreproc = '/Volumes/obob/staff/gsanchez/metaNT/preproc';


%% Loop
%-------

for iSub = Nsub
    
    disp(' ');
    disp('**********************');
    disp(['Sub = ',sprintf('%02d',iSub)]);
    
    filepoc = dir(fullfile(pathpreproc,[sprintf('%02d_',iSub),'allblocks.mat']));
    fileName = fullfile(pathpreproc,filepoc.name);
      
    saveName = fullfile(pathpreproc,['clean_',filepoc.name]);
    
    if exist(saveName,'file') ~= 0
        disp(' ');
        disp(['Clean files already exists: ' saveName])
    else
        
        
        disp(' ');
        disp(['Loading: ' fileName])
        
        data = load(fileName);
        
        disp(' ');
        disp('Oooh yeah ! Running visual rejection baby !')
        disp(' ');
        
        %% reject visual on each blocks
        
        
        
%         cfg        = [];
%         cfg.latency = [-1 1];
%         cfg.method = 'summary';
%         cfg.layout = 'neuromag306mag.lay';
%         cfg.channel = 'MEG';
%         cfg.gradscale   = 0.04;
%         cfg.preproc.lpfilter='yes';
%         cfg.preproc.lpfreq = 40;
%         data = cimec_rejectvisual(cfg,data);
        
      
        cfg        = [];
        %cfg.latency = [-1 1];
        cfg.latency = [-0.5 0.5];
        cfg.method = 'summary';
        cfg.layout = 'neuromag306mag.lay';
        cfg.channel = 'MEG';
        cfg.gradscale   = 0.04;
        % filtering only for artifact rejection
        cfg.preproc.lpfilter    = 'yes';
        cfg.preproc.lpfilttype  = 'firws';
        cfg.preproc.lpfiltwintype = 'kaiser';
        cfg.preproc.lpfreq      = 40;
        cfg.preproc.lpfiltdf = 6;
        cfg.preproc.rectify     = 'yes';
        data_post = obob_rejectvisual(cfg,data);
        
%         % visual rejection trial by trial
%         
%         cfg = [];
%         cfg.latency = [-1 0.5];
%         data_v2 = ft_selectdata(cfg,data);
%         
%         cfg=[];
%         cfg.layout = 'neuromag306mag.lay';
%         %cfg.viewmode = 'vertical';
%         cfg.showlabel = 'yes';
%         cfg.channel = 'MEG';
%         %cfg.gradscale   = 0.04;
%         cfg.magscale = 25;
%         cfg = ft_databrowser(cfg, data_v2);
%       
%         
%         % to reject the trials selected as bad during the databrowser
%         data=ft_rejectartifact(cfg, data);


        %% save and delete block files
        disp(' ');
        disp(['Saving: ' saveName])
        disp(' ');
        
        save(saveName,'-v7.3','-struct', 'data_post');
        
        close all;
    end
    
end


