function [] = hnc_erf2ga(pathERF,pathgaERF,Nbsub,erffilename)
% DO grand average ERF

%% Defaults settings
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
config = addmypaths_gs('metaNT',cfg);

%% Triggers and condlabels
%-----------------------
sensmod   = {'aud' 'vis' 'tac'};
tr1       = [100 200 300];
condlabel = {'hit' 'miss' 'sham' 'supra'};
tr2       = [34 32 12 54];

%% Load files (if necessary)
%----------------------------
for i =1:numel(Nbsub)
  iSub = Nbsub(i);
  disp(' ');
  disp(['Load Sub: ' num2str(iSub)]);
  %--------------------------
  fileName = fullfile(pathERF,sprintf(['clean_%02d_' erffilename '.mat'],iSub));
  erf{i} = load(fileName);
end
  
%% Save file
%------------
saveNameall = fullfile(pathERF,sprintf(['allERF_N%02d_' erffilename '.mat'],numel(Nbsub)));
save(saveNameall, '-v7.3', 'erf');

%% DO ga
%---------
for j = 1:numel(sensmod)
  for k = 1:numel(condlabel)
    
    % Loop over subjects
    %--------------------
    for i = 1:numel(Nbsub)
      
      iSub = Nbsub(i);
      
      disp([sensmod{j} ' - ' condlabel{k}]);
      disp(' ');
      disp(['Sub: ' num2str(iSub)]);
      
      all_erf{i} = erf{i}.(sensmod{j}).(condlabel{k});
      
      all_erf{i} = rmfield(all_erf{i},'grad'); % remove grad
    end
    
    % Grand average
    %-------------
    cfg = [];
    ga_erf.(sensmod{j}).(condlabel{k})    = ft_timelockgrandaverage(cfg,all_erf{:});
 
    
    % make data smaller: remove cfg-field & convert to single
    ga_erf.(sensmod{j}).(condlabel{k}) = rmfield(ga_erf.(sensmod{j}).(condlabel{k}), 'cfg');
    
    %ga_erf.(sensmod{j}).(condlabel{k})= ft_struct2single(ga_erf.(sensmod{j}).(condlabel{k}));
    
  end
end

if ~exist(pathgaERF,'dir');
    mkdir(pathgaERF);
end

saveName = ['ga_ERF_N',sprintf(['%02d_' erffilename],numel(Nbsub)),'.mat'];
save(fullfile(pathgaERF, saveName), '-v7.3', 'ga_erf');

