%% Do Coreg and Create HDM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


iSub = 1;

[hdm] = do_coreg_headmodel(iSub);


%% Check coregistration
%%%%%%%%%%%%%%%%%%%%%%%%%%


% paths
%--------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Subject
%---------

Sub = 1:25;


for i = 1:numel(Sub)
    
    iSub = Sub(i);
    Pathhdm   = fullfile(config.path,'mri',sprintf('s%02d',iSub));
    Pathfif   = fullfile(config.path,'fif',sprintf('s%02d',iSub));
    Pathclean = fullfile(config.path,'preproc','hpfilt_1'); % Path clean data
    
    % SET and LOAD data
    %--------------------
    
    fiffile = [sprintf('sub%02d_',iSub),sprintf('block%02d_',1),'*.fif'];
    
    fiffilename   = strtrim(ls(fullfile(Pathfif,fiffile)));
    datafile = fullfile(Pathclean,sprintf('clean_%02d_allblocks.mat',iSub));
    hdmfile  = fullfile(Pathhdm,sprintf('sub%02d_hdm.mat',iSub));
    
    filename_tr = fullfile(config.path,'preproc',sprintf('clean_%02d_allblocks_equalcond_trialindex.mat',iSub));
    
    %data = load(datafile);
    %grad = data.grad;
    %grad = ft_read_sens(fiffilename, 'fileformat','neuromag_fif','senstype','meg','coordsys','dewar');
    grad = ft_read_sens(fiffilename);
    
    % Run plot
    %----------
    do_check_coregistration(hdmfile,grad)
    
end