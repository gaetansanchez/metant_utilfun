
% RUN ERF analysis to prepare cosmoMVPA analysis


%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT',cfg);

Pathlogs = fullfile(config.path,['jobs/erf4MVPA/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '50G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------

%% Subjects
%-----------
[Suj] = Do_sel_subjects(config,'metaNT');

%% Parameter for classification
%-------------------------------
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'}
toilim      = [-0.2 0.6]; % in sec (period to decode in CosMoMVPA)
win         = 0.01; % down sampling (average time samples every X sec)
paradigm    = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham' or []
preprocfilt = 'hpfilt_0.1'; %  hpfilt_1 or hpfilt_0.1


% Set Name 
%-----------
Pathdata = config.path;
tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms'];
Nametype  = ['MVPA_erf' tsteptype];
Pathsave  = fullfile(config.path2work,'mvpa/erf4MVPA'); % saved data

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Do parallel computing
%-------------------------

for i = 1:numel(Suj)
  id = Suj(i);

  datafile = fullfile(Pathdata,'preproc',preprocfilt,sprintf('clean_%02d_allblocks.mat',id));
  savefilename = fullfile(Pathsave,sprintf([Nametype '_%02d.mat'],id));
  
  condor_struct = obob_condor_addjob(condor_struct, 'Do_erf4cosmoMVPA',datafile,sensmod,toilim,win,savefilename,paradigm);
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);