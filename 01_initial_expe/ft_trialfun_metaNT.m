function [trl_all,event] = ft_trialfun_metaNT(cfg)

% read the header information and the events from the data
hdr   = ft_read_header(cfg.dataset);
event = ft_read_event(cfg.dataset);

% determine the number of samples before and after the trigger
pretrig  = round(-cfg.trialdef.prestim  * hdr.Fs);
posttrig =  round(cfg.trialdef.poststim * hdr.Fs)-1;

% search for "stimulus" events
value  = [event(find(strcmp('STI101', {event.type}))).value]';
sample = [event(find(strcmp('STI101', {event.type}))).sample]';

% Only relevant triggers
%-------------------------
triggervals = [40 1 3 5 12 14 16 32 34 36 52 54 56];
trigNointerest = find(~ismember(value,triggervals));

value(trigNointerest) = [];
sample(trigNointerest) = [];

% define the trials
trl(:,1) = sample + pretrig;  % start of segment
trl(:,2) = sample + posttrig; % end of segment
trl(:,3) = pretrig;           % how many samples prestimulus
trl(:,4) = value;             % trigger value

%% recode triggers
%------------------
trl_all = [];

posttarget_duration = 500; % ms
stimulus_trig = [1 3 5]; % sham, thresh , supra
response_trig = [12 14 16 32 34 36 52 54 56];% x2= No det /  x4= det / x6= No resp
no_response_trig = [16 36 56]; % usefull for reaction time calculus

tr_ind = 0; % start trial indexing at 1
for iRow = 1:length(trl)
  
  TRIG = trl(iRow,4); % current trigger value
  
  if any(TRIG == stimulus_trig) % only targets centered trials
    
    % Trial indexing
    %----------------
    tr_ind = tr_ind + 1;
    
    % Code for response
    %-------------------
    if any(trl(iRow+2,4) == response_trig) % check if there is several response button press
      code = trl(iRow+2,4); % take the second response button code
      goodresp = 2; % keep track of this for reaction time
    else
      code = trl(iRow+1,4); % take the response button code
      goodresp = 1;
    end
    
    % compute reaction time (response - target onset timing - posttarget_duration)
    %----------------------------------------------------------------------------
    if goodresp == 2 || any(trl(iRow+1,4) == no_response_trig)
      RT = NaN;
    else
      RT = (trl(iRow+1,1)-trl(iRow+1,3)) - (trl(iRow,1)-trl(iRow,3)) - posttarget_duration;
    end
    
    % compute ISI (jitter between fixation cross and stimulus)
    %----------------------------------------------------------
    try
      if trl(iRow-1,4) == 40
        ISI = (trl(iRow,1)-trl(iRow,3)) - (trl(iRow-1,1)-trl(iRow-1,3));
      else
        ISI = NaN;
      end
    catch
      ISI = NaN;
    end
    
    % build new trl-field
    %---------------------
    trl_all(end+1,1:3) = trl(iRow,1:3); % target onset centered trials
    trl_all(end,4)     = code; % stimulus + response
    trl_all(end,5)     = tr_ind; % trial index
    trl_all(end,6)     = RT;
    trl_all(end,7)     = ISI;
  end
  
end

