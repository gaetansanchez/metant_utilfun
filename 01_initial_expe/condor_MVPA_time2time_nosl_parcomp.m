% RUN  MVPA time2time analysis (call in a loop = Do_MVPA_time2time_nosl.m)

%% Path settings
%----------------
project = 'metaNT';
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs(project,cfg);

Pathlogs = fullfile(config.path,['jobs/mvpa_t2t_nosl/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------

%% Subjects
%-----------
[Suj] = Do_sel_subjects(config,project);


%% Parameter for data load 
%-------------------------------
%sensmod     = {'aud' 'vis' 'tac'};
sensmod     = {'all'};
toilim      = [-0.2 0.6]; % in sec
win         = 0.01; % down sampling (1/win = sampling frequency)
paradigm    = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham' or []
preprocfilt = 'hpfilt_0.1'; %  hpfilt_1 or hpfilt_0.1
do_obobLCMV  = 1; % 0 or 1 -> if 1 = use obob LCMV beamformer function
do_parcelsrc = 0;% 0 or 1 -> if 1 = use obob LCMV beamformer with parcellation atlas

%% Parameters for MVPA time2time decoding
%------------------------------------------
time_radius   = 1; % 0 = no time neighbourhood radius or if >0 +/- time point are included
Classifier    = 'Bayes'; % 'Bayes' or 'LDA' or 'SVM'
Restype       = 'cross'; % 'cross' or 'corr'
chan_types    = {'SRC'};%{'SRC'} {'meg_axial'} or {'meg_planar_combined'} or {'meg_axial','meg_planar_combined'}

% SetUp options for cosmoMVPA: cosmo_dim_generalization_measure
%--------------------------------------------------------------
opt=struct();

switch Classifier
  case 'Bayes'
    opt.classifier = @cosmo_classify_naive_bayes;
  case 'LDA'
    opt.classifier = @cosmo_classify_lda;
  case 'SVM'
    opt.classifier = @cosmo_classify_libsvm;
    opt.svm        = 'libsvm';
    %opt.autoscale  = 0; % 1 = perform normalization before svm decoding (default)
end

switch Restype
  case 'cross'
    opt.measure = @cosmo_crossvalidation_measure;
  case 'corr'
    opt.measure = @cosmo_correlation_measure;
end

opt.normalization = 'zscore';
opt.dimension     = 'time';
opt.radius        = time_radius; % +/- time point are included

%% Setup directory name and filename
%-------------------------------------
% File type/Name
%----------------
if isfield(opt,'normalization')
  Normtype   = [opt.normalization 'Norm_trad' num2str(time_radius)];
else
  Normtype   = ['NoNorm_trad' num2str(time_radius)];
end

% Set Name & Run Jobs Loop
%---------------------------
radtype   = ['chan_' [chan_types{:}] '_' Classifier];

extra_name = [];
if do_parcelsrc
  do_obobLCMV = 1; % force obob LCMV because parcellation
  extra_name = [extra_name 'parcel'];
end
if do_obobLCMV
  extra_name = [extra_name 'OBOB'];
end

if do_obobLCMV
  tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms_' extra_name '_BeamCFiltsensmod'];
else
  tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms_BeamCFiltsensmod'];
end

if strcmp(sensmod{1},'all') == 1
  Pathtype = fullfile([Restype '_acc_' Normtype],['dd_vs_ud_allsens_' tsteptype '_' radtype]);
  Restype = [Restype '_allsens'];
else
  Pathtype = fullfile([Restype '_acc_' Normtype],[tsteptype '_' radtype]);
  Restype = [Restype '_persens'];
end

%% Datafilename to load (output from: Do_srcERF4cosmoMVPA.m)
%-----------------------------------------------------

if strcmp(chan_types{1},'SRC')
  Nametype = ['MVPA_src' tsteptype];
  Pathdatasrc = fullfile(config.path2work,'mvpa','src4MVPA'); % saved data
else
  Nametype = ['MVPA_erf' tsteptype];
  Pathdatasrc = fullfile(config.path2work,'mvpa','erf4MVPA'); % saved data
end

%% Set up output directory
%----------------------------
Pathsave = fullfile(config.path2work,'mvpa','t2t_nosl_temp',Pathtype); % data to save
if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Do parallel computing
%-------------------------
n_modalities = numel(sensmod);

for i = 1:numel(Suj)
  id = Suj(i);
  datafile = fullfile(Pathdatasrc,sprintf([Nametype '_%02d.mat'],id));
  
  if ~exist(datafile,'file') % check if file exists...
    error(['File does not exist: ' datafile]);
  end
  
  for train_modality=1:n_modalities
    for test_modality=1:n_modalities
      
      NameID   = sprintf('train%02d_test%02d',train_modality,test_modality);
      savefilename = fullfile(Pathsave,sprintf(['sub%02d_' NameID '.mat'],id));
      
      if exist(savefilename,'file')
      else
         condor_struct = obob_condor_addjob(condor_struct, 'Do_MVPAgeneric_time2time_nosl',project,datafile,train_modality,test_modality,opt,chan_types,savefilename,sensmod);
      
      end
    end
  end
  
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);
