
% RUN on cluster src ERF analysis (call in a loop = Do_srcERF.m)

%% Path settings
%----------------
project = 'metaNT';

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs(project,cfg);

%% Options
%----------

precision_grid = 'parcellations_3mm.mat'; % 1.5 in cm (template grid) or 'parcellations_3mm.mat'
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'}
toilim      = [-0.5 0.5]; % in sec used for cov_win
CFilter     = 'sensmod';% Beamformer common filter computation =  'alltrials' or 'sensmod'
paradigm    = 'all';% 'all' or 'hit_vs_miss' or 'supra_vs_sham' or 'miss_vs_sham' 
HPFilt      = 'hpfilt_0.1'; % 'hpfilt_0.1' or 'hpfilt_1'
do_trans_suffix = []; % [] or '_trans_sss'

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2src/' date]);
pathpreproc = fullfile(config.path,['preproc' do_trans_suffix],HPFilt); % Path clean data

Pathsave = fullfile(config.path2work,['src' do_trans_suffix],'srcERF_lp30Hz',HPFilt,['parcel_grid_CFilter' CFilter '_' paradigm]);
%Pathsave = fullfile(config.path2work,['src' do_trans_suffix],'srcERF_lp30Hz',HPFilt,['1.5cm_grid_CFilter' CFilter '_' paradigm]);
%Pathsave = fullfile(config.path2work,['test_src' do_trans_suffix],'srcERF_lp30Hz',HPFilt,['1.5cm_grid_CFilter' CFilter '_' paradigm]);

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '10G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
[Suj] = Do_sel_subjects(config,project);

for i = 1:numel(Suj)
  iSub = Suj(i);
  Pathhdm   = fullfile(config.path,'mri',sprintf('s%02d',iSub));
  hdmfile  = fullfile(Pathhdm,sprintf('sub%02d_hdm.mat',iSub));
  
  condor_struct = obob_condor_addjob(condor_struct, 'Do_srcERF',...
    project,iSub,hdmfile,sensmod,toilim,...
    pathpreproc,Pathsave,precision_grid,CFilter,paradigm);
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);

