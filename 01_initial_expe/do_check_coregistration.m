function [] = do_check_coregistration(hdmfile,grad)


%% Load
hdm  = load(hdmfile);

vol   = hdm.vol;
try
    shape = hdm.headshape;
catch
    shape = [];
end

mri_aligned = hdm.mri;

%% convert to m (sensors and headshape) for plotting
grad  = ft_convert_units(grad, 'm');
vol   = ft_convert_units(vol, 'm');
mri_aligned   = ft_convert_units(mri_aligned, 'm');
if ~isempty(shape)
    shape = ft_convert_units(shape, 'm');
end

% plot
figure;
ft_plot_vol(vol);

% cfg        = [];
% cfg.output = 'scalp';
% cfg.smooth = 2;
% seg_align  = ft_volumesegment(cfg, mri_aligned);
% 
% cfg             = [];
% cfg.method      = 'singleshell';
% cfg.numvertices = 20000;
% scalp           = ft_prepare_headmodel(cfg, seg_align);
% scalp           = ft_convert_units(scalp, 'm');
% 
% ft_plot_vol(scalp,'facealpha',0.5,...
%     'facecolor','k',...
%     'vertexcolor','none',...
%     'edgecolor','none');

hold on
if ~isempty(shape)
    ft_plot_headshape(shape);
    hold on
    plot3(shape.fid.pos(:,1),shape.fid.pos(:,2),shape.fid.pos(:,3),'o',...
        'MarkerFaceColor','c',...
        'MarkerSize',10,'MarkerEdgeColor','k');
end %if

plot3(grad.chanpos(:,1),grad.chanpos(:,2),grad.chanpos(:,3),'o',...
    'MarkerFaceColor','g',...
    'MarkerSize', 12,'MarkerEdgeColor','k');
view(100,10);% right side view
hold off