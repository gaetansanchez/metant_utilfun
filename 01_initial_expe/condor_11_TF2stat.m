% RUN TF2stat
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/TF2stat/' date]);

pathTF = fullfile(config.path2work,'tf');
pathstatTF = fullfile(pathTF,'stat_tf');

if ~exist(pathstatTF,'dir') % create output dir if dont exist
  mkdir(pathstatTF);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Parameters 
%-------------
tffilename = 'allblocks_mtmconvol_hanning_-1-1s_freq2-90Hz_7ftimwin_2smothF_TF_percond';

latency = [-1 0];
frequency = [8 14];
sensor = 'mag'; % 'mag' for magnetometers, anything else for gradiometers
correctm = 'cluster'; % 'no' or 'cluster'
method = 'montecarlo'; % 'analytic' or 'montecarlo' 
stattest = 'depsamplesT'; %'depsamplesT' 'obob_statfun_normchange'; 

condarray = {'aud_hit' 'aud_miss';...
             'tac_hit' 'tac_miss';...
             'vis_hit' 'vis_miss'};

%% Jobs additions
%-----------------
Suj = 1:25;
BadSuj = [];
load(fullfile(config.path,'BadsubjectsBehav'));
Suj(BadSuj) = []; % remove bad subjects

load(fullfile(config.path,'Numtrial_percondsuj_afterReject.mat'));
A = Numtr(:,2:end)<40;% find numtrials above 40 for each conditions
Suj(find(sum(A,2))) = [];



for iCond = 1:size(condarray,1);
  cond1 = condarray{iCond,1};
  cond2 = condarray{iCond,2};
  
  condor_struct = obob_condor_addjob(condor_struct, 'hnc_TF2stat',...
    pathTF,...
    tffilename,pathstatTF,Suj,cond1,cond2,...
    sensor,method,stattest,correctm,latency,frequency);
end
%% Submit
%---------
obob_condor_submit(condor_struct);
