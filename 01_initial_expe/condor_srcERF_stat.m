function [] = condor_srcERF_stat(paradigm, condarray, toiarray, avgtoi, correct, method, stattest, Pathoutdata, Pathsrc)

% RUN on cluster src ERF statistics (call in a loop = Do_srcERF_stat.m)

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs(paradigm,cfg); % load path for a specific experiment

Pathlogs = fullfile(config.path,['jobs/erfsrc2stat/' date]);
Pathdata = config.path;

%% Condor call
%---------------
cfg = [];
cfg.mem = '20G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------

for iCond = 1:size(condarray,1);
    
    cond1 = condarray{iCond,1};
    cond2 = condarray{iCond,2};
    
    for j = 1:numel(toiarray(iCond,:))  
       
    toi = toiarray{iCond,j};
    if isempty(toi)
      disp(['No TOI number ' num2str(j) ' // for ' cond1 ' vs ' cond2 '...']);
    else
      disp(['Run TOI number ' num2str(j) ' = ' num2str(toi) ' // for ' cond1 ' vs ' cond2 '...']);
      outdata = fullfile(Pathoutdata, [cond1 '-vs-' cond2 '_' method '_' correct '_' num2str(toi(1)*1000) '-' num2str(toi(2)*1000) 's.mat']);
      condor_struct = obob_condor_addjob(condor_struct, 'Do_srcERF_stat',Pathsrc,cond1,cond2,toi, avgtoi, correct, method, stattest, outdata);
    end
    end 
end
 
%% Submit
%---------
obob_condor_submit(condor_struct);