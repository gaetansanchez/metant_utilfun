
% RUN on cluster fif2epo

%% clear
%--------
clear all global
close all


%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/Docleaning/' date]);

%% Condor call
%---------------
cfg = [];
cfg.mem = '15G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
[Suj] = Do_sel_subjects(config,'metaNT');

Suj = [3 6 7 8 9 16 17 18 21]; % only rejected subjects

for i = 1:numel(Suj)
  id = Suj(i);
  condor_struct = obob_condor_addjob(condor_struct, 'Do_cleandata_based_on_previousdataset',id);
end

%% Submit
%---------
obob_condor_submit(condor_struct);
