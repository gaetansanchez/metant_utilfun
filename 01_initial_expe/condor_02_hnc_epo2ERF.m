
% RUN epo2ERF
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2ERF/' date]);

pathpreproc = fullfile(config.path,'preproc','hpfilt_0.1');
Pathsave = fullfile(config.path2work,'erf','hpfilt_0.1');

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);


%% Parameters ERF
%--------------------
filt_lim = 30;
toi      = [-.5 .7]; % [-1 1]
blwin    = [-0.2 0];
singletrials = 0; % 1 or 0

%% Subjects
%-----------
[Suj] = Do_sel_subjects(config,'metaNT');

%% Jobs additions
%-----------------

for i = 1:numel(Suj)
  id = Suj(i);
  
  input = fullfile(pathpreproc,sprintf('clean_%02d_allblocks.mat',id));
  condor_struct = obob_condor_addjob(condor_struct, 'hnc_epo2erf',...
                        input, ...
                        Pathsave,...
                        singletrials,...
                        toi,...
                        filt_lim,...
                        blwin);
end

%% Submit
%---------
obob_condor_submit(condor_struct);

