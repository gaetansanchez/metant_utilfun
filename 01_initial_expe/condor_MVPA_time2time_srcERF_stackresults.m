% RUN stack results fo time2time analysis from condor_MVPA_time2time_srcERF_parcomp
% (call in a loop = Do_mvpa_results_stack.m)
% With more paralelle computing !!!!

%% Path settings
%----------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT',cfg);

Pathlogs = fullfile(config.path,['jobs/mvpa_t2t_srcERF_stackresults/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
[Suj] = Do_sel_subjects(config,'metaNT');

%% Files name
%-------------

paradigm = 'hit_vs_miss';% 'hit_vs_miss' or 'supravs_sham' or []
HPfilt = ['hpfilt_0.1_' paradigm]; %  hpfilt_1 or hpfilt_0.1
Type1 = 'zscoreNorm_trad0';
Type2  = '_toilim_0to600_tstep20ms_srad30_Bayes';

Pathinput = fullfile(config.path2work,...
  ['mvpa/t2t_temp/cross_acc_' Type1 '/' HPfilt Type2]);

Pathoutput = fullfile(config.path2work,...
  ['mvpa/t2t_srcERF/cross_acc_' Type1 '/' HPfilt Type2]);

Nametype = 'MVPA_src';

%% Parameters + add jobs
%-----------------------
Nbrfiles = 7;
Delfile  = 1; % delete files after stack results : 1 = yes / 0 = no

for i = 1:numel(Suj)
  id = Suj(i);
  condor_struct = obob_condor_addjob(condor_struct, 'Do_mvpa_results_stack',id,Nbrfiles,Pathinput,Pathoutput,Nametype,Delfile);
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);







