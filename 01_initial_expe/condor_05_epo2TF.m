

% RUN epo2TF
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/epo2TF/' date]);

pathpreproc = fullfile(config.path,'preproc');
Pathsave = fullfile(config.path2work,'tf');

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '6G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);


%% Parameters TF
%--------------------
method = 'mtmconvol';% 'mtmconvol' 'wavelet'
taper  = 'hanning';
toi    = -1:0.05:1;
foi    = 2:90;
ftimwin= 7; % cycles per freq (or wavelet factor)
smofrq = 2;% freq smoothing 
singletrials = 0; % 1 or 0

%% Jobs additions
%-----------------
Suj = 1:25;
BadSuj = [];
load(fullfile(config.path,'BadsubjectsBehav'));
Suj(BadSuj) = []; % remove bad subjects

for i = 1:numel(Suj)
  id = Suj(i);
  
  input = fullfile(pathpreproc,sprintf('clean_%02d_allblocks.mat',id));
  condor_struct = obob_condor_addjob(condor_struct, 'hnc_epo2TF',...
                        input, ...
                        Pathsave,...
                        singletrials,...
                        method,taper,toi,foi,ftimwin,smofrq);
                      
end

%% Submit
%---------
obob_condor_submit(condor_struct);