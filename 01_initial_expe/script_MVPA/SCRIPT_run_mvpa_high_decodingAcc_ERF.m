%% Script to run MVPA with high decoding accuracy
%------------------------------------------------

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Time2Time decoding at sensors level (ERF)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1 ==> Prepare ERF (sort trials + epoched + downsample + timelocked + combine grad...)
%---------------------
condor_erf4MVPA

%% 2 ==> Do real MVPA computation with parallelisation (9 jobs per subjects)
%----------------------------------------------------------

condor_MVPA_time2time_nosl_parcomp


%% Define type of filename/path
%------------------------------
chan_types    = {'meg_axial' 'meg_planar_combined'};
chan_types    = {'meg_planar_combined'};

paradigm =  'hit_vs_miss';% 'hit_vs_miss' or 'supravs_sham' or []
HPfilt = ['hpfilt_0.1_' paradigm]; %  hpfilt_1 or hpfilt_0.1
Type1 = 'zscoreNorm_trad1';
Type2 = ['_toilim_-200to600_tstep10ms_chan_' [chan_types{:}] '_Bayes'];

Pathinput = fullfile(config.path2work,'mvpa','t2t_nosl_temp',...
  ['cross_acc_' Type1 '/' HPfilt Type2]);
Pathoutput = fullfile(config.path2work,'mvpa','t2t_nosl_ERF',...
  ['cross_acc_' Type1 '/' HPfilt Type2]);

%Nametype = [Type1 '_' Type2  '_MVPA_t2tERF_cross'];
Nametype = ['MVPA_t2tERF'];

Path2saved = fullfile(Pathoutput,'MeanDSFT');
Pathstat   = fullfile(Pathoutput,'stat'); % saved stat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 3 ==> Get results and stack data per subject
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Suj] = Do_sel_subjects(config,'metaNT');

Nbrfiles = 1;
Delfile = 1; % delete files after stack results : 1 = yes / 0 = no

[MissF] = Do_mvpa_results_stack(Suj,Nbrfiles,Pathinput,Pathoutput,Nametype,Delfile)

%--------------------------------
% Compute average Acc over group
%--------------------------------

% inspired from : SCRIPT_mvpa_t2t_analyse.m (average Acc over subjects)

if ~exist(Path2saved,'dir') % create output dir if dont exist
  mkdir(Path2saved);
else
end
AnalysisName = 'MVPA_t2tERF';

Files = dir(fullfile(Pathoutput,[AnalysisName '*']));
nbF = numel(Files);
if nbF ~= 16
  error(['Number of files is: ' num2str(nbF)]);
end


M_t = [];
for i = 1:nbF
  disp(['---> Load ' sprintf('File %02d/%02d ...',i,nbF)]);
  load(fullfile(Pathoutput,Files(i).name));
  
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      M_tot(i,j,k) = M{j,k};
      
      cdt_ds = M_tot(i,j,k);
     
      % convert to fieldtrip format
      %-----------------------------
      
      [data, labels, values] = cosmo_unflatten(cdt_ds,1);
      
      % Extract accuracy
      %------------------
      M_t(i,j,k,:,:) = data;
      
    end
  end
end

% Mean accuracy
%---------------
Msacc = squeeze(mean(M_t,1)); % average accuracy over subjects
val   = round(values{1}*1000);

% Save
%--------
disp('*** Saving Files ***');

% Save single subjects accuracy
%--------------------------------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','M_t','val');
disp('............');

% Save Mean accuracy
%-------------------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_' sprintf('N%02d',nbF) '.mat']);
save(FileNameSaved,'-v7.3','Msacc','val');

disp('*** DONE ! ***');

%% 4 ==> Statistics
%--------------------

condor_MVPA_time2time_nosl_stat

%% Plot nice figure !
%---------------------

lab = {'train_time' 'test_time'};
sensmod = {'aud' 'vis' 'tac'};

% Plot 3by3
%-----------
FileNameSaved = fullfile(Path2saved,['MeanGroupAcc_N16.mat']);
load(FileNameSaved);

%----------------
% Do the plotting
%----------------
smooth = 5;
LimAx  = [0.35 0.65];
data = Msacc;

cfg =[];
cfg.xaxis  = val;
cfg.xytick = find(ismember(val',[0:100:600]));
cfg.smooth = smooth;
%cfg.mask   = stat_mask_t;
cfg.LimAx  = LimAx;
cfg.mask_int = 'linear';
[h] = plot_3by3_MVPA(data,cfg);
colormap('jet');
set(h, 'Position', [0, 0, 1200, 1600]);


%% Plot individual subject
%---------------------------
[Suj] = Do_sel_subjects(config,'metaNT');

% Plot 3by3
%-----------
FileNameSaved = fullfile(Path2saved,['SubjectsAcc_N16.mat']);
load(FileNameSaved);


for i = 1:size(M_t,1)
    data = [];
    data = squeeze(M_t(i,:,:,:,:));
    %----------------
    % Do the plotting
    %----------------
    smooth = 0;
    LimAx  = [0.4 0.6];
    
    cfg =[];
    cfg.xaxis  = val;
    cfg.xytick = find(ismember(val',[0:100:600]));
    cfg.smooth = smooth;
    %cfg.mask   = stat_mask_t;
    cfg.LimAx  = LimAx;
    cfg.mask_int = 'linear';
    [h] = plot_3by3_MVPA(data,cfg);
   % colormap('jet');
    set(h, 'Position', [0, 0, 1200, 1600]);
    title(['Sub = ' num2str(Suj(i))],'FontSize',13,'FontWeight','bold');
    
end




