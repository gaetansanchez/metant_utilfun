% test script searchlight brain with all time features
%------------------------------------------------------

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Load data
%----------
datafile = 'MVPA_srchpfilt_0.1_hit_vs_miss_toilim_0to600_tstep10ms_BeamCFiltsensmod_01.mat';
filename = fullfile(config.path2work,'mvpa','src4MVPA',datafile);

load(filename);

%% setup decoding
%----------------


opt=struct();
opt.classifier    = @cosmo_classify_naive_bayes;
opt.normalization = 'zscore';

source_radius = 3;

train_modality = 1;
test_modality  = 1;


toi = [0.14 0.36];

%% reduce the number of time points
msk = cosmo_dim_match(ds,'time',@(x) toi(1)<=x & x<=toi(end));
ds_t = cosmo_slice(ds,msk,2);
ds_t = cosmo_dim_prune(ds_t,'matrix_labels',{'pos'});

timepoints = ds_t.a.fdim.values{3};

%% Compute t2t MVPA
%------------------------------------------------------
% select data in train and test modality
msk  = cosmo_match(ds_t.sa.modality,[train_modality test_modality]);
ds_c = cosmo_slice(ds_t,msk);
ds_c = cosmo_dim_prune(ds_c,'matrix_labels',{'pos'}); % update dim attributes
  
% Redefine chunks
%-----------------
ds_c.sa.chunks = (1:size(ds_c.samples,1))';
nchunks = 4;
ds_c.sa.chunks=cosmo_chunkize(ds_c,nchunks);

ds_sel = ds_c;
  
% Adapt partitions 
%-----------------
if train_modality==test_modality
  % within-modality cross-validation
  %--------------------------------
  par_temp = cosmo_nchoosek_partitioner(ds_sel,1);

else
  % cross-modality cross-validation
  %--------------------------------
  par_temp = cosmo_nchoosek_partitioner(ds_sel,1,'modality',test_modality);

end

opt.partitions = cosmo_balance_partitions(par_temp, ds_sel);

% Remove features with NaN, Inf or non informative (similar data for all targets)
%-------------------------------------------------------------------------------
ds_sel = cosmo_remove_useless_data(ds_sel);

% define neighborhood over space;
%--------------------------------------
nbrhood = cosmo_spherical_neighborhood(ds_sel,'radius',source_radius);

% Measure
%----------
measure   = @cosmo_crossvalidation_measure;

% Run the measure
%----------------
nbrids = 1:numel(nbrhood.neighbors); % all nrhood
opt.center_ids = nbrids; % just run MVPA for some neighborhood
ds_result = cosmo_searchlight(ds_sel,nbrhood,measure,opt);% RUN decoding !!!


%% visualize brain accuracy results

% map to FT struct for visualization
data = cosmo_map2meeg(ds_result);

data.freq = 1;
data.time = 1;

data.pow(:,1,1) = data.avg.pow;
data.mask = ones(numel(data.avg.pow),1);

val2plot = [0.5 0.7];
mask2plot = {'mask'};
param2plot = {'pow'};
colormaplabel= 'jet';
fname = 'test';
dosurface = 1;
dosavefile = 0;
toi = [1 1];

[Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile,toi);



