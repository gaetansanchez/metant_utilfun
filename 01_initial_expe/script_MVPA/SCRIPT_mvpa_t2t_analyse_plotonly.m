
% Plot MVPA brain
%-----------------

clear all;

% Path definition
%-------------------
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT',cfg);

% NFiles Name
%--------------
% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad1/toilim_-200to700_tstep10ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_trad1_toilim_-200to700_tstep10ms_srad30_Bayes_MVPA_src_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm/toilim_-500to500_tstep30ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_toilim_-500to500_tstep30ms_srad30_Bayes_MVPA_src_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm/toilim_-250to700_tstep30ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_toilim_-250to700_tstep30ms_srad30_Bayes_MVPA_src_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_NozscoreNorm/toilim_-500to500_tstep30ms_srad30_Bayes';
% AnalysisName = 'NozscoreNorm_toilim_-500to500_tstep30ms_srad30_Bayes_MVPA_src_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad1/toilim_-200to600_tstep10ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_trad1_toilim_-200to600_tstep10ms_srad30_Bayes_MVPA_srchpfilt_0.1_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad1/hpfilt_1_toilim_-200to600_tstep10ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_trad1_toilim_-200to600_tstep10ms_srad30_Bayes_MVPA_srchpfilt_1_t2tERF_BeamCFiltsensmod_cross_persens_N16';

% Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad0/hpfilt_0.1_toilim_-200to600_tstep10ms_srad30_Bayes';
% AnalysisName = 'zscoreNorm_trad1_toilim_-200to600_tstep10ms_srad30_Bayes_MVPA_srchpfilt_1_t2tERF_BeamCFiltsensmod_cross_persens_N16';
%Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad0/hpfilt_0.1_supra_vs_sham_toilim_0to600_tstep20ms_srad30_Bayes';

Pathdata  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm_trad0/dd_vs_ud_allsens_hpfilt_0.1_hit_vs_miss_toilim_-200to600_tstep10ms_srad30_Bayes';
AnalysisName = 'MVPA_SRC_N16';

toi = [0 0.5]; % in sec

%%%%%
Pathsaved = fullfile(config.path2work,Pathdata,'MeanDSFT'); % saved data
Pathstat = fullfile(config.path2work,Pathdata,'stat'); % saved stat

lab = {'train_time' 'test_time'};
%sensmod   = {'aud' 'vis' 'tac'};
sensmod   = {'all'};

% Load and threshold
%--------------------
stat_mask = [];
dat_pdata = {};
dat_sig   = {};
mask_p    = {};
sig_roi   = {};
sig_toi   = {};
sig_roi_diag   = {};
sig_roi_diag_t = {};
mask_int       = 0;
mask_brain_int = 0;

for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    load(fullfile(Pathstat,['STAT_niter10000_train_' sensmod{i} '_vs_test_' sensmod{j} '_' AnalysisName '.mat']));
    
    % To convert the output (say stat_map)
    % from cosmo montecarlo cluster stat to matrix form (time by time), do
    %-----------------------------------------------------------------------
    [data, labels, values] = cosmo_unflatten(stat_map,2,'matrix_labels',{'pos'});
    data_mat = squeeze(data);
    
    % Cut time
    %----------
    timepnts = round(values{2},2);
    startpoint = find(timepnts == toi(1));
    stoppoint  = find(timepnts == toi(2));
    
    
    disp('*********************');
    disp(['Train = ' sensmod{i} ' / Test = ' sensmod{j}])
    disp(['Analysis from ' num2str(timepnts(startpoint)*1000) ' ms to ' num2str(timepnts(stoppoint)*1000) ' ms ...']);
    data_mat = data_mat(:,startpoint:stoppoint,startpoint:stoppoint);
    
    zdata{i,j} = data_mat; % keep save zvalue
    
    Compute_sig = 'zval';
    
    switch Compute_sig
      
      case 'zval'
        
        % with z-values
        %-----------------
        pdata = data_mat;
        dat_pdata{i,j} = pdata; % save p values time course
        % sig  = data_mat>1.65; % z-values / (alpha 0.05) value > H0mean (H0mean = 0.5)
        % sig  = abs(pdata)>1.9600; % z-values / (alpha 0.01) value > H0mean (H0mean = 0.5)
        sig  = abs(pdata)>3.1;% z-values / (alpha 0.001) value > H0mean (H0mean = 0.5)
        
%         % Find 10% of significant Z-values
%         %----------------------------------
%         opt = []; opt.lim = 10; opt.flag = 1;
%         %x = pdata(abs(pdata)>1.6449);
%         x = pdata(abs(pdata)>3.1);
%         [bound] = find_distrib_percent(x,opt);
%         sig  = abs(pdata)>=bound(2);
        
      case 'pval'
        
        % with p-values
        %-----------------
        pdata = normcdf(data_mat);
        dat_pdata{i,j} = pdata; % save p values time course
        th = 1 - 0.05/9; % alpha 0.05 + Bonferroni correction for 9 decoding
        sig  = pdata>th; % p-values
    end
    
    dat_sig{i,j} = sig; % save mask p values
    
    % Extract significant time points
    %----------------------------------
    Number2sig = 1; % minimum number of significant ROI
    
    mask = squeeze(sum(sig,1))>=Number2sig; % map train / test (average sensors)
    
    if i ~= j
      mask_int = mask+mask_int;
    end
    
    mask_p{i,j} = squeeze(max(pdata));
    mask_bin{i,j} = mask;
    
    figure;
    subplot(1,3,1);
    imagesc(mask_p{i,j});colorbar;caxis([0.9 1]);
    subplot(1,3,2);
    imagesc(mask);colorbar;
    subplot(1,3,3);
    imagesc(squeeze(sum(sig,1)));colorbar;
    
    % Extract spatial mask according to significant time points
    %-----------------------------------------------------------
    Number2sig_time = 1;
    sig_roi{i,j} = squeeze(sum(sig(:,mask),2));
    sig_toi{i,j} = sig_roi{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    if i ~= j
      mask_brain_int = sig_toi{i,j}+mask_brain_int;
    end
    
    % Same but just for diagonal (same training/testing time)
    %---------------------------------------------------------
    sig_roi_diag{i,j} = squeeze(sum(sig(:,logical(eye(size(data_mat,2),size(data_mat,3)))),2));
    sig_toi_diag{i,j} = sig_roi_diag{i,j}>=Number2sig_time; % just ROI with 1 or more than 1 significant time point
    
    % Extract diagonal spatial feature (movie)
    %------------------------------------------
    for t = 1:size(sig,3)
      sig_roi_diag_t{i,j,t} = logical(squeeze(sig(:,t,t))); % take spatial mask for each diagonal time
    end
    
    % Store temporal mask
    %----------------------
    stat_mask(i,j,:,:) = mask;
  end
end

% figure;
% surf(mask_int);colorbar;

% Analysis similarity intersect
%-------------------------------
[CVroi_all,IUroi_all,AIDroi_intensity] = Do_t2t_metaNTdec_statmask_sl_intersect(sig_toi,sig_toi_diag,sig_roi);

% Average Brain Accuracy for only significant time point
%---------------------------------------------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);
if numel(sensmod) == 1
  Msacc_small = Msacc(:,startpoint:stoppoint,startpoint:stoppoint);% remove prestim data
else
  Msacc_small = Msacc(:,:,:,startpoint:stoppoint,startpoint:stoppoint);% remove prestim data
end

Msig_acc = [];
Maxsig_acc = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    
    if numel(sensmod) == 1
      % average Acc for significant Time point
      %----------------------------------------
      Msig_acc(i,j,:) = mean(squeeze(Msacc_small(:,logical(stat_mask(i,j,:,:)))),2);
      
      % Max Acc for significant Time point
      %----------------------------------------
      Maxsig_acc(i,j,:) = max(squeeze(Msacc_small(:,logical(stat_mask(i,j,:,:)))),[],2);
    else
      % average Acc for significant Time point
      %----------------------------------------
      Msig_acc(i,j,:) = mean(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),2);
      
      % Max Acc for significant Time point
      %----------------------------------------
      Maxsig_acc(i,j,:) = max(squeeze(Msacc_small(i,j,:,logical(stat_mask(i,j,:,:)))),[],2);
      
    end
    
  end
end
if numel(sensmod) == 3
  Msig_acc_Between = mean([squeeze(Msig_acc(1,2,:)) squeeze(Msig_acc(1,3,:)) ...
    squeeze(Msig_acc(2,1,:)) squeeze(Msig_acc(2,3,:)) ...
    squeeze(Msig_acc(3,1,:)) squeeze(Msig_acc(3,2,:))],2);
  
  Maxsig_acc_Between = max([squeeze(Maxsig_acc(1,2,:)) squeeze(Maxsig_acc(1,3,:)) ...
    squeeze(Maxsig_acc(2,1,:)) squeeze(Maxsig_acc(2,3,:)) ...
    squeeze(Maxsig_acc(3,1,:)) squeeze(Maxsig_acc(3,2,:))],[],2);
end

% Mask of intersect 10% accuracy
%----------------------------------
opt = []; opt.lim = 10; opt.flag = 0;
mask10  = zeros(numel(sensmod),numel(sensmod),889);

for strain = 1:numel(sensmod)
  for stest = 1:numel(sensmod)
      x = squeeze(Msig_acc(strain,stest,:));
      [bound] = find_distrib_percent(x,opt);
      mask10(strain,stest,x>=bound(2)) = 1;% mask > 10% acc
  end
end

if numel(sensmod) == 3
  mask10_Uaudvistrain = logical(sum([squeeze(mask10(1,2,:)) squeeze(mask10(2,1,:))],2)>=1);
  mask10_Uaudtactrain = logical(sum([squeeze(mask10(1,3,:)) squeeze(mask10(3,1,:))],2)>=1);
  mask10_Utacvistrain = logical(sum([squeeze(mask10(2,3,:)) squeeze(mask10(3,2,:))],2)>=1);
  
  mask10_IU = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)==3);
  mask10_U  = logical(sum([mask10_Uaudvistrain mask10_Uaudtactrain mask10_Utacvistrain],2)>=1);
end

% Find average latency for each brain nodes of Intersect(Union)
%----------------------------------------------------------------
val = timepnts(startpoint:stoppoint);

Nnodes   = 889;
Nsensmod = numel(sensmod);

Mlatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);
Slatsig_Nodes = NaN(Nnodes,Nsensmod,Nsensmod);

for k = 1:Nnodes
  for i = 1:Nsensmod
    for j = 1:Nsensmod
      latsig = [];
      latsig = val(find(sum(squeeze(dat_sig{i,j}(k,:,:)),2))); % find only significant latency
      Mlatsig_Nodes(k,i,j) = mean(latsig);
      Slatsig_Nodes(k,i,j) = std(latsig)./sqrt(length(latsig));
    end
  end
end

if numel(sensmod) == 3
  Dat4Between = [Mlatsig_Nodes(IUroi_all,logical(~eye(3,3)))];
  Mlatsig_BETW_IUroi_all = nanmean(Dat4Between,2);
  Slatsig_BETW_IUroi_all = nanstd(Dat4Between,[],2)./sqrt(sum(~isnan(Dat4Between),2));
  
  Mlatsig_BETW_CVroi_all = nanmean([Mlatsig_Nodes(CVroi_all,logical(~eye(3,3)))],2);
  
  Mlatsig_AUD_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,1,1)],2);
  Mlatsig_VIS_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,2,2)],2);
  Mlatsig_TAC_IUroi_all = nanmean([Mlatsig_Nodes(IUroi_all,3,3)],2);
end

% Max Zvalue compilation
%------------------------

Mzscore = [];
for i = 1:Nsensmod
  for j = 1:Nsensmod
    Mzscore(i,j,:,:) = mask_p{i,j};
    
  end
end


%-----------
%% Plot 3by3
%-----------
close all;

FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

% Cut time representation depending on startpoint
%--------------------------------------------------
val   = val(startpoint:stoppoint);
if numel(sensmod) == 3
  Mg = Mg(:,:,startpoint:stoppoint,startpoint:stoppoint);
else
  Mg = Mg(startpoint:stoppoint,startpoint:stoppoint);
end

% zscore or accuracy
%--------------------
DOzscore = 0;
if DOzscore == 1
  data = Mzscore;
  LimAx  = [0 4];
else
  data = Mg;
  LimAx  = [0.45 0.55];
end


if numel(size(data)) == 2 % in case we are decoding all sens (data size 33x33)
  data2(1,1,:,:) = data; % make it 1x1x33x33 for plotting
  data = data2;
end

% transparency level
%--------------------
transp = 0.5;
stat_mask_t = stat_mask+transp;
stat_mask_t(stat_mask_t>1) = 1;

% Compute Union accuracy over time
%-----------------------------------
dataU = [];
if numel(sensmod) == 3
  dataU(1,:,:) = squeeze(data(1,2,:,:));
  dataU(2,:,:) = squeeze(data(1,3,:,:));
  dataU(3,:,:) = squeeze(data(2,1,:,:));
  dataU(4,:,:) = squeeze(data(2,3,:,:));
  dataU(5,:,:) = squeeze(data(3,1,:,:));
  dataU(6,:,:) = squeeze(data(3,2,:,:));
  
  MdataU = squeeze(mean(dataU,1)); % average accuracy
  stat_mask_tU = logical(mask_int); % Union sigificant time points (between sensory modality)
  
  MdataUplot(1,1,:,:) =  MdataU;
  stat_mask_tUplot(1,1,:,:) = stat_mask_tU;
  
  stat_mask_tUplot = stat_mask_tUplot+transp; % transparency
  stat_mask_tUplot(stat_mask_tUplot>1) = 1;
end

%----------------
% Do the plotting
%----------------
val2p  = val-4;
%val2p  = ceil(val/10)*10;
xytick = find(ismember(val2p,[0:100:500]));

smooth = 5;

cfg =[];
cfg.xaxis  = val2p';
cfg.xytick = xytick;
cfg.smooth = smooth;
cfg.mask   = stat_mask_t;
cfg.LimAx  = LimAx;
cfg.mask_int = 'linear';
[h] = plot_3by3_MVPA(data,cfg);
colormap('jet');
%set(h, 'Position', [0, 0, 1100, 1600]);

if ~isempty(dataU)
  cfg =[];
  cfg.xaxis  = val2p';
  cfg.xytick = xytick;
  cfg.smooth = smooth;
  cfg.mask   = stat_mask_tUplot;
  cfg.LimAx  = LimAx;
  cfg.mask_int = 'linear';
  [h2] = plot_3by3_MVPA(MdataUplot,cfg);
  colormap('jet');
  %set(h2, 'Position', [0, 0, 400,300]);
end

dosave = 0;
fname01 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_3vs3_MaskStat_' AnalysisName]);
fname02 = fullfile(config.path,'doc','mvpa',['MVPA_t2t_Union_MaskStat_' AnalysisName]);
if dosave
  % save png for record
  %----------------------
  name2 = [fname01 '.png'];
  img = getframe(h);
  imwrite(img.cdata,name2);
  
  name3 = [fname02 '.png'];
  img = getframe(h2);
  imwrite(img.cdata,name3);
end

%% Average time course of Accuracy generalization
%-------------------------------------------------

DOzscore = 0;
if DOzscore == 1
  M2process = Mzscore; % data
else
  M2process = Mg; % data
end

try
  data = [];
  data(1,1,:,:) = M2process;
catch
  data = M2process;
end

factms = 10; % in ms
yborders  = [0 300]; % in ms
xborders  = [0 500];% in ms
acclimits = [0.5 0.53];
Do_t2t_metaNTdec_statmask_plot_generalization(data,val,stat_mask,acclimits,xborders,yborders,factms)


%% Plot just accuracy
%---------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

try
  data = [];
  data(1,1,:,:) = Mg;
catch
  data = Mg;
end


if ~exist('val','var')
  load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
  val   = round(ds_ft{1,1,1}.time*1000);
end

val2p = val-4;

cfg =[];
cfg.xaxis = val2p;
cfg.smooth = 2;
cfg.LimAx = [0.5 0.53];
%cfg.LimAx = [0 0.2];
plot_3by3_MVPA(data,cfg);

%% Extract diagonal decoding
%-----------------------------
FileNameSaved = fullfile(Pathsaved,['MeanGroupAcc_' AnalysisName '.mat']);
load(FileNameSaved);

%sensmod = {'aud' 'vis' 'tac'};
sensmod = {'all'};
if numel(sensmod)==1
  Mg2p(1,1,:,:) = Mg;
else
  Mg2p = Mg;
end


Acc = [];
for i = 1:numel(sensmod)
  for j = 1:numel(sensmod)
    for k = 1:size(Mg2p,3)
      Acc(i,j,k) = Mg2p(i,j,k,k);
    end
  end
end

col = {'r' 'k' 'g'};
lw = 2;
figure;
for i = 1:numel(sensmod)
  subplot(1,numel(sensmod),i);
  for j = 1:numel(sensmod)
    plot(val,squeeze(Acc(i,j,:)),col{j},'Linewidth',lw);hold on;
    ylim([0.5 0.55]);
  end
  legend(sensmod,'FontSize',15,'FontWeight','bold')
  title(['Tested data with Training data = ' sensmod{i}],'FontSize',20,'FontWeight','bold')
end


%% plot voxel brain: each decoding
%----------------------------------

if ~exist('ds_ft','var')
  Pathsaved = fullfile(config.path2work,Pathdata,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end

TabALL = {};
val2plotstore = {};
for strain = 1:numel(sensmod)
  for stest = 1:numel(sensmod)
    data = ds_ft{1,1,1};
    
    selroi = AIDroi{strain,stest};
    
    mask = zeros(1,889);
    mask(selroi) = 1;
    
    data.pow = data.avg.pow(data.inside,:,:);
    data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));
    
    binary_plot = 2; % 0 = number of time found as significant / 2 = accuracy / 1 = significant or not
    dosurface   = 0; % 0 = caret brain / 1 = surface brain
    dosavefile  = 1;
    
    fname = fullfile(config.path,'doc/mvpa/t2t_brain',['Test001_Cluster_train_' sensmod{strain} '_vs_test_' sensmod{stest} '_' AnalysisName]);
    
    if binary_plot == 1
      val2plot = [0 5];
      % Fill-in with fix value when source was found
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          data.pow(selroi,i,j) = 5;
          data.mask(:,i,j) = mask;
        end
      end
      
    elseif binary_plot == 0
      val2plot = [0 max(sig_roi{strain,stest}(selroi))];
      %val2plot = [0 20];
      
      % Fill-in with number of time source was found
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          data.pow(selroi,i,j) = sig_roi{strain,stest}(selroi);
          data.mask(:,i,j) = mask;
        end
      end
      
    else
      
      acc10 = 1;
      if acc10 == 1
        % Find 10% of significant accuracy
        %----------------------------------
        opt = []; opt.lim = 10; opt.flag = 0;
        %x = squeeze(Msig_acc(strain,stest,selroi));
        x = squeeze(Msig_acc(strain,stest,:));
        [bound] = find_distrib_percent(x,opt);
        mask  = zeros(1,889);
        mask(squeeze(Msig_acc(strain,stest,:))>=bound(2)) = 1;% mask > 10% acc
        %val2plot = [bound(2) max(x)];
        %val2plot = [0.45 max(x)];
        val2plot = [0.5 0.52];
        val2plotstore{strain,stest} = val2plot;
      else
        %val2plot = [0 max(Msig_acc_Between(selroi))];
        x = squeeze(Msig_acc(strain,stest,:));
        val2plot = [0.5 max(x)];
      end
      
      % Fill-in with accuracy
      for i = 1:size(data.pow,2)
        for j = 1:size(data.pow,3)
          %data.pow(selroi,i,j) = Msig_acc(strain,stest,selroi)-val2plot(1);
          data.pow(selroi,i,j) = Msig_acc(strain,stest,selroi);
          data.mask(:,i,j) = mask;
        end
      end
      
      %val2plot = val2plot-val2plot(1);
      
    end
    
    param2plot = {'pow'};
    mask2plot  = {'mask'};
    colormaplabel = 'jet';
    [TabALL{strain,stest}] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile);
    
  end
end

%% plot voxel CARET brain : Only intersect Between Modalities
%------------------------------------------------------
if ~exist('ds_ft','var')
  Pathsaved = fullfile(config.path2work,Pathdata,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end

fname = fullfile(config.path,'doc/mvpa',['IntersectIntersect_Acc_p0999_ROI1_Between_SensMod_' AnalysisName]);
%fname = fullfile(config.path,'doc/mvpa',['Test_Between_SensMod_' AnalysisName]);

dosavefile = 1;
dosurface  = 0;
% binary_plot
%-------------
% 0 = number of time found as significant
% 1 = significant or not
% 2 = accuracy
% 3 = number of crossvalidation windows
% 4 = separate sens mod training intersect (not working yet...)
binary_plot = 2;

if numel(size(ds_ft)) == 3
  data = ds_ft{1,1,1};
else
  data = ds_ft{1,1};
end

if binary_plot == 3
  mask = zeros(1,889);
  lab2mask = Umask_brain_int_123;
  %   lab2mask = mask_brain_int;
  %  lab2mask = Usensmask_brain_int_123;
  mask(lab2mask>0) = 1;
  
elseif binary_plot == 4
  mask = zeros(1,889);
  % mask(UIroi_all) = 1; % union of intersect training sens
else
  selroi = IUroi_all; % only spatial features intersect of union between cross-val
  selroi = CVroi_all; % only spatial features between modalities (intersect)
  %selroi = CVroi_all_union; % union
  
  mask = zeros(1,889);
  mask(selroi) = 1;
end

data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

if binary_plot == 1
  val2plot = [0 1];
  
  % Fill-in with fix value when source was found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.pow(selroi,i,j) = val2plot(end);
      data.mask(:,i,j) = mask;
    end
  end
  
  
  mask2plot = {'mask'};
  param2plot = {'pow'};
  
elseif binary_plot == 0
  val2plot = [0 max(AIDroi_intensity(selroi))];
  %val2plot = [0 40];
  % Fill-in with number of time source was found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.pow(selroi,i,j) = AIDroi_intensity(selroi);
      data.mask(:,i,j) = mask;
    end
  end
  
  
  mask2plot = {'mask'};
  param2plot = {'pow'};
  
elseif binary_plot == 2
  % acc10 options:
  %---------------
  % 1 = only 10% acc from mask intersect significant
  % 2 = make mask from 10% acc between sensmod decoding
  % 0 = plot all acc from mask intersect significant
  acc10 = 0; 
  val2plotstore = [];
  val2plot = [0.5 0.53];
  if acc10 == 1
    % Find 10% of significant accuracy
    %----------------------------------
    opt = []; opt.lim = 10; opt.flag = 0;
    x = Msig_acc_Between;
    [bound] = find_distrib_percent(x,opt);
    mask  = zeros(1,889);
    mask(Msig_acc_Between>=bound(2)) = 1;% mask > 10% acc
    
    selroi = 1:889; % No selection of intersection mask
    %val2plot = [bound(2) max(x)];
    %val2plot = [0.45 max(x)];
    
  elseif acc10 == 2
    % Mask of intersect of union 10% accuracy
    %----------------------------------
    mask  = zeros(1,889);
    mask  = mask10_IU';

    selroi = 1:889; % No selection of intersection mask
    x = Msig_acc_Between;
    
  else
    %val2plot = [0 max(Msig_acc_Between(selroi))];
    x = Msig_acc_Between;
  end
  
  val2plotstore = val2plot;
  
  % Fill-in with accuracy values found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.pow(selroi,i,j) = Msig_acc_Between(selroi)-val2plot(1);
      %data.pow(selroi,i,j) = Maxsig_acc_Between(selroi)-val2plot(1);
      data.mask(:,i,j) = mask;
      data.maskInt(:,i,j) = mask;
      data.maskInt(selroi,i,j) = AIDroi_intensity(selroi);
    end
  end
  
  val2plot = val2plot-val2plot(1);
  
  
  mask2plot = {'mask'};
  %mask2plot = {'maskInt'};
  param2plot = {'pow'};
  
elseif binary_plot == 3
  
  val2plot = [1 max(lab2mask)];
  % Fill-in with number of intersection was found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      %data.pow(:,i,j) = mask_brain_int;
      data.pow(:,i,j)  = lab2mask;
      data.mask(:,i,j) = mask;
    end
  end
  
  mask2plot = {'mask'};
  param2plot = {'pow'};
  
  
elseif binary_plot == 4
  val2plot = [0 1];
  % Fill-in with specific union network found
  data.powaud = data.pow;
  data.powvis = data.pow;
  data.powtac = data.pow;
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.powaud(CV_audtrain,i,j) = 1;
      data.powvis(CV_vistrain,i,j) = 1;
      data.powtac(CV_tactrain,i,j) = 1;
      
      data.maskaud(:,i,j) = mask;
      data.maskvis(:,i,j) = mask;
      data.masktac(:,i,j) = mask;
      
      data.maskaud(CV_audtrain,i,j) = 1;
      data.maskvis(CV_vistrain,i,j) = 1;
      data.masktac(CV_tactrain,i,j) = 1;
    end
  end
  
  mask2plot = {'maskaud' 'maskvis' 'masktac'};
  param2plot = {'powaud' 'powvis' 'powtac'};
  
  
end

% now virtual sensor to src
%---------------------------
colormaplabel = 'jet';
Tab = [];
[Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile);
Tab(:,5) = num2cell(cell2mat(Tab(:,5))+val2plotstore(1))

%% Check ROI
%------------
testdat = data;
sigVox  = find(squeeze(sum(sum(data.mask,2),3))); % only significant voxels

MeanAccval  = Msig_acc_Between(sigVox); % mean Acc values for these voxels
MaxAccval   = Maxsig_acc_Between(sigVox); % max Acc values for these voxels
LatVox      = Mlatsig_BETW_IUroi_all*1000; % Mean Latency for these voxels (in ms)

% sort voxels indices based on their values
%--------------------------------------------
[sort_MeanAccval,indv] = sort(MeanAccval, 'descend');

% sort according to mean accuracy
%---------------------------------
LatVox = LatVox(indv); 
MaxAccval = MaxAccval(indv);
sigVox = sigVox(indv);

noteintval = [LatVox sort_MeanAccval MaxAccval]; % mean latency / mean Acc / Max Acc

Pathatlas = fullfile(config.pathgroup,'gsanchez/util_functions/parcellation');
A = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
%A = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
savefilename = 'labelsMNI2Parcelatlas_queryrange01to03_IntersectofUnion_commonMVPA_latency.mat';

testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)
[Tab] = Do_extractROIname_atlas(A,sigVox,noteintval,testdat,[],1);

% Extract other regions name from VoxID identified by previous atlas
%-------------------------------------------------------------------
%sigVox = cell2mat(Tab(:,6));
%B = ft_read_atlas('ROI_MNI_V4.nii');
B = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
[Tab2] = Do_extractROIname_atlas(B,sigVox,noteintval,testdat,[],1);

% Link both atlas based on Voxels indices
%------------------------------------------
NbClTab = size(Tab,2);
Tab_all = {};
for i = 1:size(Tab,1)
  id_betw_atlas = find(cell2mat(Tab2(:,5))==Tab{i,5});
  if ~isempty(id_betw_atlas)
    Tab_all(i,:) = [Tab(i,1) Tab2(id_betw_atlas,1) Tab(i,2:end)];
  else
    Tab_all(i,:) = [Tab(i,1) {[]} Tab(i,2:end)];
  end
end
Tab_all
save(savefilename, 'Tab_all');

%% Process Tab for atlas Parcel
%-------------------------------
B = load(fullfile(Pathatlas,'atlas_grid_333.mat'));
[Tab_net] = Do_extractROIname_atlas(B,sigVox,noteintval,testdat,[],1);

LabFam = {'Au', 'Vi', 'Sm' ,'De' ,'Ci', 'Do','Ve','Fr'};
%ValFam = [11 21 31 42 43 44 44 45];
ValFam = [11 22 33 44 45 46 46 47];
%LabFam = {'Au', 'Vi', 'Sm' ,'Do','Ve'};
%ValFam = [11 21 31 42 42];

NbClTab = size(Tab_net,2);

for i = 1:size(Tab_net,1)
  
  id_Fam = find(ismember(LabFam,Tab_net{i,1}(1:2)));
  
  if id_Fam
    Tab_net{i,NbClTab+1} = ValFam(id_Fam);
  else
    Tab_net{i,NbClTab+1} = 0;
  end
  
end
Tab_net

%% Tim course of different networks
%-----------------------------------
%startpoint = 17; stoppoint = 33;
% Get Voxels indices for different network
%------------------------------------------
%Net_Lab = {'Sens' 'Att'};
%Net_Lab = {'Sens' 'DMN' 'Cing' 'AtVD' 'FrPa'};
Net_Lab = {'Au' 'Vi' 'Sm'  'DMN' 'Cing' 'AtVD' 'FrPa'};
Net_Vox = {};
n = 1;
NbvarNet = numel(Net_Lab);
AbsentLab = [];
for i = 1:NbvarNet
  
  IDNet = cell2mat(Tab_net(find(mod(cell2mat(Tab_net(:,end)),10)==i),6));
  if isempty(IDNet)
    AbsentLab = [AbsentLab i];
  else
    Net_Vox{n} = IDNet;
    n = n+1;
  end
end
Net_Lab(AbsentLab) = [];

if ~exist('M_tc','var')
  % Load single subject Acc values
  %---------------------------------
  Pathsaved = fullfile(config.path2work,Pathdata,'MeanDSFT'); % path for saved data
  load(fullfile(Pathsaved,['SubjectsAcc_' AnalysisName '.mat']));
  
  % Cut accroding to start and stop point
  M_tc = M_t(:,:,:,:,startpoint:stoppoint,startpoint:stoppoint);
  val =val(startpoint:stoppoint);
end

% Loop average
%-------------
OnlySig  = 1; % Only significant time points : 1=yes or 0=all time points
Nsuj     = size(M_tc,1);
Nsensmod = size(M_tc,2);
Ntime    = size(M_tc,6);
MaccNet       = zeros(Nsuj,Nsensmod,Nsensmod,numel(Net_Vox),Ntime);
MaccNet_BETW  = zeros(Nsuj,6,numel(Net_Vox),Ntime);
MaccNet_WITH  = zeros(Nsuj,3,numel(Net_Vox),Ntime);
for s = 1:Nsuj % suj
  b = 1;
  w = 1;
  for i = 1:Nsensmod % modality
    for j = 1:Nsensmod % modality
      for k = 1:numel(Net_Vox) % network
        tmpdat = squeeze(M_tc(s,i,j,Net_Vox{k},:,:));
        
        sig_traintime = logical(sum(squeeze(stat_mask(i,j,:,:)),2));% significant time point over training axes
        tmpdat_sig = tmpdat;
        tmpdat_sig(:,repmat(~sig_traintime,1,size(tmpdat,3))) = 0.5;% set non significant time point to 0.5 significant time points
        
        if OnlySig
          tmpdat = tmpdat_sig;
        end
        
        MaccNet(s,i,j,k,:) = mean(mean(tmpdat,3),1); % average over : test time + Network sources
        if i ~= j % only between sensmod decoding
          MaccNet_BETW(s,b,k,:) = mean(mean(tmpdat,3),1);
        else
          MaccNet_WITH(s,w,k,:) = mean(mean(tmpdat,3),1);
        end
      end
      if i ~= j % only between sensmod decoding
        b = b+1;
      else
        w = w+1;
      end
    end
  end
end


MaccNet_BETW_all = squeeze(mean(mean(MaccNet_BETW,2),1)); % average all between decoding + all subjects
SaccNet_BETW_all = squeeze(std(mean(MaccNet_BETW,2),1)./sqrt(size(MaccNet_BETW,1))); % SEM
MaccNet_WITH_all = squeeze(mean(mean(MaccNet_WITH,2),1)); % average all within decoding + all subjects
SaccNet_WITH_all = squeeze(std(mean(MaccNet_WITH,2),1)./sqrt(size(MaccNet_WITH,1))); % SEM

%------
% Plot
%------
figure; set(gcf,'color','w');
color4Net ={'-k' '-r' '-g' '-c' '-b' '-m' '-y'};
LimYAx = [0.495 0.515];
subplot(2,2,1);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_WITH_all(k,:),SaccNet_WITH_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,3);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_WITH_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
title('Within Modality','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');

subplot(2,2,2);

for k = 1:numel(Net_Vox)
  shadedErrorBar(val,MaccNet_BETW_all(k,:),SaccNet_BETW_all(k,:),color4Net{k},1);
  hold on;
end
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');
subplot(2,2,4);
for k = 1:numel(Net_Vox)
  plot(val,MaccNet_BETW_all(k,:),color4Net{k},'Linewidth',2);
  hold on;
end
title('Between Modalities','FontSize',17,'FontWeight','bold');
legend(Net_Lab,'Location','Best','FontSize',15,'FontWeight','bold');
ylim(LimYAx)
set(gca,'FontSize',15,'FontWeight','bold');


%% Explore voxel brain : Only intersect Between Modalities
%------------------------------------------------------
if ~exist('ds_ft','var')
  Pathsaved = fullfile(config.path2work,Pathdata,'MeanDSFT'); % saved data
  load(fullfile(Pathsaved,['FTstruct_' AnalysisName '.mat']));
end

data = ds_ft{1,1,1};
selroi = IUroi_all; % only spatial features between modalities (intersect of union)
%selroi = CVroi_all; % only spatial features between modalities (intersect of intersect)

binary_plot= 2;


mask = zeros(1,889);
mask(selroi) = 1;

data.pow = data.avg.pow(data.inside,:,:);
data.pow = zeros(size(data.pow,1),size(data.pow,2),size(data.pow,3));

if binary_plot == 1
  val2plot = [0 4];
  
  % Fill-in with fix value when source was found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.pow(selroi,i,j) = val2plot(end);
      data.mask(:,i,j) = mask;
    end
  end
  
elseif binary_plot == 0
  val2plot = [0 max(AIDroi_intensity(selroi))];
  %val2plot = [0 40];
  % Fill-in with number of time source was found
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      data.pow(selroi,i,j) = AIDroi_intensity(selroi);
      data.mask(:,i,j) = mask;
    end
  end
  
else
  %val2plot = [0 max(Msig_acc_Between(selroi))];
  val2plot = [0.45 0.55];
  % Fill-in with number of time source was found
  %-----------------------------------------------
  for i = 1:size(data.pow,2)
    for j = 1:size(data.pow,3)
      % data.pow(selroi,i,j) = Msig_acc_Between(selroi)-val2plot(1);
      data.pow(selroi,i,j) = Maxsig_acc_Between(selroi)-val2plot(1);
      data.mask(:,i,j) = mask;
    end
  end
  
  val2plot = val2plot-val2plot(1);
end

data.dimord = 'chan_freq_time';

param2plot = 'pow';


load standard_mri
% load standard_mri_better
% load mri_better_segmented
% msk = segment_brain.brain; % build mask from anatomy (i.e. take only functional activity where there is anatomical activity)

load mni_grid_1_5_cm_889pnts.mat
% now virtual sensor to src
cfg = [];
cfg.mri = mri;
cfg.sourcegrid = template_grid;
cfg.parameter = {'pow' 'mask'};
cfg.toilim = [0 0.5];
cfg.foilim = [0 0.5];
source = obob_svs_virtualsens2source(cfg,data);

% % set non-brain activity to NaN (in another field)
% source.coordsys = 'mni';
% source.mask(~msk) = NaN;
% % change the inside field (only inside functional activity is plotted)
% source.inside = msk;


cfg = [];
cfg.funparameter = param2plot;
cfg.maskparameter = 'mask';
cfg.funcolorlim = val2plot;%'maxabs'
cfg.atlas = 'ROI_MNI_V4.nii';%'TTatlas+tlrc.BRIK';
%source.coordsys = 'tal';
%cfg.atlas = ['/Volumes/obob/staff/gsanchez/git/obob_ownft/external/fieldtrip/template/atlas/afni/TTatlas+tlrc.HEAD'];
cfg.method = 'ortho';% 'surface' 'ortho' 'slice'
cfg.projmethod = 'nearest'; % needed for method ='nearest'; 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
ft_sourceplot(cfg, source)

%% Mask ROI test
A = ft_read_atlas('ROI_MNI_V4.nii');

cfg= [];
%cfg.roi      = cfg.roi;
cfg.atlas    = 'ROI_MNI_V4.nii';
cfg.maskparameter = 'Temporal_Sup_R';
cfg.inputcoord = source.coordsys;
labels_roi = ft_volumelookup(cfg, source);


