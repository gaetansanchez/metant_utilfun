%% test script Cosmo searchlight with obob source localization



cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Load data
%----------
Pathdata = config.path;
id = 1; % subject


sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'}
toilim      = [0 0.6]; % in sec (period to decode in CosMoMVPA)
win         = 0.01; % down sampling (average time samples every X sec)
CFilter     = 'sensmod';% Beamformer common filter computation =  'alltrials' or 'sensmod'
preprocfilt = 'hpfilt_0.1'; %  hpfilt_1 or hpfilt_0.1
paradigm    = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham' or 'miss_vs_sham' 

% Set Name & Run Jobs Loop
%--------------------------
tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms'];
Nametype  = ['MVPA_src' tsteptype '_BeamCFilt' CFilter];
Pathsave  = fullfile(config.path2work,'mvpa/obobsrc4MVPA'); % saved data
if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%%
datafile = fullfile(Pathdata,'preproc',preprocfilt,sprintf('clean_%02d_allblocks.mat',id));
hdmfile  = fullfile(Pathdata,'mri',sprintf('s%02d',id),sprintf('sub%02d_hdm.mat',id));
savefilename = fullfile(Pathsave,sprintf([Nametype '_%02d.mat'],id));

[~, fname] = fileparts(datafile);
data_raw = load(datafile);

%% 
tr1 = [100 200 300];

condlabel = {'dd' 'ud'};
tr2       = [34 32];
tr3       = [1 2]; % recode dd=1 ud=2

[data,trig,trial_id,trig_sens] = Do_equalcond(config.path,fname,sensmod,tr1,condlabel,tr2,data_raw,paradigm);
clear data_raw;

% Select just trig conditions (in this case: hit and miss)
%---------------------------------------------------------------------
cfg = [];
cfg.trials = find(ismember(data.trialinfo(:,1),trig));
data = ft_selectdata(cfg,data);

%%
% Lowpass filter + Baseline correction
%----------------------------------------
cfg = [];
cfg.lpfilter ='yes';
cfg.lpfreq = 30; % or 40?
% cfg.lpfilttype = 'firws';
% cfg.lpfiltdf = 6;
% cfg.lpfiltwintype = 'kaiser';
% cfg.demean        = 'yes';
% cfg.baselinewindow = [-0.2 0];
data = ft_preprocessing(cfg, data);

% Downsampling (save computation time)
%-----------------
cfg = [];
cfg.resamplefs = 1/win; % win = should be in second
cfg.detrend = 'no';
data_tr = ft_resampledata(cfg, data);

%% obob LCMV beamformer

toilim   = [-1 1];
cov_win  = 'all';
freq     = 'low';
precision_grid = 1.5;
paradigm       = 'metaNT_HitMiss';
CFilter        = 'sensmod';

[datasrc] = Do_epo2src(data_tr,hdmfile,toilim,cov_win,freq,precision_grid,paradigm,CFilter);

%% or fieldtrip
hdm = load(hdmfile);
[datasrc2] = Do_src_LCMV_alltrials(data_tr,hdm);

%% Convert MVPA

ds = cosmo_meeg_dataset(datasrc);















