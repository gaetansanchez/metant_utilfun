



% Loop average
%-------------
OnlySig  = 0; % Only significant time points : 1=yes or 0=all time points
Nsuj     = size(M_tc,1);
Nsensmod = size(M_tc,2);
Ntime    = size(M_tc,6);
MaccNet       = zeros(Nsuj,Nsensmod,Nsensmod,numel(Net_Vox),Ntime);
MaccNet_BETW  = zeros(Nsuj,6,numel(Net_Vox),Ntime);
MaccNet_WITH  = zeros(Nsuj,3,numel(Net_Vox),Ntime);
for s = 1:Nsuj % suj
  b = 1;
  w = 1;
  for i = 1:Nsensmod % modality
    for j = 1:Nsensmod % modality
      for k = 1:numel(Net_Vox) % network
        tmpdat = squeeze(M_tc(s,i,j,Net_Vox{k},:,:));
        
        sig_traintime = logical(sum(squeeze(stat_mask(i,j,startpoint:stoppoint,startpoint:stoppoint)),2));% significant time point over training axes
        tmpdat_sig = tmpdat;
        tmpdat_sig(:,repmat(~sig_traintime,1,size(tmpdat,3))) = 0.5;% set non significant time point to 0.5 significant time points
        
        if OnlySig
          tmpdat = tmpdat_sig;
        end
        
        MaccNet(s,i,j,k,:) = mean(mean(tmpdat,3),1); % average over : test time + Network sources
        if i ~= j % only between sensmod decoding
          MaccNet_BETW(s,b,k,:) = mean(mean(tmpdat,3),1);
        else
          MaccNet_WITH(s,w,k,:) = mean(mean(tmpdat,3),1);
        end
      end
      if i ~= j % only between sensmod decoding
        b = b+1;
      else
        w = w+1;
      end
    end
  end
end