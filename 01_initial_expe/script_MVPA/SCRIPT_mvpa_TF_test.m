%% Script classification different modalities


%% Path setup
%-------------

clear all;

% Path definition

addpath('/home/gaetan.sanchez/git/cimec_ownft')
cfg = [];
cfg.package.svs = true;
cimec_init_ft(cfg);

addpath('/mnt/storage/tier1/natwei/Shared/gaetan/util_functions/')
addpath('/mnt/storage/tier1/natwei/Shared/gaetan/metaNT/code');
addpath('/mnt/storage/tier1/natwei/Shared/gaetan/CoSMoMVPA/mvpa');

% Load the data
Pathdata = '/mnt/storage/tier1/natwei/Shared/gaetan/metaNT/mvpa/sl_tf';

%% Do MVPA
%----------

% USE --> meta_do_mvpa_TF

%% Load Data
%-------------


Nsuj = 1:25;
for train = 1:3  % train
  for test = 1:3 % test
    
    for i = 1:numel(Nsuj) % subjects
      
      Datfile = [sprintf('%02d_',Nsuj(i)) 'TF_MVPA_Bayes_GRC_train' num2str(train) '_test' num2str(test) '.mat'];
      load(fullfile(Pathdata,Datfile));
      
      % deduce layout from output
      layout=cosmo_meeg_find_layout(ds_sc_res);
      
      sl_map = cosmo_map2meeg(ds_sc_res);
      if i == 1
        ds_tmp = sl_map;
        ds_tmp = rmfield(ds_tmp,'powspctrm');
        ds_tmp.powspctrm(i,:,:,:) = sl_map.powspctrm;
      else
        ds_tmp.powspctrm(i,:,:,:) = sl_map.powspctrm;
      end
      
      
    end
    
    % all subjects
    %------------------
    ds_all(train,test) = ds_tmp;
    
    % Average
    %--------
    ds_tot(train,test) = ds_tmp;
    ds_tot(train,test).powspctrm = squeeze(mean(ds_all(train,test).powspctrm,1));
    
    disp(' ');
    disp(['train = ' num2str(train) ' / test = ' num2str(test)]);
    
  end
end

Badsuj = [6 7 9 17 18 21]; % subject with low number of trials after equalization (nbtr<40)
%Badsuj = [8 15 20];
Goodsuj = setdiff(Nsuj,Badsuj);

train = 1;
test = 1;

sl_plot = ds_all(train,test);
sl_plot.powspctrm = squeeze(mean(ds_all(train,test).powspctrm(Goodsuj,:,:,:),1));

%% PLOT
%--------
% show figure
close(gcf)
cfg = [];
cfg.interactive = 'yes';
cfg.showlabels = 'yes';
cfg.zlim=[.5 .65];
cfg.layout       = layout;
ft_multiplotTFR(cfg, sl_plot);

% ROI parietal
roi = {'MEG1912+1913', 'MEG2012+2013', 'MEG2022+2023', 'MEG2032+2033', 'MEG2042+2043', 'MEG2312+2313'};
roi = {'MEG1922+1923', 'MEG2032+2033', 'MEG2042+2043', 'MEG2112+2113'};
% all group
%------------
kplot = 1;
for train = 1:3
  for test = 1:3
    
    
    sl_plot = ds_all(train,test);
    sl_plot.powspctrm = squeeze(mean(ds_all(train,test).powspctrm(Goodsuj,:,:,:),1));
    
    cfg = [];
    cfg.channel = roi;
    cfg.channel = [];
    cfg.zlim=[.5 .6];
    cfg.layout = layout;
    cimec_singleplotTFR(cfg, sl_plot)
    
    hold on;
    title(['train ' num2str(train) ' test ' num2str(test)]);
    
    kplot = kplot+1;
  end
end



% single subject
%--------------
train = 1;
test = 1;
for i = 1:size(ds_all(train,test).powspctrm,1)
  
  sl_plot = ds_all(train,test);
  sl_plot.powspctrm = squeeze(sl_plot.powspctrm(i,:,:,:));
  
  cfg = [];
  cfg.channel = roi;
  cfg.zlim=[.5 .65];
  cfg.layout       = layout;
  cimec_singleplotTFR(cfg, sl_plot)
  
  
end


cfg = [];
%cfg.channel = 'all';
cfg.zlim=[.4 .65];
cfg.layout       = layout;
ft_singleplotTFR(cfg, sl_plot)

cimec_singleplotTFR(cfg, sl_plot)

% all group
%----------
kplot = 1;
for j = 1:size(M,1)
  for k = 1:size(M,2)
    
    subplot(size(Mg,1),size(Mg,2),kplot);
    
    time_values=sl_map.a.fdim.values{1}; % first dim (channels got nuked)
    shadedErrorBar(time_values,squeeze(Mg(j,k,:)),squeeze(Ms(j,k,:)))
    %plot(time_values,squeeze(Mg(j,k,:)));
    ylim([.45 .6])
    xlim([min(time_values),max(time_values)]);
    ylabel('classification accuracy (chance=.5)');
    xlabel('time');
    hold on;
    plot([time_values(1) time_values(end)],[.5 .5],'--k','LineWidth',2);
    
    kplot = kplot+1;
    
  end
end

% Single subjects
%-----------------

for i = 1:size(M_tot,1)
  figure;
  kplot = 1;
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      subplot(size(Mg,1),size(Mg,2),kplot);
      
      time_values=sl_map.a.fdim.values{1}; % first dim (channels got nuked)
      %shadedErrorBar(time_values,squeeze(Mg(j,k,:)),squeeze(Ms(j,k,:)))
      plot(time_values,squeeze(M_tot(i,j,k,:)));
      ylim([.45 .7])
      xlim([min(time_values),max(time_values)]);
      ylabel('classification accuracy (chance=.5)');
      xlabel('time');
      hold on;
      plot([time_values(1) time_values(end)],[.5 .5],'--k','LineWidth',2);
      kplot = kplot+1;
   
    end
  end
  pause;
  close all;
end





