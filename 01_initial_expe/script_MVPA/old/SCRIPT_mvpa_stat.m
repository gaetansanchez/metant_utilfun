
% Example script to show how to perform CosmoStat analysis

%% STAT for time2time group analysis
%-------------------------------------
% Each dataset has the train_time and test_time attribute in the sample dimension
% and they have to be moved to the feature dimension to use cosmo montecarlo cluster stat):

n_subjects = numel(ds_cell);
ds_cell_tr = cell(n_subjects,1);

for k = 1:n_subjects
  ds_cell_tr{k} = cosmo_dim_transpose(ds_cell{k},{'train_time','test_time'},2);
end


for k=1:n_subjects
    % here we assume a single output (sample) for each
    % searchlight. For statistical analysis later, where
    % we want to do a one-sample t-test, we set
    % .sa.targets to 1 (any constant value will do) and
    % .sa.chunks to the subject number.
    % nsamples=size(result.samples,1);
    %
    % Notes:
    % - these values can also be set after the analysis is run,
    %   although that may be more error-prone
    % - for other statistical tests, such as one-way ANOVA,
    %   repeated-measures ANOVA, paired-sample t-test and
    %   two-sample t-tests, chunks and targets have to be
    %   set differently. See the documentation of
    %   cosmo_montecarlo_cluster_stat for details.

    ds_cell_tr{k}.sa.chunks=k;  % k-th subject
    ds_cell_tr{k}.sa.targets=1; % all same condition

end

ds_tr = cosmo_stack(ds_cell_tr);

%%%%%%%%%%%%%%%%%%%%%%%
% --> RUN CosmoMontecarlo cluster stat
%%%%%%%%%%%%%%%%%%%%%%%%


% To convert the output (say stat_map) 
% from cosmo montecarlo cluster stat to matrix form (time by time), do
[data, labels, values] = cosmo_unflatten(stat_map,2);

data_mat = squeeze(data);


%%%%%%%%%%%%%%
%%

% one-sample t-test against 0
% (for representational similarity analysis)
h0_mean=1/2;

% number of null iterations.
% values of at least 10,000 are recommended for publication-quality
niter=10000;

%
% Set neighborhood for clustering
cluster_nbrhood=cosmo_cluster_neighborhood(result);

stat_map=cosmo_montecarlo_cluster_stat(result, cluster_nbrhood,...
                                        'niter', niter,...
                                        'h0_mean', h0_mean);
                                      
                                      
                                      
                                      
                                      
                                      