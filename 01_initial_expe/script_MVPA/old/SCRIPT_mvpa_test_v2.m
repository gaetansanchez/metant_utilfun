%% Script classification different modalities


%% Path setup
%-------------

clear all;

% Path definition
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT',cfg);

%% Test
%------
id = 1;
datafile = fullfile(config.path,'preproc',sprintf('clean_%02d_allblocks.mat',id));
hdmfile  = fullfile(config.path,'mri',sprintf('s%02d',id),sprintf('sub%02d_hdm.mat',id));
savefilename = fullfile(config.path2work,'mvpa','sl_srcERF',sprintf('TEST_MVPA_src_slERF_%02d.mat',id));
  
[M,sl_map] = Do_mvpa_srcERF(datafile,hdmfile,savefilename);

[M,sl_map] = Do_mvpa_time2time_srcERF(datafile,hdmfile,savefilename);

%% Do MVPA with Sources analysis
%----------------------------------

% time locked decoding
%----------------------
condor_MVPA_srcERF; % call ==> Do_mvpa_srcERF.m

% time 2 time decoding
%----------------------
condor_MVPA_time2time_srcERF; % call ==> Do_mvpa_time2time_srcERF.m


%% Extract saved file from jobs submission MVPAsrc
%---------------------------------------------------

%----------------------
%% time locked classif
%----------------------
Pathsaved = fullfile(config.path2work,'mvpa/sl_srcERF'); % saved data

%Files = dir(fullfile(Pathsaved,'MVPA_src_slERF_*'));
%Files = dir(fullfile(Pathsaved,'par_srad2trad4_MVPA_src_slERF_*'));
%Files = dir(fullfile(Pathsaved,'NEWpar_srad2trad2_MVPA_src_slERF_*'));
%Files = dir(fullfile(Pathsaved,'NEWpar_tstep15ms_srad2trad2_MVPA_src_slERF_*'));

% Namefile = {'*par_tstep15ms_srad2trad2_*';...
%             '*par_tstep30ms_srad2trad2_*';...
%             '*par_tstep30ms_Znorm_srad2trad2_*'};   

% PLOT Searchlight ERF
%------------------------
Namefile = {'*par_tstep30ms_srad2trad2_*'};     

col = {'-k' '-g' '-r' '-m' '--k'};

for i = 1:numel(Namefile)
  Files = dir(fullfile(Pathsaved,Namefile{i}));
  
  Do_plot_3by3_slMVPA_group(Pathsaved,Files,col{i})
end

%% PLOT time2time searchligth ERF
%---------------------------------
Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
Files = dir(fullfile(Pathsaved,'tstep30ms_srad4_MVPA_src_t2tERF_*'));
%Files = dir(fullfile(Pathsaved,'tstep15ms_srad2_MVPA_src_t2tERF_*'));

[data,cfg] = Do_plot_3by3_t2tslMVPA_group(Pathsaved,Files);


cfg =[];
%cfg.xaxis = val;
plot_3by3_MVPA(data,cfg)

%%
data = dat{16,2,2};
data.pow = data.avg.pow(data.inside,:);
data.dimord = 'chan_time';
% Do some plotting
load standard_mri
load mni_grid_1_5_cm_889pnts.mat
% now virtual sensor to src
cfg = [];
cfg.mri = mri;
cfg.sourcegrid = template_grid;
cfg.parameter = 'pow';
cfg.toilim = [0.2 0.4];
source = obob_svs_virtualsens2source(cfg,data);
% now plot it
cfg=[];%%
cfg.method='ortho';%%
cfg.funparameter='pow';
cfg.funcolorlim=[0.5 0.6];
cfg.interactive='yes';
ft_sourceplot(cfg, source);



%% Do MVPA sl or t2t sensors level
%---------------------------------

% Path definition
Pathsave = fullfile(config.path,'mvpa/test'); % to save data
Pathdata = fullfile(config.path2work,'erf'); % to load data

method ='time2time'; % time2time or 'classif'

M_tot = [];
for i = 1:25
  
  disp(' ');
  disp('*********************');
  disp(['Processing sub ' num2str(i)]);
  
  switch method
    case 'classif'
      [M,sl_map] = Do_mvpa_test(i,Pathdata);
    case 'time2time'
      [M,sl_map] = Do_mvpa_time2time(i,Pathdata);
  end
  
  
  NameChan = fieldnames(M{1,1});
   
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      for c = 1:numel(NameChan)
        M_tot.(NameChan{c})(i,j,k,:) = M{j,k}.(NameChan{c}).samples;
      end
    end
  end
  
  % Save
  %-------
  
  %save(fullfile(Pathsave,'ERF_MVPA_3vs3_Bayes_GRD_MAG.mat'),'M_tot','Acc_tot','sl_map');
  %save(fullfile(Pathsave,'ERF_MVPA_3vs3_LDAzscore_GRD_MAG.mat'),'M_tot','Acc_tot','sl_map');
  save(fullfile(Pathsave,'ERF_MVPAtime2time_3vs3_LDAzscore_MAG.mat'),'M_tot','sl_map');
  
end

% average
%---------
chan = 'meg_axial'; % 'meg_axial' 'meg_planar'

M_t = [];
for i = 1:size(M_tot.(chan),1)
  for j = 1:size(M_tot.(chan),2)
    for k = 1:size(M_tot.(chan),3)
    
      M_t(i,j,k,:) = M_tot.(chan)(i,j,k,:);
    end
  end
end
Mg = squeeze(mean(M_t,1));
Ms = squeeze(std(M_t,1))./sqrt(size(M_t,1));

% Mg = squeeze(mean(cell2mat(M_tot.(chan)),1));
% Ms = squeeze(std(M_tot.(chan),1))./sqrt(size(M_tot.(chan),1));



%% PLOT
%--------
% all group
%----------


values = sl_map.a.sdim.values;

values{1} = ceil(values{1}*100)*10;
values{2} = ceil(values{2}*100)*10;
labels = sl_map.a.sdim.labels;
ntest = numel(sl_map.a.sdim.values{1});

kplot = 1;
data = [];
figure;
set(gcf,'color','white');
for i = 1:size(Mg,1)
  for j = 1:size(Mg,2)
    
    subplot(size(Mg,1),size(Mg,2),kplot);
    
    data = reshape(Mg(i,j,:),ntest,ntest);
    
    % show the results
    %surf(1:size(data,1),1:size(data,2),data');
    
    imagesc(flipud(data), [values{1}(1) values{1}(end)]);
    nticks=5;
    
    ytick=round(linspace(1, numel(values{1}), nticks));
    ylabel(strrep(labels{1},'_',' '),'FontSize',15,'FontWeight','bold');
    set(gca,'Ytick',ytick,'YTickLabel',flipud(ceil(values{1}(ytick))));
    
    xtick=round(linspace(1, numel(values{2}), nticks));
    xlabel(strrep(labels{2},'_',' '),'FontSize',15,'FontWeight','bold');
    set(gca,'Xtick',xtick,'XTickLabel',ceil(values{2}(xtick)));
    
    colorbar();
    caxis([0.5 0.53]);
    set(gca,'FontSize',15,'FontWeight','bold','LineWidth',2);
    
    kplot = kplot+1;
    
  end
end



% Single subjects
%-----------------
time_values = sl_map.a.fdim.values{1}; % first dim (channels got nuked)
time_values = dat{1,1,1}.time;

for i = 1:size(M_tot,1)
  figure;
  kplot = 1;
  for j = 1:size(M,1)
    for k = 1:size(M,2)
      
      subplot(size(Mg,1),size(Mg,2),kplot);
      
      %shadedErrorBar(time_values,squeeze(Mg(j,k,:)),squeeze(Ms(j,k,:)))
      plot(time_values,squeeze(M_t(i,j,k,:)));
      ylim([.45 .7])
      xlim([min(time_values),max(time_values)]);
      ylabel('classification accuracy (chance=.5)');
      xlabel('time');
      hold on;
      plot([time_values(1) time_values(end)],[.5 .5],'--k','LineWidth',2);
      kplot = kplot+1;
   
    end
  end
  pause;
  close all;
end





