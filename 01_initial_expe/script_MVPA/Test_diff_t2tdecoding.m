

clear all;

% Path definition
%-------------------
cfg = [];
cfg.package.svs = true;
config = addmypaths_gs('metaNT',cfg);

Pathdata1  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm/tstep30ms_srad30';
AnalysisName1 = 'zscoreNorm_tstep30ms_srad30_MVPA_src_t2tERF_BeamCFiltsensmod_cross_persens_N16';
Pathsaved1 = fullfile(config.path2work,Pathdata1,'MeanDSFT'); % saved data
FileNameSaved1 = fullfile(Pathsaved1,['MeanGroupAcc_' AnalysisName1 '.mat']);

Pathdata2  = 'mvpa/t2t_srcERF/cross_acc_zscoreNorm/tstep30ms_srad30_LDA';
AnalysisName2 = 'zscoreNorm_tstep30ms_srad30_LDA_MVPA_src_t2tERF_BeamCFiltalltrials_cross_persens_N16';
Pathsaved2 = fullfile(config.path2work,Pathdata2,'MeanDSFT'); % saved data
FileNameSaved2 = fullfile(Pathsaved2,['MeanGroupAcc_' AnalysisName2 '.mat']);


load(FileNameSaved1);
Mg1 = Mg;
load(FileNameSaved2);
Mg2 = Mg;

data = Mg2-Mg1;

cfg =[];
cfg.xaxis = val;
cfg.smooth = 2;
cfg.LimAx = [-0.05 0.05];
plot_3by3_MVPA(data,cfg);