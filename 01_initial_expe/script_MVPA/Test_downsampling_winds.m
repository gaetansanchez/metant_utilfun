%% Load
%-------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs('metaNT',cfg);
Pathdata = config.path;

datafile = fullfile(Pathdata,'preproc','hpfilt_0.1',sprintf('clean_%02d_allblocks.mat',1));
data = load(datafile);

%% Test downsampling effect
%--------------------------
toilim = [0 0.6];
win = 0.02;


cfg = [];
cfg.trials = 'all';
cfg.toilim = toilim;
cfg.trials = 1;
data_tr   = ft_redefinetrial(cfg, data);


cfg = [];
cfg.lpfilter ='yes';
cfg.lpfreq = 30; % or 40?
data_tr = ft_preprocessing(cfg, data_tr);

% downsample to 256hz (save computation time)
%--------------------
cfg = [];
cfg.resamplefs = 1/win; % win = should be in second
cfg.detrend = 'no';
data_tr = ft_resampledata(cfg, data_tr);

val_time = data_tr.time{1};

datawin = (abs(min(val_time)) + max(val_time)) / length(val_time);
down_sampling = round(win/datawin);

ind = 1:down_sampling:length(val_time);

val_time(ind)


