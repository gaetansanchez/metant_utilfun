function [] = reject_artifacts_allblocks(Nsub)
% reject artifact all blocks

%% Path settings
%------------------

config = addmypaths_gs('metaNT');
pathpreproc_artdef = fullfile(config.path,'preproc','artdef');
pathpreproc_file   = fullfile(config.path,'preproc','hpfilt_0.1');

for iSub = Nsub
  %% Automatic artefact rejection
  %-------------------------------
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)]);
  
  filepoc = dir(fullfile(pathpreproc_file,[sprintf('%02d_',iSub),'allblocks.mat']));
  fileName = fullfile(pathpreproc_file,filepoc.name);
  
  saveName = fullfile(pathpreproc_file,['clean_',filepoc.name]);
  saveName_art = fullfile(pathpreproc_artdef,['artifact_rejectoptions_',filepoc.name]);
  
  %% Load and reshape trial length
  %-------------------------------
  disp(' ');
  disp(['Loading: ' fileName])
  
  data = load(fileName);
  
  if exist(saveName_art,'file') == 2
    disp('*********');
    disp('Previous artifact rejection options are available !!');
    disp(['Loading: ' saveName_art]);
    
    load(saveName_art);
  else
    
    cfg = [];
    cfg.trials    = 'all';
    cfg.toilim    = [-1 0.5];
    [data_short] = ft_redefinetrial(cfg, data);
    
    %% Jump
    %-------
    cfg                    = [];
    % channel selection, cutoff and padding
    cfg.artfctdef.zvalue.channel    = 'megmag';
    cfg.artfctdef.zvalue.cutoff     = 50;
    cfg.artfctdef.zvalue.trlpadding = 0;
    cfg.artfctdef.zvalue.artpadding = 0;
    cfg.artfctdef.zvalue.fltpadding = 0;
    % algorithmic parameters
    cfg.artfctdef.zvalue.cumulative    = 'yes';
    cfg.artfctdef.zvalue.medianfilter  = 'yes';
    cfg.artfctdef.zvalue.medianfiltord = 9;
    cfg.artfctdef.zvalue.absdiff       = 'yes';
    % make the process interactive
    cfg.artfctdef.zvalue.interactive = 'yes';
    
    [cfg_jump, artifact_jump] = ft_artifact_zvalue(cfg, data_short);
    
    %% Muscle
    %-------
    cfg                    = [];
    % channel selection, cutoff and padding
    cfg.artfctdef.zvalue.channel     = 'megmag';
    cfg.artfctdef.zvalue.cutoff      = 20;
    cfg.artfctdef.zvalue.trlpadding  = 0;
    cfg.artfctdef.zvalue.fltpadding  = 0.5;
    cfg.artfctdef.zvalue.artpadding  = 0.1;
    % algorithmic parameters
    cfg.artfctdef.zvalue.bpfilter    = 'yes';
    cfg.artfctdef.zvalue.bpfreq      = [110 140];
    cfg.artfctdef.zvalue.bpfiltord   = 9;
    cfg.artfctdef.zvalue.bpfilttype  = 'but';
    cfg.artfctdef.zvalue.hilbert     = 'yes';
    cfg.artfctdef.zvalue.boxcar      = 0.2;
    % make the process interactive
    cfg.artfctdef.zvalue.interactive = 'yes';
    
    [cfg_muscle, artifact_muscle] = ft_artifact_zvalue(cfg, data_short);
    
    %% Blink
    %--------
    
    cfg                    = [];
    % channel selection, cutoff and padding
    cfg.artfctdef.zvalue.channel     = 'megmag';
    cfg.artfctdef.zvalue.cutoff      = 30;
    cfg.artfctdef.zvalue.trlpadding  = 0;
    cfg.artfctdef.zvalue.artpadding  = 0.1;
    cfg.artfctdef.zvalue.fltpadding  = 1;
    % algorithmic parameters
    cfg.artfctdef.zvalue.bpfilter   = 'yes';
    cfg.artfctdef.zvalue.bpfilttype = 'but';
    cfg.artfctdef.zvalue.bpfreq     = [1 15];
    cfg.artfctdef.zvalue.bpfiltord  = 4;
    cfg.artfctdef.zvalue.hilbert    = 'yes';
    % feedback
    cfg.artfctdef.zvalue.interactive = 'yes';
    [cfg_blink, artifact_blink] = ft_artifact_zvalue(cfg, data_short);
    
    %% save rejection options
    %------------------------
    save(saveName_art,'-v7.3','cfg_blink','artifact_blink',...
    'cfg_jump','artifact_jump',...
    'cfg_muscle','artifact_muscle');
    
    
  end
  
  %% Reject based on Zvalue
  %--------------------------
  cfg=[];
  cfg.artfctdef.reject = 'complete'; % this rejects complete trials, use 'partial' if you want to do partial artifact rejection
  cfg.artfctdef.eog.artifact = artifact_blink; %
  cfg.artfctdef.jump.artifact = artifact_jump;
  cfg.artfctdef.muscle.artifact = artifact_muscle;
  data_cleanv1 = ft_rejectartifact(cfg,data);
  
  %% Visual inspection
  %--------------------
  
  cfg        = [];
  cfg.latency = [-1 0.5];
  cfg.method = 'summary';
  cfg.layout = 'neuromag306mag.lay';
  cfg.channel = 'MEG';
  cfg.gradscale   = 0.04;
  % filtering only for artifact rejection
  cfg.preproc.lpfilter      = 'yes';
  cfg.preproc.lpfilttype    = 'firws';
  cfg.preproc.lpfiltwintype = 'kaiser';
  cfg.preproc.lpfreq        = 40;
  cfg.preproc.lpfiltdf      = 6;
  cfg.preproc.rectify       = 'yes';
  data_cleanv2 = obob_rejectvisual(cfg,data_cleanv1);
 
  
  %% Conditions check
  %------------------
  CondtrNumber = [];
  for i = 1:3
    % 'AUD' = 100 / 'VIS' = 200 / 'TAC' = 300
    CondtrNumber(1,i) = numel(find(data_cleanv2.trialinfo(:,1) == (34+100*i)));
    CondtrNumber(2,i) = numel(find(data_cleanv2.trialinfo(:,1) == (32+100*i)));
  end
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)]);
  disp('Post rejection Number of trials per conditions = ');
  CondtrNumber
  
  %% save and delete block files
  %------------------------------
  disp(' ');
  disp(['Saving: ' saveName])
  disp(' ');
  
  save(saveName_art,'-v7.3','CondtrNumber','cfg_blink','artifact_blink',...
    'cfg_jump','artifact_jump',...
    'cfg_muscle','artifact_muscle');
  
  save(saveName,'-v7.3','-struct', 'data_cleanv2');
  
  % Deleted previous file
  %-----------------------
  disp(' ');
  disp(['Deleting: ' fileName])
  disp(' ');
  system(['rm ' fileName]);
  
  close all;
  
end

%% Plot number of trials
%------------------------

% config = addmypaths_gs('metaNT');
% pathpreproc_artdef = fullfile(config.path,'preproc','artdef');
% 
% load(fullfile(pathpreproc_artdef,'Numtrial_percondsuj_afterReject.mat'))
% 
% figure;set(gcf,'color','white');
% imagesc(Numtr(:,2:end));
% colorbar();
% caxis([30 100]);
% set(gca,'FontSize',15,'FontWeight','bold','LineWidth',2);
% 
% A = Numtr(:,2:end)<40;
% 
% figure;set(gcf,'color','white');
% imagesc(A);
% set(gca,'FontSize',15,'FontWeight','bold','LineWidth',2);


