
% RUN TF2ga
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/TF2ga/' date]);

pathTF = fullfile(config.path2work,'tf');
pathgaTF = fullfile(pathTF,'ga_tf');

if ~exist(pathgaTF,'dir') % create output dir if dont exist
  mkdir(pathgaTF);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '20G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Parameters 
%-------------
tffilename = 'allblocks_mtmconvol_hanning_-1-1s_freq2-90Hz_7ftimwin_2smothF_TF_percond';

%% Jobs additions
%-----------------
Suj = 1:25;
BadSuj = [];
load(fullfile(config.path,'BadsubjectsBehav'));
Suj(BadSuj) = []; % remove bad subjects

load(fullfile(config.path,'Numtrial_percondsuj_afterReject.mat'));
A = Numtr(:,2:end)<40;% find numtrials above 40 for each conditions
Suj(find(sum(A,2))) = [];

condor_struct = obob_condor_addjob(condor_struct, 'hnc_TF2ga',...
                        pathTF,...
                        pathgaTF,...
                        Suj,...
                        tffilename);

%% Submit
%---------
obob_condor_submit(condor_struct);

