
% RUN epo2ERF
%-------------

%% clear
%--------
clear all global
close all

%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/test_numberoftrials/' date]);

pathpreproc = fullfile(config.path,'preproc','hpfilt_0.1');
Pathsave = fullfile(config.path2work,'test');

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '5G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
[Suj] = Do_sel_subjects(config,'metaNT');

paradigm = 'hit_vs_miss'; % 'supra_vs_sham' or 'hit_vs_miss' or 'miss_vs_sham' or 'allcond'

for i = 1:numel(Suj)
  id = Suj(i);
  
  input = fullfile(pathpreproc,sprintf('clean_%02d_allblocks.mat',id));
  condor_struct = obob_condor_addjob(condor_struct, 'hnc_testnumberoftrials',...
                        input, ...
                        Pathsave,...
                        paradigm);
end

%% Submit
%---------
obob_condor_submit(condor_struct);

