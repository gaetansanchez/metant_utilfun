
% Script plot ERF

clear all;
% Path
%-------

config = addmypaths_gs('metaNT');

pathbase = config.path;
pathavg = fullfile(config.path2work,'erf');

%% Load all subjects ERFs
%------------------------

%load(fullfile(pathavg,'allsub25_avg_erf.mat'));
%load(fullfile(pathavg,'allERF_N19_allblocks_-0.5-0.5s_lp30_erf_percond.mat'));
load(fullfile(pathavg,'allERF_N16_allblocks_-0.5-0.5s_lp30_erf_percond.mat'));

load(fullfile(pathavg,'ga_erf','ga_ERF_N16_allblocks_-0.5-0.5s_lp30_erf_percond.mat'));

%% Quick plot to check all the subjects
%=-------------------------------------

% For individual subjects
%-------------------------
Nsub = 1:length(erf);
channel = 'GRC';% 'GRC', 'MAG'
Do_plot_ERF(erf,Nsub,channel,1)


% For Grand average
%------------------
ga{1} = ga_erf;
Nsub = 1;
channel = 'MAG';% 'GRC', 'MAG'
Do_plot_ERF(ga,Nsub,channel,0)

%% Plot multiplot Grand Average
%-----------------------------
toplot = 'tac';
dat_1 = ga_erf.(toplot).hit;
dat_2 = ga_erf.(toplot).miss;

cfg = [];
cfg.latency = [0 0.5];
dat_1 = ft_selectdata(cfg,dat_1);
dat_2 = ft_selectdata(cfg,dat_2);

dat_1 = ft_combineplanar([],dat_1);
dat_2 = ft_combineplanar([],dat_2);

% contrast
dat_diff = dat_1;
dat_diff.avg = dat_1.avg - dat_2.avg;

cfg = [];
cfg.layout = 'neuromag306cmb.lay';
%cfg.layout = 'neuromag306mag.lay';
%ft_multiplotER(cfg, dat_diff);
%ft_movieplotER(cfg, dat_diff);
ft_multiplotER(cfg, dat_1,dat_2);

%% Organize data to plot
%---------------------------

data_ga = ft_combineplanar([],erf{1}.aud.hit);% initialize structure

sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss'};

blwin = [-200 0];

for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:numel(erf)
      
      
      datint = ft_combineplanar([],erf{n}.(sensmod{i}).(condlabel{j}));
      
      cfg = [];
      cfg.baseline     = blwin;
      cfg.channel      = 'all';
      cfg.parameter    = 'avg';
      datint = ft_timelockbaseline(cfg,datint);
  
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
    end
  end
end



%% Test plot generic
%---------------------
plot_what_1 = 'tachit';%'ud';%'ud';%'dd';
plot_what_2 = 'tacmiss';%'ud';%'ud';%'dd';

sel = 'GRC';%'GRC'; %'MAG';
if strcmp(sel, 'GRC')
  factor = 1e12;
  ylabel = 'pt/m^2';
  yaxis = [-1 2.5];
  ytick = 0.5;
  rms_rm = 1;
  sens_sel = meg_select_sensors(sel);
  
elseif strcmp(sel, 'MAG')
  factor = 1e15;
  ylabel = 'ft';
  yaxis = [-70 70];
  ytick = 10;
  rms_rm = 1;
  sens_sel = meg_select_sensors(sel);
end
sens_idx = ismember(data_ga.label, sens_sel);
rois = sens_idx;

% Test plot Publi
%-----------------
Do_Publi_plot_ERF(data_ga,plot_what_1,plot_what_2,sel)

%% plot grand avg butterfly - plot_what_1
%-----------------------------------------
cfg = [];
cfg.data = squeeze(mean(data_ga.(plot_what_1)(:,rois,:),1)).*factor;
cfg.limits	= [min(data_ga.time)*1000 max(data_ga.time)*1000 yaxis(1) yaxis(2)];
if ~isempty(strfind(plot_what_1,'hit'))
    cfg.col = {'r'};
elseif ~isempty(strfind(plot_what_1,'miss'))
    cfg.col = {'k'};
end
meeg_plot_butterfly_col(cfg)

set(gca, 'Visible', 'off');
cfgaxes.origin = [0 yaxis(1)];
cfgaxes.lims = [-200 500 yaxis];
cfgaxes.ylabel = ylabel;
cfgaxes.tick_steps = [100 ytick];
cfgaxes.text_dis    = [2 0.010];
plot_axes(cfgaxes);

axis([-210 502 yaxis(1)-yaxis(2)/20 yaxis(2)+yaxis(2)/20])

% plot grand avg butterfly - plot_what_2
%----------------------------------------
cfg = [];
cfg.data = squeeze(mean(data_ga.(plot_what_2)(:,rois,:),1)).*factor;
cfg.limits	= [min(data_ga.time)*1000 max(data_ga.time)*1000 yaxis(1) yaxis(2)];
if ~isempty(strfind(plot_what_2,'hit'))
    cfg.col = {'r'};
elseif ~isempty(strfind(plot_what_2,'miss'))
    cfg.col = {'k'};
end
meeg_plot_butterfly_col(cfg)
set(gca, 'Visible', 'off');
cfgaxes.origin = [0 yaxis(1)];
cfgaxes.lims = [-200 500 yaxis];
cfgaxes.ylabel = ylabel;
cfgaxes.tick_steps = [100 ytick];
cfgaxes.text_dis    = [2 0.010];
plot_axes(cfgaxes);

axis([-210 502 yaxis(1)-yaxis(2)/20 yaxis(2)+yaxis(2)/20])


%% do stats first to get sig intervals

timeWin = [-200 500];
pnts = time2points(timeWin, data_ga.fsample, min(data_ga.time)*1000);

sel = 'GRC';%'GRC'; %'MAG';
sens_sel = meg_select_sensors(sel);
sens_idx = ismember(data_ga.label, sens_sel);
rois = sens_idx;


statdata = [];
stats = [];
statsmod = {};
sensmod = {'aud' 'vis' 'tac'};

for i = 1:numel(sensmod)
  test1 = [sensmod{i} 'hit'];
  test2 = [sensmod{i} 'miss'];
  
  for iSub = 1:size(data_ga.(test1),1)
    statdata(:,:,1, iSub) = data_ga.(test1)(iSub,rois,pnts(1):pnts(2))*factor; %fT
    statdata(:,:,2, iSub) = data_ga.(test2)(iSub,rois,pnts(1):pnts(2))*factor;
  end
  
  cfg =[];
  cfg.data = statdata;
  cfg.alpha  = 0.05;
  cfg.type = 'rms';
  cfg.sfreq = data_ga.fsample;
  cfg.contrasts = [1 2];
  cfg.slwin = 40;
  cfg.overlap = 20;
  
  cfg.time = data_ga.time(pnts(1):pnts(2))*1000;
  cfg.dylim = [0 yaxis(2)-ytick];
  cfg.dcolor = {'b' 'r'};
  cfg.con_names = {'Hit' 'Miss'};
  %cfg.cor_mode = 'fdr';
  stats = meeg_plot_ttest_tcourse(cfg);
  
  % Track start and end of significant period
  %------------------------------------------
  onset = find(diff(abs(stats.tvals) > stats.btcrit) == 1)' + 1;
  offset = find(diff(abs(stats.tvals) > stats.btcrit) == -1)';
  stats.sig_time = stats.time([onset offset]);
  
  % save info
  %----------
  stats.sensors = sel;
  statsmod.(sensmod{i}) = stats;
  
end

%%
% sig_time = stats.time(abs(stats.tvals) > stats.btcrit);

onset = find(diff(abs(stats.tvals) > stats.btcrit) == 1)' + 1;
offset = find(diff(abs(stats.tvals) > stats.btcrit) == -1)';

sig_time = stats.time([onset offset]);

% if numel(onset)<numel(offset)
%   sig_time = stats.time([[offset(1); onset] offset]);
% elseif numel(onset)<numel(offset)
%   sig_time = stats.time([onset [offset; onset(end)]]);
% else
%   sig_time = stats.time([onset offset]);
% end

%% plot grand avg rms

cfg = [];
plotdata(1,:) = squeeze(mean(sqrt(mean(data_ga.(test1)(:,rois,:).^2,2)),1).*factor); %fT (check top)
plotdata(2,:) = squeeze(mean(sqrt(mean(data_ga.(test2)(:,rois,:).^2,2)),1).*factor);

cfg.xaxis = data_ga.time; 
cfg.color = {'r' 'k'}; % {'r' 'k'}
cfg.linewidth = [2 2];
cfg.ylim = [0 yaxis(2)-ytick*rms_rm];
% cfg.vline = [150 300 600];
% cfg.marker =  sig_time;%[40 60; 110 160; 180 266; 274 410]./1e3;%[100 248; 252 398; 402 550]
% cfg.marker =  [100 160; 203 270; 320 420]./1e3;%[100 248; 252 398; 402 550]
plot_TA(plotdata, cfg)

% plot sig line
if mean(sig_time(:)) > 1
  sig_time = sig_time ./1000;
end
for i = 1:size(sig_time,1)
  plot(sig_time(i,:), [cfg.ylim(2)/10 cfg.ylim(2)/10], 'k', 'LineWidth', 2)
end

set(gca, 'Visible', 'off');

cfgaxes = [];
cfgaxes.lims = [-.2 0.5 0.5 yaxis(2)-ytick*rms_rm];
cfgaxes.ylabel = '';
cfgaxes.tick_steps = [.1 ytick]; %round to an even tenth
cfgaxes.text_dis = [0.02*ytick 0.010];
plot_axes(cfgaxes);

axis([-.202 .502 0-yaxis(2)/20 yaxis(2)-ytick*rms_rm+yaxis(2)/20])

% fileplotsaved = fullfile(config.path, 'doc', 'erf',  [ 'erf_dif_' sel add '.svg']);
% plot2svg(fileplotsaved);

%% topos

timewins = sig_time;
% timewins = [150 300; 300 400]./1000;    

cfg =[];
data_ga.avg = squeeze(mean(data_ga.(test1)-data_ga.(test2),1));
data_ga.dimord = 'chan_time';

% reduce even more as for rms (cause this is the difference, i.e. tiny)
yreduce = rms_rm + 2;

for i = 1:size(timewins,1)
    
    if strcmp(sel, 'MAG')
        cfg.layout = 'neuromag306mag.lay';
    elseif strcmp(sel, 'GRC')
        cfg.layout = 'neuromag306cmb.lay';
    end
    cfg.parameter = 'avg';
    cfg.fontsize = 16;
    cfg.comment = 'no';
    cfg.xlim = timewins(i,:);
    if strcmp(sel, 'GRC')
       cfg.zlim = [0  yaxis(2)-ytick*yreduce ]./(factor);
    else
        cfg.zlim = [-yaxis(2)+ytick*yreduce  yaxis(2)-ytick*yreduce ]./(factor);
    end
    figure;set(gcf,'color','w'); ft_topoplotER(cfg, data_ga)
    %colormap jet
    %
%     save_name = fullfile(config.path, 'doc', 'topo',  [ 'erf_dif_' num2str(timewins(i,1)) '-' num2str(timewins(i,2)) '_' sel add '.eps']);
%     save_figure(save_name, 150, 1);
end

%% plot single subjects
test1 = 'vishit';
test2 = 'vismiss';
dd_rms = squeeze(sqrt(mean(data_ga.(test1)(:,sens_idx,:).^2,2)));
ud_rms = squeeze(sqrt(mean(data_ga.(test2)(:,sens_idx,:).^2,2)));

figure;
for iS = 1:size(dd_rms,1)
  
  subplot(4,5,iS)
  
  cfg = [];
  cfg.color = {'r' 'k'};
  cfg.newfig = 0;
  cfg.xaxis = data_ga.time;
  plot_TA([dd_rms(iS,:); ud_rms(iS,:)], cfg)
  title(['Subjec #' num2str(iS)])
end


%% Plot single subject ERFs
%-------------------------

% Keep only good subjects index (based on FA or Det rate)
%---------------------------------------------------------
Nsub = 1:25;
BadSuj = [];
load(fullfile(config.path,'BadsubjectsBehav'));
Nsub(BadSuj) = []; % remove bad subjects

% Within these good subject removed those with few trials in one condition
%--------------------------------------------------------------------------
load Numtrial_percondsuj_afterReject.mat
MaxNumtr = 50; % max number of trials in one condition
SubIndex = find(sum((Numtr(:,2:end)<MaxNumtr),2)); % Removed subject index in good subjects
SubNum = Numtr(SubIndex,1); % Bad subject number from 25 subjects

%Allgoodsuj = [1 2 15 24 25];
Allgoodsuj = 1:19;
Allgoodsuj(SubIndex) = [];

channel = 'GRC'; % 'GRC', 'MAG'
Do_plot_ERF(erf,Allgoodsuj,channel)

% Topo
%------
data1 = erf{1}.tac.hit;
data2 = erf{1}.tac.miss;
%
cfg = [];
cfg.fontsize = 10;
cfg.interactive = 'yes';
cfg.channel = 'MEGGRAD'; % MEGMAG MEGGRAD MEG
cfg.linewidth = 0.8;
cfg.xlim = [-.1 .45];
cfg.layout = 'neuromag306cmb.lay'; %mag, planar, cmb
cfg.zlim = (1.0e-12*[-5 5]);
figure;
ft_multiplotER(cfg,data1,data2);

%% grand average
%---------------
load(fullfile(pathavg,'ga_erf','ga_ERF_Nbsuj25.mat'));

data1 = ga_erf.tac.hit;
data2 = ga_erf.tac.miss;


%
cfg = [];
cfg.fontsize = 10;
cfg.interactive = 'yes';
cfg.channel = 'MEGGRAD'; % MEGMAG MEGGRAD MEG
cfg.linewidth = 0.8;
cfg.xlim = [-.1 .45];
cfg.layout = 'neuromag306cmb.lay'; %mag, planar, cmb
cfg.zlim = (1.0e-12*[-5 5]);
figure;
ft_multiplotER(cfg,data1,data2);

%caxis(1.e-14*[-2 2])

cfg=[];
cfg.method='power';
cfg.channel='MEGGRAD';
gfdata1=ft_globalmeanfield(cfg, data1);
gfdata2=ft_globalmeanfield(cfg, data2);

plot(gfdata1.time, gfdata1.avg); 
hold on
plot(gfdata2.time, gfdata2.avg); 
xlim([-.1 .5])
hold off

%% Garnd average
%-----------------

%addpath('/mnt/storage/tier1/natwei/Shared/gaetan/util_functions')

%load(fullfile(pathavg,'ga_erf','ga_ERF_Nbsuj23.mat'));


cond = {'tac' 'aud' 'vis'};

for i = 1:numel(cond)
  figuretitle = [cond{i} ': Hit & Miss'];
  data1 = ga_erf.(cond{i}).supra;
  data2 = ga_erf.(cond{i}).hit;
  data3 = ga_erf.(cond{i}).miss;
  cfg = [];
  cfg.latency = [-.1 .45];
  data1 = ft_selectdata(cfg,data1);
  data2 = ft_selectdata(cfg,data2);
  data3 = ft_selectdata(cfg,data3);
  % Baseline correction
  cfg = [];
  cfg.baseline     = [-.1 0];
  cfg.channel      = 'all';
  cfg.parameter    = 'avg';
  data1 = ft_timelockbaseline(cfg,data1);
  data2 = ft_timelockbaseline(cfg,data2);
  data3 = ft_timelockbaseline(cfg,data3);
  % select channels & adapt unit
  channel = 'MAG'; % 'GRC', 'MAG'
  chan_names = meg_select_sensors(channel); % ATTENTION: MAG -->  1.0e-13; GRC -->  1.0e-12
  [~,indchan]=intersect(data1.label,chan_names);
  if strcmp(channel,'MAG')
    data1.avg = sqrt(mean(data1.avg(indchan,:) .^ 2,1)).*1e15; % femto-Tesla
    data2.avg = sqrt(mean(data2.avg(indchan,:) .^ 2,1)).*1e15; % femto-Tesla
    data3.avg = sqrt(mean(data3.avg(indchan,:) .^ 2,1)).*1e15; % femto-Tesla
  else
    data1.avg = sqrt(mean(data1.avg(indchan,:) .^ 2,1)).*1e12; % pico-Tesla
    data2.avg = sqrt(mean(data2.avg(indchan,:) .^ 2,1)).*1e12; % pico-Tesla
    data3.avg = sqrt(mean(data3.avg(indchan,:) .^ 2,1)).*1e12; % pico-Tesla
  end
  % plot nice
  cfg = [];
  cfg.data = data1.avg;
  cfg.data2 = data2.avg;
  cfg.data3 = data3.avg;
  cfg.col = {'b'};
  cfg.col2 = {'r'};
  cfg.col3 = {'g'};
  cfg.linewidth = 3;
  cfg.limits	= [data1.time(1) data1.time(end) min(data1.avg)-0.2 max(data1.avg)+0.2];
  cfg.caption = figuretitle; %'erf left cue (all channels, gradiometers)'%chan_names;
  %cfg.outfile = figure_filename; %'figures/erf_left_cue_GRC.eps';
  meeg_plot_butterfly_col(cfg);
end

%% 
data1 = ga_erf.vis.hit;
data2 = ga_erf.vis.miss;

cfg = [];
cfg.latency = [-.1 .45];
data1 = ft_selectdata(cfg,data1);
data2 = ft_selectdata(cfg,data2);

channel = 'MAG';
if strcmp(channel,'MAG')
  data1.avg = data1.avg(:,:) .*1e15; % femto-Tesla
  data2.avg = data2.avg(:,:) .*1e15; % femto-Tesla
else
  data1.avg = data1.avg(:,:) .*1e12; % pico-Tesla
  data2.avg = data2.avg(:,:) .*1e12; % pico-Tesla
end

cfg = [];
cfg.fontsize = 10;
cfg.interactive = 'yes';
cfg.channel = 'MEGMAG'; % MEGMAG MEGGRAD MEG
cfg.linewidth = 0.8;
cfg.layout = 'neuromag306mag.lay'; %mag, planar, cmb

% figure;
% ft_multiplotER(cfg,data1,data2);

figure; set(gcf, 'Color', [1 1 1]); 
cfg.xlim = [.1 .15];
ft_topoplotER(cfg,data1)
caxis([-60 60]);
%%


for i = 1:12
  
  figuretitle = ['tac: sub' num2str(i)];
  data1 = erfblcmb{i}.tac.hit;
  data2 = erfblcmb{i}.tac.miss;
  
  % select channels & adapt unit
  channel = 'GRC'; % 'GRC', 'MAG'
  
  chan_names = meg_select_sensors(channel); % ATTENTION: MAG -->  1.0e-13; GRC -->  1.0e-12
  [~,indchan]=intersect(data1.label,chan_names);
  
  if strcmp(channel,'MAG')
    data1.avg = sqrt(mean(data1.avg(indchan,:) .^ 2,1)).*1e15; % femto-Tesla
    data2.avg = sqrt(mean(data2.avg(indchan,:) .^ 2,1)).*1e15; % femto-Tesla
  else
    data1.avg = sqrt(mean(data1.avg(indchan,:) .^ 2,1)).*1e12; % pico-Tesla
    data2.avg = sqrt(mean(data2.avg(indchan,:) .^ 2,1)).*1e12; % pico-Tesla
  end
  
  
  % plot nice
  cfg = [];
  cfg.data = data1.avg;
  cfg.data2 = data2.avg;
  cfg.col = {'b'};
  cfg.col2 = {'r'};
  cfg.limits	= [data1.time(1) data1.time(end) min(data1.avg)-0.2 max(data1.avg)+0.2];
  cfg.caption = figuretitle; %'erf left cue (all channels, gradiometers)'%chan_names;
  %cfg.outfile = figure_filename; %'figures/erf_left_cue_GRC.eps';
  
  meeg_plot_butterfly_col(cfg);
end








