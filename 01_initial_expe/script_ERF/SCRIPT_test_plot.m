
%%
data = load('clean_01_allblocks_-0.5-0.5s_lp30_erf_percond.mat');


dd = data.vis.hit;
ud = data.vis.miss;

%%

load ga_ERF_N19_allblocks_-0.5-0.5s_lp30_erf_percond.mat
%%
mod = 'tac';
dd = ga_erf.(mod).hit;
ud = ga_erf.(mod).miss;

%
cfg=[];
dd_c = ft_combineplanar(cfg,dd);
ud_c = ft_combineplanar(cfg,ud);
ddo_c = ft_combineplanar(cfg,ddo);
udo_c = ft_combineplanar(cfg,udo);

cfg = [];
cfg.xlim = 'maxmin';
cfg.layout = 'neuromag306mag.lay';
cfg.layout = 'neuromag306cmb.lay';
figure; ft_multiplotER(cfg, dd_c, ud_c);

%%
cfg = [];
cfg.xlim = 'maxmin';
cfg.zlim = [0 1.5].*1e-12;
cfg.xlim = [0.25 0.35];
cfg.layout = 'neuromag306cmb.lay';
figure; ft_topoplotER(cfg, dd_c);
figure; ft_topoplotER(cfg, ud_c);