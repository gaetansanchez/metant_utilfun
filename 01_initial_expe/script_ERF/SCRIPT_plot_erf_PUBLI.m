
%% SCRIPT Plot ERF Publication
%-----------------------------

clear all;
config = addmypaths_gs('metaNT');
pathbase = config.path;

%% Perform ERF
%-------------

%% 01 --> Compute individual ERF
%---------------------------------
condor_02_hnc_epo2ERF

%% 02 --> Grand average
%-----------------------
condor_03_hnc_erf2ga

%% Load ERF
%-----------

% Load all subjects ERFs
%------------------------
% pathavg = fullfile(config.path2work,'erf');
% load(fullfile(pathavg,'allERF_N16_allblocks_-0.5-1s_lp30_erf_percond.mat'));
% load(fullfile(pathavg,'ga_erf','ga_ERF_N16_allblocks_-0.5-1s_lp30_erf_percond.mat'));

pathavg = fullfile(config.path2work,'erf','hpfilt_0.1');
load(fullfile(pathavg,'allERF_N16_allblocks_-0.5-0.7s_lp30_erf_percond.mat'));
load(fullfile(pathavg,'ga_erf','ga_ERF_N16_allblocks_-0.5-0.7s_lp30_erf_percond.mat'));


%% Quick plot to check all the subjects
%-------------------------------------

% For individual subjects
%-------------------------
Nsub = 1:length(erf);
channel = 'GRC';% 'GRC', 'MAG'
Do_plot_ERF(erf,Nsub,channel,0)

% For Grand average
%------------------
ga{1} = ga_erf;
Nsub = 1;
channel = 'GRC';% 'GRC', 'MAG'
Do_plot_ERF(ga,Nsub,channel,0)

%% Plot multiplot Grand Average
%-----------------------------
toplot = 'tac';
dat_1 = ga_erf.(toplot).hit;
dat_2 = ga_erf.(toplot).miss;

cfg = [];
cfg.latency = [0 0.6];
dat_1 = ft_selectdata(cfg,dat_1);
dat_2 = ft_selectdata(cfg,dat_2);

dat_1 = ft_combineplanar([],dat_1);
dat_2 = ft_combineplanar([],dat_2);

% contrast
dat_diff = dat_1;
dat_diff.avg = dat_1.avg - dat_2.avg;

cfg = [];
cfg.layout = 'neuromag306cmb.lay';
%cfg.layout = 'neuromag306mag.lay';
%ft_multiplotER(cfg, dat_diff);
%ft_movieplotER(cfg, dat_diff);
ft_multiplotER(cfg, dat_1,dat_2);

%% Organize data to plot (Combine GRAD)
%---------------------------
%data_ga = ft_combineplanar([],erf{1}.aud.hit);% initialize structure
data_ga = erf{1}.aud.hit
sensmod   = {'aud' 'vis' 'tac'};
condlabel = {'hit' 'miss' 'supra' 'sham'};
for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    for n = 1:numel(erf)
      %datint = ft_combineplanar([],erf{n}.(sensmod{i}).(condlabel{j}));
      datint = erf{n}.(sensmod{i}).(condlabel{j});
      data_ga.([sensmod{i} condlabel{j}])(n,:,:) = datint.avg;
    end
  end
end

%% Plot PUBLI
%------------------
% test1 = 'vishit';%'ud';%'ud';%'dd';
% test2 = 'vismiss';%'ud';%'ud';%'dd';
% 
% sel = 'GRC';%'GRC'; %'MAG';
% Pathoutput = fullfile(config.path,'doc/erf');
% Pathoutput = [];
% 
% [timewins] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput)
% %[timewins] = Do_Publi_plot_ERF(data_ga,test1,test2,sel)



testlabel = {'audhit','vishit','tachit';...
  'audmiss','vismiss','tacmiss'};
%  
% testlabel = {'audsupra','vissupra','tacsupra';...
%    'audsham','vissham','tacsham'};

% testlabel = {'tachit';...
%   'tacmiss'};
 
sel = 'GRC';%'GRC'; %'MAG';
timeWin = [-200 550]; % in ms
data_ga.fsample = 512;
Pathoutput = fullfile(config.path,'doc/erf');
%Pathoutput = [];
for i = 1:size(testlabel,2)
test1 = testlabel{1,i};
test2 = testlabel{2,i};

[timewins,timemax] = Do_Publi_plot_ERF(data_ga,test1,test2,sel,Pathoutput,timeWin)
end


%% Load erf4MVPA
%---------------

cosmofileName = fullfile(config.path2work,'mvpa','erf4MVPA','MVPA_erfhpfilt_0.1_supra_vs_sham_toilim_-200to600_tstep10ms_10.mat');
load(cosmofileName);

dat = cosmo_map2meeg(ds);


trlab = sort(unique(dat.trialinfo(:,1))); % sort + unique extraction of conditions labels


M = [];
for i = 1:numel(trlab)
    tr2avg = find(dat.trialinfo(:,1)==trlab(i));
    
    M(i,:,:) = squeeze(mean(dat.trial(tr2avg,:,:),1));
    
end

Mabs = squeeze(mean(abs(M),2));

[conds,conds_erf] = make_condsTRIG4src('metaNT');

figure;
n = 0;
for id = 1:3
  subplot(3,1,id)
  n = n+1;
  plot(dat.time,Mabs(n,:),'--k','LineWidth',2)
  hold on;
  n = n+1;
  plot(dat.time,Mabs(n,:),'r','LineWidth',2)
  title(conds{id,2});
  
end


%% Fieldtrip plot
id2p = find(ismember(cell2mat(conds_erf(:,1)),trlab));

daterf = {};
for i = 1:numel(trlab)
  cfg = [];
  cfg.trials = find(dat.trialinfo(:,1)==trlab(i));
  daterf.(conds_erf{id2p(i),2}) = ft_timelockanalysis(cfg,dat);
  
end

lab2p = {'aud_sham' 'aud_supra'};

cfg = [];
cfg.layout = 'neuromag306cmb.lay';
ft_multiplotER(cfg,daterf.(lab2p{1}),daterf.(lab2p{2}));



