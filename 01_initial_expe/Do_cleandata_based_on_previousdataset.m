function [] = Do_cleandata_based_on_previousdataset(Nsub)


% reject artifact all blocks

%% Path settings
%------------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%pathpreproc_new = fullfile(config.path,'preproc','hpfilt_0.1');
pathpreproc_new = fullfile(config.path,'data2share');
pathpreproc_old = fullfile(config.path,'preproc','hpfilt_0.1');

for iSub = Nsub
  %% Automatic artefact rejection
  %-------------------------------
  disp(' ');
  disp('**********************');
  disp(['Sub = ',sprintf('%02d',iSub)]);
  
  % File already cleaned 
  %-----------------------
  filepoc_old = dir(fullfile(pathpreproc_old,[sprintf('clean_%02d_',iSub),'allblocks.mat']));
  fileName_old = fullfile(pathpreproc_old,filepoc_old.name);
  
  % File to clean
  %---------------
  filepoc_new = dir(fullfile(pathpreproc_new,[sprintf('%02d_',iSub),'allblocks.mat']));
  fileName_new = fullfile(pathpreproc_new,filepoc_new.name);
  
  saveName = fullfile(pathpreproc_new,['clean_' filepoc_new.name]);
  saveNamegoodinfo = fullfile(pathpreproc_new,['infoclean_' filepoc_new.name]);
  %% Load datasets
  %----------------
  disp(' ');
  disp(['Loading: ' fileName_old])
  
  data_cleaned = load(fileName_old);
  
  disp(' ');
  disp(['Loading: ' fileName_new])
  
  data = load(fileName_new);
  
  %% Check parameters
  %--------------------------
  
  % Number of channels (based on channels labels)
  %--------------------
  ID_goodchan  = find(ismember(data.label,data_cleaned.label));
  
  % Numbers of trials (based on trials indices)
  %-------------------
  ID_goodtrials = find(ismember(data.trialinfo(:,2),data_cleaned.trialinfo(:,2)));
  
  save(saveNamegoodinfo,'-v7.3','ID_goodchan','ID_goodtrials');
  
  %% Apply cleaning
  %-----------------
  cfg = [];
  cfg.trials  = ID_goodtrials;
  cfg.channel = ID_goodchan;
  [data_clean] = ft_selectdata(cfg,data);
  
  %% Keep track of important fields
  %---------------------------------
  data_clean.hdr        = data_cleaned.hdr;
  data_clean.sampleinfo = data_cleaned.sampleinfo;
  
  %% Save new file
  %-----------------
  save(saveName,'-v7.3','-struct', 'data_clean');
  
  %% Deleted previous file
  %-----------------------
%   disp(' ');
%   disp(['Deleting: ' fileName_new])
%   disp(' ');
%   delete(fileName_new);
%   
end