
% RUN on cluster fif2epo

%% clear
%--------
clear all global
close all


%% Path settings toolbox
%------------------------
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT',cfg);

%% Paths file and data
%-----------------------
Pathlogs = fullfile(config.path,['jobs/fif2epo/' date]);
pathfif = fullfile(config.path,'fif');

%% HP Filter choice
%--------------------
filthp = 0.1; % high pass filter frequency

%pathpreproc = fullfile(config.path,'preproc',['hpfilt_' num2str(filthp)]);
pathpreproc = fullfile(config.path,'data2share');

if ~exist(pathpreproc,'dir') % create output dir if dont exist
  mkdir(pathpreproc);
else
end

%% Condor call
%---------------
cfg = [];
cfg.mem = '15G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------
%[Suj] = Do_sel_subjects(config,'metaNT');
Suj = 1:25;

for i = 1:numel(Suj)
  
  id = Suj(i);
  cleanfilename = fullfile(pathpreproc,sprintf('%02d_allblocks.mat',id));
  if ~exist(cleanfilename,'file') % if file does not exist compute it
    
    condor_struct = obob_condor_addjob(condor_struct, 'hnc_fif2epo',...
      pathfif, ...
      pathpreproc,...
      id,...
      filthp);
  end
end

%% Submit
%---------
obob_condor_submit(condor_struct);
