
% RUN src ERF analysis to prepare cosmoMVPA analysis

%% Path settings
%----------------

project = 'metaNT';

cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true; 
cfg.package.hnc_condor = true;

config = addmypaths_gs(project,cfg);

Pathlogs = fullfile(config.path,['jobs/src4MVPA/' date]);
Pathdata = config.path;


%% Condor call
%---------------
cfg = [];
cfg.mem = '50G';
cfg.jobsdir = Pathlogs;
condor_struct = obob_condor_create(cfg);

%% Jobs additions
%-----------------

%% Subjects
%-----------
[Suj] = Do_sel_subjects(config,'metaNT');

%% Parameter for classification
%-------------------------------
do_parcelsrc = 0;% 0 or 1 -> if 1 = use obob LCMV beamformer with parcellation atlas
do_obobLCMV = 1; % 0 or 1 -> if 1 = use obob LCMV beamformer function
sensmod     = {'aud' 'vis' 'tac'}; % {'aud' 'vis' 'tac'} or {'all'}
toilim      = [-0.1 0.6]; % in sec (period to decode in CosMoMVPA)
win         = 0.01; % down sampling (average time samples every X sec)
CFilter     = 'sensmod';% Beamformer common filter computation =  'alltrials' or 'sensmod'
preprocfilt = 'hpfilt_0.1'; %  hpfilt_1 or hpfilt_0.1
paradigm    = 'hit_vs_miss';% 'hit_vs_miss' or 'supra_vs_sham' or 'miss_vs_sham' 
do_trans_suffix = []; % [] or '_trans_sss'

% Set Name & Run Jobs Loop
%--------------------------
tsteptype = [preprocfilt '_' paradigm '_toilim_' num2str(toilim(1)*1000) 'to' num2str(toilim(2)*1000) '_tstep' num2str(win*1000) 'ms'];

extra_name = [];
if do_parcelsrc
  do_obobLCMV = 1; % force obob LCMV because parcellation
  extra_name = [extra_name 'parcel'];
end
if do_obobLCMV
  extra_name = [extra_name 'OBOB'];
end

Nametype  = ['MVPA_src' tsteptype '_' extra_name '_BeamCFilt' CFilter];

Pathsave  = fullfile(config.path2work,'mvpa/src4MVPA'); % saved data

if ~exist(Pathsave,'dir') % create output dir if dont exist
  mkdir(Pathsave);
else
end

%% Do parallel computing
%-------------------------

for i = 1:numel(Suj)
  id = Suj(i);
  
  datafile = fullfile(Pathdata,'preproc',preprocfilt,sprintf('clean_%02d_allblocks.mat',id));
  hdmfile  = fullfile(Pathdata,'mri',sprintf('s%02d',id),sprintf('sub%02d_hdm.mat',id));
  savefilename = fullfile(Pathsave,sprintf([Nametype '_%02d.mat'],id));
  
  if exist(savefilename,'file')
  else
    condor_struct = obob_condor_addjob(condor_struct, 'Do_srcERF4cosmoMVPA',project,datafile,hdmfile,sensmod,toilim,win,savefilename,CFilter,paradigm,do_obobLCMV,do_parcelsrc);
  end
end

 
%% Submit
%---------
obob_condor_submit(condor_struct);