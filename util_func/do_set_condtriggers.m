function [tr1,tr2,tr3,condlabel] = do_set_condtriggers(sensmod,paradigm)

for i = 1:numel(sensmod)
  if strcmp(sensmod{i},'aud')
    cod = 100;
  elseif strcmp(sensmod{i},'vis')
    cod = 200;
  elseif strcmp(sensmod{i},'tac')
    cod = 300;
  else
    error(['This sensmod ' sensmod{i} ' is not correct...'])
  end
  tr1(i) = cod;
end

switch paradigm
  
  case 'hit_vs_miss_respcontrol'
    
    tr1 = tr1*10; % set sensmod code to 1000 2000 or 3000
    
    condlabel = {'hit_resp' 'miss_resp' 'hit_noresp' 'miss_noresp'};
    tr2       = [341 321 340 320];
    tr3       = [1 2 -1 -2]; % recode dd_r=1 ud_r=2 dd_nr=-1 ud_nr=-2
  
  case 'supra_vs_sham'
    condlabel = {'supra' 'sham'};
    tr2       = [54 12];
    tr3       = [1 2];
    
  case 'hit_vs_miss'
    condlabel = {'hit' 'miss'};
    tr2       = [34 32];
    tr3       = [1 2]; % recode dd=1 ud=2
    
  case 'miss_vs_sham'
    condlabel = {'miss' 'sham'};
    tr2       = [32 12];
    tr3       = [1 2]; % recode dd=1 ud=2
    
  case 'all'

    % Triggers and condlabels
    %-----------------------
    condlabel = {'hit' 'miss' 'sham' 'supra'};
    tr2       = [34 32 12 54];
    tr3       = [1 2 2 1]; % recode dd=1 ud=2  
     
end