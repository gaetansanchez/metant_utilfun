clear all global
close all

%%
worksp='/mnt';
load(fullfile(worksp, '/obob/staff/nsuess/code_snippets/mni2982_Networklabels.mat'));
load('/mnt/obob/staff/ahauswald/templates/atlas/atlas_parcel333.mat')

%% init obob_ownft
addpath('/mnt/obob/obob_ownft/');

cfg = [];
cfg.package.svs = 'true';
cfg.package.obsolete = 'true';

% roi package
cfg.package.gm2 = 'true';

obob_init_ft(cfg);

%%
load mni_grid_1_cm_2982pnts

% choose region
chansel=ft_channelselection(cell({'*DorAt*'}), label);
%[C,ia]=setdiff(label, chansel);
[C,ia]=intersect(label, chansel);


 idx_inside2all = find(template_grid.inside);%important to apply find the indeces of all fields not only inside...
 ia_all = idx_inside2all(ia);

newgrid=template_grid;
newgrid.inside(template_grid.inside==1) = 0;
newgrid.inside(ia_all) = 1;

% chosing the voxels corresponding to the chosen region
stat.stat2=stat.stat(ia)
%%
% Network parcel plotting

cfg = [];
cfg.frequency = [1 3];
cfg.parameter = 'stat2';
cfg.sourcegrid = newgrid;
source1 = obob_svs_virtualsens2source(cfg, stat);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.parameter = 'stat2';
cfg.interpmethod = 'nearest';
inter_source = ft_sourceinterpolate(cfg, source1, mri);
inter_source.coordsys = 'mni';

%%
% cfg = [];
% cfg.parameter = 'stat2';
% cfg.atlas = atlas;
% cfg.frequency = [1 3];
% cfg.time = [-1 1];
% cfg.sourcegrid = newgrid;
% cfg.mri = mri;
% source = obob_svs_virtualsens2source(cfg, stat);
% source.coordsys = 'mni';

%%
template_grid=ft_convert_units(template_grid, 'm');

%atlas = ft_read_atlas(fullfile(wkspace, '/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii'));
atlas = ft_convert_units(atlas,'m'); % assure that atlas and template_grid are expressed in the same units
%%

inter_source.stat2(isnan(inter_source.stat2)) = 0;

source1.stat2(isnan(source1.stat2)) = 0;

%%
%source.stat3 = source.stat2.*source.stat2;

%%
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'stat2';
cfg.maskparameter = 'stat2'; %'mask';
%cfg.crosshair = 'yes';
cfg.coordsys = 'mni';
cfg.inputcoord = 'mni';
cfg.interactive = 'yes';
%cfg.funcolorlim = [-4.5 0];
cfg.atlas = atlas;
cfg.atlas.coordsys = 'mni';
% cfg.marker        = template_grid.pos(ia_all,:,1);
ft_sourceplot(cfg, inter_source);

inter_source2 = inter_source;
%%
% plot own stats 

stat.stat2=stat.stat.*stat.mask;

stat.stat2=stat.mask==1

%%
cfg = [];
cfg.frequency = [1 3];
cfg.parameter = 'stat2';
cfg.sourcegrid = template_grid;
source2 = obob_svs_virtualsens2source(cfg, stat);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.parameter = 'stat2';
inter_source = ft_sourceinterpolate(cfg, source2, mri);
inter_source.coordsys = 'mni';

%%
inter_source.stat2(isnan(inter_source.stat2)) = 0;
 
%%
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'stat2';
cfg.maskparameter = 'stat2'; %'mask';
%cfg.crosshair = 'yes';
cfg.coordsys = 'mni';
cfg.inputcoord = 'mni';
cfg.interactive = 'yes';
%cfg.funcolorlim = [-4.5 0];
cfg.atlas = atlas;
cfg.atlas.coordsys = 'mni';
% cfg.marker        = template_grid.pos(ia_all,:,1);
ft_sourceplot(cfg, inter_source);

%%
sourceStat=inter_source;
sourceDorAT=inter_source2;
%%

% give them different numbers - making new masks

load mri_better_segmented.mat
sourceStat.stat2=(sourceStat.stat2>=0.1).*2;%*(segment_brain.brain==1);
%sourceStat.stat2=(sourceStat.stat2>=1).*2;  %.*(segment_brain.brain==1);

sourceStat.stat2=(sourceStat.stat2).*(segment_brain.brain==1);

sourceDorAT.stat2=(sourceDorAT.stat2>=1).*(segment_brain.brain==1);

sourceStat.stat2(isnan(sourceStat.stat2)) = 0;
sourceDorAT.stat2(isnan(sourceDorAT.stat2)) = 0;

sourceStat.mask=sourceStat.stat2 %>= (stat.critval(2))).*(segment_brain.brain==1);
sourceDorAT.mask=sourceDorAT.stat2 %>= (sourceDorAT.critval)).*(segment_brain.brain==1);

cfg=[];
cfg.funparameter='stat2';
cfg.funcolormap='jet';
cfg.maskparameter='mask';
ft_sourceplot(cfg, sourceStat)%[253 183 165] Heschl R!
ft_sourceplot(cfg, sourceDorAT) % [240 178 162] Heschl R (max eigentlich calcarine [153 83 185])

%% multiply the masks and plot 

sourceMASK=sourceStat;
sourceMASK.stat=abs(sourceStat.mask + (sourceDorAT.mask));
sourceMASK.mask= sourceMASK.stat > 0.5;

cfg=[];
cfg.method='surface';
cfg.funparameter='stat';
cfg.funcolormap='jet';
cfg.funcolorlim=[0 3];
%cfg.opacitymap=[0 3];
cfg.maskparameter='mask';% wenn du stat nimmst, wird die transparenz skaliert
cfg.projmethod = 'nearest';
cfg.crosshair='no';
cfg.camlight='no';
cfg.atlas=atlas;
ft_sourceplot(cfg, sourceMASK)


%% plot ortho

cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'stat';
cfg.maskparameter = 'mask'; %'mask';
%cfg.crosshair = 'yes';
cfg.coordsys = 'mni';
cfg.inputcoord = 'mni';
cfg.interactive = 'yes';
%cfg.funcolorlim = [-4.5 0];
cfg.atlas = atlas;
cfg.atlas.coordsys = 'mni';
% cfg.marker        = template_grid.pos(ia_all,:,1);
cfg.funcolormap = 'jet';
ft_sourceplot(cfg, sourceMASK);


%% plot caret style
cfg = [];
cfg.funparameter = 'stat';
cfg.funcolorlim = [0 3];
%cfg.maskparameter = 'mask2';
cfg.method = 'surface';
cfg.projthresh = 0.1;
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';
cfg.surffile = 'surf_caretleft.mat';
ft_sourceplot(cfg, sourceMASK)

colormap(jet)
%colormap(hot)
view_angle=[90 0];
%view_angle=[16 27];
plot_caret_style('source', 300, [], view_angle, 'left');


%% look for overlaps

comp_masks = [source1.inside source2.inside];
%%
new_mask = comp_masks(:, 1) == 1 & comp_masks(:, 2) == 1;

%%
out_fname = sprintf('attention_overlap.mat');

save(out_fname, 'val');