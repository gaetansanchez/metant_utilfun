function [] = Do_srcERF(project,iSub,hdmfile,sensmod,toilim,pathpreproc,Pathsave,precision_grid,CFilter,paradigm)
% Do source reconstruction with Beamformer filters
% Setup data structure for CosmoMVPA analysis
%---------
% INPUTS
%----------
% project = define paths
% iSub = subject number
% hdmfile  = headmodel file (with hdm structure; see 'Do_epo2src.m' for details)
% sensmod  = cell array of sensory modality to decode = {'all'} or {'aud' 'vis' 'tac'}
% toilim   = time range to decode in sec (i.e toilim = [-0.5 0.5])
% pathpreproc = path where are epoched files
% Patsave  = output path and filename
% precision_grid = filename of template or in cm (template grid) (i.e = 1.5)
% CFilter  = compute filter for each modality ('sensmod') or for all modalities ('alltrials');
% paradigm = 'hit_vs_miss' or 'supra_vs_sham'
%----------------------
% Gaetan - 21/01/2017

%% init obob
%------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs(project,cfg);

%% load + clean
%-------
% load data + info clean (from reject_visual_blocks.m)
%-----------------------
if strcmp(project,'metaNT')
  filepoc = dir(fullfile(pathpreproc,sprintf('clean_%02d_allblocks.mat',iSub)));
  fileName = fullfile(pathpreproc,filepoc.name);
  [~, fname] = fileparts(fileName);
  data_raw = load(fileName);% load already cleaned data
else
  filepoc = dir(fullfile(pathpreproc,sprintf('%02d_allblocks.mat',iSub)));
  fileName = fullfile(pathpreproc,filepoc.name);
  [~, fname] = fileparts(fileName);
  data_raw = do_reject4cleaning(datafile);% do reject based on manual cleaning
end

%% SET data

[tr1,tr2,tr3,condlabel] = do_set_condtriggers(sensmod,paradigm);

% if sensmod = all --> Recode trialinfo (remove 100 200 or 300)
%--------------------------------------------------------------
if strcmp(sensmod{1},'all')
  Val2rm = floor( data_raw.trialinfo(:,1)./100)*100;
  data_raw.trialinfo(:,1) = data_raw.trialinfo(:,1) - Val2rm;
  tr1       = 0;
end

%% Equalize trial number per condition
%--------------------------------------
[data,trig,trial_id,trig_sens] = Do_equalcond(pathpreproc,fname,sensmod,tr1,condlabel,tr2,data_raw,paradigm);

clear data_raw;

% Select just trig conditions (in this case: hit and miss)
%---------------------------------------------------------------------
cfg = [];
cfg.trials = find(ismember(data.trialinfo(:,1),trig));
data = ft_selectdata(cfg,data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PREPARE SOURCE LCMV-DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[conds,~] = make_condsTRIG4src(sensmod,condlabel,tr1,tr2);

cov_win  = 'all';
freq     = 'low';
[datasrc] = Do_epo2src(data,hdmfile,toilim,cov_win,freq,precision_grid,conds,CFilter);

%% make conditions triggers for average
[~,conds_erf] = make_condsTRIG4src(sensmod,condlabel,tr1,tr2);

%% compute ERFs
for iCerf = 1:size(conds_erf,1)
     
    trial_id = find(ismember(datasrc.trialinfo(:,1), conds_erf{iCerf, 1}));
    
    % check number of remaining trials
    %----------------------------------
    if numel(trial_id) < 9 % not enough trial to compute average
      erf.(conds_erf{iCerf,2}) = []; 
      erfbl.(conds_erf{iCerf,2}) = [];
    else
      
      % select conditions (and time if you want)
      cfg         = [];
      cfg.trials  = trial_id;
      cfg.latency = toilim;
      tmp         = ft_selectdata(cfg, datasrc);
      
      cfg = [];
      cfg.preproc.lpfilter     = 'yes';
      cfg.preproc.lpfreq       = 30;
      erf.(conds_erf{iCerf,2}) = ft_timelockanalysis(cfg, tmp);
      
      cfg = [];
      cfg.baseline = [-.2 0];
      cfg.baselinetype = 'relchange';
      erfbl.(conds_erf{iCerf,2}) = obob_svs_timelockbaseline(cfg, erf.(conds_erf{iCerf,2}));
      
      % BEFORE SAVING DATA, REMOVE CFG-FIELD
      erf.(conds_erf{iCerf,2}) = rmfield(erf.(conds_erf{iCerf,2}), 'cfg');
      erfbl.(conds_erf{iCerf,2}) = rmfield(erfbl.(conds_erf{iCerf,2}), 'cfg');
    end
    
end


%% save data 
if ~exist(Pathsave, 'dir')
    mkdir(Pathsave);
end

% save
sname = fullfile(Pathsave,[sprintf('s%02d',iSub) '_src_erf.mat']);
save(sname, '-v7.3', 'erf', 'erfbl')
disp(['Saving ' sname]);


