function [source] = get_atlasparcel_ROI(labelnames,val_toplot,flag)
%
% INPUTS
%%%%%%%%%
% labelnames : cell-array of labels inside parcel atlas
%              i.e : labelnames = {'Sal';'At'};
% val_toplot : values to plot each networks parcel (on colormap scale jet(10) 
%              between 0 and 1 
%              i.e : val_toplot = [.95,.45];
% flag       : plot the relevant parcels on a caret brain (just as example)
%
% OUTPUTS
%%%%%%%%%
% source : interpolate mesh with relevant fields to plot 
%
% Gaetan - 16/06/2019

load standard_mri
load parcellations_3mm.mat
template_grid = parcellation.template_grid;
parcel_grid = parcellation.parcel_grid;


% Change units
%-------------
parcel_grid   = ft_convert_units(parcel_grid,'m'); 
mri           = ft_convert_units(mri, 'm');
template_grid = ft_convert_units(template_grid, 'm');


%% choose region
%-------------------
idx_inside2all = find(parcel_grid.inside);%important to apply find the indeces of all fields not only inside...
label = parcel_grid.label;

for i = 1:numel(labelnames)
  chansel = ft_channelselection(cell({['*' labelnames{i} '*']}), label);
  [C,id{i}]=intersect(label, chansel);
  id_all{i} = idx_inside2all(id{i});
end

newgrid = parcel_grid;
newgrid.toplot(parcel_grid.inside==1) = 0;
newgrid.toplot = newgrid.toplot';

% all networks
%-----------------
masklabelnames = {};
for i = 1:numel(id_all)
  newgrid.toplot(id_all{i}) = val_toplot(i);
  
  newgrid.(labelnames{i})(parcel_grid.inside==1) = 0;
  newgrid.(labelnames{i}) = newgrid.(labelnames{i})';
  newgrid.(labelnames{i})(id_all{i}) = val_toplot(i);
  
  masklabelnames{i,1} = ['mask' labelnames{i}];
  newgrid.(masklabelnames{i}) = zeros(size(newgrid.(labelnames{i})));
  newgrid.(masklabelnames{i})(newgrid.(labelnames{i})~=0) = 1;
end

newgrid.mask = zeros(size(newgrid.toplot));
newgrid.mask(newgrid.toplot~=0) = 1;

cfg = [];
cfg.parameter = [labelnames;masklabelnames;{'toplot';'mask'}];
source = ft_sourceinterpolate(cfg, newgrid, mri);

if flag
  %% Example to plot caret brain all networks together
  %-----------------------------------------------------
  cfg = [];
  cfg.funparameter = 'toplot';
  cfg.maskparameter = 'mask';
  cfg.method = 'surface';
  cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
  cfg.colorbar = 'no';
  cfg.camlight = 'no';
  cfg.funcolorlim = [0 1];
  
  % make figures for right hemisphere
  hemibrain = {'right'};
  viewbrain = {'right'};
  for i = 1:numel(hemibrain)
    for j = 1:numel(viewbrain)
      h = hemibrain{i};
      cfg.surffile = ['surf_caret' h '.mat'];
      ft_sourceplot(cfg, source);
      colormap(jet(10));
      v = viewbrain{j};
      plot_caret_style([], 100, [], v);
      
    end
  end
  
end