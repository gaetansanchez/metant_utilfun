function [MissF] = Do_mvpa_results_stack(Suj,Nbrfiles,Pathinput,Pathoutput,Nametype,Delfile,sensmod)
%
% Append results from parallelisation of MVPA
% Use outputs from Do_MVPA_time2time_parcomp.m
% (used in: condor_MVPA_time2time_srcERF_parcomp.m)


%% Do stacking
%-------------
n_modalities = numel(sensmod);
MissF   = [];
for i = 1:numel(Suj)
  
  id = Suj(i);
  
  MissF(i).suj = id;
  
  savefilename = fullfile(Pathoutput,sprintf([Nametype '_%02d.mat'],id));
  
  if ~exist(savefilename,'file')
    %% check files number is correct (for all conditions)
    %-------------------------------
    
    Nostack = [];
    for train_modality=1:n_modalities
      for test_modality=1:n_modalities
        
        %allfiles = dir(fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d_centerids_*.mat',id,train_modality,test_modality)));
        allfiles = dir(fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d*',id,train_modality,test_modality)));
        if isempty(allfiles)
          %error(['No file = ' fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d_centerids_*.mat',id,train_modality,test_modality))]);
          error(['No file = ' fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d*',id,train_modality,test_modality))]);
        end
        
        if numel(allfiles) == Nbrfiles
          Nostack = 0;
        else
          Nostack = 1;
        end
        MissF(i).files(train_modality,test_modality) = Nostack;% keep track of missing files subjects number
      end
    end
    
    
    %% RUN stack for all files if everything is here
    %-------------------------------------------------
    disp(['Subject = ' num2str(MissF(i).suj) ...
      ' --> Start processing...']);
    
    
    % report if missing data on command window and save
    if sum(sum(MissF(i).files)) == 0 % NO missing data
      
      %% stack the data per conditions
      %--------------------------------
      M = {};
      for train_modality=1:n_modalities
        for test_modality=1:n_modalities
          
          %allfiles = dir(fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d_centerids_*.mat',id,train_modality,test_modality)));
          allfiles = dir(fullfile(Pathinput,sprintf('sub%02d_train%02d_test%02d*',id,train_modality,test_modality)));
          
          result_map_cell = {};
          for k = 1:Nbrfiles
            % load data
            %-----------
            datafile = fullfile(Pathinput,allfiles(k).name);
            load(datafile);
            result_map_cell{k} = ds_result;
          end
          
          results_map=cosmo_stack(result_map_cell,2); % stack data
          cosmo_check_dataset(results_map); % check
          
          % store variable
          %---------------
          M{train_modality,test_modality} = results_map;
        end
      end
      
      
      
      %% save data
      %-----------
      if ~exist(Pathoutput,'dir')
        mkdir(Pathoutput);
      end
      
      save(savefilename,'-v7.3','M');
      
      %% remove file if needed
      %-----------------------
      if Delfile
        datafilesSUJ = fullfile(Pathinput,sprintf('sub%02d_*.mat',id));
        delete(datafilesSUJ);
      end
      
      disp(['Subject = ' num2str(MissF(i).suj) ...
        ' --> OK']);
      
      
    else % if there is missing data do not
      
      %% if any missing files for 9 conditions (3 x 3 train/test)
      %----------------------------------------------------------
      disp(['Subject = ' num2str(MissF(i).suj) ...
        ' --> Missing files nothing was saved for this subject']);
      
    end
  else
    disp(['Subject = ' num2str(MissF(i).suj) ...
        ' --> Already done previously']);
  end
  
  disp('************');
  
  
end

end




