function [data_out,trig,trial_id,trig_sens] = Do_equalcond(fpath,fname,sensmod,tr1,condlabel,tr2,data_in,paradigm)
% Check if equalization of condition was already done and use it
% Or do it from scratch and save the resulting trials indices in a file

%% take only some conditions for all modalities
%------------------------------------------
trial_id = [];
n = 1;
for j = 1:numel(sensmod)
  
  m = 1;
  for k = 1:numel(condlabel)
    trig(n)    = tr1(j)+tr2(k);
    trial_id = [trial_id;find(data_in.trialinfo(:,1) == trig(n))];
    n = n+1;
    
    tr_sens{m} = tr1(j)+tr2(k);
    m = m+1;
  end
  
  trig_sens{j} = tr_sens;
  tr_sens =[];
end

%% Check if equal cond file already exist or create it
%-----------------------------------------------------
path2save = fullfile(fpath,'equal_Nbtrials_index');
if ~exist(path2save,'dir') % create output dir if dont exist
  mkdir(path2save);
end

if strcmp(paradigm,'all') % take trials sorting from hit_vs_miss equalization ==> supra and sham wont be balanced but its fine
  filename_tr = fullfile(path2save,[fname '_hit_vs_miss_equalcond_trialindex.mat']);
  if ~exist(filename_tr,'file')
    filename_tr = fullfile(path2save,[fname '_all_equalcond_trialindex.mat']);
  end
else
  filename_tr = fullfile(path2save,[fname '_' paradigm '_equalcond_trialindex.mat']);
end


if ~exist(filename_tr,'file')
  disp(' ');
  disp('**************');
  disp('Equalize conditions');
  disp('**************');
  
  % equalize conditions (in each sensmod)
  %-------------------------------------
  for i = 1:numel(trig_sens)
    cfg = [];
    cfg.trigger = trig_sens{i};
    data_in = meg_equalize_conditions(cfg, data_in);
  end
  
  data_out = data_in;
  % Save equalized conditions trials number
  %-------------------------------------------
  sel_tr = data_out.trialinfo(:,2);
  save(filename_tr,'sel_tr');
  
else
  disp(' ');
  disp('**************');
  disp(['Using this file to equalize conditions ==> ' filename_tr]);
  disp('**************');
  % Load previous sorted conditions
  %--------------------------------
  load(filename_tr);
  
  % select trials
  %--------------
  cfg         = [];
  cfg.trials  = find(ismember(data_in.trialinfo(:,2),sel_tr));
  data_out    = ft_selectdata(cfg, data_in);
  
end