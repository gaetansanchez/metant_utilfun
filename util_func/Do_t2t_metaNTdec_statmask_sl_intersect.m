function [CVroi_all,IUroi_all,AIDroi_intensity,AIDroi] = Do_t2t_metaNTdec_statmask_sl_intersect(sig_toi,sig_toi_diag,sig_roi)


% Analysis similarity between conditions
%---------------------------------------
AIDroi = {};
AIDroidiag = {};
for i = 1:size(sig_toi,1)
  for j = 1:size(sig_toi,2)
    AIDroi{i,j} = find(sig_toi{i,j});
    AIDroidiag{i,j} = find(sig_toi_diag{i,j});
  end
end

% Compute intersection scale up to 3
%-----------------------------------
Imask_brain_int_123 = [];
Isensmask_brain_int_123 = [];
Umask_brain_int_123 = [];
Usensmask_brain_int_123 = [];

if numel(sig_toi) ~= 1 % dont do this if only one modality
  UV_audvis_sig_toi = logical(sig_toi{1,2}+sig_toi{2,1});% union
  UV_tacaud_sig_toi = logical(sig_toi{3,1}+sig_toi{1,3});
  UV_vistac_sig_toi = logical(sig_toi{3,2}+sig_toi{2,3});
  
  UV_audtrain_sig_toi = logical(sig_toi{1,2}+sig_toi{1,3});% union
  UV_vistrain_sig_toi = logical(sig_toi{2,1}+sig_toi{2,3});
  UV_tactrain_sig_toi = logical(sig_toi{3,1}+sig_toi{3,2});
  
  IV_audvis_sig_toi = logical((sig_toi{1,2}+sig_toi{2,1})>1);%intersect
  IV_tacaud_sig_toi = logical((sig_toi{3,1}+sig_toi{1,3})>1);
  IV_vistac_sig_toi = logical((sig_toi{3,2}+sig_toi{2,3})>1);
  
  IV_audtrain_sig_toi = logical((sig_toi{1,2}+sig_toi{1,3})>1);% intersect
  IV_vistrain_sig_toi = logical((sig_toi{2,1}+sig_toi{2,3})>1);
  IV_tactrain_sig_toi = logical((sig_toi{3,1}+sig_toi{3,2})>1);
  
  Umask_brain_int_123 = UV_audvis_sig_toi + UV_tacaud_sig_toi + UV_vistac_sig_toi;
  Imask_brain_int_123 = IV_audvis_sig_toi + IV_tacaud_sig_toi + IV_vistac_sig_toi;
  Usensmask_brain_int_123 = UV_audtrain_sig_toi + UV_vistrain_sig_toi + UV_tactrain_sig_toi;
  Isensmask_brain_int_123 = IV_audtrain_sig_toi + IV_vistrain_sig_toi + IV_tactrain_sig_toi;
end

% Different intersect or union masking
%--------------------------------------
if size(AIDroi,1) == 1 && size(AIDroi,2) == 1
  CVroi_all        = AIDroi{1,1};
  CVroi_all_d      = AIDroidiag{1,1};
  AIDroi_intensity = sig_roi{1,1};
  IUroi_all = [];
else
  
  % Sens Modality restriction - intersect
  CV_audtrain = intersect(AIDroi{1,2},AIDroi{1,3});
  CV_vistrain = intersect(AIDroi{2,1},AIDroi{2,3});
  CV_tactrain = intersect(AIDroi{3,1},AIDroi{3,2});
  
  % Sens Modality restriction - union
  UV_audtrain = union(AIDroi{1,2},AIDroi{1,3});
  UV_vistrain = union(AIDroi{2,1},AIDroi{2,3});
  UV_tactrain = union(AIDroi{3,1},AIDroi{3,2});
  
  % Cross validation like - intersect
  CV_audvis = intersect(AIDroi{1,2},AIDroi{2,1});
  CV_tacaud = intersect(AIDroi{3,1},AIDroi{1,3});
  CV_vistac = intersect(AIDroi{3,2},AIDroi{2,3});
  
  % Cross validation like - union
  UV_audvis = union(AIDroi{1,2},AIDroi{2,1});
  UV_tacaud = union(AIDroi{3,1},AIDroi{1,3});
  UV_vistac = union(AIDroi{3,2},AIDroi{2,3});
  
  
  CVroi_all     = intersect(intersect(CV_audtrain,CV_vistrain),CV_tactrain); % intersect of intersect
  IUroi_all     = intersect(intersect(UV_audvis,UV_tacaud),UV_vistac);% intersect of union mirror decoding
  IUsensroi_all = intersect(intersect(UV_audtrain,UV_vistrain),UV_tactrain);% intersect of union sens training
  UIroi_all     = union(union(CV_audtrain,CV_vistrain),CV_tactrain); % union of intersect training
  
  CVroi_all_union = union(union(CV_audvis,CV_tacaud),CV_vistac);
  
  CV_audtrain_d = intersect(AIDroidiag{1,2},AIDroidiag{1,3});
  CV_vistrain_d = intersect(AIDroidiag{2,1},AIDroidiag{2,3});
  CV_tactrain_d = intersect(AIDroidiag{3,1},AIDroidiag{3,2});
  CV_visaud_d   = intersect(CV_audtrain_d,CV_vistrain_d);
  CVroi_all_d   = intersect(intersect(CV_audtrain_d,CV_vistrain_d),CV_tactrain_d);
  
  % Keep number of time each ROI is significant (between modalities)
  %----------------------------------------------------------------
  AIDroi_intensity = sig_roi{1,2} + sig_roi{1,3} +...
    sig_roi{2,1} + sig_roi{2,3} +...
    sig_roi{3,1} + sig_roi{3,2};
end
