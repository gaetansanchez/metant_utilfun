function [conds,conds_erf] = make_condsTRIG4src(sensmod,condlabel,tr1,tr2)

% separate main conditions (3 sensory modalities)
conds = {tr1(1)+tr2 , sensmod{1};...
  tr1(2)+tr2 , sensmod{2};...
  tr1(3)+tr2 , sensmod{3}};

% separate all conditions
n = 1;
for i = 1:numel(sensmod)
  for j = 1:numel(condlabel)
    conds_erf{n,1} = tr1(i)+tr2(j);
    conds_erf{n,2} = [sensmod{i} '_' condlabel{j}];
    n = n+1;
  end
end

