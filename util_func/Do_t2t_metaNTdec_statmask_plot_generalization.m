function [] = Do_t2t_metaNTdec_statmask_plot_generalization(data,val,stat_mask,acclimits,xborders,yborders,factms)

% if acclimits = [0 1] this function nomrlaize accuracies between 0 and 1

%% Average time course of Accuracy
%----------------------------------
colormap_choice = 'jet';

TimeClus = 2; % number of points off the diagonal
Mv = [];
Sv = [];
DMv = [];
nonDMv = [];
Ssig = [];
SsigWITH = [];
SsigBETW = [];
AccWITH = [];
SsigWITH = [];
AccBETW = [];
SsigBETW = [];
b = 1;
w = 1;
for s1 = 1:size(data,1)
  for s2 = 1:size(data,2)
    
    % Number of significant decoding based on training data
    %-------------------------------------------------------
    S = squeeze(stat_mask(s1,s2,:,:)); % significant time points
    Ssig(s1,s2,:) = sum(S,2);
    
    A = flipud(squeeze(data(s1,s2,:,:))); % accuracy
    %A = squeeze(mean(zdata{s1,s2},1)); % zvalues
    
    maskA = squeeze(data(s1,s2,:,:)).*S; % only significant accuracy
    %maskA = squeeze(data(s1,s2,:,:)); % all accuracy values
    maskA(maskA==0) = NaN;
    
    TimePoint = [-size(A,1)+1:size(A,1)-1];
    
    for i = 1:numel(TimePoint)
      
      val2M = diag(A,TimePoint(i));
      
      if numel(val2M) <= TimeClus*2+1
        Mv(s1,s2,i) = mean(val2M); % averarge accuracy
        Sv(s1,s2,i) = std(val2M)/sqrt(numel(val2M));
        
      else
        lval2M = numel(val2M);
        if mod(lval2M,2) % odd number (impair)
          startp = ceil(lval2M/2)-TimeClus;
          stoptp = ceil(lval2M/2)+TimeClus;
        else % even number (pair)
          startp = ceil(lval2M/2)-TimeClus;
          stoptp = ceil(lval2M/2)+TimeClus-1;
        end
        Mv(s1,s2,i) = mean(val2M(startp:stoptp)); % average accuracy
        Sv(s1,s2,i) = std(val2M(startp:stoptp))/sqrt(numel(val2M(startp:stoptp)));
      end
      
    end
    
    if s1 == s2 % within same modality
      
      AccWITH(w,:) = nanmean(maskA,2); % Accuracy for each row only for significant time points
      
      SsigWITH(w,:) = squeeze(Ssig(s1,s2,:))'; % number of significant time point only within
      w = w+1;
      
      if isempty(DMv) % only accuracy within
        DMv(1,:) = squeeze(Mv(s1,s2,:))';
      else
        DMv(end+1,:) = squeeze(Mv(s1,s2,:))';
      end
      
    else % between different modalities
      
      AccBETW(b,:) = nanmean(maskA,2); % Accuracy for each row only for significant time points
      
      SsigBETW(b,:) = squeeze(Ssig(s1,s2,:))';% number of significant time point only between
      b = b+1;
      
      if isempty(nonDMv) % only accuracy between
        nonDMv(1,:) = squeeze(Mv(s1,s2,:))';
      else
        nonDMv(end+1,:) = squeeze(Mv(s1,s2,:))';
      end
    end
    
  end
end

if size(Ssig,1) == size(Ssig,2) == 1 % only for all versus all
  AccBETW(b,:) = nanmean(maskA,2); % Accuracy for each row only for significant time points
  
  SsigBETW(b,:) = squeeze(Ssig(s1,s2,:))';% number of significant time point only between
  b = b+1;
  
  if isempty(nonDMv) % only accuracy between
    nonDMv(1,:) = squeeze(Mv(s1,s2,:))';
  else
    nonDMv(end+1,:) = squeeze(Mv(s1,s2,:))';
  end
end

%
mSsigWITH = mean(SsigWITH,1);% all within average
mSsigBETW = mean(SsigBETW,1);% all between average
sSsigWITH = std(SsigWITH,1,1);
sSsigBETW = std(SsigBETW,1,1);

if size(Ssig,1) == 3
  ModmSsigBETW(1,:) = mean([squeeze(Ssig(1,2,:))';squeeze(Ssig(1,3,:))']); % auditory training
  ModmSsigBETW(2,:) = mean([squeeze(Ssig(2,1,:))';squeeze(Ssig(2,3,:))']); % visual training
  ModmSsigBETW(3,:) = mean([squeeze(Ssig(3,1,:))';squeeze(Ssig(3,2,:))']); % tactile training
elseif size(Ssig,1) == 1
  ModmSsigBETW(1,:) = mean(squeeze(Ssig(1,1,:))'); % all training
else
  error('Check dimension of sensmod');
end

if acclimits(1) == 0 && acclimits(end) == 1
  % Normalize accuracy between 0 and 1 with y = (x - min(x))/(max(x)-min(x))
  %%%%%%%%%%%%%%  
  [AccWITH] = Do_normalize(AccWITH,2);
  [AccBETW] = Do_normalize(AccBETW,2);
  %%%%%%%%%%%%%
end

MAccWITH = nanmean(AccWITH,1);
MAccBETW = nanmean(AccBETW,1);

SAccWITH = nanstd(AccWITH,1,1);
SAccBETW = nanstd(AccBETW,1,1);

MAccWITH(isnan(MAccWITH)) = 0.5;
MAccBETW(isnan(MAccBETW)) = 0.5;
SAccWITH(isnan(SAccWITH)) = 0.5;
SAccBETW(isnan(SAccBETW)) = 0.5;

mDMv = mean(DMv,1);
sDMv = std(DMv,1)/sqrt(size(DMv,1));
mnonDMv = mean(nonDMv,1);
snonDMv = std(nonDMv,1)/sqrt(size(nonDMv,1));

%% Plot figure
%------------------
figure;set(gcf,'color','w');
% factms    = 10; % in ms
% yborders  = [0 35]*factms; % in ms
% xborders  = [0 500];% in ms
%acclimits = [0.5 0.525]; % in percentage

for i = 1:size(SsigWITH,1)
  subplot(size(SsigWITH,1),1,i);
  plot(val,SsigWITH(i,:)*factms,'k','LineWidth',3);hold on;
  plot(val,ModmSsigBETW(i,:)*factms,'r','LineWidth',3);
  if i == 1
    %legend({'Within'; 'Between'});
    xlabel('Training time points (ms)','FontSize',17,'FontWeight','bold');
    ylabel({'Generalization';'over testing time (ms)'},'FontSize',17,'FontWeight','bold');
  end
  set(gca,'FontSize',15,'FontWeight','bold');
  ylim(yborders)
  xlim(xborders)
end

figure;set(gcf,'color','w');
plot(val,MAccWITH,'k','LineWidth',4); hold on;
plot(val,MAccBETW,'r','LineWidth',4);
shadedErrorBar(val,MAccWITH,SAccWITH,'-k');
shadedErrorBar(val,MAccBETW,SAccBETW,'-r');

%---------------------------------
% Do Figure summary Generalization
%---------------------------------
figure;set(gcf,'color','w');

subplot(2,1,1);
hold on;
x = val';
y = (mSsigWITH*factms)';
M = MAccWITH';
Do_patchUnderplot(x,y,M);
caxis([0.5 max(MAccWITH)]);
caxis(acclimits);
plot(val,mSsigWITH*factms,'k','LineWidth',4);
xlabel('Training time points (ms)','FontSize',15,'FontWeight','bold');
ylabel({'Generalization';'over testing time (ms)'},'FontSize',15,'FontWeight','bold');
set(gca,'FontSize',15,'FontWeight','bold');
ylim(yborders);
xlim(xborders);

subplot(2,1,2);
hold on;
x = val';
y = (mSsigBETW*factms)';
M = MAccBETW';
Do_patchUnderplot(x,y,M);
caxis([0.5 max(MAccBETW)]);
caxis(acclimits);
colormap(colormap_choice)
plot(val,mSsigBETW*factms,'r','LineWidth',4);
%legend({'Average all within modality decoding'; 'Average all between modality decoding'});
xlabel('Training time points (ms)','FontSize',15,'FontWeight','bold');
ylabel({'Generalization';'over testing time (ms)'},'FontSize',15,'FontWeight','bold');
set(gca,'FontSize',15,'FontWeight','bold');
ylim(yborders);
xlim(xborders);

% 
% %%
% xval = round([0:15.1515:500]);
% 
% figure;set(gcf,'color','w');
% plot(xval(1:end-1),mDMv,'k'); hold on;
% plot(xval(1:end-1),mnonDMv,'r');
% legend({'Within'; 'Between'});
% shadedErrorBar(xval(1:end-1),mDMv,sDMv,'-k');
% shadedErrorBar(xval(1:end-1),mnonDMv,snonDMv,'-r');
% ylim([0.499 0.52])

% %%
% figure;
% np = 1;
% for s1 =1:3
%   for s2 = 1:3
%     subplot(3,3,np);
%     %plot(xval(2:end),squeeze(Mv(s1,s2,:)));
%     shadedErrorBar(xval(2:end),Mv(s1,s2,:),Sv(s1,s2,:),'-k');
%     ylim([0.495 0.52])
%     np = np+1;
%   end
% end


