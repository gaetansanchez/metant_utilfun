function [ds_result] = Do_MVPAgeneric_time2time_parcomp(project,datafile,train_modality,test_modality,opt,source_radius,nbrids,savefilename,sensmod)
% Do MVPA time2time generalization
% Based on previously source localization data saved in datafile 
% (output from: Do_srcERF4cosmoMVPA.m)
%---------
% INPUTS
%----------
% project         = define path (i.e. 'metaNT' or 'meteNT_v2')
% datafile        = source data in CosmoMVPA format
% train_modality  = number of sensory modality used for training
% test_modality   = number of sensory modality used for testing
% opt             = opt for decoding (see cosmo_searchlight) 
%     for instance:
%            opt = struct();
%            opt.classifier = @cosmo_classify_svm;
%            opt.normalization = 'zscore';
%            opt.dimension = 'time';
%            opt.measure = @cosmo_crossvalidation_measure;
% source_radius   = source radius to define neighborhood;
% nbrids          = vector number of neighborhood strcuture to compute
% savefilename    = output path and filename
% sensmod         = {'aud' 'vis' 'tac'} or {'all'} 
%                   {'all'} -> just decode hit vs miss across all sensmod (with equal number of targets)
%                   {'all_resp'} -> just decode resp vs noresp across all sensmod (with equal number of targets)
%
%----------------------
% Gaetan - 21/01/2017

%% init obob
%------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs(project,cfg);

%% Load cosmoMVPA data structure (output from: Do_srcERF4cosmoMVPA.m)
%--------------------------------
[~, fname] = fileparts(datafile);
load(datafile);

%% Identify type of decoding
%------------------------------
if strcmp(sensmod{1},'all') % hit vs. miss across all sensmod
  doallsensmod = 1;
else
  doallsensmod = 0; % hit vs. miss for each sensmod
end

%% Define what is targets 
%----------------------------
oldtarget_vec = ds.sa.targets; % dd_r=1 ud_r=2 dd_nr=-1 ud_nr=-2
if contains(sensmod{1},'resp') % target = resp vs. noresp
  tmp_vec = ones(size(oldtarget_vec,1),1)*2;
  tmp_vec(oldtarget_vec>0) = 1;
  ds.sa.targets = tmp_vec; % 1 = resp / 2 = no resp
else % target = hit vs. miss
  ds.sa.targets = abs(oldtarget_vec); % 1 = dd / 2 = ud
end

%% Compute t2t MVPA
%------------------------------------------------------
if doallsensmod
  % select only trials to make equal number of targets for each sensmod (see Do_srcERF4cosmoMVPA)
  %----------------------------------------------------------------------------------------------
  msk  = cosmo_match(ds.sa.equal_hit_miss_trials,1);
  ds_c = cosmo_slice(ds,msk);
else
  % select data in train and test modality
  %----------------------------------------
  msk  = cosmo_match(ds.sa.modality,[train_modality test_modality]);
  ds_c = cosmo_slice(ds,msk);
end

ds_c = cosmo_dim_prune(ds_c,'matrix_labels',{'pos'}); % update dim attributes

if train_modality==test_modality
  
  % within-modality cross-validation
  %---------------------------------
  
  % Redefine chunks
  %-----------------
  ds_c.sa.chunks = (1:size(ds_c.samples,1))';
  nchunks=2;
  ds_c.sa.chunks=cosmo_chunkize(ds_c,nchunks);
  
  ds_sel = ds_c;
  
else
  % cross-modality decoding
  %------------------------
  
  % select data in train and test modality
  %-----------------------------------------
  msk_train = cosmo_match(ds_c.sa.modality,train_modality);
  msk_test  = cosmo_match(ds_c.sa.modality,test_modality);
  
  ds_sel_train = cosmo_slice(ds_c,msk_train);
  ds_sel_test  = cosmo_slice(ds_c,msk_test);
  
  % Redefine chunks (chunk = 1 -> train // chunk = 2 -> test)
  %-----------------------------------------------------------
  ds_sel_train.sa.chunks = ones(size(ds_sel_train.samples,1),1);
  ds_sel_test.sa.chunks = 2*ones(size(ds_sel_test.samples,1),1);
  
  % Append data
  %---------------
  ds_sel = cosmo_stack({ds_sel_train, ds_sel_test});
  
end

measure = @cosmo_dim_generalization_measure;

% make 'time' a sample dimension
% (this necessary for cosmo_dim_generalization_measure)
ds_time = cosmo_dim_transpose(ds_sel,'time',1);

% define neighborhood over space;
%--------------------------------------
% Modification of cosmo_vol_grid_convert - line 166 to accept non regular grid
nbrhood = cosmo_spherical_neighborhood(ds_time,'radius',source_radius);


% Run the measure
%----------------
opt.center_ids = nbrids; % just run MVPA for some neighborhood
ds_result = cosmo_searchlight(ds_time,nbrhood,measure,opt);

%% save
%-------
save(savefilename,'-v7.3','ds_result');
