function [sourc] = Do_src_obobLCMV_alltrials(data,hdm)

%% Change HDM units
data.grad       = ft_convert_units(data.grad, 'm');
hdm.vol         = ft_convert_units(hdm.vol, 'm');
hdm.mri         = ft_convert_units(hdm.mri, 'm');
hdm.grid_warped = ft_convert_units(hdm.grid_warped, 'm');
hdm.grid_templateMNI = ft_convert_units(hdm.grid_templateMNI, 'm');


%% calculate leadfield, normalize to get rid of depth bias
cfg = [];
cfg.headmodel = hdm.vol;
cfg.channel   = {'MEG'};% Use all channels in order to get normalized leadfield
cfg.grid      = hdm.grid_warped;
cfg.grad      = data.grad;
cfg.normalize = 'yes';% to remove depth bias (Q in eq. 27 of van Veen et al, 1997)
lf = ft_prepare_leadfield(cfg, data);

%% Now do the obob LCMV beamform
%-----------------------------
cfg = [];   
cfg.lpfilter = 'yes';
cfg.lpfreq   = 30;
cfg.channel  = 'MEG';
% cfg.regfac   = '5%';
cfg.hdm      = hdm.vol;
cfg.grid     = lf;
cfg.cov_win  = [-0.5 0.5]; % 'all'

if isfield(hdm,'parcellation')
  cfg.parcel   = hdm.parcellation;
  cfg.parcel_avg = 'svd_sources'; % 'avg_filters' or 'svd_sources'
end

sourc = obob_svs_beamtrials_lcmv(cfg, data);


