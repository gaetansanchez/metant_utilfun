function [] = Do_mvpa_stat_t2t(Pathsaved,Files,cond_train,cond_test,h0_mean,niter,outputfile,nproc)

%Pathsaved = fullfile(config.path2work,'mvpa/t2t_srcERF'); % saved data
%Files = dir(fullfile(Pathsaved,'tstep30ms_srad2_MVPA_src_t2tERF_*'));

% Path definition
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs('metaNT_v2',cfg);

%% Load all files
%----------------
n_subjects = numel(Files);

ds_stat = {};
for i = 1:n_subjects
  disp(['---> Load ' sprintf('File %02d/%02d ...',i,n_subjects)]);
  load(fullfile(Pathsaved,Files(i).name));
  
  % select specific condition
  %--------------------------
  j = cond_train;
  k = cond_test;
  
  cdt_ds = M{j,k};
  
  % move {train,test}_time from being sample dimensions to feature
  % dimensions, so they can be mapped to a fieldtrip struct & use in cosmo montecarlo cluster stat):
  %----------------------------------------------------------------
  cdt_tf_ds = cosmo_dim_transpose(cdt_ds,{'train_time','test_time'},2);
  
  % store variable for statistic later
  %----------------------------------
  ds_stat{i} = cdt_tf_ds;
  
end

%% Statistics with Cosmo MVPA
%------------------------------
% Set chunk and targets properly
%--------------------------------
for i=1:n_subjects
    % here we assume a single output (sample) for each
    % searchlight. For statistical analysis later, where
    % we want to do a one-sample t-test, we set
    % .sa.targets to 1 (any constant value will do) and
    % .sa.chunks to the subject number.
    % nsamples=size(result.samples,1);

    ds_stat{i}.sa.chunks=i;  % k-th subject
    ds_stat{i}.sa.targets=1; % all same condition
end

ds_tr = cosmo_stack(ds_stat,1,'drop_nonunique');

% Do montecarlo analysis
%--------------------------
%h0_mean = 1/2;
%niter   = 10000;

cluster_nbrhood = cosmo_cluster_neighborhood(ds_tr);

stat_map = cosmo_montecarlo_cluster_stat(ds_tr, cluster_nbrhood,...
                                        'niter', niter,...
                                        'h0_mean', h0_mean,...
                                        'nproc',nproc);

% %% To convert the output (say stat_map) 
% % from cosmo montecarlo cluster stat to matrix form (time by time), do
% %--------------------------------------------------------
% [data, labels, values] = cosmo_unflatten(stat_map,2);
% data_mat = squeeze(data);

%% save
%--------
save(outputfile,'-v7.3','stat_map');
