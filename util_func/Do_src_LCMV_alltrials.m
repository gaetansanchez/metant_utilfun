function [sourc] = Do_src_LCMV_alltrials(data,hdm)

%% adjust grad to use mags and grads
% data.grad = obob_adjust_tra(data.grad); % NOT NEEDED ANYMORE if chanunit = T/cm or T/m

%% Change HDM units
data.grad       = ft_convert_units(data.grad, 'm');
hdm.vol         = ft_convert_units(hdm.vol, 'm');
hdm.mri         = ft_convert_units(hdm.mri, 'm');
hdm.grid_warped = ft_convert_units(hdm.grid_warped, 'm');
hdm.grid_templateMNI = ft_convert_units(hdm.grid_templateMNI, 'm');

%% recut
%--------
cfg = [];
cfg.trials  = 'all';
cfg.latency = [-0.5 0.5]; % [-1 1]
data   = ft_selectdata(cfg, data);

%% calculate leadfield, normalize to get rid of depth bias
cfg = [];
cfg.vol       = hdm.vol;
cfg.channel   = data.label;
cfg.grid      = hdm.grid_warped;
cfg.grad      = data.grad;
cfg.normalize = 'yes';% to remove depth bias (Q in eq. 27 of van Veen et al, 1997)
lf = ft_prepare_leadfield(cfg, data);

%% Now do the classic LCMV beamform

% timelock analysis for covariance
cfg  = [];
cfg.channel    = 'MEG';
cfg.covariance = 'yes';
cfg.keeptrials = 'yes';
cfg.vartrllength = 2;
tlck = ft_timelockanalysis(cfg, data);

% do LCMV beamforming
cfg            = [];
cfg.method     = 'lcmv';
cfg.lcmv.keepleadfield = 'yes';
cfg.lcmv.keepfilter    = 'yes';
cfg.lcmv.keepcov       = 'yes';
%cfg.lcmv.lambda        = '5%';
cfg.lcmv.fixedori      = 'yes'; % project on axis of most variance using SVD
cfg.grid    = lf;
cfg.vol     = hdm.vol;
sourcelcmv  = ft_sourceanalysis(cfg, tlck); % compute filter on all trials

cfg.keeptrials  = 'yes';
cfg.rawtrial    = 'yes';
cfg.grid.filter = sourcelcmv.avg.filter;
sourc = ft_sourceanalysis(cfg, tlck); % apply filter to all single trials

%% Now because we want a linear grid: move the pos back to the template (instead of grid_warped)
sourc.inside = hdm.grid_templateMNI.inside;
sourc.pos    = hdm.grid_templateMNI.pos;
sourc.dim    = hdm.grid_templateMNI.dim;

