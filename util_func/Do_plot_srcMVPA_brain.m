function [Tab] = Do_plot_srcMVPA_brain(config,data,param2plot,mask2plot,val2plot,colormaplabel,fname,dosurface,dosavefile,toi)

if nargin < 10
  toi = [0 0.5];
end

%% get some networks parcel labels
%-----------------------------------
label_parcel_overplot = 1;

if label_parcel_overplot
  
  labelnames = {'Sal';'At'};
  val_toplot = [.9,.1];
  flag = 0;
  [source_networks] = get_atlasparcel_ROI(labelnames,val_toplot,flag);
  
  data.pow = data.mask;
  val2plot = [0 1];
  
end

%% Interpolate data to plot
%----------------------------

data.dimord = 'chan_freq_time';

load standard_mri
load mni_grid_1_5_cm_889pnts.mat

% Change units
%-------------
mri           = ft_convert_units(mri, 'm');
template_grid = ft_convert_units(template_grid, 'm');

% now virtual sensor to src
%---------------------------
cfg = [];
cfg.mri = mri;
cfg.sourcegrid = template_grid;
cfg.parameter = [param2plot mask2plot];
cfg.toilim = toi;% [0 0.5]
cfg.foilim = toi;
source = obob_svs_virtualsens2source(cfg,data);

if label_parcel_overplot
  source.pow(source.pow>0) = 0.5; % make it yellow
  source.pow(isnan(source.pow)) = 0;
  source.mask(source.mask>0.1) = 1; % remove some border masking
  source.mask(isnan(source.mask)) = 0;
  
  source_networks.toplot = source_networks.toplot+source.pow;
  source_networks.toplot(source_networks.toplot==1.4) = 0.7; % make intersect different color
  source_networks.toplot(source_networks.toplot==0.6) = 0.3; % make intersect different color
  
  
  source_networks.mask = source_networks.mask+source.mask;
  
  cfg = [];
  cfg.funparameter = 'toplot';
  cfg.maskparameter = 'mask';
  cfg.method = 'surface';
  cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
  cfg.colorbar = 'no';
  cfg.camlight = 'no';
  cfg.funcolorlim = [0 1];
  
  % make figures
  hemibrain = {'left' 'right'};
  viewbrain = {'left' 'right' 'dorsal'};
  for i = 1:numel(hemibrain)
    for j = 1:numel(viewbrain)
      h = hemibrain{i};
      cfg.surffile = ['surf_caret' h '.mat'];
      ft_sourceplot(cfg, source_networks);
      
      colorsmix = [0 0 1;0 1 0; 1 1 0; 1 0.6 0 ; 1 0 0]; % colorsmix = [b g y o r]
      colormap(colorsmix);
      
      v = viewbrain{j};
      name = [fname '_' h];
      if dosavefile == 1
        plot_caret_style(name, 100, [], v);
      else
        plot_caret_style([], 100, [], v);
      end
      
    end
  end
  
else
  
  
  
  if numel(param2plot)>= 2
    
    source = rmfield(source,'coordsys'); % because if not create problem with caret plot....
    
    cfg = [];
    cfg.funcolorlim   = val2plot;
    cfg.method        = 'surface';
    cfg.projmethod    = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
    cfg.colorbar = 'no';
    cfg.camlight = 'no';
    % make figures for right view of right hemisphere
    hemibrain = {'right'};
    viewbrain = {'right'};
    colorvizu = {[1 0 0] [1 1 0] [0 0 1]};
    for i =1:numel(hemibrain)
      for j = 1:numel(viewbrain)
        h = hemibrain{i};
        cfg.surffile = ['surf_caret' h '.mat'];
        for k = 1:numel(param2plot)
          cfg.funparameter  = param2plot{k};
          cfg.maskparameter = mask2plot{k};
          ft_sourceplot(cfg, source);
          map = colormap;
          valbar = size(map,1);
          map(1:valbar,:) = repmat(colorvizu{k},valbar,1);
          colormap(map);
        end
        % colormap(colormaplabel);
        %     map = colormap;
        %     valbar = size(map,1)/2;
        %     map(1:valbar,:) = repmat([0.5 0.5 0.5],valbar,1);
        %     colormap(map);
        if dosavefile == 1
          v = viewbrain{j};
          name = [fname '_' h];
          plot_caret_style(name, 100, [], v);
        end
      end
    end
    
  else
    
    % setup plot fieldtrip
    %---------------------
    cfg = [];
    cfg.funparameter = param2plot;
    cfg.maskparameter = mask2plot;
    cfg.funcolorlim = val2plot;
    cfg.method = 'surface';
    cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
    cfg.colorbar = 'no';
    cfg.camlight = 'no';
    
    
    if dosurface == 1
      
      ft_sourceplot(cfg, source)
      
      % Save
      %------
      if dosavefile == 1
        view(-90,0) % left
        name = [fname '_SURF_LEFTview.png'];
        save_figure(name, 100);
        
        view(0,90) % top
        name = [fname '_SURF_TOPview.png'];
        save_figure(name, 100);
        
        view(90,0) % right
        name = [fname '_SURF_RIGHTview.png'];
        save_figure(name, 100);
      end
      
    elseif dosurface == 2 % plot ortho
      cfg.method = 'ortho';
      cfg.atlas = 'ROI_MNI_V4.nii';
      ft_sourceplot(cfg, source);
      
    elseif dosurface == 3 % plot inflated caret surface
      cfg.surfinflated = 'surface_inflated_both_caret.mat';
      ft_sourceplot(cfg, source);
      view_position = [-70 20 50];
      view(view_position);
      light('Position',[-70 20 50]);
      material dull
      
    else
      
      source = rmfield(source,'coordsys'); % because if not create problem with caret plot....
      
      % make figures for right hemisphere
      hemibrain = {'left' 'right'};
      viewbrain = {'left' 'right' 'dorsal'};
      
      for i = 1:numel(hemibrain)
        for j = 1:numel(viewbrain)
          h = hemibrain{i};
          cfg.surffile = ['surf_caret' h '.mat'];
          ft_sourceplot(cfg, source);
          
          colormap(colormaplabel);
          %colormap('jet');
          %colormap('autumn');
          %     map = colormap;
          %     valbar = size(map,1)/2;
          %     map(1:valbar,:) = repmat([0.5 0.5 0.5],valbar,1);
          %     colormap(map);
          
          v = viewbrain{j};
          name = [fname '_' h];
          if dosavefile == 1
            plot_caret_style(name, 100, [], v);
          else
            plot_caret_style([], 100, [], v);
          end
          
          
        end
      end
    end
    
  end
  
  
  %% Check ROI on atlas
  %----------------------
  [~,filenameinput,~] = fileparts(fname);
  testdat = data;
  
  testdat = ft_convert_units(testdat, 'mm'); % change coordinates cm to mm (to fit with atlas)
  
  sigVox  = find(squeeze(sum(sum(testdat.mask,2),3))); % only significant voxels
  intval  = squeeze(mean(mean(testdat.pow(sigVox,:,:),2),3)); % mean values for these voxels
  
  % sort voxels indices based on their values
  %--------------------------------------------
  [sigVal,indv] = sort(intval, 'descend');
  sigVox = sigVox(indv);
  
  
  % load atlas
  %------------
  A = ft_read_atlas('ROI_MNI_V4.nii'); % load atlas
  savefilename = fullfile(config.path,'doc','atlas_labels',['labelsAALatlasMNI_' filenameinput '.mat']);
  
  [Tab] = Do_extractROIname_atlas(A,sigVox,sigVal,testdat,savefilename)
  
end

