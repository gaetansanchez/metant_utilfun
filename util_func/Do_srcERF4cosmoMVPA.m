function [ds] = Do_srcERF4cosmoMVPA(project,datafile,hdmfile,sensmod,toilim,win,savefilename,CFilter,paradigm,do_obobLCMV,do_parcelsrc)
% Do source reconstruction with Beamformer filters
% Setup data structure for CosmoMVPA analysis
%---------
% INPUTS
%----------
% project  = name to setup project's path (i.e. 'metaNT' or 'metaNT_v2')
% datafile = clean epoched file
% hdmfile  = headmodel file (with hdm structure; see 'Do_src_LCMV_alltrials.m' for details)
% sensmod  = cell array of sensory modality to decode = {'all'} or {'aud' 'vis' 'tac'}
% toilim   = time range to decode in sec (i.e toilim = [0 0.6])
% win      = time step in second for signal resampling before LCMV and decoding 
%             (i.e. win = 0.03 for steps of 30 ms)
% savefilename  = output path and filename
% CFilter  = compute filter for each modality ('sensmod') or for all modalities ('alltrials');
% paradigm = 'hit_vs_miss'(default) or 'supra_vs_sham' or 'hit_vs_miss_respcontrol'
% do_obobLCMV = 0 or 1 --> if 1 = use obob LCMV beamformer function 
% do_parcelsrc = 0 or 1 --> if 1 = use obob LCMV beamformer with parcellation atlas
%                                  also force do_obobLCMV = 1
%----------------------
% Gaetan - 21/01/2017

%% init obob
%------------
addpath('/mnt/obob/staff/gsanchez');
cfg = [];
cfg.package.svs = true;
cfg.package.gm2 = true;
cfg.package.hnc_condor = true;
config = addmypaths_gs(project,cfg);

%% load + clean
%-------
% load data + info clean (from reject_visual_blocks.m)
%-----------------------
[pathpreproc, fname] = fileparts(datafile);

if strcmp(project,'metaNT')
  data_raw = load(datafile);% load already cleaned data
else
  data_raw = do_reject4cleaning(datafile);% do reject based on manual cleaning
end

%% SET data
if contains(paradigm,'respcontrol')
  for i = 1:size(data_raw.trialinfo,1)
   data_raw.trialinfo(i,1) = str2double(sprintf('%d%d',data_raw.trialinfo(i,1),data_raw.trialinfo(i,3)));
  end
end

[tr1,tr2,tr3,condlabel] = do_set_condtriggers(sensmod,paradigm);

% if sensmod = all --> Recode trialinfo (remove 100 200 or 300)
%--------------------------------------------------------------
if strcmp(sensmod{1},'all')
  if contains(paradigm,'respcontrol')
    error('You have to use sensmod aud vis tac if you are using respcontrol');
  end
  Val2rm = floor( data_raw.trialinfo(:,1)./100)*100;
  data_raw.trialinfo(:,1) = data_raw.trialinfo(:,1) - Val2rm;
  tr1       = 0;
end

%% Equalize trial number per condition
%--------------------------------------
[data,trig,trial_id,trig_sens] = Do_equalcond(pathpreproc,fname,sensmod,tr1,condlabel,tr2,data_raw,paradigm);

clear data_raw;

% Select just trig conditions (in this case: hit and miss)
%---------------------------------------------------------------------
cfg = [];
cfg.trials = find(ismember(data.trialinfo(:,1),trig));
data = ft_selectdata(cfg,data);


%% Load hdm grid
%-------------------

hdm = load(hdmfile);

if do_parcelsrc
  do_obobLCMV = 1; % force obob LCMV because parcellation
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%% Adapat code for parcellation SRC
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  load('parcellations_3mm.mat'); % load parcellation
  hdm.parcellation = parcellation;
  
  % change label of parcels
  %------------------------
  for i = 1:numel(hdm.parcellation.parcel_array)
    hdm.parcellation.parcel_array{i}.roi_name = num2str(i);% remove labels just keep numbers for cosmo
  end
  
  % warp grid
  %-----------
  cfg = [];
  cfg.coordsys       = 'neuromag';
  cfg.grid.warpmni   = 'yes';
  cfg.grid.template  = ft_convert_units(hdm.parcellation.template_grid, 'm');
  cfg.grid.nonlinear = 'yes'; % use non-linear normalization
  cfg.grid.unit      = 'm';
  cfg.mri            = hdm.mri;
  hdm.grid_warped   = ft_prepare_sourcemodel(cfg); % replace grid_warped with parcellation template grid
  
  grid2use = hdm.parcellation.parcel_grid;
  
  % set xyz fields for bypass check_inputs function inside convert_ds_mni_grid.m
  grid2use.xgrid = [];
  grid2use.ygrid = [];
  grid2use.zgrid = [];
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
  load mni_grid_1_5_cm_889pnts;
  grid2use = ft_convert_units(template_grid,'m');
end



%% Some preprocessing if not using obob LCMV
%---------------------------------------------
if ~do_obobLCMV
  % Lowpass filter 
  %----------------
  cfg = [];
  cfg.lpfilter ='yes';
  cfg.lpfreq = 30; 
  data = ft_preprocessing(cfg, data);
  
  % Downsampling (save computation time)
  %-----------------
  cfg = [];
  cfg.resamplefs = 1/win; % win = should be in second
  cfg.detrend = 'no';
  data_tr = ft_resampledata(cfg, data);
  
else
  data_tr = data;
end
clear data;

%% Source computation
%---------------------

switch CFilter
  case 'alltrials'
    
    if do_obobLCMV
      
      [sourc] = Do_src_obobLCMV_alltrials(data_tr,hdm);
      
      % get proper fieldtrip single trial structure for cosmo
      %-------------------------------------------------------
      cfg  = [];
      cfg.preproc.lpfilter = 'yes'; % filter
      cfg.preproc.lpfreq   = 30;
      cfg.keeptrials       = 'yes';
      sourc_tr = ft_timelockanalysis(cfg, sourc);
      
      % downsampling
      %----------------
      cfg = [];
      cfg.resamplefs = 1/win; % win = should be in second
      cfg.detrend = 'no';
      sourc_tr = ft_resampledata(cfg, sourc_tr);
      
      % Transform source data into cosmo format
      %----------------------------------------
      ds = cosmo_meeg_dataset(sourc_tr);
      
      % transform the template grid in COSMO format and substitute the ds.sa.chan field with ds.sa.pos field --- %
      %-----------------------
      ds = convert_ds_mni_grid(ds, grid2use);
      
    else
      
      [sourc] = Do_src_LCMV_alltrials(data_tr,hdm);
      
      % Transform into cosmo format
      %-----------------------------
      ds = cosmo_meeg_dataset(sourc,'data_field', 'mom','data_dim','3d');
      
    end
    
  case 'sensmod'
    
    ds_s = {};
    for i = 1:numel(trig_sens)
      cfg = [];
      cfg.trials = find(ismember(data_tr.trialinfo(:,1),cell2mat(trig_sens{i})));
      data_s{i} = ft_selectdata(cfg,data_tr);
      
      if do_obobLCMV
        
        [sourc_s{i}] = Do_src_obobLCMV_alltrials(data_s{i},hdm);
        
        % get proper fieldtrip single trial structure for cosmo
        %-------------------------------------------------------
        cfg  = [];
        cfg.preproc.lpfilter = 'yes';
        cfg.preproc.lpfreq   = 30;
        cfg.keeptrials = 'yes';
        sourc_tr{i} = ft_timelockanalysis(cfg, sourc_s{i});
        
        % downsampling
        %----------------
        cfg = [];
        cfg.resamplefs = 1/win; % win = should be in second
        cfg.detrend = 'no';
        sourc_tr{i} = ft_resampledata(cfg, sourc_tr{i});
        
        % Transform source data into cosmo format
        %----------------------------------------
        ds_s{i} = cosmo_meeg_dataset(sourc_tr{i});
        
        % transform the template grid in COSMO format and substitute the ds.sa.chan field with ds.sa.pos field --- %
        %------------------------------------------------
        ds_s{i} = convert_ds_mni_grid(ds_s{i}, grid2use);

        
      else
        
        [sourc_s{i}] = Do_src_LCMV_alltrials(data_s{i},hdm);
        
        % Transform into cosmo format
        %-----------------------------
        ds_s{i} = cosmo_meeg_dataset(sourc_s{i},'data_field', 'mom','data_dim','3d');
        
      end
      
    end 
    
    % append data
    %-------------
    ds = cosmo_stack(ds_s,1,1); 
    
    clear ds_s
    clear sourc_s
    
  otherwise
    error(['*** CFilter = ' CFilter ' is not implemented']);
end

% SetUp cosmo format
%--------------------
ds = cosmo_dim_prune(ds,'matrix_labels',{'pos'}); % update dim attributes

%% Reslice time decoding based on toilim
%---------------------------------------
selector = @(x) toilim(1)<=x & x<=toilim(end); % use element-wise logical-and
msktime  = cosmo_dim_match(ds,'time',selector);
ds       = cosmo_slice(ds,msktime,2);
ds       = cosmo_dim_prune(ds,'matrix_labels',{'pos'});

%% Set up targets and modalities conditions
%------------------------------------------
ds.sa.targets  = (1:size(ds.samples,1))';
ds.sa.modality = (1:size(ds.samples,1))';

n_modalities = numel(sensmod);

index2label = {};
idlab = 1;
labels_id = zeros(size(ds.samples,1),1);

for j = 1:n_modalities
  
  sens_id = [];
  
  for k = 1:numel(condlabel)
    
    % Labels each trial
    %------------------
    index2label{idlab} = [sensmod{j} condlabel{k}];
    
    % triggers specific
    %------------------
    trig     = [];
    trig     = tr1(j)+tr2(k);
    trial_id = find(ds.sa.trialinfo(:,1) == trig);
    sens_id  = [sens_id;trial_id];
    
    ds.sa.targets(trial_id) = tr3(k); % recode dd=1 and ud=2
    
    labels_id(trial_id) = idlab;
    idlab = idlab+1;
    
  end
  
  ds.sa.modality(sens_id) = j; % recode modality aud(1) vis(2) tac(3)
  
end

ds.sa.labels=cellfun(@(x)index2label(x),num2cell(labels_id));

ds.sa.chunks = ds.sa.modality;

%% Create vector with equal number of targets (hit/miss) for each conditions (sensmod)
%--------------------------------------------------------------------------------------
allmod = unique(ds.sa.modality);
alltar = unique(ds.sa.targets);
nb_modtar = [];
id_modtar = {};
for i = 1:numel(allmod)
  for j = 1:numel(alltar)
    idmod = [];idmod = find(ds.sa.modality == allmod(i));
    idtar = [];idtar = find(ds.sa.targets == alltar(j));
    nb_modtar(i,j) = numel(intersect(idmod,idtar));
    id_modtar{i,j} = intersect(idmod,idtar);
  end
end

nbmincond = min(min(nb_modtar)); % get minimum number of trials per conds and targets

rng(1,'twister'); % set random process to initial similar state

idrand_madtar = {};
id_oktrials   = [];
for i = 1:size(id_modtar,1)
  for j = 1:size(id_modtar,2)
    idrand_madtar{i,j} = datasample(id_modtar{i,j},nbmincond,'Replace',false);
    id_oktrials = [id_oktrials ; idrand_madtar{i,j}];
  end
end

ds.sa.equal_hit_miss_trials = zeros(size(ds.samples,1),1);
ds.sa.equal_hit_miss_trials(id_oktrials) = 1;

%% just to check everything is ok
cosmo_check_dataset(ds);

ds = cosmo_dim_prune(ds,'matrix_labels',{'pos'}); % update dim attributes


%% save ds data structure
%--------------------------
save(savefilename,'-v7.3','ds');

