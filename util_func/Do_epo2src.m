function [datasrc] = Do_epo2src(data,hdmfile,toilim,cov_win,freq,precision_grid,conds,CFilter)

unival = 'm'; % unti value for source localization 'm'

%% load hdm
hdm = load(hdmfile);

%% adjust grad to use mags and grads & change HDM units

%data.grad = obob_adjust_tra(data.grad); % NO NEED TO DO THIS ANYMORE

data.grad = ft_convert_units(data.grad, unival);

hdm.vol   = ft_convert_units(hdm.vol, unival);
hdm.mri   = ft_convert_units(hdm.mri, unival);

%% Do grid_warped with mni precision
%------------------------------------
parcellation = [];

if precision_grid == 1.5
  load mni_grid_1_5_cm_889pnts
  
elseif precision_grid == 1
  load mni_grid_1_cm_2982pnts
  
%   %create mask (keep only grey matter voxels)
%   %-------------------------------------------
%   atlas = ft_read_atlas('ROI_MNI_V4.nii');
%   atlas = ft_convert_units(atlas,'cm'); % assure that atlas and template_grid are expressed in the same units
%   
%   cfg = [];
%   cfg.atlas = atlas;
%   cfg.roi   = atlas.tissuelabel;
%   cfg.inputcoord = 'mni';
%   mask = ft_volumelookup(cfg,template_grid);
%   template_grid.inside(template_grid.inside==1) = 0;
%   template_grid.inside(mask) = 1;
  
elseif precision_grid == 2
  load mni_grid_2cm_372pnts
  
elseif ischar(precision_grid)% if you provide a file name for a parcellation grid
  load(precision_grid); % load parcellation
  template_grid = parcellation.template_grid; % template grid 3mm
  
else
  error(['No precision mni_grid for precision = ' num2str(precision_grid)]);
  
end

% warp grid
%-----------
cfg = [];
cfg.coordsys       = 'neuromag';
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = ft_convert_units(template_grid, unival);
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.grid.unit      = unival;
cfg.mri            = hdm.mri;
hdm.grid_warped2   = ft_prepare_sourcemodel(cfg);

if strcmp(CFilter,'alltrials')
    % calculate leadfield, normalize to get rid of depth bias
    %----------------------------------------------------------
    cfg=[];
    cfg.vol       = hdm.vol;
    cfg.channel   = data.label;
    cfg.grid      = ft_convert_units(hdm.grid_warped2, unival);
    cfg.grad      = data.grad;
    cfg.normalize = 'yes';
    lf = ft_prepare_leadfield(cfg, data);
end

%% select smaller data set
%-------------------------
cfg = [];
cfg.trials = 'all';
cfg.toilim = toilim;
data   = ft_redefinetrial(cfg, data);

%% now project condition wise if necessary
%-----------------------------------------
for iC = 1:size(conds,1)
  
  % select conditions (and time if you want)
  %-----------------------------------------
  cfg = [];
  cfg.trials = find(ismember(data.trialinfo(:,1), conds{iC, 1}));
  temp = ft_selectdata(cfg, data);
  
  % Compute leadfield per modality (sensmod)
  %------------------------------------------
  if strcmp(CFilter,'sensmod')
    lf = [];
    % calculate leadfield, normalize to get rid of depth bias
    %----------------------------------------------------------
    cfg=[];
    cfg.vol       = hdm.vol;
    cfg.channel   = temp.label;
    cfg.grid      = ft_convert_units(hdm.grid_warped2, unival);
    cfg.grad      = temp.grad;
    cfg.normalize = 'yes';
    lf = ft_prepare_leadfield(cfg, temp);    
  end
  
  % beam time series to sources
  %-----------------------------
  cfg=[];
  switch freq
    case 'low'
      cfg.lpfilter='yes';
      cfg.lpfreq=30;
    case 'high'
      cfg.lpfilter='yes';
      cfg.lpfreq=100;
      cfg.hpfilter='yes';
      cfg.hpfreq=30;
    case 'no'
  end
  cfg.channel  = data.label;
  %cfg.regfac='5%';
  cfg.hdm      = hdm.vol;
  cfg.grid     = lf;
  cfg.cov_win  = cov_win;
  cfg.parcel   = parcellation; 
  cfg.parcel_avg = 'svd_sources'; % 'avg_filters' or 'svd_sources'
  
  tmpsrc_cond{iC} = obob_svs_beamtrials_lcmv(cfg, temp);
  
  clear temp
end


datasrc = ft_appenddata([],tmpsrc_cond{:});
clear tmpsrc_cond



