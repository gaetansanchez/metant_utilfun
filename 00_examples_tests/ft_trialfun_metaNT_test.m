function [trl_all,event] = ft_trialfun_metaNT_test(cfg)

% read the header information and the events from the data
hdr   = ft_read_header(cfg.dataset);
event = ft_read_event(cfg.dataset);

% determine the number of samples before and after the trigger
pretrig  = round(-cfg.trialdef.prestim  * hdr.Fs);
posttrig =  round(cfg.trialdef.poststim * hdr.Fs)-1;

% search for "stimulus" events
value  = [event(find(strcmp('STI101', {event.type}))).value]';
sample = [event(find(strcmp('STI101', {event.type}))).sample]';

% Only relevant triggers
%-------------------------
% cfg.triggers.onset_fixation = 40; 
% cfg.triggers.question_mark  = 10;
% cfg.triggers.sham  = 1;
% cfg.triggers.NT    = 3;
% cfg.triggers.catch = 5;
% + 128 when its tactile stimulation !
% sometimes there is a 128 ????? check later
% response press = 8192
% onset_fixation if response button was press during triggering = 8232 

triggervals = [40 10 1 3 5 128 129 131 133 8192 8232];
trigNointerest = find(~ismember(value,triggervals));

value(trigNointerest) = [];
sample(trigNointerest) = [];

% correct tactile triggers
trigtac_vals = [129 131 133];
trigtac_int = find(ismember(value,trigtac_vals));
value(trigtac_int) = value(trigtac_int)-128;

% define the trials
trl(:,1) = sample + pretrig;  % start of segment
trl(:,2) = sample + posttrig; % end of segment
trl(:,3) = pretrig;           % how many samples prestimulus
trl(:,4) = value;             % trigger value

%% recode triggers
%------------------
trl_all = [];

stimulus_trig = [1 3 5]; % sham, thresh , supra
question_trig = 10;
response_trig = [8192];% button press
onsettrial_trig = [40 8192 8232]; % usefull for ISI calculus

tr_ind = 0; % start trial indexing at 1
for iRow = 1:length(trl)
  
  TRIG = trl(iRow,4); % current trigger value
  
  if any(TRIG == stimulus_trig) % only targets centered trials
    
    % Trial indexing
    %----------------
    tr_ind = tr_ind + 1;
    
    % Code for response
    %-------------------
    if any(trl(iRow+1,4) == question_trig) % check if there is a question mark trig
      try % just try because does not work for last trial if there is no response
        if any(trl(iRow+2,4) == response_trig) % check if there is a response after question mark
          code = 1; % keep track of button press
          goodresp = 1; % keep track of this for reaction time
        else
          code = 0; % keep track of no response
          goodresp = 0;
        end
      catch
        code = 0; % keep track of no response
        goodresp = 0;
      end
    else
      code = NaN; % no question mark = strange trial
      goodresp = 0;
    end
    
    % compute reaction time (response - target onset timing - posttarget_duration)
    %----------------------------------------------------------------------------
    if goodresp == 0
      RT = NaN;
    else
      RT = (trl(iRow+2,1)-trl(iRow+2,3)) - (trl(iRow+1,1)-trl(iRow+1,3));
    end
    
    PostTar_delay = (trl(iRow+1,1)-trl(iRow+1,3)) - (trl(iRow,1)-trl(iRow,3)); % target - question mark
    
    % compute ISI (jitter between fixation cross and stimulus)
    %----------------------------------------------------------
    try
      if any(trl(iRow-1,4) == onsettrial_trig) % check if there is a onset trial trig
        ISI = (trl(iRow,1)-trl(iRow,3)) - (trl(iRow-1,1)-trl(iRow-1,3));
      else
        ISI = NaN;
      end
    catch
      ISI = NaN;
    end
    
    % build new trl-field
    %---------------------
    trl_all(end+1,1:3) = trl(iRow,1:3); % target onset centered trials
    trl_all(end,4)     = trl(iRow,4)*10; % target type (10 , 30 , 50) 
    trl_all(end,5)     = tr_ind; % trial index
    trl_all(end,6)     = code; % response (1) or not (0)
    trl_all(end,7)     = RT;
    trl_all(end,8)     = ISI;
    trl_all(end,9)     = PostTar_delay;
  end
  
end

